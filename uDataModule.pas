unit uDataModule;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Phys.ADSDef, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.ADS, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, IOUtils,
  FireDAC.Comp.DataSet, FireDAC.VCLUI.Login, FireDAC.Comp.UI, VCL.StdCtrls,
  TMSLoggingCore, TMSLoggingUtils, uSentry, VCL.TMSLogging, IniFiles,
  TMSLoggingTextOutputHandler, VCL.Forms, uColumnVerifProgressBar,
  VCL.Dialogs, uColumnDefinition;

type
  TVerboseLevel = (vlInfo, vlDebug, vlTrace, vlException);
  TSetVerboseLevel = set of TVerboseLevel;
  TColumnCreationStatus = (ccsExists, ccsNotExists, ccsError, ccsOK);

  TMyOutputHandler = class(TTMSLoggerBaseOutputHandler)
  private
    FMemo: TMemo;
  protected
    procedure Clear; override;
    procedure LogOutput(const AOutputInformation: TTMSLoggerOutputInformation); override;
  public
    constructor Create(const AMemo: TMemo); reintroduce; virtual;
    property Memo: TMemo read FMemo write FMemo;
  end;

  TDMDatabaseFixer = class(TDataModule)
    FDPhysADSDriverLink1: TFDPhysADSDriverLink;
    FDConn: TFDConnection;
    tabDiagnosticQry: TFDTable;
    tabDiagnosticQryQuery_ID: TGuidField;
    tabDiagnosticQryCategory: TStringField;
    tabDiagnosticQrySmall_Description: TStringField;
    tabDiagnosticQryLong_Description: TStringField;
    tabDiagnosticQryPass_Fail_State: TIntegerField;
    tabDiagnosticQryDT_Last_Check: TSQLTimeStampField;
    tabDiagnosticQryDT_Last_Fix: TSQLTimeStampField;
    tabDiagnosticQryLast_Fail_Count: TIntegerField;
    tabDiagnosticQrySQL_Count: TStringField;
    tabDiagnosticQrySQL_Detail: TStringField;
    tabDiagnosticQrySQL_Fix: TStringField;
    tabDiagnosticQryFail_Score: TIntegerField;
    tabDiagnosticQrySentry_TAG: TStringField;
    tabDiagnosticQryLog_Remotely: TBooleanField;
    tabDiagnosticQryMin_DB_Ver_Required: TStringField;
    tabDiagnosticQryStatus: TBooleanField;
    tabDiagnosticQrySQL_Detail_Status: TBooleanField;
    tabDiagnosticQrySQL_Fix_Status: TBooleanField;
    tabDiagnosticQrySelected: TBooleanField;
    tabDiagnosticQryRow_Version_Number: TIntegerField;
    FDRemoteConn: TFDConnection;
    tabRemoteDiagnosticQry: TFDTable;
    FDConnDiag: TFDConnection;
    FDConnLocal: TFDConnection;
    tabStatistics: TFDTable;
    tabDiagnosticQryCalc_Pass_Fail_State: TIntegerField;
    tabDiagnosticQryCalc_DT_Last_Check: TSQLTimeStampField;
    tabDiagnosticQryCalc_DT_Last_Fix: TSQLTimeStampField;
    tabDiagnosticQryCalc_Last_Fail_Count: TIntegerField;
    tabDiagFix: TFDTable;
    tabDiagParams: TFDTable;
    tabGlobalFix: TFDTable;
    tabDiagnosticQrySubcategory: TStringField;
    procedure tabDiagnosticQryBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure tabDiagnosticQryAfterPost(DataSet: TDataSet);
    procedure tabDiagnosticQryAfterDelete(DataSet: TDataSet);
    procedure tabDiagnosticQryNewRecord(DataSet: TDataSet);
    procedure tabDiagnosticQryCalcFields(DataSet: TDataSet);
    procedure tabDiagFixAfterDelete(DataSet: TDataSet);
    procedure tabDiagFixAfterPost(DataSet: TDataSet);
    procedure tabDiagFixBeforePost(DataSet: TDataSet);
    procedure tabDiagParamsAfterDelete(DataSet: TDataSet);
    procedure tabDiagParamsAfterPost(DataSet: TDataSet);
    procedure tabDiagParamsAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
    function TableExists (TableName: string): boolean;
  public
    { Public declarations }
    LogLocal:   TTMSLoggerBaseOutputHandler;
    LogSentry:  TSentryOutputHandler;
    LogConsole: TMyOutputHandler;
    vlConsole:  TSetVerboseLevel;
    vlLocal:    TSetVerboseLevel;
    vlSentry:   TSetVerboseLevel;

    //Global configuration variables
    gPOSADSAliasName:    string;
    gDIAGADSAliasName:   string;
    gREMOTEADSAliasName: string;
    gSiteName:           string;
    gSentryDSN:          string;
    gSentryTags:         string;
    gPOSServerType:      string;
    gDIAGServerType:     string;
    gREMOTEServerType:   string;
    gSentryTagScore:     string;

    Importing: boolean;
    function NewQuery (DefaultConnection: string = ''): TFDQuery;
    function GetNewID: string;
    function GetServerVersion: string;
    function DiagnosticTableExists: boolean;
    function FixTableExists: boolean;
    function ParamsTableExists: boolean;
    function GlobalFixTableExists: boolean;
    procedure CreateDiagnosticTable;
    procedure CreateFixTable;
    procedure CreateParamsTable;
    procedure CreateGlobalFixTable;
    procedure CumulativeColumnVerification (Full: boolean = False);
    procedure ParametrizeDriverLink (Driver: TFDPhysADSDriverLink);
    procedure ParametrizeConnection (Conn: TFDConnection);
    function FindStatisticsQueryID (QueryID: string): boolean;

    //TMSLogger wrap methods
    procedure Log_Info (s: string);
    procedure Log_Exception (s: string);
    procedure Log_Trace (s: string);
    procedure Log_Debug (s: string);
  end;

var
  DMDatabaseFixer: TDMDatabaseFixer;

implementation
uses uMainDoctor, uConfigForm;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TMyOutputHandler.Clear;
begin
  inherited;

  if Assigned(Memo) then
    Memo.Lines.Clear;
end;

constructor TMyOutputHandler.Create(const AMemo: TMemo);
begin
  inherited Create;
  FMemo := AMemo;
end;

procedure TMyOutputHandler.LogOutput(const AOutputInformation: TTMSLoggerOutputInformation);
begin
  inherited;
  if Assigned(Memo) then
  Memo.Lines.Add(TTMSLoggerUtils.StripHTML(TTMSLoggerUtils.GetConcatenatedLogMessage(AOutputInformation, True)));
end;

function TDMDatabaseFixer.NewQuery (DefaultConnection: string = ''): TFDQuery;
begin
  result := TFDQuery.Create(Self);
  result.Close;
  result.SQL.Clear;

  if DefaultConnection = '' then
    result.ConnectionName := FDConn.ConnectionName
  else
    result.ConnectionName := DefaultConnection;
end;

procedure TDMDatabaseFixer.DataModuleCreate(Sender: TObject);
var
  Ini: TIniFile;
  IniFileName, OldAliasName, OldServerType: string;
  Lst: TStringList;
  strVerboseLevel: WideString;

  function SplitVerboseLevel (strVerboseLevel: WideString): TSetVerboseLevel;
  var
    s: WideString;
    p: integer;
  begin
    result := [];

    //Remove brackets
    if Pos ('[', strVerboseLevel) = 1 then
      Delete (strVerboseLevel, 1, 1);

    if Pos (']', strVerboseLevel) = Length (strVerboseLevel) then
      Delete (strVerboseLevel, Length (strVerboseLevel), 1);

    //Split
    while strVerboseLevel <> '' do
    begin
      p := Pos(',', strVerboseLevel);
      if p = 0 then
      begin
        s               := strVerboseLevel;
        strVerboseLevel := '';
      end
      else
      begin
        s := Copy (strVerboseLevel, 1, p - 1);
        Delete(strVerboseLevel, 1, p);
      end;
      s := UpperCase (Trim (s));

      if s = 'INFO' then
        result := result + [vlInfo]
      else if s = 'DEBUG' then
        result := result + [vlDebug]
      else if s = 'TRACE' then
        result := result + [vlTrace]
      else if s = 'EXCEPTION' then
        result := result + [vlException];
    end;
  end;

begin
  Importing := False;
  IniFileName := Copy (ParamStr (0), 1, Length (ParamStr (0)) - 3) + 'cfg';
  //Create config file with default values
  if not FileExists (IniFileName) then
  begin
    Lst := TStringList.Create;
    try
      Lst.Add(';Remarks ');
      Lst.Add(';  POS_ServerType, DIAG_ServerType and REMOTE_ServerType valid values: stRemote, stLocal');
      Lst.Add(';');
      Lst.Add('[Global Settings]');
      Lst.Add('POS_ADSAliasName=TisWin3dd');
      Lst.Add('DIAG_ADSAliasName=TisWin3dd');
      Lst.Add('REMOTE_ADSAliasName=');
      Lst.Add('SiteName=');
      Lst.Add('SentryDSN=https://80a9540fcf1042c395598dad7569b1d4@o338504.ingest.sentry.io/5192726');
      Lst.Add('SentryTags=');
      Lst.Add('POS_ServerType=stRemote');
      Lst.Add('DIAG_ServerType=stRemote');
      Lst.Add('REMOTE_ServerType=');
      Lst.Add('SentryTagScore=');
      Lst.Add('');
      Lst.Add(';Remarks ');
      Lst.Add(';  VerboseLevel valid values: [Info,Trace,Debug,Exception]');
      Lst.Add(';    Can include zero or more values comma separated');
      Lst.Add(';    For NONE = []');
      Lst.Add(';');
      Lst.Add('[Verbose Level]');
      Lst.Add('Console=[Exception]');
      Lst.Add('Local=[Exception]');
      Lst.Add('Remote=[Exception]');
      Lst.SaveToFile(IniFileName);
    finally
      Lst.Free;
    end;
  end;

  //Read global parameters
  Ini                 := TIniFile.Create(IniFileName);
  gPOSADSAliasName    := Ini.ReadString('Global Settings', 'POS_ADSAliasName',  '');
  gDIAGADSAliasName   := Ini.ReadString('Global Settings', 'DIAG_ADSAliasName', '');
  gREMOTEADSAliasName := Ini.ReadString('Global Settings', 'REMOTE_ADSAliasName', '');
  gSiteName           := Ini.ReadString('Global Settings', 'SiteName',          '');
  gSentryDSN          := Ini.ReadString('Global Settings', 'SentryDSN',         '');
  gSentryTags         := Ini.ReadString('Global Settings', 'SentryTags',        '');
  gPOSServerType      := Ini.ReadString('Global Settings', 'POS_ServerType',    'stRemote');
  gDIAGServerType     := Ini.ReadString('Global Settings', 'DIAG_ServerType',   'stRemote');
  gREMOTEServerType   := Ini.ReadString('Global Settings', 'REMOTE_ServerType', 'stRemote');
  gSentryTagScore     := Ini.ReadString('Global Settings', 'SentryTagScore',    '');
  OldAliasName        := Ini.ReadString('Global Settings', 'ADSAliasName',      '');
  OldServerType       := Ini.ReadString('Global Settings', 'ServerType',        'stRemote');

  if (gPOSADSAliasName = '') and (gDIAGADSAliasName = '') and (OldAliasName <> '') then
  begin
    gPOSADSAliasName  := OldAliasName;
    gDIAGADSAliasName := OldAliasName;
    gPOSServerType    := OldServerType;
    gDIAGServerType   := OldServerType;
  end;

  //Read verbose level for each log
  strVerboseLevel := Ini.ReadString('Verbose Level', 'Console',  '[]');
  vlConsole       := SplitVerboseLevel (strVerboseLevel);
  strVerboseLevel := Ini.ReadString('Verbose Level', 'Local',  '[]');
  vlLocal         := SplitVerboseLevel (strVerboseLevel);
  strVerboseLevel := Ini.ReadString('Verbose Level', 'Remote',  '[]');
  vlSentry        := SplitVerboseLevel (strVerboseLevel);

  //Create the loggers
  LogConsole       := (TMSLogger.RegisterOutputHandlerClass(TMyOutputHandler, [FMainDoctor.Console]) as TMyOutputHandler);
  LogLocal         := TMSLogger.RegisterOutputHandlerClass(TTMSLoggerTextOutputHandler, [Format ('.\Logs\Log_%s.log', [FormatDateTime ('yyyy_mm_dd', Now)] )]);
  LogSentry        := (TMSLogger.RegisterOutputHandlerClass(TSentryOutputHandler) as TSentryOutputHandler);
  LogSentry.DSN    := gSentryDSN;

  //Connect to local directory
  FDConnLocal.ConnectionName := 'LOCAL';
  FDConnLocal.DriverName     := 'ADS';
  FDConnLocal.Params.Clear;
  FDConnLocal.Params.Add('DriverID=ADS');
  FDConnLocal.Params.Add(Format ('Database=%s', [ExtractFileDir (ParamStr (0))]));
  FDConnLocal.Params.Add('ServerTypes=Local');
  FDConnLocal.Params.Add('TableType=ADT');
  try
    FDConnLocal.Connected := True;
  except
  end;

  //Create the physical table if it doesn't exists
  if not FileExists (ExtractFilePath (ParamStr (0)) + 'Diag_Statistics.adt') then
  begin
    tabStatistics.Close;
    tabStatistics.FieldDefs.Add('Query_ID',        ftGuid,   38, True);
    tabStatistics.FieldDefs.Add('Pass_Fail_State', ftInteger);
    tabStatistics.FieldDefs.Add('DT_Last_Check',   ftDateTime);
    tabStatistics.FieldDefs.Add('DT_Last_Fix',     ftDateTime);
    tabStatistics.FieldDefs.Add('Last_Fail_Count', ftInteger);
    tabStatistics.CreateDataSet;
    tabStatistics.Close;
  end;

  tabStatistics.Open;
end;

function TDMDatabaseFixer.FindStatisticsQueryID (QueryID: string): boolean;
var
  bm: TBookmark;
begin
  result := False;
  if tabStatistics.RecordCount = 0 then exit;

  bm := tabStatistics.GetBookmark;
  tabStatistics.DisableControls;
  try
    tabStatistics.First;
    while not tabStatistics.Eof do
    begin
      if tabStatistics.FieldByName ('Query_ID').AsString = QueryID then
      begin
        result := True;
        exit;
      end;

      tabStatistics.Next;
    end;
  finally
    if not result then
      tabStatistics.GotoBookmark(bm);

    tabStatistics.FreeBookmark(bm);
    tabStatistics.EnableControls;
  end;
end;

function TDMDatabaseFixer.GetNewID: string;
var
  Query: TFDQuery;
begin
  result         := '';
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace ('function TDMDatabaseFixer.GetNewID(ConnectionName: string): string; [Start]');

  Query := NewQuery(FDConnDiag.ConnectionName);
  try
    try
      Query.SQL.Add('Select NewID() ID');
      Query.SQL.Add('  From System.Dictionary');
      Query.SQL.Add(' Where RowNum () = 1 ');
      Query.Open;

      if not Query.Eof then
        result := Query.FieldByName('ID').AsString;
    except
      on E: EFDDBEngineException do
        DMDatabaseFixer.Log_Exception ('Error on uDataModule [function TDMDatabaseFixer.GetNewID(ConnectionName: string): string;]' + sLineBreak +
                                       'Cause: Error getting the NewID ()' + sLineBreak +
                                       Format ('Message: [%s]', [E.Message]) + sLineBreak + Query.SQL.Text);
    end;
  finally
    Query.Free;
    DMDatabaseFixer.Log_Trace ('function TDMDatabaseFixer.GetNewID(ConnectionName: string): string; [Finish]');
  end;
end;

function TDMDatabaseFixer.GetServerVersion: string;
var
  Query: TFDQuery;
begin
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace('function TDMDatabaseFixer.GetServerVersion: string;');
  result := '';
  Query  := NewQuery;
  try
    try
      Query.SQL.Add('EXECUTE PROCEDURE sp_mgGetInstallInfo()');
      Query.Open;
      result := Query.FieldByName('Version').AsString;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;

  finally
    Query.Free;
  end;
end;

function TDMDatabaseFixer.TableExists (TableName: string): boolean;
var
  Query: TFDQuery;
begin
  result         := False;
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace(Format ('function TDMDatabaseFixer.TableExists (''%s''): boolean;', [TableName]));
  Query  := NewQuery (FDConnDiag.ConnectionName);
  try
    try
      Query.SQL.Add(Format ('Execute Procedure sp_GetTables(Null, Null, ''%s'', ''TABLE'')', [TableName]));
      Query.Open;
      result := not Query.Eof and (Query.FieldByName('Table_Name').AsString = TableName);
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;

  finally
    Query.Free;
  end;
end;

function TDMDatabaseFixer.DiagnosticTableExists: boolean;
begin
  result := TableExists ('Diagnostic_Queries');
end;

function TDMDatabaseFixer.FixTableExists: boolean;
begin
  result := TableExists ('Diagnostic_Fix_Queries');
end;

function TDMDatabaseFixer.ParamsTableExists: boolean;
begin
  result := TableExists ('Diagnostic_Params');
end;

function TDMDatabaseFixer.GlobalFixTableExists: boolean;
begin
  result := TableExists ('Diagnostic_Global_Queries');
end;

procedure TDMDatabaseFixer.CreateDiagnosticTable;
var
  Query: TFDQuery;
begin
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TDMDatabaseFixer.CreateDiagnosticTable;');
  Query  := NewQuery (FDConnDiag.ConnectionName);
  try
    try
      Query.SQL.Add('Create Table Diagnostic_Queries (');
      Query.SQL.Add('  Query_ID            GUID           Not Null Primary Key,');
      Query.SQL.Add('  Category            Varchar (100)  Not Null,');
      Query.SQL.Add('  Small_Description   Varchar (100)  Not Null,');
      Query.SQL.Add('  Long_Description    Varchar (1024),');
      Query.SQL.Add('  Pass_Fail_State     Integer,');
      Query.SQL.Add('  DT_Last_Check       TimeStamp,');
      Query.SQL.Add('  DT_Last_Fix         TimeStamp,');
      Query.SQL.Add('  Last_Fail_Count     Integer,');
      Query.SQL.Add('  SQL_Count           Varchar (8192) Not Null,');
      Query.SQL.Add('  SQL_Detail          Varchar (8192),');
      Query.SQL.Add('  SQL_Fix             Varchar (8192),');
      Query.SQL.Add('  Row_Version_Number  Integer,');
      Query.SQL.Add('  Fail_Score          Integer,');
      Query.SQL.Add('  Sentry_TAG          Varchar (512),');
      Query.SQL.Add('  Log_Remotely        Logical,');
      Query.SQL.Add('  Min_DB_Ver_Required Varchar (25),');
      Query.SQL.Add('  Status              Logical Not Null,');
      Query.SQL.Add('  SQL_Detail_Status   Logical Not Null,');
      Query.SQL.Add('  SQL_Fix_Status      Logical Not Null,');
      Query.SQL.Add(') In Database');
      Query.ExecSQL;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;

  finally
    Query.Free;
  end;
end;

procedure TDMDatabaseFixer.CreateFixTable;
var
  Query: TFDQuery;
begin
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TDMDatabaseFixer.CreateFixTable;');
  Query  := NewQuery (FDConnDiag.ConnectionName);
  try
    try
      Query.SQL.Add('Create Table Diagnostic_Fix_Queries (');
      Query.SQL.Add('  Query_ID            GUID           Not Null,');
      Query.SQL.Add('  Fix_ID              GUID           Not Null Primary Key,');
      Query.SQL.Add('  Fix_Description     Varchar (100)  Not Null,');
      Query.SQL.Add('  SQL_Fix             Varchar (8192) Not Null,');
      Query.SQL.Add('  SQL_Fix_Status      Logical');
      Query.SQL.Add(') In Database');
      Query.ExecSQL;
    except
      on E: System.SysUtils.Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
        exit;
      end;
    end;

    try
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('Create Index Idx_Fix_Query On Diagnostic_Fix_Queries (Query_ID, Fix_ID)');
      Query.ExecSQL;
    except
      on E: System.SysUtils.Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
        exit;
      end;
    end;

    try
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('Insert Into Diagnostic_Fix_Queries (Query_ID, Fix_ID, Fix_Description, SQL_Fix, SQL_Fix_Status)');
      Query.SQL.Add('  Select Query_ID, NewID() FixID, Small_Description, SQL_Fix, SQL_Fix_Status');
      Query.SQL.Add('    From Diagnostic_Queries');
      Query.SQL.Add('   Where Length (SQL_Fix) > 0');
      Query.ExecSQL;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;
  finally
    Query.Free;
  end;
end;

procedure TDMDatabaseFixer.CreateParamsTable;
var
  Query: TFDQuery;
begin
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TDMDatabaseFixer.CreateParamsTable;');
  Query  := NewQuery (FDConnDiag.ConnectionName);
  try
    try
      Query.SQL.Add('Create Table Diagnostic_Params (');
      Query.SQL.Add('  Param    Varchar (200)  Not Null Primary Key,');
      Query.SQL.Add('  DataType Varchar (20)   Not Null,');
      Query.SQL.Add('  Value    Varchar (1024)');
      Query.SQL.Add(') In Database');
      Query.ExecSQL;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;

  finally
    Query.Free;
  end;
end;

procedure TDMDatabaseFixer.CreateGlobalFixTable;
var
  Query: TFDQuery;
begin
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TDMDatabaseFixer.CreateGlobalFixTable;');
  Query  := NewQuery (FDConnDiag.ConnectionName);
  try
    try
      Query.SQL.Add('Create Table Diagnostic_Global_Queries (');
      Query.SQL.Add('  SQL_Before_Fix        Varchar (8192),');
      Query.SQL.Add('  SQL_After_Fix         Varchar (8192),');
      Query.SQL.Add('  SQL_Before_Fix_Status Logical,');
      Query.SQL.Add('  SQL_After_Fix_Status  Logical');
      Query.SQL.Add(') In Database');
      Query.ExecSQL;

      Query.SQL.Clear;
      Query.SQL.Add('Insert Into Diagnostic_Global_Queries (SQL_Before_Fix_Status, SQL_After_Fix_Status) Values (0, 0)');
      Query.ExecSQL;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;

  finally
    Query.Free;
  end;
end;

procedure TDMDatabaseFixer.CumulativeColumnVerification (Full: boolean = False);
var
  Query: TFDQuery;

  function TestForColumn (TableName, ColumnName: WideString): TColumnCreationStatus;
  begin
    result := ccsNotExists;

    try
      DMDatabaseFixer.Log_Debug(Format ('function TDMDatabaseFixer.CreateDiagnosticTable.TestForColumn (%s, %s);', [TableName, ColumnName]));

      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('Select Upper (Name) Name');
      Query.SQL.Add('  From System.Columns');
      Query.SQL.Add(' Where Upper (Parent) = Upper (:TableName)');
      Query.SQL.Add('   And Upper (Name) = Upper (:ColumnName)');
      Query.ParamByName('TableName').AsString  := TableName;
      Query.ParamByName('ColumnName').AsString := ColumnName;
      Query.Open;

      if not Query.Eof and (Query.FieldByName ('Name').AsString = UpperCase (ColumnName)) then
        result := ccsExists;
    except
      on E: System.SysUtils.Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
        result := ccsError;
      end;
    end;
  end;

  procedure CreateColumnIfNotExists (TableName, ColumnName, ColumnDefinition: WideString; DefaultValue: WideString = ''; SQLInitialization: WIdeString = '');
  var
    Status: TColumnCreationStatus;
    s: WideString;
  begin
    Status := TestForColumn(TableName, ColumnName);
    if Status <> ccsNotExists then exit;

   try
      DMDatabaseFixer.Log_Debug(Format ('procedure TDMDatabaseFixer.CreateDiagnosticTable.CreateColumnIfNotExists (%s, %s, %s, %s);', [TableName, ColumnName, ColumnDefinition, SQLInitialization]));
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add(Format ('Alter Table %s Add %s;', [TableName, ColumnDefinition]));

      SQLInitialization := Trim (SQLInitialization);
      if SQLInitialization <> '' then
      begin
        if Copy (SQLInitialization, Length (SQLInitialization), 1) <> ';' then
          SQLInitialization := SQLInitialization + ';';

        Query.SQL.Add (SQLInitialization);
      end;
      Query.ExecSQL;

      if DefaultValue <> '' then
      begin
        s := Format ('EXECUTE PROCEDURE sp_ModifyFieldProperty ( ''%s'',  ''%s'', ''Field_Default_Value'', ''%s'', ''APPEND_FAIL'', ''%sfail'' );',
                     [TableName, ColumnName, DefaultValue, TableName]);
        Query.Close;
        Query.SQL.Text := s;
        Query.ExecSQL;
      end;

      DMDatabaseFixer.Log_Info(Format ('Column %s.%s added', [TableName, ColumnName]));
   except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
   end;
  end;

  procedure VerifyRowVersion;
  begin
    try
      Query.Close;
      Query.SQL.Clear;
      Query.SQL.Add('Select Parent, Name, Field_Type');
      Query.SQL.Add('  From System.Columns');
      Query.SQL.Add(' Where Upper (Parent) = ''DIAGNOSTIC_QUERIES''');
      Query.SQL.Add('   And Upper (Name)   = ''ROW_VERSION_NUMBER''');
      Query.Open;

      //If Row_Version_Number is defined as RowVersion, then alter the column to integer
      if not Query.Eof and (Query.FieldByName ('Field_Type').AsInteger = 21) then
      begin
        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add('Alter Table Diagnostic_Queries Alter Column Row_Version_Number Row_Version_Number Integer');
        Query.ExecSQL;
      end;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;
  end;

var
  i: integer;
begin
  Query            := NewQuery;
  Query.Connection := FDConnDiag;
  DMDatabaseFixer.Log_Trace('procedure TDMDatabaseFixer.CreateDiagnosticTable; [Start]');

  try
    //Version 0.1.0.1
    CreateColumnIfNotExists ('Diagnostic_Queries', 'Status',            'Status Logical Not Null',            '', 'Update Diagnostic_Queries Set Status = 1');
    CreateColumnIfNotExists ('Diagnostic_Queries', 'SQL_Detail_Status', 'SQL_Detail_Status Logical Not Null', '', 'Update Diagnostic_Queries Set SQL_Detail_Status = 1');
    CreateColumnIfNotExists ('Diagnostic_Queries', 'SQL_Fix_Status',    'SQL_Fix_Status Logical Not Null',    '', 'Update Diagnostic_Queries Set SQL_Fix_Status = 1');
    VerifyRowVersion;

    //Version 0.2.0.0
    CreateColumnIfNotExists ('Diagnostic_Fix_Queries', 'SQL_Single_Before_Fix', 'SQL_Single_Before_Fix Varchar (8192)', '', '');
    CreateColumnIfNotExists ('Diagnostic_Fix_Queries', 'SQL_Single_After_Fix',  'SQL_Single_After_Fix Varchar (8192)',  '', '');
    CreateColumnIfNotExists ('Diagnostic_Fix_Queries', 'SQL_Before_Fix_Status', 'SQL_Before_Fix_Status Logical',        '', '');
    CreateColumnIfNotExists ('Diagnostic_Fix_Queries', 'SQL_After_Fix_Status',  'SQL_After_Fix_Status  Logical',        '', '');

    //Version 0.2.1.0
    CreateColumnIfNotExists ('Diagnostic_Queries', 'Subcategory', 'Subcategory Varchar (100)', '', '');

    if Full then
    begin
      //Verifies all the columns on all the tables, and create which are missing
      FColumnVerifProgressBar.ProgressBar1.Position := 0;
      FColumnVerifProgressBar.btnCancel.Enabled     := True;
      FColumnVerifProgressBar.Caption := 'Column verification process';
      FColumnVerifProgressBar.Show;
      for i := Low (ArrTabCol) to High (ArrTabCol) do
      begin
        CreateColumnIfNotExists(ArrTabCol [i, 0], ArrTabCol [i, 1], ArrTabCol [i, 2], ArrTabCol [i, 3]);
        FColumnVerifProgressBar.ProgressBar1.Position := Round ((i / High (ArrTabCol)) * 100);
        FColumnVerifProgressBar.Caption := Format ('Column verification process %d%s', [FColumnVerifProgressBar.ProgressBar1.Position, '%']);

        Application.ProcessMessages;

        if not FColumnVerifProgressBar.Showing then
        begin
          MessageDlg ('Process aborted', mtInformation, [mbOk], 0);
          Break;
        end;
      end;
      if FColumnVerifProgressBar.Showing then
      begin
        FColumnVerifProgressBar.Close;
        MessageDlg ('Column verification concluded', mtInformation, [mbOk], 0);
      end;
    end;

    finally
    Query.Free;
    DMDatabaseFixer.Log_Trace('procedure TDMDatabaseFixer.CreateDiagnosticTable; [Finish]');
  end;
end;


procedure TDMDatabaseFixer.tabDiagFixAfterDelete(DataSet: TDataSet);
begin
  FConfigurationMode.HasChanged := True;
end;

procedure TDMDatabaseFixer.tabDiagFixAfterPost(DataSet: TDataSet);
begin
  FConfigurationMode.HasChanged := True;
end;

procedure TDMDatabaseFixer.tabDiagFixBeforePost(DataSet: TDataSet);
var
  ID: string;
begin
  if (tabDiagFix.State <> dsInsert) or Importing then exit;
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace ('procedure TDMDatabaseFixer.tabDiagFixBeforePost(DataSet: TDataSet); [Start]');

  ID := GetNewID;
  try
    try
      tabDiagFix.FieldByName('Query_ID').AsString := tabDiagnosticQry.FieldByName('Query_ID').AsString;
      if ID <> '' then
        tabDiagFix.FieldByName('Fix_ID').AsString := ID;
    except
      on E: EFDDBEngineException do
        DMDatabaseFixer.Log_Exception ('Error on uDataModule [procedure TDMDatabaseFixer.tabDiagFixBeforePost(DataSet: TDataSet);]' + sLineBreak +
                                       Format ('Message: [%s]', [E.Message]));
    end;
  finally
    DMDatabaseFixer.Log_Trace ('procedure TDMDatabaseFixer.tabDiagFixBeforePost(DataSet: TDataSet); [Finish]');
  end;
end;

procedure TDMDatabaseFixer.tabDiagnosticQryAfterDelete(DataSet: TDataSet);
begin
  FConfigurationMode.HasChanged := True;
end;

procedure TDMDatabaseFixer.tabDiagnosticQryAfterPost(DataSet: TDataSet);
begin
  FConfigurationMode.HasChanged := True;
end;

procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet);
var
  ID: string;
begin
  if (tabDiagnosticQry.State <> dsInsert) or Importing then exit;
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace ('procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet); [Start]');

  ID := GetNewID;
  try
    try
      if ID <> '' then
        tabDiagnosticQry.FieldByName('Query_ID').AsString := ID;
    except
      on E: EFDDBEngineException do
        DMDatabaseFixer.Log_Exception ('Error on uDataModule [procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet);]' + sLineBreak +
                                       Format ('Message: [%s]', [E.Message]));
    end;
  finally
    DMDatabaseFixer.Log_Trace ('procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet); [Finish]');
  end;
end;

(*
procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet);
var
  Query: TFDQuery;
begin
  if (tabDiagnosticQry.State <> dsInsert) or Importing then exit;
  LogSentry.Tags := gSentryTags;
  DMDatabaseFixer.Log_Trace ('procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet); [Start]');
  DMDatabaseFixer.Log_Info ('Get Diagnostic_Queries.Query_ID using NewID ()');

  Query := NewQuery;
  try
    try
      Query.SQL.Add('Select NewID() ID');
      Query.SQL.Add('  From System.Dictionary');
      Query.SQL.Add(' Where RowNum () = 1 ');
      Query.Open;

      if not Query.Eof then
        tabDiagnosticQry.FieldByName('Query_ID').AsString := Query.FieldByName('ID').AsString;
    except
      on E: EFDDBEngineException do
        DMDatabaseFixer.Log_Exception ('Error on uDataModule [procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet);]' + sLineBreak +
                                       'Cause: Error getting the NewID () for Diagnostic_Queries table' + sLineBreak +
                                       Format ('Message: [%s]', [E.Message]) + sLineBreak + Query.SQL.Text);
    end;
  finally
    Query.Free;
    DMDatabaseFixer.Log_Trace ('procedure TDMDatabaseFixer.tabDiagnosticQryBeforePost(DataSet: TDataSet); [Finish]');
  end;
end;
*)

procedure TDMDatabaseFixer.tabDiagnosticQryCalcFields(DataSet: TDataSet);
var
  bm: TBookmark;
begin
  tabDiagnosticQryCalc_Pass_Fail_State.Clear;
  tabDiagnosticQryCalc_DT_Last_Check.Clear;
  tabDiagnosticQryCalc_DT_Last_Fix.Clear;
  tabDiagnosticQryCalc_Last_Fail_Count.Clear;

  bm := tabStatistics.GetBookmark;
  tabStatistics.DisableControls;

  try
    if FindStatisticsQueryID (tabDiagnosticQryQuery_ID.AsString) then
    begin
      if not tabStatistics.FieldByName('Pass_Fail_State').IsNull then
        tabDiagnosticQryCalc_Pass_Fail_State.AsInteger := tabStatistics.FieldByName('Pass_Fail_State').AsInteger;

      if not tabStatistics.FieldByName('DT_Last_Check').IsNull then
        tabDiagnosticQryCalc_DT_Last_Check.AsDateTime  := tabStatistics.FieldByName('DT_Last_Check').AsDateTime;

      if not tabStatistics.FieldByName('DT_Last_Fix').IsNull then
        tabDiagnosticQryCalc_DT_Last_Fix.AsDateTime    := tabStatistics.FieldByName('DT_Last_Fix').AsDateTime;

      if not tabStatistics.FieldByName('Last_Fail_Count').IsNull then
        tabDiagnosticQryCalc_Last_Fail_Count.AsInteger := tabStatistics.FieldByName('Last_Fail_Count').AsInteger;
    end;
  finally
    tabStatistics.GotoBookmark(bm);
    tabStatistics.FreeBookmark(bm);
    tabStatistics.EnableControls;
  end;
end;

procedure TDMDatabaseFixer.tabDiagnosticQryNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('Status').AsBoolean            := True;
  DataSet.FieldByName('SQL_Detail_Status').AsBoolean := True;
  DataSet.FieldByName('SQL_Fix_Status').AsBoolean    := True;
end;

procedure TDMDatabaseFixer.tabDiagParamsAfterDelete(DataSet: TDataSet);
begin
  FConfigurationMode.HasChanged := True;
  FConfigurationMode.PopulateCompletionProposals;
end;

procedure TDMDatabaseFixer.tabDiagParamsAfterInsert(DataSet: TDataSet);
begin
  tabDiagParams.FieldByName('DataType').AsString := 'String';
end;

procedure TDMDatabaseFixer.tabDiagParamsAfterPost(DataSet: TDataSet);
begin
  FConfigurationMode.HasChanged := True;
  FConfigurationMode.PopulateCompletionProposals;
end;

procedure TDMDatabaseFixer.ParametrizeDriverLink (Driver: TFDPhysADSDriverLink);
begin
  Driver.VendorLib := FDPhysADSDriverLink1.VendorLib;
end;

procedure TDMDatabaseFixer.ParametrizeConnection (Conn: TFDConnection);
begin
  Conn.Params.Assign(FDConn.Params);
  Conn.Params.UserName := FDConn.Params.UserName;
  Conn.Params.Password := FDConn.Params.Password;
end;

//TMSLogger wrap methods
procedure TDMDatabaseFixer.Log_Info (s: string);
begin
  LogLocal.Active   := vlInfo in vlLocal;
  LogConsole.Active := vlInfo in vlConsole;
  LogSentry.Active  := vlInfo in vlSentry;

  TMSLogger.Info (s);
end;

procedure TDMDatabaseFixer.Log_Exception (s: string);
begin
  LogLocal.Active   := vlException in vlLocal;
  LogConsole.Active := vlException in vlConsole;
  LogSentry.Active  := vlException in vlSentry;

  TMSLogger.Exception(s);
end;

procedure TDMDatabaseFixer.Log_Trace (s: string);
begin
  LogLocal.Active   := vlTrace in vlLocal;
  LogConsole.Active := vlTrace in vlConsole;
  LogSentry.Active  := vlTrace in vlSentry;

  TMSLogger.Trace(s);
end;

procedure TDMDatabaseFixer.Log_Debug (s: string);
begin
  LogLocal.Active   := vlDebug in vlLocal;
  LogConsole.Active := vlDebug in vlConsole;
  LogSentry.Active  := vlDebug in vlSentry;

  TMSLogger.Debug(s);
end;

end.
