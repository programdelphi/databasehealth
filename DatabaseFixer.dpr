program DatabaseFixer;

uses
  Vcl.Forms,
  uMainDoctor in 'uMainDoctor.pas' {FMainDoctor},
  UConfigForm in 'UConfigForm.pas' {FConfigurationMode},
  uDataModule in 'uDataModule.pas' {DMDatabaseFixer: TDataModule},
  uSQLTester in 'uSQLTester.pas' {FQueryBuilder},
  uSentry in 'uSentry.pas',
  uLoginConfigMode in 'uLoginConfigMode.pas' {FConfigModeLogin},
  uColumnVerifProgressBar in 'uColumnVerifProgressBar.pas' {FColumnVerifProgressBar},
  uColumnDefinition in 'uColumnDefinition.pas',
  uCloneDiagnosticQueryDlg in 'uCloneDiagnosticQueryDlg.pas' {FCloneDiagnosticQueryDlg},
  uParameters in 'uParameters.pas' {FParameters},
  uNewVersionDlg in 'uNewVersionDlg.pas' {FNewVersionDlg};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMainDoctor, FMainDoctor);
  Application.CreateForm(TFConfigurationMode, FConfigurationMode);
  Application.CreateForm(TDMDatabaseFixer, DMDatabaseFixer);
  Application.CreateForm(TFQueryBuilder, FQueryBuilder);
  Application.CreateForm(TFConfigModeLogin, FConfigModeLogin);
  Application.CreateForm(TFColumnVerifProgressBar, FColumnVerifProgressBar);
  Application.CreateForm(TFCloneDiagnosticQueryDlg, FCloneDiagnosticQueryDlg);
  Application.CreateForm(TFParameters, FParameters);
  Application.CreateForm(TFNewVersionDlg, FNewVersionDlg);
  Application.Run;
end.
