unit uNewVersionDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  dxSkinsDefaultPainters, cxButtons;

type
  TFNewVersionDlg = class(TForm)
    labOperation: TLabel;
    Label1: TLabel;
    Notes: TMemo;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
    FInstalled: boolean;
    FVersion:   string;
    procedure SetInstalled (Value: boolean);
    procedure SetVersion (Value: string);
    procedure UpdateLabOperation;
  public
    { Public declarations }
    property Installed: boolean read FInstalled write SetInstalled;
    property Version: string read FVersion write SetVersion;
  end;

var
  FNewVersionDlg: TFNewVersionDlg;

implementation

{$R *.dfm}

procedure TFNewVersionDlg.cxButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TFNewVersionDlg.FormCreate(Sender: TObject);
begin
  FInstalled := False;
  FVersion   := '0.0.0';
  UpdateLabOperation;
end;

procedure TFNewVersionDlg.UpdateLabOperation;
begin
  if Installed then
    labOperation.Caption := Format ('A new version %s has been installed', [Version])
  else
    labOperation.Caption := Format ('A new version %s is available',       [Version])
end;

procedure TFNewVersionDlg.SetInstalled (Value: boolean);
begin
  if FInstalled <> Value then
  begin
    FInstalled := Value;
    UpdateLabOperation;
  end;
end;

procedure TFNewVersionDlg.SetVersion (Value: string);
begin
  if FVersion <> Value then
  begin
    FVersion := Value;
    UpdateLabOperation;
  end;
end;

end.
