unit uCloneDiagnosticQueryDlg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons;

type
  TFCloneDiagnosticQueryDlg = class(TForm)
    Label1: TLabel;
    edSmallDescription: TEdit;
    Label2: TLabel;
    edNewSmallDescription: TEdit;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    procedure edNewSmallDescriptionChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCloneDiagnosticQueryDlg: TFCloneDiagnosticQueryDlg;

implementation

{$R *.dfm}

procedure TFCloneDiagnosticQueryDlg.edNewSmallDescriptionChange(Sender: TObject);
begin
  btnOK.Enabled := edNewSmallDescription.Text <> '';
end;

procedure TFCloneDiagnosticQueryDlg.FormShow(Sender: TObject);
begin
  edNewSmallDescription.Text := edSmallDescription.Text;
  edNewSmallDescription.SetFocus;
end;

end.
