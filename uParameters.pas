unit uParameters;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, Data.DB, cxDBData, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxmdaset, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxDropDownEdit, cxDBNavigator,
  Vcl.Grids, Vcl.DBGrids, cxEditRepositoryItems, cxTextEdit;

type
  TFParameters = class(TForm)
    GridDBTableView1: TcxGridDBTableView;
    GridLevel1: TcxGridLevel;
    Grid: TcxGrid;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    dsQueryParams: TDataSource;
    memAllParams: TdxMemData;
    memAllParamsParam: TStringField;
    memAllParamsValue: TStringField;
    GridDBTableView1RecId: TcxGridDBColumn;
    GridDBTableView1Param: TcxGridDBColumn;
    GridDBTableView1Value: TcxGridDBColumn;
    memAllParamsDataType: TStringField;
    GridDBTableView1DataType: TcxGridDBColumn;
    EditRep: TcxEditRepository;
    EditRepText: TcxEditRepositoryTextItem;
    EditRepCalc: TcxEditRepositoryCalcItem;
    EditRepSpin: TcxEditRepositorySpinItem;
    EditRepDate: TcxEditRepositoryDateItem;
    memParams: TdxMemData;
    memParamsParam: TStringField;
    memParamsDataType: TStringField;
    memParamsValue: TStringField;
    procedure btnOKClick(Sender: TObject);
    procedure GridDBTableView1ValueGetProperties(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure dsQueryParamsStateChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    function FindAllParam (Param: string): boolean;
    function FindParam (Param: string): boolean;
  public
    { Public declarations }
    ExcludedParams: TStringList;
    procedure CopyAllParams;
    procedure GetQueryParams (Query: TFDQuery);
    procedure SetQueryParams (Query: TFDQuery);
    procedure ClearParameters;
    procedure AddParameter (Param: string);
    procedure SaveTemporaryParameters;
  end;

var
  FParameters: TFParameters;

implementation
uses uConfigForm, uDataModule;

{$R *.dfm}

procedure TFParameters.btnOKClick(Sender: TObject);
begin
  if memParams.State in [dsInsert, dsEdit] then
    memParams.Post;

  SaveTemporaryParameters;
  ModalResult := mrOK;
end;

procedure TFParameters.CopyAllParams;
begin
  memAllParams.CopyFromDataSet(DMDatabaseFixer.tabDiagParams);
end;

procedure TFParameters.dsQueryParamsStateChange(Sender: TObject);
begin
  btnOK.Enabled := dsQueryParams.State = dsBrowse;
end;

function TFParameters.FindAllParam (Param: string): boolean;
var
  bm: TBookmark;
begin
  result := False;
  Param  := UpperCase (Param);

  if not memAllParams.Active or (memAllParams.RecordCount = 0) then exit;

  bm := memAllParams.GetBookmark;
  try
    memAllParams.DisableControls;
    memAllParams.First;
    while not memAllParams.Eof do
    begin
      if Param = UpperCase (memAllParams.FieldByName ('Param').AsString) then
      begin
        result := True;
        exit;
      end;

      memAllParams.Next;
    end;
  finally
    if not result then
      memAllParams.GotoBookmark(bm);

    memAllParams.FreeBookmark(bm);
    memAllParams.DisableControls;
  end;
end;

function TFParameters.FindParam (Param: string): boolean;
var
  bm: TBookmark;
begin
  result := False;
  Param  := UpperCase (Param);

  if not memParams.Active or (memParams.RecordCount = 0) then exit;

  bm := memParams.GetBookmark;
  memParams.DisableControls;
  try
    memParams.First;
    while not memParams.Eof do
    begin
      if UpperCase (memParams.FieldByName('Param').AsString) = Param then
      begin
        result := True;
        exit;
      end;

      memParams.Next;
    end;
  finally
    if not result then
      memParams.GotoBookmark(bm);

    memParams.FreeBookmark(bm);
    memParams.EnableControls;
  end;
end;


procedure TFParameters.FormCreate(Sender: TObject);
begin
  ExcludedParams := TStringList.Create;
end;

procedure TFParameters.FormDestroy(Sender: TObject);
begin
  ExcludedParams.Free;
end;

procedure TFParameters.GetQueryParams (Query: TFDQuery);
var
  i: integer;
begin
  memParams.Close;
  memParams.Open;
  try
    for i := 0 to Query.ParamCount - 1 do
    begin
      if ExcludedParams.IndexOf (UpperCase (Query.Params [i].Name)) = -1 then
      begin
        memParams.Append;
        memParams.FieldByName('Param').AsString := Query.Params [i].Name;
        if FindAllParam (Query.Params [i].Name) then
        begin
          memParams.FieldByName('DataType').AsString := memAllParams.FieldByName('DataType').AsString;
          memParams.FieldByName('Value').AsString    := memAllParams.FieldByName('Value').AsString;
        end
        else
        begin
          memParams.FieldByName('DataType').AsString := 'String';
          memParams.FieldByName('Value').AsString    := '';
        end;
        memParams.Post;
      end;
    end;
  finally
    memParams.First;
    dsQueryParams.DataSet := memParams;
  end;
end;

procedure TFParameters.GridDBTableView1ValueGetProperties(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  if      ARecord.Values [2] = 'String' then
    AProperties := EditRepText.Properties
  else if ARecord.Values [2] = 'Integer' then
    AProperties := EditRepSpin.Properties
  else if ARecord.Values [2] = 'Date' then
    AProperties := EditRepDate.Properties
  else if ARecord.Values [2] = 'Float' then
    AProperties := EditRepCalc.Properties
end;

procedure TFParameters.SetQueryParams (Query: TFDQuery);
var
  bm: TBookmark;
  ParamName: string;
begin
  bm := memParams.GetBookmark;
  memParams.DisableControls;
  memParams.First;
  while not memParams.Eof do
  begin
    ParamName := memParams.FieldByName('Param').AsString;
    if Query.FindParam (ParamName) <> nil then
    begin
      if memParams.FieldByName('DataType').AsString = 'Float' then
      begin
        Query.ParamByName(ParamName).DataType := ftFloat;
        Query.ParamByName(ParamName).AsFloat  := memParams.FieldByName('Value').AsFloat;
      end
      else if memParams.FieldByName('DataType').AsString = 'Date' then
      begin
        Query.ParamByName(ParamName).DataType   := ftDateTime;
        Query.ParamByName(ParamName).AsDateTime := memParams.FieldByName('Value').AsDateTime;
      end
      else if memParams.FieldByName('DataType').AsString = 'Integer' then
      begin
        Query.ParamByName(ParamName).DataType  := ftInteger;
        Query.ParamByName(ParamName).AsInteger := memParams.FieldByName('Value').AsInteger;
      end
      else if memParams.FieldByName('DataType').AsString = 'String' then
      begin
        Query.ParamByName(ParamName).DataType := ftString;
        Query.ParamByName(ParamName).AsString := memParams.FieldByName('Value').AsString;
      end;
    end;

    memParams.Next;
  end;
  memParams.GotoBookmark(bm);
  memParams.FreeBookmark(bm);
  memParams.EnableControls;
end;

procedure TFParameters.ClearParameters;
begin
  memParams.Close;
  memParams.Open;
end;

procedure TFParameters.AddParameter (Param: string);
begin
  if FindParam (Param) or (ExcludedParams.IndexOf (UpperCase (Param)) <> -1) then exit;

  memParams.Append;
  memParams.FieldByName('Param').AsString := Param;
  if FindAllParam (Param) then
  begin
    memParams.FieldByName('DataType').AsString := memAllParams.FieldByName('DataType').AsString;
    memParams.FieldByName('Value').AsString    := memAllParams.FieldByName('Value').AsString;
  end
  else
  begin
    memParams.FieldByName('DataType').AsString := 'String';
    memParams.FieldByName('Value').AsString    := '';
  end;
  memParams.Post;
end;

procedure TFParameters.SaveTemporaryParameters;
var
  bm: TBookmark;
begin
  bm := memParams.GetBookmark;
  memParams.DisableControls;
  try
    memParams.First;
    while not memParams.Eof do
    begin
      if FindAllParam (memParams.FieldByName ('Param').AsString) then
        memAllParams.Edit
      else
      begin
        memAllParams.Append;
        memAllParams.FieldByName('Param').AsString    := memParams.FieldByName('Param').AsString;
      end;

      memAllParams.FieldByName('DataType').AsString := memParams.FieldByName('DataType').AsString;
      memAllParams.FieldByName('Value').AsString    := memParams.FieldByName('Value').AsString;
      memAllParams.Post;

      memParams.Next;
    end;
  finally
    memParams.GotoBookmark(bm);
    memParams.FreeBookmark(bm);
    memParams.EnableControls;
  end;
end;

end.
