Create Table Diagnostic_Queries (
  Query_ID            GUID           Not Null Primary Key,
  Category            Varchar (100)  Not Null,
  Small_Description   Varchar (100)  Not Null,
  Long_Description    Varchar (1024),
  Pass_Fail_State     Integer,
  DT_Last_Check       TimeStamp,
  DT_Last_Fix         TimeStamp,
  Last_Fail_Count     Integer,
  SQL_Count           Varchar (8192) Not Null,
  SQL_Detail          Varchar (8192),
  SQL_Fix             Varchar (8192),
  Row_Version_Number  Integer,
  Fail_Score          Integer,
  Sentry_TAG          Varchar (512),
  Log_Remotely        Logical,
  Min_DB_Ver_Required Varchar (25)
) In Database;