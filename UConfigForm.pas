unit UConfigForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uDataModule, Data.DB, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.Mask, Vcl.DBCtrls, SynEdit, SynDBEdit, SynEditHighlighter,
  SynHighlighterSQL, Vcl.Buttons, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxDBEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, dxDateRanges, VCL.TMSLogging, FireDAC.Phys.ADS, FireDAC.Phys.Intf,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, uSQLTester, Vcl.Menus, cxButtons,
  cxCheckBox, dxmdaset, System.ImageList, Vcl.ImgList, System.Actions,
  Vcl.ActnList, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Phys.ADSDef,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, JvMemoryDataset, IniFiles,
  FireDAC.Stan.StorageBin, JvExMask, JvToolEdit, JvCombobox, TypInfo, Vcl.Grids,
  Vcl.DBGrids, uCloneDiagnosticQueryDlg, SynCompletionProposal, uParameters,
  cxEditRepositoryItems;

const
  ValidCommands: WideString = '|ALTER|BEGIN|COMMIT|CREATE|DELETE|DROP|EXECUTE|GRANT|INSERT|MERGE|REVOKE|ROLLBACK|SAVEPOINT|SELECT|SET|TRIM|UPDATE|' +
                              'CATCH|DECLARE|IF|WHILE|OPEN|TRY|CACHE|';
  coCmpLocal           = 'Local';
  coCmpRemote          = 'Remote';
  coCmpDifferent       = 'Different';
  coCmpMissingOnLocal  = 'Missing on Local';
  coCmpMissingOnRemote = 'Missing on remote';
type
  PString = ^String;

  {Class created to avoid the auto new line in the TStringList class}
  TStringCollection = class
  private
    List: TList;
    function GetCount: integer;
    function GetValue (Index: integer): string;
    procedure SetValue (Index: integer; Value: string);
  public
    constructor Create;
    destructor Destroy;
    procedure Add (s: string);
    procedure Clear;
    function Join (IncludeLastInstruction: boolean): WideString;
    property Count: integer read GetCount;
    property Items [Index: integer]: string read GetValue write SetValue; default;
  end;

  TFConfigurationMode = class(TForm)
    PageControl1: TPageControl;
    tabSingleView: TTabSheet;
    tabGridView: TTabSheet;
    DSDiagnosticQueries: TDataSource;
    gbQueryIdentification: TGroupBox;
    dbmLongDescription: TDBMemo;
    Label4: TLabel;
    dbeSmallDescription: TDBEdit;
    Label3: TLabel;
    Label2: TLabel;
    dbeQuery_ID: TDBEdit;
    Label1: TLabel;
    gbQueryBehavior: TGroupBox;
    SynSQL: TSynSQLSyn;
    Label8: TLabel;
    dbeRowVersionNumber: TDBEdit;
    Label9: TLabel;
    dbeDBMinVer: TDBEdit;
    Label10: TLabel;
    dbeFailScore: TDBEdit;
    dbcbLogRemotely: TDBCheckBox;
    Label11: TLabel;
    dbeSentryTag: TDBEdit;
    gbLastExecutionInformation: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    dbeLastFailCount: TDBEdit;
    Label14: TLabel;
    dbePassFailState: TDBEdit;
    Label15: TLabel;
    DBNavigator1: TDBNavigator;
    dbedatLastCheck: TcxDBDateEdit;
    dbedatLastFix: TcxDBDateEdit;
    btnOK: TcxButton;
    SearchGrid: TcxGrid;
    SearchGridDBTableView1: TcxGridDBTableView;
    SearchGridLevel1: TcxGridLevel;
    Shape1: TShape;
    Shape2: TShape;
    dbcbActive: TDBCheckBox;
    SearchGridDBTableView1Query_ID: TcxGridDBColumn;
    SearchGridDBTableView1Category: TcxGridDBColumn;
    SearchGridDBTableView1Small_Description: TcxGridDBColumn;
    SearchGridDBTableView1Long_Description: TcxGridDBColumn;
    SearchGridDBTableView1Pass_Fail_State: TcxGridDBColumn;
    SearchGridDBTableView1DT_Last_Check: TcxGridDBColumn;
    SearchGridDBTableView1DT_Last_Fix: TcxGridDBColumn;
    SearchGridDBTableView1Last_Fail_Count: TcxGridDBColumn;
    SearchGridDBTableView1SQL_Count: TcxGridDBColumn;
    SearchGridDBTableView1SQL_Detail: TcxGridDBColumn;
    SearchGridDBTableView1Row_Version_Number: TcxGridDBColumn;
    SearchGridDBTableView1Fail_Score: TcxGridDBColumn;
    SearchGridDBTableView1Sentry_TAG: TcxGridDBColumn;
    SearchGridDBTableView1Log_Remotely: TcxGridDBColumn;
    SearchGridDBTableView1Min_DB_Ver_Required: TcxGridDBColumn;
    SearchGridDBTableView1Status: TcxGridDBColumn;
    SearchGridDBTableView1SQL_Detail_Status: TcxGridDBColumn;
    SearchGridDBTableView1SQL_Fix_Status: TcxGridDBColumn;
    SearchGridDBTableView1Selected: TcxGridDBColumn;
    memDiagnostic_Queries: TdxMemData;
    RGViewMode: TRadioGroup;
    DSMemDiagnostic_Queries: TDataSource;
    GridImageList: TImageList;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ActionList1: TActionList;
    actExpandAll: TAction;
    actCollapseAll: TAction;
    actSelectAll: TAction;
    actUnselectAll: TAction;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    actExport: TAction;
    cxButton5: TcxButton;
    Label16: TLabel;
    edExportFileName: TEdit;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    actGenerateColumnsUnit: TAction;
    SaveUnitDlg: TSaveDialog;
    tabConfigFile: TTabSheet;
    Shape3: TShape;
    gbDatabase: TGroupBox;
    Label17: TLabel;
    edPOSADSAliasName: TEdit;
    rgPOSServerType: TRadioGroup;
    Label18: TLabel;
    edSiteName: TEdit;
    gbSentry: TGroupBox;
    Label19: TLabel;
    edTag: TEdit;
    Label20: TLabel;
    edTagScore: TEdit;
    Label21: TLabel;
    edDSN: TEdit;
    gbVerboseLevel: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    chkcbConsole: TJvCheckedComboBox;
    chkcbLocal: TJvCheckedComboBox;
    chkcbRemote: TJvCheckedComboBox;
    btnCfgFileApply: TcxButton;
    btnCfgFileCancel: TcxButton;
    Label25: TLabel;
    edDIAGADSAliasName: TEdit;
    rgDIAGServerType: TRadioGroup;
    Label26: TLabel;
    edREMOTEADSAliasName: TEdit;
    rgREMOTEServerType: TRadioGroup;
    tabImportExportDiagnosticQueries: TTabSheet;
    Shape4: TShape;
    gbImportExport: TGroupBox;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    GroupBox1: TGroupBox;
    actConnectToRemoteAndCompare: TAction;
    actDisconnectFromRemote: TAction;
    Button1: TButton;
    Button2: TButton;
    DSCompare: TDataSource;
    FDMemCompare: TFDMemTable;
    actRefreshCompare: TAction;
    GridCompareDBTableView1: TcxGridDBTableView;
    GridCompareLevel1: TcxGridLevel;
    GridCompare: TcxGrid;
    rgCompareFilter: TRadioGroup;
    actPullAllFromRemote: TAction;
    actPushAllFromLocal: TAction;
    actCompareSelectAll: TAction;
    actCompareUnselectAll: TAction;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    GridCompareDBTableView1Query_ID: TcxGridDBColumn;
    GridCompareDBTableView1Category: TcxGridDBColumn;
    GridCompareDBTableView1Small_Description: TcxGridDBColumn;
    GridCompareDBTableView1Long_Description: TcxGridDBColumn;
    GridCompareDBTableView1Pass_Fail_State: TcxGridDBColumn;
    GridCompareDBTableView1DT_Last_Check: TcxGridDBColumn;
    GridCompareDBTableView1DT_Last_Fix: TcxGridDBColumn;
    GridCompareDBTableView1Last_Fail_Count: TcxGridDBColumn;
    GridCompareDBTableView1SQL_Count: TcxGridDBColumn;
    GridCompareDBTableView1SQL_Detail: TcxGridDBColumn;
    GridCompareDBTableView1SQL_Fix: TcxGridDBColumn;
    GridCompareDBTableView1Fail_Score: TcxGridDBColumn;
    GridCompareDBTableView1Sentry_TAG: TcxGridDBColumn;
    GridCompareDBTableView1Log_Remotely: TcxGridDBColumn;
    GridCompareDBTableView1Min_DB_Ver_Required: TcxGridDBColumn;
    GridCompareDBTableView1Status: TcxGridDBColumn;
    GridCompareDBTableView1SQL_Detail_Status: TcxGridDBColumn;
    GridCompareDBTableView1SQL_Fix_Status: TcxGridDBColumn;
    GridCompareDBTableView1Selected: TcxGridDBColumn;
    GridCompareDBTableView1Row_Version_Number: TcxGridDBColumn;
    GridCompareDBTableView1Location: TcxGridDBColumn;
    GridCompareDBTableView1Row_Type: TcxGridDBColumn;
    actPullSelectedFromRemote: TAction;
    actPushSelectedFromLocal: TAction;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    cxButton10: TcxButton;
    cxButton11: TcxButton;
    actCloneDiagnosticQuery: TAction;
    btnCloneFix: TcxButton;
    PageControlSQL: TPageControl;
    tabSQLCount: TTabSheet;
    tabSQLDetail: TTabSheet;
    tabSQLFix: TTabSheet;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    pnlSQLCount: TPanel;
    Panel5: TPanel;
    btnSQLCount: TBitBtn;
    Panel8: TPanel;
    dbsyneSQLCount: TDBSynEdit;
    pnlSQLDetail: TPanel;
    Panel6: TPanel;
    btnSQLDetail: TBitBtn;
    dbcbSQL_Detail_Active: TDBCheckBox;
    Panel9: TPanel;
    dbsyneSQLDetail: TDBSynEdit;
    GridFixDBTableView1: TcxGridDBTableView;
    GridFixLevel1: TcxGridLevel;
    GridFix: TcxGrid;
    DSSQLFix: TDataSource;
    GridFixDBTableView1Fix_ID: TcxGridDBColumn;
    GridFixDBTableView1Fix_Description: TcxGridDBColumn;
    GridFixDBTableView1SQL_Fix_Status: TcxGridDBColumn;
    tabParams: TTabSheet;
    Shape9: TShape;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DBNavigator2: TDBNavigator;
    DSParameters: TDataSource;
    cxGrid1DBTableView1Param: TcxGridDBColumn;
    cxGrid1DBTableView1Value: TcxGridDBColumn;
    propSQLCount: TSynCompletionProposal;
    propSQLDetail: TSynCompletionProposal;
    propSQLFix: TSynCompletionProposal;
    cxGrid1DBTableView1DataType: TcxGridDBColumn;
    EditRep: TcxEditRepository;
    EditRepText: TcxEditRepositoryTextItem;
    EditRepCalc: TcxEditRepositoryCalcItem;
    EditRepSpin: TcxEditRepositorySpinItem;
    EditRepDate: TcxEditRepositoryDateItem;
    propSQLBeforeFix: TSynCompletionProposal;
    propSQLAfterFix: TSynCompletionProposal;
    tabGlobalSQLBeforeAndAfterFix: TTabSheet;
    Shape12: TShape;
    pnlBeforeAndAfter: TPanel;
    pnlAfterFix: TPanel;
    Panel3: TPanel;
    btnSQLAfterFix: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    Panel4: TPanel;
    dbsyneSQLAfterFix: TDBSynEdit;
    SplitterBeforeAfter: TSplitter;
    pnlBeforeFix: TPanel;
    Panel7: TPanel;
    btnSQLBeforeFix: TBitBtn;
    DBCheckBox2: TDBCheckBox;
    Panel10: TPanel;
    dbsyneSQLBeforeFix: TDBSynEdit;
    DSGlobalFix: TDataSource;
    pnlBeforeAfterNavigator: TPanel;
    DBNavigator3: TDBNavigator;
    PageFixControl: TPageControl;
    pagSingleBeforeFix: TTabSheet;
    pagSingleFix: TTabSheet;
    pagSingleAfterFix: TTabSheet;
    Shape8: TShape;
    Shape10: TShape;
    pnlFix: TPanel;
    dbsyneSQLFix: TDBSynEdit;
    btnSQLFix: TBitBtn;
    Shape11: TShape;
    btnBeforeSQLFix: TBitBtn;
    dbsyneBeforeSQLFix: TDBSynEdit;
    cbSingleBeforeFix: TDBCheckBox;
    btnAfterSQLFix: TBitBtn;
    cbSingleAfterFix: TDBCheckBox;
    dbsyneAfterSQLFix: TDBSynEdit;
    propBeforeSQLFix: TSynCompletionProposal;
    propAfterSQLFix: TSynCompletionProposal;
    Label5: TLabel;
    dbcbCategory: TcxDBComboBox;
    dbcbSubcategory: TcxDBComboBox;
    SearchGridDBTableView1Subcategory: TcxGridDBColumn;
    procedure btnSQLCountClick(Sender: TObject);
    procedure btnSQLDetailClick(Sender: TObject);
    procedure DSDiagnosticQueriesStateChange(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure RGViewModeClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure actCollapseAllExecute(Sender: TObject);
    procedure actExpandAllExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure actSelectAllExecute(Sender: TObject);
    procedure actUnselectAllExecute(Sender: TObject);
    procedure actExportExecute(Sender: TObject);
    procedure actGenerateColumnsUnitExecute(Sender: TObject);
    procedure btnCfgFileCancelClick(Sender: TObject);
    procedure btnCfgFileApplyClick(Sender: TObject);
    procedure edPOSADSAliasNameChange(Sender: TObject);
    procedure rgPOSServerTypeClick(Sender: TObject);
    procedure edSiteNameChange(Sender: TObject);
    procedure edTagChange(Sender: TObject);
    procedure edTagScoreChange(Sender: TObject);
    procedure edDSNChange(Sender: TObject);
    procedure chkcbConsoleChange(Sender: TObject);
    procedure chkcbLocalChange(Sender: TObject);
    procedure chkcbRemoteChange(Sender: TObject);
    procedure actConnectToRemoteAndCompareExecute(Sender: TObject);
    procedure actDisconnectFromRemoteExecute(Sender: TObject);
    procedure actRefreshCompareExecute(Sender: TObject);
    procedure rgCompareFilterClick(Sender: TObject);
    procedure actCompareSelectAllExecute(Sender: TObject);
    procedure actCompareUnselectAllExecute(Sender: TObject);
    procedure actPullAllFromRemoteExecute(Sender: TObject);
    procedure actPullSelectedFromRemoteExecute(Sender: TObject);
    procedure actPushAllFromLocalExecute(Sender: TObject);
    procedure actPushSelectedFromLocalExecute(Sender: TObject);
    procedure actCloneDiagnosticQueryExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnSQLFixClick(Sender: TObject);
    procedure DSSQLFixStateChange(Sender: TObject);
    procedure cxGrid1DBTableView1ValueGetProperties(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure btnSQLBeforeFixClick(Sender: TObject);
    procedure btnSQLAfterFixClick(Sender: TObject);
    procedure pnlBeforeAndAfterResize(Sender: TObject);
    procedure btnBeforeSQLFixClick(Sender: TObject);
    procedure btnAfterSQLFixClick(Sender: TObject);
    procedure SearchGridDBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure DSDiagnosticQueriesDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure OpenQueryBuilder (FieldName: string);
    procedure UpdateCategoryCombos;
  public
    { Public declarations }
    HasChanged:     boolean;
    CfgChanged:     boolean;
    CfgLoading:     boolean;
    RefreshCompare: boolean;
    function SplitInstructionsOracleStyle (SQL: WideString; IncludeFirstToken: boolean): TStringCollection;
    function SplitInstructionsSemicolon   (SQL: WideString; IncludeFirstToken: boolean): TStringCollection;
    function IsSelectInto (SQL: WideString): boolean;
    function GetStatementType (SQL: WideString): WideString;
    procedure ReadGlobalSettings;
    procedure SaveGlobalSettings;
    function ValidateConnection (Conn: TFDConnection): boolean;
    procedure ReadPOSConnection    (Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);
    procedure ReadDIAGConnection   (Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);
    procedure ReadREMOTEConnection (Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);
    procedure CompareDiagnosticQry (Local, Remote: TFDTable; Result: TFDMemTable);
    procedure SyncDQ (Target: TFDTable; SourceLocation: string; AllRows: boolean; CheckRowVersion: boolean = False);
    procedure PullAllCommandLine;
    procedure PopulateCompletionProposals;
  end;

var
  FConfigurationMode: TFConfigurationMode;

implementation

{$R *.dfm}
uses uMainDoctor;

constructor TStringCollection.Create;
begin
  inherited Create;

  List := TList.Create;
end;

destructor TStringCollection.Destroy;
begin
  Clear;
  List.Free;

  inherited Destroy;
end;

function TStringCollection.GetCount: integer;
begin
  result := List.Count;
end;

procedure TStringCollection.Add (s: string);
var
  PS: PString;
begin
  New (PS);
  PS^ := s;
  List.Add(PS)
end;

procedure TStringCollection.Clear;
var
  PS: PString;
begin
  while List.Count > 0 do
  begin
    PS := List [0];
    List.Delete(0);
    PS^ := '';
    Dispose (PS);
  end;
end;

function TStringCollection.Join (IncludeLastInstruction: boolean): WideString;
var
  i: integer;
begin
  result := '';

  for i := 0 to Count - 1 do
  begin
    if IncludeLastInstruction or (i < (Count - 1)) then
      result := result + Items [i];
  end;
end;


function TStringCollection.GetValue (Index: integer): string;
begin
  result := string (List [Index]^);
end;

procedure TStringCollection.SetValue (Index: integer; Value: string);
begin
  string (List [Index]^) := Value;
end;

procedure TFConfigurationMode.OpenQueryBuilder (FieldName: string);
begin
  FParameters.CopyAllParams;
  FQueryBuilder.propQueryBuilder.ItemList.Assign(propSQLCount.ItemList);
  if (FieldName = 'SQL_Fix') or (FieldName = 'SQL_Single_After_Fix') or (FieldName = 'SQL_Single_Before_Fix') then
  begin
    FQueryBuilder.SynEdit1.Text              := DSSQLFix.DataSet.FieldByName(FieldName).AsString;
    FQueryBuilder.acSQLBuilderPlainText1.SQL := DSSQLFix.DataSet.FieldByName(FieldName).AsString;
    if (FQueryBuilder.ShowModal = mrOk) and (DSSQLFix.State in [dsEdit, dsInsert]) then
      DSSQLFix.DataSet.FieldByName(FieldName).AsString := FQueryBuilder.SynEdit1.Text;
  end
  else if (FieldName = 'SQL_Before_Fix') or (FieldName = 'SQL_After_Fix') then
  begin
    FQueryBuilder.SynEdit1.Text              := DSGlobalFix.DataSet.FieldByName(FieldName).AsString;
    FQueryBuilder.acSQLBuilderPlainText1.SQL := DSGlobalFix.DataSet.FieldByName(FieldName).AsString;
    if (FQueryBuilder.ShowModal = mrOk) and (DSGlobalFix.State in [dsEdit, dsInsert]) then
      DSGlobalFix.DataSet.FieldByName(FieldName).AsString := FQueryBuilder.SynEdit1.Text;
  end
  else
  begin
    FQueryBuilder.SynEdit1.Text              := DSDiagnosticQueries.DataSet.FieldByName(FieldName).AsString;
    FQueryBuilder.acSQLBuilderPlainText1.SQL := DSDiagnosticQueries.DataSet.FieldByName(FieldName).AsString;
    if (FQueryBuilder.ShowModal = mrOk) and (DSDiagnosticQueries.State in [dsEdit, dsInsert]) then
      DSDiagnosticQueries.DataSet.FieldByName(FieldName).AsString := FQueryBuilder.SynEdit1.Text;
  end;
end;

procedure TFConfigurationMode.PageControl1Change(Sender: TObject);
begin
  //When tab changes, allways select search mode in grid view
  RGViewMode.ItemIndex := 0;
end;

procedure TFConfigurationMode.pnlBeforeAndAfterResize(Sender: TObject);
begin
  pnlBeforeFix.Height := (pnlBeforeAndAfter.ClientHeight - (pnlBeforeAfterNavigator.Height + SplitterBeforeAfter.Height)) div 2;
end;

procedure TFConfigurationMode.RGViewModeClick(Sender: TObject);
var
  Selected: TField;
begin
  case RGViewMode.ItemIndex of
    -1, 0: SearchGridDBTableView1.DataController.DataSource := DSDiagnosticQueries;
    1, 2: begin
      if RGViewMode.Tag < 1 then  //-1, 0
      begin
        memDiagnostic_Queries.Close;
        memDiagnostic_Queries.Fields.Clear;
        memDiagnostic_Queries.FieldDefs.Clear;
        memDiagnostic_Queries.FieldDefs.Assign(DMDatabaseFixer.tabDiagnosticQry.FieldDefs);

        Selected           := TBooleanField.Create(memDiagnostic_Queries);
        Selected.FieldName := 'Selected';
        Selected.DataSet   := memDiagnostic_Queries;
        memDiagnostic_Queries.Open;

        memDiagnostic_Queries.LoadFromDataSet(DMDatabaseFixer.tabDiagnosticQry);
        SearchGridDBTableView1.DataController.DataSource := DSMemDiagnostic_Queries;
      end;

      case RGViewMode.ItemIndex of
        1: begin
             SearchGridDBTableView1.DataController.Groups.ClearGrouping;
             SearchGridDBTableView1Category.Visible := True;
        end;
        2: SearchGridDBTableView1Category.GroupBy(SearchGridDBTableView1Category.Index);
      end;
    end;
  end;

  RGViewMode.Tag := RGViewMode.ItemIndex;
end;

function TFConfigurationMode.SplitInstructionsOracleStyle (SQL: WideString; IncludeFirstToken: boolean): TStringCollection;
var
  Syn: TSynSQLSyn;
  s, FirstToken, UpperToken, Token: WideString;
  PrevNewLine, PrevNewLineSlash: boolean;
begin
  result := TStringCollection.Create;
  Syn    := TSynSQLSyn.Create(Self);
  try
    Syn.SetLine(SQL, 1);
    PrevNewLine      := False;
    PrevNewLineSlash := False;
    s                := '';
    FirstToken       := '';
    while Syn.GetTokenID <> tkNull do
    begin
      if PrevNewLineSlash and (Syn.GetTokenID = tkSpace) and (Syn.GetToken = #13#10) then
      begin
        Delete (s, Length (s) - 2, 3);
        if IncludeFirstToken then
          result.Add(FirstToken + '|' + s)
        else
          result.Add(s);
        s          := '';
        FirstToken := '';
      end
      else
      begin
        Token      := Syn.GetToken;
        UpperToken := UpperCase (Token);
        s          := s + Token;

        if (FirstToken = '') and
           (Pos ('|' + UpperToken + '|', ValidCommands) > 0) then
           FirstToken := UpperToken;
      end;

      PrevNewLineSlash := PrevNewLine and (Syn.GetTokenID = tkSymbol) and (Syn.GetToken = '/');
      PrevNewLine      := (Syn.GetTokenID = tkSpace) and (Syn.GetToken = #13#10);

      Syn.Next;
    end;

    if s <> '' then
    begin
      if IncludeFirstToken then
        result.Add(FirstToken + '|' + s)
      else
        result.Add(s);
    end;
  finally
    Syn.Free;
  end;
end;

function TFConfigurationMode.SplitInstructionsSemicolon (SQL: WideString; IncludeFirstToken: boolean): TStringCollection;
var
  Syn: TSynSQLSyn;
  s, FirstToken, UpperToken, Token: WideString;
begin
  result := TStringCollection.Create;
  Syn    := TSynSQLSyn.Create(Self);
  try
    Syn.SetLine(SQL, 1);
    s                := '';
    FirstToken       := '';
    while Syn.GetTokenID <> tkNull do
    begin
      if (Syn.GetTokenID = tkSymbol) and (Syn.GetToken = ';') then
      begin
        if FirstToken <> '' then
        begin
          if IncludeFirstToken then
            result.Add(FirstToken + '|' + s + ';')
          else
            result.Add(s + ';');
        end;
        s          := '';
        FirstToken := '';
      end
      else
      begin
        Token      := Syn.GetToken;
        UpperToken := UpperCase (Token);
        s          := s + Token;

        if (FirstToken = '') and
           (Pos ('|' + UpperToken + '|', ValidCommands) > 0) then
           FirstToken := UpperToken;
      end;

      Syn.Next;
    end;

    if (s <> '') and (FirstToken <> '') then
    begin
      if IncludeFirstToken then
        result.Add(FirstToken + '|' + s)
      else
        result.Add(s);
    end;
  finally
    Syn.Free;
  end;
end;

function TFConfigurationMode.IsSelectInto (SQL: WideString): boolean;
var
  Syn: TSynSQLSyn;
  FirstToken, UpperToken: WideString;
begin
  result := False;
  Syn    := TSynSQLSyn.Create(Self);
  try
    Syn.SetLine(SQL, 1);
    while (Syn.GetTokenID <> tkNull) and ((FirstToken = '') or (FirstToken = 'SELECT')) do
    begin
      UpperToken := UpperCase (Syn.GetToken);
      if (FirstToken = '') and (Pos ('|' + UpperToken + '|', ValidCommands) > 0) then
      begin
        FirstToken := UpperToken;

        if FirstToken <> 'SELECT' then exit;
      end
      else if (FirstToken = 'SELECT') and (UpperToken = 'INTO') then
      begin
        result  := True;
        exit;
      end;

      Syn.Next;
    end;
  finally
    Syn.Free;
  end;
end;

function TFConfigurationMode.GetStatementType (SQL: WideString): WideString;
var
  Syn: TSynSQLSyn;
  UpperToken: WideString;
begin
  result := '';
  Syn    := TSynSQLSyn.Create(Self);
  try
    Syn.SetLine(SQL, 1);
    while Syn.GetTokenID <> tkNull do
    begin
      UpperToken := UpperCase (Syn.GetToken);
      if (result = '') and (Pos ('|' + UpperToken + '|', ValidCommands) > 0) then
      begin
        result := UpperToken;
        exit;
      end;

      Syn.Next;
    end;
  finally
    Syn.Free;
  end;
end;

procedure TFConfigurationMode.actCloneDiagnosticQueryExecute(Sender: TObject);
var
  Category, SmallDescription, LongDescription, SQLCount, SQLDetail, SQLFix,
  SentryTag, MinDBVerRequired: string;

  PassFailState, LastFailCount, FailScore, RowVersionNumber: integer;
  DTLastCheck, DTLastFix: TDateTime;
  LogRemotely, Status, SQLDetailStatus, SQLFixStatus: boolean;

  NullPassFailState, NullLastFailCount, NullFailScore, NullRowVersionNumber,
  NullDTLastCheck, NullDTLastFix, NullLogRemotely,
  NullStatus, NullSQLDetailStatus, NullSQLFixStatus: boolean;
begin
  FCloneDiagnosticQueryDlg.edSmallDescription.Text := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Small_Description').AsString;
  if FCloneDiagnosticQueryDlg.ShowModal = mrOK then
  begin
    Category             := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Category').AsString;
    SmallDescription     := FCloneDiagnosticQueryDlg.edNewSmallDescription.Text;
    LongDescription      := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Long_Description').AsString;
    SQLCount             := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Count').AsString;
    SQLDetail            := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail').AsString;
    SQLFix               := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix').AsString;
    SentryTag            := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Sentry_TAG').AsString;
    MinDBVerRequired     := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Min_DB_Ver_Required').AsString;

    PassFailState        := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Pass_Fail_State').AsInteger;
    DTLastCheck          := DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Check').AsDateTime;
    DTLastFix            := DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Fix').AsDateTime;
    LastFailCount        := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Last_Fail_Count').AsInteger;
    FailScore            := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Fail_Score').AsInteger;
    LogRemotely          := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Log_Remotely').AsBoolean;
    Status               := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Status').AsBoolean;
    SQLDetailStatus      := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail_Status').AsBoolean;
    SQLFixStatus         := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix_Status').AsBoolean;
    RowVersionNumber     := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Row_Version_Number').AsInteger;

    NullPassFailState    := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Pass_Fail_State').IsNull;
    NullDTLastCheck      := DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Check').IsNull;
    NullDTLastFix        := DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Fix').IsNull;
    NullLastFailCount    := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Last_Fail_Count').IsNull;
    NullFailScore        := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Fail_Score').IsNull;
    NullLogRemotely      := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Log_Remotely').IsNull;
    NullStatus           := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Status').IsNull;
    NullSQLDetailStatus  := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail_Status').IsNull;
    NullSQLFixStatus     := DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix_Status').IsNull;
    NullRowVersionNumber := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Row_Version_Number').IsNull;

    DMDatabaseFixer.tabDiagnosticQry.Append;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Category').AsString            := Category;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Small_Description').AsString   := SmallDescription;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Long_Description').AsString    := LongDescription;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Count').AsString           := SQLCount;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail').AsString          := SQLDetail;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix').AsString             := SQLFix;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Sentry_TAG').AsString          := SentryTag;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Min_DB_Ver_Required').AsString := MinDBVerRequired;

    if not NullPassFailState then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Pass_Fail_State').AsInteger    := PassFailState;

    if not NullDTLastCheck then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Check').AsDateTime     := DTLastCheck;

    if not NullDTLastFix then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Fix').AsDateTime       := DTLastFix;

    if not NullLastFailCount then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Last_Fail_Count').AsInteger    := LastFailCount;

    if not NullFailScore then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Fail_Score').AsInteger         := FailScore;

    if not NullLogRemotely then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Log_Remotely').AsBoolean       := LogRemotely;

    if not NullStatus then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Status').AsBoolean             := Status;

    if not NullSQLDetailStatus then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail_Status').AsBoolean  := SQLDetailStatus;

    if not NullSQLFixStatus then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix_Status').AsBoolean     := SQLFixStatus;

    if not NullRowVersionNumber then
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Row_Version_Number').AsInteger := RowVersionNumber;

    DMDatabaseFixer.tabDiagnosticQry.Post;
    MessageDlg ('Diagnostic query cloned successfully', mtInformation, [mbOK], 0);
  end;
end;

procedure TFConfigurationMode.actCollapseAllExecute(Sender: TObject);
begin
  SearchGridDBTableView1.ViewData.Collapse(True);
end;

procedure TFConfigurationMode.actCompareSelectAllExecute(Sender: TObject);
var
  bm: TBookmark;
begin
  if not FDMemCompare.Active or (FDMemCompare.RecordCount = 0) then exit;

  bm := FDMemCompare.GetBookmark;
  FDMemCompare.DisableControls;
  try
    FDMemCompare.First;
    while not FDMemCompare.Eof do
    begin
      FDMemCompare.Edit;
      FDMemCompare.FieldByName('Selected').AsBoolean := True;
      FDMemCompare.Post;

      FDMemCompare.Next;
    end;
  finally
    FDMemCompare.GotoBookmark(bm);
    FDMemCompare.FreeBookmark(bm);
    FDMemCompare.EnableControls;
  end;
end;

procedure TFConfigurationMode.actCompareUnselectAllExecute(Sender: TObject);
var
  bm: TBookmark;
begin
  if not FDMemCompare.Active or (FDMemCompare.RecordCount = 0) then exit;

  bm := FDMemCompare.GetBookmark;
  FDMemCompare.DisableControls;
  try
    FDMemCompare.First;
    while not FDMemCompare.Eof do
    begin
      FDMemCompare.Edit;
      FDMemCompare.FieldByName('Selected').AsBoolean := False;
      FDMemCompare.Post;

      FDMemCompare.Next;
    end;
  finally
    FDMemCompare.GotoBookmark(bm);
    FDMemCompare.FreeBookmark(bm);
    FDMemCompare.EnableControls;
  end;
end;

procedure TFConfigurationMode.actConnectToRemoteAndCompareExecute(Sender: TObject);
begin
  ReadREMOTEConnection(DMDatabaseFixer.FDRemoteConn, 'ADSRemoteConn', False);
  DMDatabaseFixer.tabRemoteDiagnosticQry.Open;
  DMDatabaseFixer.tabDiagnosticQry.Refresh;
  DMDatabaseFixer.tabRemoteDiagnosticQry.Refresh;
  CompareDiagnosticQry(DMDatabaseFixer.tabDiagnosticQry,
                       DMDatabaseFixer.tabRemoteDiagnosticQry,
                       FDMemCompare);
end;

procedure TFConfigurationMode.actDisconnectFromRemoteExecute(Sender: TObject);
begin
  DMDatabaseFixer.FDRemoteConn.Connected := False;
end;

procedure TFConfigurationMode.actExpandAllExecute(Sender: TObject);
begin
  SearchGridDBTableView1.ViewData.Expand(True);
end;

procedure TFConfigurationMode.actExportExecute(Sender: TObject);
var
  memTmp:   TFDMemTable;
  FileName: WideString;
begin
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.actExportExecute(Sender: TObject);');
  FileName := Format ('.\%s\%s.%s', [DmpDirectory, edExportFileName.Text, DmpExtension]);
  if FileExists (FileName) and (MessageDlg (Format ('Filename [%s] already exists. Do you want override it?', [FileName]), mtConfirmation, mbYesNoCancel, 0) <> mrYes) then
    Exit;

  memTmp := TFDMemTable.Create(Self);
  try
    try
      //Copy all diagnostic queries
      memTmp.CopyDataSet(memDiagnostic_Queries, [coStructure, coRestart, coAppend]);

      //Remove unselected rows
      memTmp.First;
      while not memTmp.Eof do
      begin
        if memTmp.FieldByName ('Selected').AsBoolean then
          memTmp.Next
        else
          memTmp.Delete;
      end;
    except
      on E: Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Export error. Class [%s] Message [%s]', [E.ClassName, E.Message]));
        MessageDlg ('An error has occurred during the export process', mtError, [mbOK], 0);
        exit;
      end;
    end;

    try
      memTmp.SaveToFile(FileName, sfBinary);
      MessageDlg ('Export successful!', mtInformation, [mbOk], 0);
    except
      on E: Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Export error. Class [%s] Message [%s]', [E.ClassName, E.Message]));
        MessageDlg ('An error has occurred during the export process', mtError, [mbOK], 0);
        exit;
      end;
    end;

  finally
    if Assigned (memTmp) then
      memTmp.Free;
  end;
end;

procedure TFConfigurationMode.actGenerateColumnsUnitExecute(Sender: TObject);
var
  Code: TStringList;
  Query: TFDQuery;
begin
  SaveUnitDlg.InitialDir := ExtractFilePath (ParamStr (0));
  if not SaveUnitDlg.Execute (Self.WindowHandle) then exit;

  Query := DMDatabaseFixer.NewQuery;
  Code := TStringList.Create;
  try
    Query.SQL.Add('Select T.Name                                                   As Table_Name,');
    Query.SQL.Add('       C.Field_Num,');
    Query.SQL.Add('       C.Name                                                   As Column_Name,');
    Query.SQL.Add('	   C.Name + '' '' +');
    Query.SQL.Add('	   Case C.Field_Type');
    Query.SQL.Add('	     When  1 Then ''Logical''');
    Query.SQL.Add('  		 When  2 Then ''Numeric''');
    Query.SQL.Add('  		 When  3 Then ''Date''');
    Query.SQL.Add('  		 When  4 Then ''Char''   ---''String''');
    Query.SQL.Add('  		 When  5 Then ''Memo''');
    Query.SQL.Add('  		 When  6 Then ''Binary''');
    Query.SQL.Add('  		 When  7 Then ''Image''');
    Query.SQL.Add('  		 When  8 Then ''Varchar''');
    Query.SQL.Add(' 		 When  9 Then ''Compactdate''');
    Query.SQL.Add('      When 10 Then ''Double''');
    Query.SQL.Add('  		 When 11 Then ''Integer''');
    Query.SQL.Add(' 		 When 12 Then ''ShortInt''');
    Query.SQL.Add('	  	 When 13 Then ''Time''');
    Query.SQL.Add(' 		 When 14 Then ''TimeStamp''');
    Query.SQL.Add(' 		 When 15 Then ''AutoInc''');
    Query.SQL.Add(' 		 When 16 Then ''Raw''');
    Query.SQL.Add(' 		 When 17 Then ''CurDoble''');
    Query.SQL.Add(' 		 When 18 Then ''Money''');
    Query.SQL.Add(' 		 When 19 Then ''LongInt''');
    Query.SQL.Add(' 		 When 20 Then ''CIString''');
    Query.SQL.Add(' 		 When 21 Then ''RowVersion''');
    Query.SQL.Add(' 		 When 22 Then ''ModTime''');
    Query.SQL.Add(' 		 When 23 Then ''VarCharFox''');
    Query.SQL.Add(' 		 When 24 Then ''VarBinaryFox''');
    Query.SQL.Add(' 		 When 26 Then ''NChar''');
    Query.SQL.Add(' 		 When 27 Then ''NVarChar''');
    Query.SQL.Add(' 		 When 28 Then ''NMemo''');
    Query.SQL.Add(' 		 When 29 Then ''GUID''');
    Query.SQL.Add('	   End +');
    Query.SQL.Add('	   Case');
    Query.SQL.Add('	     When Field_Type In (1, 3, 5, 6, 7, 11, 12, 13, 14, 18, 19, 21, 22, 28, 29) Then ''''');
    Query.SQL.Add('		 Else');
    Query.SQL.Add('  	       Case');
    Query.SQL.Add('             When Field_Length = 0 Then ''''');
    Query.SQL.Add('	          Else '' ('' + Trim (Str (Field_Length, 32, 0)) +');
    Query.SQL.Add('			        Case');
    Query.SQL.Add('			          When Field_Decimal = 0 Then ''''');
    Query.SQL.Add('				        Else '', '' + Trim (Str (Field_Decimal, 32, 0))');
    Query.SQL.Add('			        End + '')''');
    Query.SQL.Add('	         End');
    Query.SQL.Add('	   End                                                     As Column_Definition,');
    Query.SQL.Add('	   C.Field_Default_Value');
    Query.SQL.Add('  From System.Tables  T');
    Query.SQL.Add('  Join System.Columns C On T.Name = C.Parent');
    Query.SQL.Add(' Where T.Table_Relative_Path Is Not Null');
    Query.SQL.Add(' Order By T.Name, C.Field_Num');
    Query.Open;

    Code.Add('unit uColumnDefinition;');
    Code.Add('');
    Code.Add('interface');
    Code.Add('type');
    Code.Add('  MatTabCol = array of array of string;');
    Code.Add('');
    Code.Add('const');
    Code.Add('  ArrTabCol: MatTabCol = [');

    while not Query.Eof do
    begin
      Code.Add(Format ('                            [''%s'', ''%s'', ''%s %s'', ''%s''],',
                       [Query.FieldByName ('Table_Name').AsString,
                        Query.FieldByName ('Column_Name').AsString,
                        Query.FieldByName ('Column_Name').AsString,
                        Query.FieldByName ('Column_Definition').AsString,
                        Query.FieldByName ('Field_Default_Value').AsString
                       ]));

      Query.Next;
    end;

    Code [Code.Count - 1] := Copy (Code [Code.Count - 1], 1, Length (Code [Code.Count - 1]) - 1);
    Code.Add('                         ];');
    Code.Add('');
    Code.Add('implementation');
    Code.Add('');
    Code.Add('end.');
    Code.SaveToFile(SaveUnitDlg.FileName);

    MessageDlg ('Unit uColumnDefinition generated successfuly', mtInformation, [mbOk], 0);
  finally
    Code.Free;
    Query.Free;
  end;
end;

procedure TFConfigurationMode.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  actExpandAll.Enabled                  := RGViewMode.ItemIndex = 2;
  actCollapseAll.Enabled                := RGViewMode.ItemIndex = 2;
  actExport.Enabled                     := (RGViewMode.ItemIndex in [1, 2]) and (edExportFileName.Text <> '');
  btnCfgFileApply.Enabled               := CfgChanged;
  btnCfgFileCancel.Enabled              := CfgChanged;
  actConnectToRemoteAndCompare.Enabled  := not RefreshCompare and not DMDatabaseFixer.FDRemoteConn.Connected;
  actDisconnectFromRemote.Enabled       := not RefreshCompare and DMDatabaseFixer.FDRemoteConn.Connected;
  actRefreshCompare.Enabled             := not RefreshCompare                             and
                                           DMDatabaseFixer.FDConn.Connected               and
                                           DMDatabaseFixer.FDRemoteConn.Connected         and
                                           DMDatabaseFixer.tabDiagnosticQry.Active        and
                                           DMDatabaseFixer.tabRemoteDiagnosticQry.Active;
  actCompareSelectAll.Enabled           := actRefreshCompare.Enabled;
  actCompareUnselectAll.Enabled         := actRefreshCompare.Enabled;
  actPullAllFromRemote.Enabled          := actRefreshCompare.Enabled;
  actPushAllFromLocal.Enabled           := actRefreshCompare.Enabled;
  actPullSelectedFromRemote.Enabled     := actRefreshCompare.Enabled;
  actPushSelectedFromLocal.Enabled      := actRefreshCompare.Enabled;
end;

procedure TFConfigurationMode.actPullAllFromRemoteExecute(Sender: TObject);
begin
  SyncDQ(DMDatabaseFixer.tabDiagnosticQry, coCmpRemote, True);
  actRefreshCompare.Execute;
  MessageDlg ('Pull all successul', mtInformation, [mbOK], 0);
end;

procedure TFConfigurationMode.actPullSelectedFromRemoteExecute(Sender: TObject);
begin
  SyncDQ(DMDatabaseFixer.tabDiagnosticQry, coCmpRemote, False);
  actRefreshCompare.Execute;
  MessageDlg ('Pull selected successul', mtInformation, [mbOK], 0);
end;

procedure TFConfigurationMode.actPushAllFromLocalExecute(Sender: TObject);
begin
  SyncDQ(DMDatabaseFixer.tabRemoteDiagnosticQry, coCmpLocal, True);
  actRefreshCompare.Execute;
  MessageDlg ('Push all successul', mtInformation, [mbOK], 0);
end;

procedure TFConfigurationMode.actPushSelectedFromLocalExecute(Sender: TObject);
begin
  SyncDQ(DMDatabaseFixer.tabRemoteDiagnosticQry, coCmpLocal, False);
  actRefreshCompare.Execute;
  MessageDlg ('Push selected successul', mtInformation, [mbOK], 0);
end;

procedure TFConfigurationMode.actRefreshCompareExecute(Sender: TObject);
begin
  RefreshCompare := True;
  try
    DMDatabaseFixer.FDRemoteConn.Connected := False;
    ReadREMOTEConnection(DMDatabaseFixer.FDRemoteConn, 'ADSRemoteConn', False);
    DMDatabaseFixer.tabRemoteDiagnosticQry.Open;
    DMDatabaseFixer.tabDiagnosticQry.Refresh;
    DMDatabaseFixer.tabRemoteDiagnosticQry.Refresh;
    CompareDiagnosticQry(DMDatabaseFixer.tabDiagnosticQry,
                         DMDatabaseFixer.tabRemoteDiagnosticQry,
                         FDMemCompare);
  finally
    RefreshCompare := False;
  end;
end;

procedure TFConfigurationMode.actSelectAllExecute(Sender: TObject);
var
  bm: TBookmark;
  Category: string;
begin
  bm       := memDiagnostic_Queries.GetBookmark;
  Category := memDiagnostic_Queries.FieldByName ('Category').AsString;
  memDiagnostic_Queries.DisableControls;

  try
    memDiagnostic_Queries.First;
    while not memDiagnostic_Queries.Eof do
    begin
      if (RGViewMode.ItemIndex = 1) or
         ((RGViewMode.ItemIndex = 2) and (memDiagnostic_Queries.FieldByName ('Category').AsString = Category)) then
      begin
        memDiagnostic_Queries.Edit;
        memDiagnostic_Queries.FieldByName('Selected').AsBoolean := True;
        memDiagnostic_Queries.Post;
      end;

      memDiagnostic_Queries.Next;
    end;
  finally
    memDiagnostic_Queries.GotoBookmark(bm);
    memDiagnostic_Queries.FreeBookmark(bm);
    memDiagnostic_Queries.EnableControls;
  end;
end;

procedure TFConfigurationMode.actUnselectAllExecute(Sender: TObject);
var
  bm: TBookmark;
  Category: string;
begin
  bm       := memDiagnostic_Queries.GetBookmark;
  Category := memDiagnostic_Queries.FieldByName ('Category').AsString;
  memDiagnostic_Queries.DisableControls;

  try
    memDiagnostic_Queries.First;
    while not memDiagnostic_Queries.Eof do
    begin
      if (RGViewMode.ItemIndex = 1) or
         ((RGViewMode.ItemIndex = 2) and (memDiagnostic_Queries.FieldByName ('Category').AsString = Category)) then
      begin
        memDiagnostic_Queries.Edit;
        memDiagnostic_Queries.FieldByName('Selected').AsBoolean := False;
        memDiagnostic_Queries.Post;
      end;

      memDiagnostic_Queries.Next;
    end;
  finally
    memDiagnostic_Queries.GotoBookmark(bm);
    memDiagnostic_Queries.FreeBookmark(bm);
    memDiagnostic_Queries.EnableControls;
  end;
end;

procedure TFConfigurationMode.btnAfterSQLFixClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_Single_After_Fix');
end;

procedure TFConfigurationMode.btnBeforeSQLFixClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_Single_Before_Fix');
end;

procedure TFConfigurationMode.btnCfgFileApplyClick(Sender: TObject);
begin
  SaveGlobalSettings;
  CfgChanged := False;
  HasChanged := True;
end;

procedure TFConfigurationMode.btnCfgFileCancelClick(Sender: TObject);
begin
  CfgChanged := False;
  ReadGlobalSettings;
end;

procedure TFConfigurationMode.btnOKClick(Sender: TObject);
begin
  Close;
end;

procedure TFConfigurationMode.btnSQLAfterFixClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_After_Fix');
end;

procedure TFConfigurationMode.btnSQLBeforeFixClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_Before_Fix');
end;

procedure TFConfigurationMode.btnSQLCountClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_Count');
end;

procedure TFConfigurationMode.btnSQLDetailClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_Detail');
end;

procedure TFConfigurationMode.btnSQLFixClick(Sender: TObject);
begin
  OpenQueryBuilder ('SQL_Fix');
end;

procedure TFConfigurationMode.chkcbConsoleChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.chkcbLocalChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.chkcbRemoteChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.UpdateCategoryCombos;
var
  Qry: TFDQuery;
begin
  if not Showing then exit;

  dbcbCategory.Properties.BeginUpdate;
  dbcbSubcategory.Properties.BeginUpdate;
  Qry := DMDatabaseFixer.NewQuery(DMDatabaseFixer.tabDiagnosticQry.Connection.ConnectionName);
  try
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('Select Distinct Category');
    Qry.SQL.Add('  From Diagnostic_Queries');
    Qry.Open;

    dbcbCategory.Properties.Items.Clear;
    while not Qry.Eof do
    begin
      dbcbCategory.Properties.Items.Add(Qry.FieldByName('Category').AsString);
      Qry.Next;
    end;

    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add('Select Distinct Subcategory');
    Qry.SQL.Add('  From Diagnostic_Queries');
    Qry.SQL.Add(' Where Upper (Category) = Upper (:Category)');
    Qry.SQL.Add('   And Subcategory Is Not Null');
    Qry.ParamByName('Category').AsString := DMDatabaseFixer.tabDiagnosticQry.FieldByName('Category').AsString;
    Qry.Open;

    dbcbSubcategory.Properties.Items.Clear;
    while not Qry.Eof do
    begin
      dbcbSubcategory.Properties.Items.Add(Qry.FieldByName('Subcategory').AsString);
      Qry.Next;
    end;
  finally
    dbcbCategory.Properties.EndUpdate;
    dbcbSubcategory.Properties.EndUpdate;
    Qry.Free;
  end;
end;

procedure TFConfigurationMode.DSDiagnosticQueriesDataChange(Sender: TObject;
  Field: TField);
begin
  UpdateCategoryCombos;
end;

procedure TFConfigurationMode.DSDiagnosticQueriesStateChange(Sender: TObject);
var
  CanEditSQL, CanFixEdit: boolean;
begin
  CanEditSQL                      := Assigned (DSDiagnosticQueries.DataSet) and (DSDiagnosticQueries.State in [dsInsert, dsEdit, dsBrowse]);
  CanFixEdit                      := Assigned (DSDiagnosticQueries.DataSet) and (DSDiagnosticQueries.State = dsBrowse);
  btnSQLCount.Enabled             := CanEditSQL;
  btnSQLDetail.Enabled            := CanEditSQL;
  btnOK.Enabled                   := Assigned (DSDiagnosticQueries.DataSet) and (DSDiagnosticQueries.State = dsBrowse);
  actCloneDiagnosticQuery.Enabled := DMDatabaseFixer.tabDiagnosticQry.Active             and
                                     (DMDatabaseFixer.tabDiagnosticQry.State = dsBrowse) and
                                     (DMDatabaseFixer.tabDiagnosticQry.RecordCount > 0);

  DMDatabaseFixer.tabDiagFix.UpdateOptions.ReadOnly := not CanFixEdit;

  GridFixDBTableView1.OptionsData.Appending := CanFixEdit;
  GridFixDBTableView1.OptionsData.Inserting := CanFixEdit;
  GridFixDBTableView1.OptionsData.Editing   := CanFixEdit;
  GridFixDBTableView1.OptionsData.Deleting  := CanFixEdit;
end;

procedure TFConfigurationMode.DSSQLFixStateChange(Sender: TObject);
begin
  btnSQLFix.Enabled := Assigned (DSSQLFix.DataSet) and (DSSQLFix.State in [dsInsert, dsEdit, dsBrowse]);
end;

procedure TFConfigurationMode.edPOSADSAliasNameChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.edDSNChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.edSiteNameChange(Sender: TObject);
begin
  if CfgLoading then Exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.edTagChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.edTagScoreChange(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if DMDatabaseFixer.tabDiagnosticQry.Active then
  begin
    DMDatabaseFixer.tabDiagnosticQry.Close;
    DMDatabaseFixer.tabDiagFix.Close;
    DMDatabaseFixer.tabDiagnosticQry.Open;
    DMDatabaseFixer.tabDiagFix.Open;
    DMDatabaseFixer.tabDiagParams.Close;
    DMDatabaseFixer.tabDiagParams.Open;
  end;
end;

procedure TFConfigurationMode.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if Assigned (DSDiagnosticQueries.DataSet) and (DSDiagnosticQueries.State in [dsEdit, dsInsert]) then
  begin
     if MessageDlg ('Closing the window without saving will cause changes to be lost. Are you sure to leave without saving the changes?',
                 mtConfirmation, mbYesNo, 0) = mrYes then
       DSDiagnosticQueries.DataSet.Cancel
     else
       CanClose := False;
  end;
end;

procedure TFConfigurationMode.FormShow(Sender: TObject);
begin
  HasChanged              := False;
  CfgChanged              := False;
  CfgLoading              := False;
  RefreshCompare          := False;
  PageControl1.TabIndex   := 0;
  PageControlSQL.TabIndex := 0;
  PageFixControl.TabIndex := 0;
  ReadGlobalSettings;
  PopulateCompletionProposals;
  UpdateCategoryCombos;
end;

procedure TFConfigurationMode.ReadGlobalSettings;

  procedure LoadVerboseLevel (vl: TSetVerboseLevel; cb: TJvCheckedComboBox);
  var
    l: TVerboseLevel;
    s: string;
  begin
    cb.Items.Clear;

    for l := Low (TVerboseLevel) to High (TVerboseLevel) do
    begin
      s := GetEnumName (TypeInfo (TVerboseLevel), Integer (l));
      s := Copy (s, 3, Length (s));

      cb.Items.Add(s);
    end;

    for l := Low (TVerboseLevel) to High (TVerboseLevel) do
      cb.Checked [Ord (l)] := l in vl;
  end;

begin
  CfgLoading                := True;
  edPOSADSAliasName.Text    := DMDatabaseFixer.gPOSADSAliasName;
  edDIAGADSAliasName.Text   := DMDatabaseFixer.gDIAGADSAliasName;
  edREMOTEADSAliasName.Text := DMDatabaseFixer.gREMOTEADSAliasName;
  edSiteName.Text           := DMDatabaseFixer.gSiteName;
  edTag.Text                := DMDatabaseFixer.gSentryTags;
  edTagScore.Text           := DMDatabaseFixer.gSentryTagScore;
  edDSN.Text                := DMDatabaseFixer.gSentryDSN;

  rgPOSServerType.ItemIndex := -1;
  if DMDatabaseFixer.gPOSServerType = 'stRemote' then
    rgPOSServerType.ItemIndex := 0
  else if DMDatabaseFixer.gPOSServerType = 'stLocal' then
    rgPOSServerType.ItemIndex := 1;

  rgDIAGServerType.ItemIndex := -1;
  if DMDatabaseFixer.gDIAGServerType = 'stRemote' then
    rgDIAGServerType.ItemIndex := 0
  else if DMDatabaseFixer.gDIAGServerType = 'stLocal' then
    rgDIAGServerType.ItemIndex := 1;

  rgREMOTEServerType.ItemIndex := -1;
  if DMDatabaseFixer.gREMOTEServerType = 'stRemote' then
    rgREMOTEServerType.ItemIndex := 0
  else if DMDatabaseFixer.gREMOTEServerType = 'stLocal' then
    rgREMOTEServerType.ItemIndex := 1;

  LoadVerboseLevel (DMDatabaseFixer.vlConsole, chkcbConsole);
  LoadVerboseLevel (DMDatabaseFixer.vlLocal,   chkcbLocal);
  LoadVerboseLevel (DMDatabaseFixer.vlSentry,  chkcbRemote);
  CfgLoading := False;
end;

procedure TFConfigurationMode.rgCompareFilterClick(Sender: TObject);
begin
  GridCompareDBTableView1.DataController.Filter.Clear;
  case rgCompareFilter.ItemIndex of
    1: begin
         //Changes on Local
         GridCompareDBTableView1.DataController.Filter.AddItem(nil, GridCompareDBTableView1Location, foEqual, coCmpLocal, coCmpLocal);
         GridCompareDBTableView1.DataController.Filter.Active := True;
    end;
    2: begin
         //Changes on Remote
         GridCompareDBTableView1.DataController.Filter.AddItem(nil, GridCompareDBTableView1Location, foEqual, coCmpRemote, coCmpRemote);
         GridCompareDBTableView1.DataController.Filter.Active := True;
    end;
  end;

end;

procedure TFConfigurationMode.rgPOSServerTypeClick(Sender: TObject);
begin
  if CfgLoading then exit;

  CfgChanged := True;
end;

procedure TFConfigurationMode.SaveGlobalSettings;
var
  Ini:        TIniFile;
  IniFileName: string;
  strConsole:  string;
  strRemote:   string;
  strLocal:    string;

  function VerboseLevelToString (vl: TSetVerboseLevel): string;
  var
    l: TVerboseLevel;
    s: string;
  begin
    result := '[';

    for l := Low (TVerboseLevel) to High (TVerboseLevel) do
      if l in vl then
      begin
        s := GetEnumName (TypeInfo (TVerboseLevel), Integer (l));
        s := Copy (s, 3, Length (s));

        result := result + s + ',';
      end;

    if Length (result) > 1 then
      result := Copy (result, 1, Length (result) - 1);

    result := result + ']';
  end;

  function GetVerboseLevel (cb: TJvCheckedComboBox): TSetVerboseLevel;
  var
    l: TVerboseLevel;
  begin
    result := [];

    for l := Low (TVerboseLevel) to High (TVerboseLevel) do
      if cb.Checked [Ord (l)] then
        result := result + [l];
  end;

begin
  //Reload global variables
  DMDatabaseFixer.gPOSADSAliasName    := edPOSADSAliasName.Text;
  DMDatabaseFixer.gDIAGADSAliasName   := edDIAGADSAliasName.Text;
  DMDatabaseFixer.gREMOTEADSAliasName := edREMOTEADSAliasName.Text;
  DMDatabaseFixer.gSiteName           := edSiteName.Text;
  DMDatabaseFixer.gSentryDSN          := edDSN.Text;
  DMDatabaseFixer.gSentryTags         := edTag.Text;
  DMDatabaseFixer.gSentryTagScore     := edTagScore.Text;

  case rgPOSServerType.ItemIndex of
    0: DMDatabaseFixer.gPOSServerType := 'stRemote';
    1: DMDatabaseFixer.gPOSServerType := 'stLocal';
  end;

  case rgDIAGServerType.ItemIndex of
    0: DMDatabaseFixer.gDIAGServerType := 'stRemote';
    1: DMDatabaseFixer.gDIAGServerType := 'stLocal';
  end;

  case rgREMOTEServerType.ItemIndex of
    0: DMDatabaseFixer.gREMOTEServerType := 'stRemote';
    1: DMDatabaseFixer.gREMOTEServerType := 'stLocal';
  end;

  DMDatabaseFixer.vlConsole := GetVerboseLevel (chkcbConsole);
  DMDatabaseFixer.vlSentry  := GetVerboseLevel (chkcbRemote);
  DMDatabaseFixer.vlLocal   := GetVerboseLevel (chkcbLocal);
  strConsole                := VerboseLevelToString (DMDatabaseFixer.vlConsole);
  strLocal                  := VerboseLevelToString (DMDatabaseFixer.vlLocal);
  strRemote                 := VerboseLevelToString (DMDatabaseFixer.vlSentry);

  IniFileName := Copy (ParamStr (0), 1, Length (ParamStr (0)) - 3) + 'cfg';
  Ini         := TIniFile.Create(IniFileName);

  try
    //Update config file
    Ini.WriteString('Global Settings', 'POS_ADSAliasName',    DMDatabaseFixer.gPOSADSAliasName);
    Ini.WriteString('Global Settings', 'DIAG_ADSAliasName',   DMDatabaseFixer.gDIAGADSAliasName);
    Ini.WriteString('Global Settings', 'REMOTE_ADSAliasName', DMDatabaseFixer.gREMOTEADSAliasName);
    Ini.WriteString('Global Settings', 'SiteName',            DMDatabaseFixer.gSiteName);
    Ini.WriteString('Global Settings', 'SentryDSN',           DMDatabaseFixer.gSentryDSN);
    Ini.WriteString('Global Settings', 'SentryTags',          DMDatabaseFixer.gSentryTags);
    Ini.WriteString('Global Settings', 'POS_ServerType',      DMDatabaseFixer.gPOSServerType);
    Ini.WriteString('Global Settings', 'DIAG_ServerType',     DMDatabaseFixer.gDIAGServerType);
    Ini.WriteString('Global Settings', 'REMOTE_ServerType',   DMDatabaseFixer.gREMOTEServerType);
    Ini.WriteString('Global Settings', 'SentryTagScore',      DMDatabaseFixer.gSentryTagScore);

    Ini.WriteString('Verbose Level', 'Console',  strConsole);
    Ini.WriteString('Verbose Level', 'Local',    strLocal);
    Ini.WriteString('Verbose Level', 'Remote',   strRemote);
  finally
    Ini.Free;
  end;

  //Update sentry and verbose levels
  DMDatabaseFixer.LogSentry.DSN := DMDatabaseFixer.gSentryDSN;
  DMDatabaseFixer.vlConsole     := GetVerboseLevel (chkcbConsole);
  DMDatabaseFixer.vlSentry      := GetVerboseLevel (chkcbRemote);
  DMDatabaseFixer.vlLocal       := GetVerboseLevel (chkcbLocal);

  ReadPOSConnection   (DMDatabaseFixer.FDConn,       'DiagnosticADS');
  ReadDIAGConnection  (FMainDoctor.ThreadConn,       'ThreadConn');
  ReadREMOTEConnection(DMDatabaseFixer.FDRemoteConn, 'ADSRemoteConn');
  DMDatabaseFixer.tabDiagnosticQry.Open;
end;

procedure TFConfigurationMode.SearchGridDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  PageControl1.ActivePage := tabSingleView;
end;

function TFConfigurationMode.ValidateConnection (Conn: TFDConnection): boolean;
var
  Query: TFDQuery;
begin
  result                         := False;
  Query                          := DMDatabaseFixer.NewQuery(Conn.ConnectionName);
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('function TFConfigurationMode.ValidateConnection (Conn: TFDConnection): boolean;');
  try
    try
      Query.SQL.Add('Select Count (*) Total From System.Tables Where Name = ''X''');
      Query.Open;
      result := True;
    except
      on E: System.SysUtils.Exception do
        DMDatabaseFixer.Log_Exception(Format ('ConnectionName: [%s] don''t set correctly', [Conn.ConnectionName]) +
                                      sLineBreak + Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;
  finally
    Query.Free;
  end;
end;

procedure TFConfigurationMode.ReadPOSConnection(Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);
var
  Alias, SiteName, ServerType: string;
begin
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.ReadPOSConnection(Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);');

  if UseGlobalVars then
  begin
    Alias      := DMDatabaseFixer.gPOSADSAliasName;
    SiteName   := DMDatabaseFixer.gSiteName;
    ServerType := DMDatabaseFixer.gPOSServerType;
  end
  else
  begin
    Alias    := edPOSADSAliasName.Text;
    SiteName := edSiteName.Text;

    case rgPOSServerType.ItemIndex of
      0: ServerType := 'stRemote';
      1: ServerType := 'stLocal';
    end;
  end;

  Conn.Close;
  Conn.Params.Clear;
  Conn.Params.Add('DriverID=ADS');

  if Alias <> '' then
    Conn.Params.Add('Alias=' + Alias);

  if SiteName <> '' then
    Conn.Params.Add('Server=' + SiteName);

  if ServerType <> '' then
  begin
    if SameText(ServerType, 'stLocal') then
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stLocal
    else
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stRemote;
  end;

  Conn.ConnectionName  := ConnectionName;
  Conn.Params.UserName := UserName;
  Conn.Params.Password := Password;
  if Alias <> '' then
  begin
    Conn.Open;
    Conn.Connected := ValidateConnection(Conn);
  end;
end;

procedure TFConfigurationMode.ReadDIAGConnection(Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);
var
  Alias, SiteName, ServerType: string;
begin
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.ReadDIAGConnection(Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);');

  if UseGlobalVars then
  begin
    Alias      := DMDatabaseFixer.gDIAGADSAliasName;
    SiteName   := DMDatabaseFixer.gSiteName;
    ServerType := DMDatabaseFixer.gDIAGServerType;
  end
  else
  begin
    Alias    := edDIAGADSAliasName.Text;
    SiteName := edSiteName.Text;

    case rgDIAGServerType.ItemIndex of
      0: ServerType := 'stRemote';
      1: ServerType := 'stLocal';
    end;
  end;

  Conn.Close;
  Conn.Params.Clear;
  Conn.Params.Add('DriverID=ADS');

  if Alias <> '' then
    Conn.Params.Add('Alias=' + Alias);

  if SiteName <> '' then
    Conn.Params.Add('Server=' + SiteName);

  if ServerType <> '' then
  begin
    if SameText(ServerType, 'stLocal') then
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stLocal
    else
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stRemote;
  end;

  Conn.ConnectionName  := ConnectionName;
  Conn.Params.UserName := UserName;
  Conn.Params.Password := Password;
  if Alias <> '' then
  begin
    Conn.Open;
    Conn.Connected := ValidateConnection(Conn);
  end;
end;

procedure TFConfigurationMode.ReadREMOTEConnection (Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);
var
  Alias, SiteName, ServerType: string;
begin
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.ReadREMOTEConnection (Conn: TFDConnection; ConnectionName: string; UseGlobalVars: boolean = True);');

  if UseGlobalVars then
  begin
    Alias      := DMDatabaseFixer.gREMOTEADSAliasName;
    SiteName   := DMDatabaseFixer.gSiteName;
    ServerType := DMDatabaseFixer.gREMOTEServerType;
  end
  else
  begin
    Alias    := edREMOTEADSAliasName.Text;
    SiteName := edSiteName.Text;

    case rgREMOTEServerType.ItemIndex of
      0: ServerType := 'stRemote';
      1: ServerType := 'stLocal';
    end;
  end;

  Conn.Close;
  Conn.Params.Clear;
  Conn.Params.Add('DriverID=ADS');

  if Alias <> '' then
    Conn.Params.Add('Alias=' + Alias);

  if SiteName <> '' then
    Conn.Params.Add('Server=' + SiteName);

  if ServerType <> '' then
  begin
    if SameText(ServerType, 'stLocal') then
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stLocal
    else
      TFDPhysADSConnectionDefParams(Conn.Params).ServerTypes := stRemote;
  end;

  Conn.ConnectionName  := ConnectionName;
  Conn.Params.UserName := UserName;
  Conn.Params.Password := Password;
  if Alias <> '' then
  begin
    Conn.Open;
    Conn.Connected := ValidateConnection(Conn);
  end;
end;

procedure TFConfigurationMode.CompareDiagnosticQry (Local, Remote: TFDTable; Result: TFDMemTable);
var
  bml, bmr: TBookmark;
  i: integer;
  Found: boolean;
  FieldName: string;
  TmpTable: TFDTable;
begin
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.CompareDiagnosticQry (Local, Remote: TFDTable; Result: TFDMemTable);');

  try
    TmpTable            := TFDTable.Create(Self);
    TmpTable.Connection := DMDatabaseFixer.FDConn;
    TmpTable.TableName  := 'Diagnostic_Queries';
    TmpTable.Active     := True;
    Result.Close;
    Result.Fields.Clear;
    Result.FieldDefs.Clear;
    Result.FieldDefs.Add('Selected', ftBoolean);
    Result.FieldDefs.Add('Location', ftString, 10);
    Result.FieldDefs.Add('Row_Type', ftString, 30);
    for i := 0 to TmpTable.FieldDefs.Count - 1 do
      Result.FieldDefs.Add(TmpTable.FieldDefs.Items [i].Name,
                           TmpTable.FieldDefs.Items [i].DataType,
                           TmpTable.FieldDefs.Items [i].Size);
    Result.Open;

    bml := nil;
    bmr := nil;

    Local.Refresh;
    Remote.Refresh;

    if Local.RecordCount > 0 then
      bml := Local.GetBookmark;

    if Remote.RecordCount > 0 then
      bmr := Remote.GetBookmark;

    Local.DisableControls;
    Remote.DisableControls;
    try
      //Find for missing rows on remote, or rows with differences
      Local.First;
      while not Local.Eof do
      begin
        if Remote.FindKey ([Local.FieldByName ('Query_ID').AsString]) then
        begin
          //Check for differences
          Found := False;
          for i := 0 to TmpTable.Fields.Count - 1 do
          begin
            FieldName := TmpTable.Fields [i].FieldName;
            if //(Remote.FieldDefs.IndexOf(FieldName) <> -1) and
               (Local.FieldByName (FieldName).AsString <> Remote.FieldByName (FieldName).AsString) then
            begin
              Found := True;
              break;
            end;
          end;

          if Found then
          begin
            Result.Insert;
            Result.FieldByName('Location').AsString := coCmpLocal;
            Result.FieldByName('Row_Type').AsString := coCmpDifferent;
            for i := 0 to TmpTable.FieldDefs.Count - 1 do
            begin
              FieldName                              := TmpTable.FieldDefs.Items [i].Name;
              Result.FieldByName(FieldName).AsString := Local.FieldByName(FieldName).AsString;
            end;
            Result.Post;

            Result.Insert;
            Result.FieldByName('Location').AsString := coCmpRemote;
            Result.FieldByName('Row_Type').AsString := coCmpDifferent;
            for i := 0 to TmpTable.FieldDefs.Count - 1 do
            begin
              FieldName                              := TmpTable.FieldDefs.Items [i].Name;
              Result.FieldByName(FieldName).AsString := Remote.FieldByName(FieldName).AsString;
            end;
            Result.Post;
          end;
        end
        else
        begin
          //Insert row as missing on remote
          Result.Insert;
          Result.FieldByName('Location').AsString := coCmpLocal;
          Result.FieldByName('Row_Type').AsString := coCmpMissingOnRemote;
          for i := 0 to TmpTable.FieldDefs.Count - 1 do
          begin
            FieldName                              := TmpTable.FieldDefs.Items [i].Name;
            Result.FieldByName(FieldName).AsString := Local.FieldByName(FieldName).AsString;
          end;
          Result.Post;
        end;

        Local.Next;
      end;

      //Find for missing rows on local
      Remote.First;
      while not Remote.Eof do
      begin
        if not Local.FindKey ([Remote.FieldByName('Query_ID').AsString]) then
        begin
          //Insert row as missing on local
          Result.Insert;
          Result.FieldByName('Location').AsString := coCmpRemote;
          Result.FieldByName('Row_Type').AsString := coCmpMissingOnLocal;
          for i := 0 to TmpTable.FieldDefs.Count - 1 do
          begin
            FieldName                              := TmpTable.FieldDefs.Items [i].Name;
            Result.FieldByName(FieldName).AsString := Remote.FieldByName(FieldName).AsString;
          end;
          Result.Post;
        end;

        Remote.Next;
      end;
    finally
      if Assigned (bml) then
      begin
        Local.GotoBookmark(bml);
        Local.FreeBookmark(bml);
      end;

      if Assigned (bmr) then
      begin
        Remote.GotoBookmark(bmr);
        Remote.FreeBookmark(bmr);
      end;

      Local.EnableControls;
      Remote.EnableControls;

      TmpTable.Close;
      TmpTable.Free;
    end;
  except
    on E: System.SysUtils.Exception do
      DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]));
  end;
end;

procedure TFConfigurationMode.cxGrid1DBTableView1ValueGetProperties(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  if      ARecord.Values [1] = 'String' then
    AProperties := EditRepText.Properties
  else if ARecord.Values [1] = 'Integer' then
    AProperties := EditRepSpin.Properties
  else if ARecord.Values [1] = 'Date' then
    AProperties := EditRepDate.Properties
  else if ARecord.Values [1] = 'Float' then
    AProperties := EditRepCalc.Properties
end;

procedure TFConfigurationMode.SyncDQ (Target: TFDTable; SourceLocation: string; AllRows: boolean; CheckRowVersion: boolean = False);
var
  bmT, bmMem: TBookmark;
  i: integer;
  FieldName: string;
  Discard: boolean;
begin
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.SyncDQ (Target: TFDTable; SourceLocation: string; AllRows: boolean; CheckRowVersion: boolean = False);');

  if not Target.Active       or (Target.RecordCount       = 0) or
     not FDMemCompare.Active or (FDMemCompare.RecordCount = 0) then exit;

  DMDatabaseFixer.Importing := True;
  bmT                       := Target.GetBookmark;
  bmMem                     := FDMemCompare.GetBookmark;
  Target.DisableControls;
  FDMemCompare.DisableControls;
  try
    FDMemCompare.First;
    while not FDMemCompare.Eof do
    begin
      if (FDMemCompare.FieldByName ('Location').AsString = SourceLocation) and
         (AllRows or FDMemCompare.FieldByName ('Selected').AsBoolean) then
      begin
        Discard := False;

        try
          if FDMemCompare.FieldByName ('Row_Type').AsString = coCmpDifferent then
          begin
            Target.FindKey([FDMemCompare.FieldByName ('Query_ID').AsString]);
            if CheckRowVersion and (SourceLocation = coCmpRemote) and
              (Target.FieldByName('Row_Version_Number').AsInteger > FDMemCompare.FieldByName ('Row_Version_Number').AsInteger) then
              Discard := True
            else
              Target.Edit;
          end
          else
            Target.Append;

          if not Discard then
          begin
            for i := 0 to Target.FieldDefs.Count - 1 do
            begin
              FieldName := Target.FieldDefs.Items [i].Name;
              Target.FieldByName(FieldName).AsString := FDMemCompare.FieldByName(FieldName).AsString;
            end;
            Target.Post;
          end;
        except
          on E: System.SysUtils.Exception do
            DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]));
        end;
      end;

      FDMemCompare.Next;
    end;
  finally
    DMDatabaseFixer.Importing := False;

    Target.GotoBookmark(bmT);
    Target.FreeBookmark(bmT);

    FDMemCompare.GotoBookmark(bmMem);
    FDMemCompare.FreeBookmark(bmMem);

    Target.EnableControls;
    FDMemCompare.EnableControls;
  end;
end;

procedure TFConfigurationMode.PullAllCommandLine;
begin
  DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
  DMDatabaseFixer.Log_Trace('procedure TFConfigurationMode.PullAllCommandLine;');

  ReadREMOTEConnection(DMDatabaseFixer.FDRemoteConn, 'ADSRemoteConn', True);
  try
    DMDatabaseFixer.tabRemoteDiagnosticQry.Open;
  except
    on E: System.SysUtils.Exception do
    begin
      DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) + sLineBreak +
                                    Format ('Error openning table %s on %s connection',
                                            [DMDatabaseFixer.tabRemoteDiagnosticQry.TableName,
                                             DMDatabaseFixer.FDRemoteConn.ConnectionName]));
      MessageDlg (Format ('Error openning table %s on %s connection',
                          [DMDatabaseFixer.tabRemoteDiagnosticQry.TableName,
                           DMDatabaseFixer.FDRemoteConn.ConnectionName]), mtError, [mbOK], 0);
      Exit;
    end;
  end;
  CompareDiagnosticQry(DMDatabaseFixer.tabDiagnosticQry,
                       DMDatabaseFixer.tabRemoteDiagnosticQry,
                       FDMemCompare);
  SyncDQ(DMDatabaseFixer.tabDiagnosticQry, coCmpRemote, True, True);
  FDMemCompare.Close;
  DMDatabaseFixer.FDRemoteConn.Close;
end;

procedure TFConfigurationMode.PopulateCompletionProposals;
var
  L: TStringList;
  bm: TBookmark;
begin
  propSQLCount.ItemList.Clear;
  propSQLDetail.ItemList.Clear;
  propSQLFix.ItemList.Clear;
  propSQLBeforeFix.ItemList.Clear;
  propSQLAfterFix.ItemList.Clear;
  propBeforeSQLFix.ItemList.Clear;
  propAfterSQLFix.ItemList.Clear;
  if not DMDatabaseFixer.tabDiagFix.Active or (DMDatabaseFixer.tabDiagParams.RecordCount = 0) then exit;

  L := TStringList.Create;
  try
    bm := DMDatabaseFixer.tabDiagParams.GetBookmark;
    DMDatabaseFixer.tabDiagParams.First;
    while not DMDatabaseFixer.tabDiagParams.Eof do
    begin
      L.Add(':' + DMDatabaseFixer.tabDiagParams.FieldByName('Param').AsString);

      DMDatabaseFixer.tabDiagParams.Next;
    end;
  finally
    DMDatabaseFixer.tabDiagParams.GotoBookmark(bm);
    DMDatabaseFixer.tabDiagParams.FreeBookmark(bm);

    propSQLCount.ItemList.Assign(L);
    propSQLDetail.ItemList.Assign(L);
    propSQLFix.ItemList.Assign(L);
    propSQLBeforeFix.ItemList.Assign(L);
    propSQLAfterFix.ItemList.Assign(L);
    propBeforeSQLFix.ItemList.Assign(L);
    propAfterSQLFix.ItemList.Assign(L);
    L.Free;
  end;
end;

end.
