unit uSentry;

interface

uses
  System.SysUtils,
  System.Classes,
  System.RegularExpressions,
  System.Net.HTTPClient,
  System.JSON,
  TMSLoggingCore, TMSLoggingUtils, System.SyncObjs;

type
  TSentryOutputHandler = class(TTMSLoggerBaseOutputHandler)
  private
    FDSN: string;
    FProtocol: string;
    FPublicKey: string;
    FHost: string;
    FProjectID: string;
    FHTTPClient: THTTPClient;
    FRelease: string;
    FPlatform: string;
    FTags: string;
    procedure SetDSN(const Value: string);

  protected
    procedure LogOutput(const AOutputInformation: TTMSLoggerOutputInformation); override;
    property HTTPClient: THTTPClient read FHTTPClient;

  public
    property DSN: string read FDSN write SetDSN;
    property &Platform: string read FPlatform;
    property Release: string read FRelease;
    property Tags: string read FTags write FTags;

    constructor Create(); override;
    destructor Destroy; override;

  class var
    LastResponseCode: Integer;

    class destructor Destroy;
  end;

implementation

uses
  JvVersionInfo, System.IOUtils, System.Threading, Spring.Collections,
  System.StrUtils, System.Types;

{ TSentryOutputHandler }

procedure TSentryOutputHandler.SetDSN(const Value: string);
var
  LMatch: TMatch;
begin
  FDSN := Value;
// {PROTOCOL}://{PUBLIC_KEY}@{HOST}/{PROJECT_ID}
  LMatch := TRegEx.Match(Value, '(.*):\/\/(.*)@(.*)\/(.*)');
  if LMatch.Success then
  begin
    FProtocol := LMatch.Groups[1].Value;
    FPublicKey := LMatch.Groups[2].Value;
    FHost := LMatch.Groups[3].Value;
    FProjectID := LMatch.Groups[4].Value;
  end;

end;

constructor TSentryOutputHandler.Create;
var
  VI: TJvVersionInfo;
begin
  inherited;
  VI := TJvVersionInfo.Create(ParamStr(0));
  try
    FRelease := LongVersionToString(VI.FileLongVersion);
  finally
    VI.Free;
  end;

  FPlatform := TPath.GetFileName(ParamStr(0));
end;

destructor TSentryOutputHandler.Destroy;
begin
  inherited;
end;

procedure TSentryOutputHandler.LogOutput(const AOutputInformation: TTMSLoggerOutputInformation);
var
  JSON: TJSONObject;
  LL: string;
  LTags: TStringDynArray;
  I: Integer;
  LProtocol, LHost, LProjectID, LPublicKey : string;
begin
  inherited;
  if not SameText(FProtocol, 'https') then
    Exit;

  JSON := TJSONObject.Create;
  case AOutputInformation.LogLevel of
    Trace:
      LL := 'debug';
    Debug:
      LL := 'debug';
    Info:
      LL := 'info';
    Warning:
      LL := 'warning';
    Error:
      LL := 'error';
    Exception:
      LL := 'fatal';
    All:
      LL := 'info';
    Custom:
      LL := 'info';
  end;
  JSON.AddPair('level', LL);
  JSON.AddPair('message', Trim(AOutputInformation.ValueOutput));
  JSON.AddPair('release', Release);
  JSON.AddPair('logger', &Platform);
  JSON.AddPair('tags', TJSONArray.Create);
  LTags := SplitString(FTags, ',');
  I := 0;
  while (I + 1) < Length(LTags) do
  begin
    TJSONArray(JSON.Values['tags']).AddElement(TJSONArray.Create(LTags[I], LTags[I + 1]));
    I := I + 2;
  end;
  LProtocol := FProtocol;
  LHost := FHost;
  LProjectID := FProjectID;
  LPublicKey := FPublicKey;

  TTask.Run(
    procedure
    var
      URL: string;
      HTTPClient: THTTPClient;
      Response: IHTTPResponse;
      Stream: TStringStream;
    begin
      try
        URL := Format('%s://%s/api/%s/store/?sentry_key=%s&sentry_version=7',
            [LProtocol, LHost, LProjectID, LPublicKey]);
        HTTPClient := THTTPClient.Create;
        Stream := TStringStream.Create(JSON.ToString);
        try
          Response := HTTPClient.Post(URL, Stream);
          if Assigned(Response) then
            LastResponseCode := Response.StatusCode
          else
            LastResponseCode := 0;
        finally
          Stream.Free;
          HTTPClient.Free;
          JSON.Free;
        end
      except
      end;
    end);
end;

class destructor TSentryOutputHandler.Destroy;
begin
end;


initialization

RegisterClasses([TSentryOutputHandler]);

finalization


end.
