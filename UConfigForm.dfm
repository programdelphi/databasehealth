object FConfigurationMode: TFConfigurationMode
  Left = 0
  Top = 0
  Caption = 'Configuration Mode'
  ClientHeight = 581
  ClientWidth = 1029
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    1029
    581)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 1017
    Height = 565
    ActivePage = tabSingleView
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    OnChange = PageControl1Change
    object tabSingleView: TTabSheet
      Caption = 'Single View'
      DesignSize = (
        1009
        537)
      object Shape2: TShape
        Left = 0
        Top = 0
        Width = 1009
        Height = 537
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Color = clBtnFace
        ExplicitHeight = 577
      end
      object gbQueryIdentification: TGroupBox
        Left = 3
        Top = 3
        Width = 1003
        Height = 150
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Query Identification '
        TabOrder = 0
        DesignSize = (
          1003
          150)
        object Label4: TLabel
          Left = 19
          Top = 67
          Width = 79
          Height = 13
          Caption = 'Long Description'
        end
        object Label3: TLabel
          Left = 576
          Top = 24
          Width = 80
          Height = 13
          Caption = 'Small Description'
        end
        object Label2: TLabel
          Left = 19
          Top = 24
          Width = 45
          Height = 13
          Caption = 'Category'
        end
        object Label1: TLabel
          Left = 129
          Top = 16
          Width = 44
          Height = 13
          Caption = 'Query ID'
          Visible = False
        end
        object Label5: TLabel
          Left = 330
          Top = 24
          Width = 61
          Height = 13
          Caption = 'Subcategory'
        end
        object dbmLongDescription: TDBMemo
          Left = 19
          Top = 88
          Width = 969
          Height = 49
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Long_Description'
          DataSource = DSDiagnosticQueries
          TabOrder = 4
        end
        object dbeSmallDescription: TDBEdit
          Left = 576
          Top = 40
          Width = 412
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          DataField = 'Small_Description'
          DataSource = DSDiagnosticQueries
          TabOrder = 3
        end
        object dbeQuery_ID: TDBEdit
          Left = 179
          Top = 13
          Width = 121
          Height = 21
          DataField = 'Query_ID'
          DataSource = DSDiagnosticQueries
          ParentShowHint = False
          ReadOnly = True
          ShowHint = False
          TabOrder = 0
          Visible = False
        end
        object dbcbCategory: TcxDBComboBox
          Left = 16
          Top = 40
          DataBinding.DataField = 'Category'
          DataBinding.DataSource = DSDiagnosticQueries
          TabOrder = 1
          Width = 308
        end
        object dbcbSubcategory: TcxDBComboBox
          Left = 330
          Top = 40
          DataBinding.DataField = 'Subcategory'
          DataBinding.DataSource = DSDiagnosticQueries
          TabOrder = 2
          Width = 240
        end
      end
      object gbQueryBehavior: TGroupBox
        Left = 3
        Top = 159
        Width = 1003
        Height = 245
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = ' Behavior '
        TabOrder = 1
        DesignSize = (
          1003
          245)
        object Label8: TLabel
          Left = 24
          Top = 195
          Width = 99
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Row Version Number'
          ExplicitTop = 208
        end
        object Label9: TLabel
          Left = 150
          Top = 195
          Width = 103
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Database Min Version'
          ExplicitTop = 208
        end
        object Label10: TLabel
          Left = 278
          Top = 195
          Width = 46
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Fail Score'
          ExplicitTop = 208
        end
        object Label11: TLabel
          Left = 559
          Top = 192
          Width = 53
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Sentry Tag'
          ExplicitTop = 205
        end
        object dbeRowVersionNumber: TDBEdit
          Left = 24
          Top = 211
          Width = 121
          Height = 21
          Anchors = [akLeft, akBottom]
          DataField = 'Row_Version_Number'
          DataSource = DSDiagnosticQueries
          TabOrder = 0
        end
        object dbeDBMinVer: TDBEdit
          Left = 151
          Top = 211
          Width = 121
          Height = 21
          Anchors = [akLeft, akBottom]
          DataField = 'Min_DB_Ver_Required'
          DataSource = DSDiagnosticQueries
          TabOrder = 1
        end
        object dbeFailScore: TDBEdit
          Left = 278
          Top = 211
          Width = 121
          Height = 21
          Anchors = [akLeft, akBottom]
          DataField = 'Fail_Score'
          DataSource = DSDiagnosticQueries
          TabOrder = 2
        end
        object dbcbLogRemotely: TDBCheckBox
          Left = 472
          Top = 211
          Width = 81
          Height = 17
          Anchors = [akLeft, akBottom]
          Caption = 'Log Remotely'
          DataField = 'Log_Remotely'
          DataSource = DSDiagnosticQueries
          TabOrder = 4
          ValueChecked = 'T'
          ValueUnchecked = 'F'
        end
        object dbeSentryTag: TDBEdit
          Left = 559
          Top = 211
          Width = 429
          Height = 21
          Anchors = [akLeft, akRight, akBottom]
          DataField = 'Sentry_TAG'
          DataSource = DSDiagnosticQueries
          TabOrder = 5
        end
        object dbcbActive: TDBCheckBox
          Left = 405
          Top = 211
          Width = 49
          Height = 17
          Anchors = [akLeft, akBottom]
          Caption = 'Active'
          DataField = 'Status'
          DataSource = DSDiagnosticQueries
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object PageControlSQL: TPageControl
          Left = 19
          Top = 15
          Width = 969
          Height = 174
          ActivePage = tabSQLFix
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 6
          object tabSQLCount: TTabSheet
            Caption = 'SQL Count'
            object Shape5: TShape
              Left = 0
              Top = 0
              Width = 961
              Height = 146
              Align = alClient
              Brush.Color = clBtnFace
              Pen.Style = psClear
              ExplicitLeft = 48
              ExplicitTop = 32
              ExplicitWidth = 65
              ExplicitHeight = 65
            end
            object pnlSQLCount: TPanel
              Left = 0
              Top = 0
              Width = 961
              Height = 146
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel5: TPanel
                Left = 0
                Top = 0
                Width = 53
                Height = 146
                Align = alLeft
                TabOrder = 0
                object btnSQLCount: TBitBtn
                  Left = 8
                  Top = 8
                  Width = 35
                  Height = 25
                  Glyph.Data = {
                    C6070000424DC607000000000000360000002800000016000000160000000100
                    2000000000009007000000000000000000000000000000000000000000000000
                    00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                    78A3000000160000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                    0010000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                    FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                    2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                    A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                    000000000000000000000000000000000000000000249F9F9FEB949494E20000
                    004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                    66F0999999E60000001E00000000000000000000000000000000000000000000
                    0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                    D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                    94E5000000070000000000000000000000000000000000000000000000000000
                    0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                    A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                    000000000000000000000000000000000000000000000000000005050545CBCB
                    CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                    ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                    000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                    CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                    E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                    00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                    7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                    C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                    C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                    78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                    D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                    D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                    8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                    A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                    D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                    C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                    004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                    CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                    D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                    00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                    E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                    A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                    EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                    E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                    000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                    FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                    E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                    0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                    EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                    F6FF191919C30000002B00000000000000000000000000000000000000000000
                    00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                    FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                    62ED0000005A0000000100000000000000000000000000000000000000000000
                    000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                    000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                    00000000000000000000}
                  TabOrder = 0
                  TabStop = False
                  OnClick = btnSQLCountClick
                end
              end
              object Panel8: TPanel
                Left = 53
                Top = 0
                Width = 908
                Height = 146
                Align = alClient
                TabOrder = 1
                object dbsyneSQLCount: TDBSynEdit
                  Left = 1
                  Top = 1
                  Width = 906
                  Height = 144
                  DataField = 'SQL_Count'
                  DataSource = DSDiagnosticQueries
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 0
                  Gutter.Font.Charset = DEFAULT_CHARSET
                  Gutter.Font.Color = clWindowText
                  Gutter.Font.Height = -11
                  Gutter.Font.Name = 'Courier New'
                  Gutter.Font.Style = []
                  Gutter.ShowLineNumbers = True
                  Highlighter = SynSQL
                end
              end
            end
          end
          object tabSQLDetail: TTabSheet
            Caption = 'SQL Detail'
            ImageIndex = 1
            object Shape6: TShape
              Left = 0
              Top = 0
              Width = 961
              Height = 146
              Align = alClient
              Brush.Color = clBtnFace
              Pen.Style = psClear
              ExplicitLeft = 48
              ExplicitTop = 32
              ExplicitWidth = 65
              ExplicitHeight = 65
            end
            object pnlSQLDetail: TPanel
              Left = 0
              Top = 0
              Width = 961
              Height = 146
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 65
                Height = 146
                Align = alLeft
                TabOrder = 0
                object btnSQLDetail: TBitBtn
                  Left = 8
                  Top = 8
                  Width = 35
                  Height = 25
                  Glyph.Data = {
                    C6070000424DC607000000000000360000002800000016000000160000000100
                    2000000000009007000000000000000000000000000000000000000000000000
                    00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                    78A3000000160000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                    0010000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                    FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                    2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                    A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                    000000000000000000000000000000000000000000249F9F9FEB949494E20000
                    004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                    66F0999999E60000001E00000000000000000000000000000000000000000000
                    0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                    D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                    94E5000000070000000000000000000000000000000000000000000000000000
                    0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                    A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                    000000000000000000000000000000000000000000000000000005050545CBCB
                    CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                    ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                    000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                    CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                    E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                    00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                    7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                    C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                    C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                    78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                    D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                    D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                    8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                    A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                    D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                    C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                    004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                    CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                    D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                    00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                    E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                    A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                    EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                    E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                    000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                    FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                    E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                    0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                    EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                    F6FF191919C30000002B00000000000000000000000000000000000000000000
                    00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                    FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                    62ED0000005A0000000100000000000000000000000000000000000000000000
                    000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                    000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                    00000000000000000000}
                  TabOrder = 0
                  TabStop = False
                  OnClick = btnSQLDetailClick
                end
                object dbcbSQL_Detail_Active: TDBCheckBox
                  Left = 8
                  Top = 48
                  Width = 97
                  Height = 17
                  Caption = 'Active'
                  DataField = 'SQL_Detail_Status'
                  DataSource = DSDiagnosticQueries
                  TabOrder = 1
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
              end
              object Panel9: TPanel
                Left = 65
                Top = 0
                Width = 896
                Height = 146
                Align = alClient
                TabOrder = 1
                object dbsyneSQLDetail: TDBSynEdit
                  Left = 1
                  Top = 1
                  Width = 894
                  Height = 144
                  DataField = 'SQL_Detail'
                  DataSource = DSDiagnosticQueries
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 0
                  Gutter.Font.Charset = DEFAULT_CHARSET
                  Gutter.Font.Color = clWindowText
                  Gutter.Font.Height = -11
                  Gutter.Font.Name = 'Courier New'
                  Gutter.Font.Style = []
                  Gutter.ShowLineNumbers = True
                  Highlighter = SynSQL
                end
              end
            end
          end
          object tabSQLFix: TTabSheet
            Caption = 'SQL Fix'
            ImageIndex = 2
            object Shape7: TShape
              Left = 257
              Top = 0
              Width = 704
              Height = 146
              Align = alClient
              Brush.Color = clBtnFace
              Pen.Style = psClear
              ExplicitLeft = 48
              ExplicitTop = 32
              ExplicitWidth = 65
              ExplicitHeight = 65
            end
            object GridFix: TcxGrid
              Left = 0
              Top = 0
              Width = 257
              Height = 146
              Align = alLeft
              TabOrder = 0
              object GridFixDBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                Navigator.Buttons.PriorPage.Enabled = False
                Navigator.Buttons.PriorPage.Visible = False
                Navigator.Buttons.NextPage.Visible = False
                Navigator.Buttons.Append.Visible = False
                Navigator.Buttons.SaveBookmark.Visible = False
                Navigator.Buttons.GotoBookmark.Visible = False
                Navigator.Buttons.Filter.Visible = False
                Navigator.Visible = True
                DataController.DataSource = DSSQLFix
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsView.GroupByBox = False
                object GridFixDBTableView1SQL_Fix_Status: TcxGridDBColumn
                  Caption = 'Active'
                  DataBinding.FieldName = 'SQL_Fix_Status'
                  PropertiesClassName = 'TcxCheckBoxProperties'
                end
                object GridFixDBTableView1Fix_Description: TcxGridDBColumn
                  Caption = 'Description'
                  DataBinding.FieldName = 'Fix_Description'
                end
                object GridFixDBTableView1Fix_ID: TcxGridDBColumn
                  DataBinding.FieldName = 'Fix_ID'
                  PropertiesClassName = 'TcxTextEditProperties'
                  Properties.ReadOnly = True
                end
              end
              object GridFixLevel1: TcxGridLevel
                GridView = GridFixDBTableView1
              end
            end
            object PageFixControl: TPageControl
              Left = 257
              Top = 0
              Width = 704
              Height = 146
              ActivePage = pagSingleAfterFix
              Align = alClient
              TabOrder = 1
              object pagSingleBeforeFix: TTabSheet
                Caption = 'SQL Before Fix'
                object Shape8: TShape
                  Left = 0
                  Top = 0
                  Width = 64
                  Height = 118
                  Align = alClient
                  Brush.Color = clBtnFace
                  Pen.Style = psClear
                  ExplicitLeft = 48
                  ExplicitTop = 16
                  ExplicitWidth = 65
                  ExplicitHeight = 65
                end
                object btnBeforeSQLFix: TBitBtn
                  Left = 6
                  Top = 8
                  Width = 35
                  Height = 25
                  Glyph.Data = {
                    C6070000424DC607000000000000360000002800000016000000160000000100
                    2000000000009007000000000000000000000000000000000000000000000000
                    00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                    78A3000000160000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                    0010000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                    FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                    2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                    A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                    000000000000000000000000000000000000000000249F9F9FEB949494E20000
                    004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                    66F0999999E60000001E00000000000000000000000000000000000000000000
                    0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                    D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                    94E5000000070000000000000000000000000000000000000000000000000000
                    0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                    A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                    000000000000000000000000000000000000000000000000000005050545CBCB
                    CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                    ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                    000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                    CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                    E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                    00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                    7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                    C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                    C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                    78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                    D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                    D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                    8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                    A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                    D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                    C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                    004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                    CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                    D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                    00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                    E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                    A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                    EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                    E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                    000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                    FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                    E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                    0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                    EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                    F6FF191919C30000002B00000000000000000000000000000000000000000000
                    00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                    FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                    62ED0000005A0000000100000000000000000000000000000000000000000000
                    000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                    000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                    00000000000000000000}
                  TabOrder = 0
                  TabStop = False
                  OnClick = btnBeforeSQLFixClick
                end
                object dbsyneBeforeSQLFix: TDBSynEdit
                  Left = 64
                  Top = 0
                  Width = 632
                  Height = 118
                  DataField = 'SQL_Single_Before_Fix'
                  DataSource = DSSQLFix
                  Align = alRight
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 1
                  Gutter.Font.Charset = DEFAULT_CHARSET
                  Gutter.Font.Color = clWindowText
                  Gutter.Font.Height = -11
                  Gutter.Font.Name = 'Courier New'
                  Gutter.Font.Style = []
                  Highlighter = SynSQL
                end
                object cbSingleBeforeFix: TDBCheckBox
                  Left = 8
                  Top = 40
                  Width = 50
                  Height = 17
                  Caption = 'Active'
                  DataField = 'SQL_Before_Fix_Status'
                  DataSource = DSSQLFix
                  TabOrder = 2
                end
              end
              object pagSingleFix: TTabSheet
                Caption = 'SQL Fix'
                ImageIndex = 1
                object Shape10: TShape
                  Left = 0
                  Top = 0
                  Width = 696
                  Height = 118
                  Align = alClient
                  Brush.Color = clBtnFace
                  Pen.Style = psClear
                  ExplicitLeft = 56
                  ExplicitTop = 24
                  ExplicitWidth = 65
                  ExplicitHeight = 65
                end
                object pnlFix: TPanel
                  Left = 0
                  Top = 0
                  Width = 696
                  Height = 118
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object dbsyneSQLFix: TDBSynEdit
                    Left = 64
                    Top = 0
                    Width = 632
                    Height = 118
                    DataField = 'SQL_Fix'
                    DataSource = DSSQLFix
                    Align = alRight
                    Anchors = [akLeft, akTop, akRight, akBottom]
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Courier New'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                    Gutter.Font.Charset = DEFAULT_CHARSET
                    Gutter.Font.Color = clWindowText
                    Gutter.Font.Height = -11
                    Gutter.Font.Name = 'Courier New'
                    Gutter.Font.Style = []
                    Highlighter = SynSQL
                  end
                  object btnSQLFix: TBitBtn
                    Left = 6
                    Top = 8
                    Width = 35
                    Height = 25
                    Glyph.Data = {
                      C6070000424DC607000000000000360000002800000016000000160000000100
                      2000000000009007000000000000000000000000000000000000000000000000
                      00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                      78A3000000160000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                      0010000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                      FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                      2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                      A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                      000000000000000000000000000000000000000000249F9F9FEB949494E20000
                      004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                      66F0999999E60000001E00000000000000000000000000000000000000000000
                      0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                      D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                      94E5000000070000000000000000000000000000000000000000000000000000
                      0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                      A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                      000000000000000000000000000000000000000000000000000005050545CBCB
                      CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                      ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                      000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                      CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                      E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                      00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                      7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                      C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                      C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                      78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                      D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                      D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                      8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                      A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                      D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                      C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                      004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                      CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                      D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                      00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                      E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                      A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                      EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                      E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                      000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                      FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                      E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                      0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                      EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                      F6FF191919C30000002B00000000000000000000000000000000000000000000
                      00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                      FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                      62ED0000005A0000000100000000000000000000000000000000000000000000
                      000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                      000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                      00000000000000000000}
                    TabOrder = 1
                    TabStop = False
                    OnClick = btnSQLFixClick
                  end
                end
              end
              object pagSingleAfterFix: TTabSheet
                Caption = 'SQL After Fix'
                ImageIndex = 2
                object Shape11: TShape
                  Left = 0
                  Top = 0
                  Width = 64
                  Height = 118
                  Align = alClient
                  Brush.Color = clBtnFace
                  Pen.Style = psClear
                  ExplicitLeft = 48
                  ExplicitTop = 16
                  ExplicitWidth = 65
                  ExplicitHeight = 65
                end
                object btnAfterSQLFix: TBitBtn
                  Left = 6
                  Top = 8
                  Width = 35
                  Height = 25
                  Glyph.Data = {
                    C6070000424DC607000000000000360000002800000016000000160000000100
                    2000000000009007000000000000000000000000000000000000000000000000
                    00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                    78A3000000160000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                    0010000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                    FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                    2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                    0000000000000000000000000000000000000000000000000000000000000000
                    00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                    A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                    000000000000000000000000000000000000000000249F9F9FEB949494E20000
                    004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                    66F0999999E60000001E00000000000000000000000000000000000000000000
                    0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                    D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                    94E5000000070000000000000000000000000000000000000000000000000000
                    0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                    A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                    000000000000000000000000000000000000000000000000000005050545CBCB
                    CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                    ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                    000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                    CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                    E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                    00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                    7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                    C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                    C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                    78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                    D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                    D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                    8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                    A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                    D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                    C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                    004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                    CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                    D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                    00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                    E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                    A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                    EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                    E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                    000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                    FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                    E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                    0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                    EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                    F6FF191919C30000002B00000000000000000000000000000000000000000000
                    00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                    FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                    62ED0000005A0000000100000000000000000000000000000000000000000000
                    000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                    000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                    00000000000000000000}
                  TabOrder = 0
                  TabStop = False
                  OnClick = btnAfterSQLFixClick
                end
                object cbSingleAfterFix: TDBCheckBox
                  Left = 8
                  Top = 40
                  Width = 50
                  Height = 17
                  Caption = 'Active'
                  DataField = 'SQL_After_Fix_Status'
                  DataSource = DSSQLFix
                  TabOrder = 1
                end
                object dbsyneAfterSQLFix: TDBSynEdit
                  Left = 64
                  Top = 0
                  Width = 632
                  Height = 118
                  DataField = 'SQL_Single_After_Fix'
                  DataSource = DSSQLFix
                  Align = alRight
                  Anchors = [akLeft, akTop, akRight, akBottom]
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Courier New'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  TabOrder = 2
                  Gutter.Font.Charset = DEFAULT_CHARSET
                  Gutter.Font.Color = clWindowText
                  Gutter.Font.Height = -11
                  Gutter.Font.Name = 'Courier New'
                  Gutter.Font.Style = []
                  Highlighter = SynSQL
                end
              end
            end
          end
          object tabGlobalSQLBeforeAndAfterFix: TTabSheet
            Caption = 'Global SQL Before and After Fix'
            ImageIndex = 5
            object Shape12: TShape
              Left = 0
              Top = 0
              Width = 961
              Height = 146
              Align = alClient
              Brush.Color = clBtnFace
              Pen.Style = psClear
              ExplicitLeft = 88
              ExplicitTop = 16
              ExplicitWidth = 65
              ExplicitHeight = 65
            end
            object pnlBeforeAndAfter: TPanel
              Left = 0
              Top = 0
              Width = 961
              Height = 146
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              OnResize = pnlBeforeAndAfterResize
              object SplitterBeforeAfter: TSplitter
                Left = 0
                Top = 57
                Width = 961
                Height = 3
                Cursor = crVSplit
                Align = alTop
                ExplicitWidth = 69
              end
              object pnlAfterFix: TPanel
                Left = 0
                Top = 60
                Width = 961
                Height = 54
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Panel3: TPanel
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 54
                  Align = alLeft
                  TabOrder = 0
                  object btnSQLAfterFix: TBitBtn
                    Left = 8
                    Top = 23
                    Width = 35
                    Height = 25
                    Glyph.Data = {
                      C6070000424DC607000000000000360000002800000016000000160000000100
                      2000000000009007000000000000000000000000000000000000000000000000
                      00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                      78A3000000160000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                      0010000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                      FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                      2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                      A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                      000000000000000000000000000000000000000000249F9F9FEB949494E20000
                      004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                      66F0999999E60000001E00000000000000000000000000000000000000000000
                      0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                      D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                      94E5000000070000000000000000000000000000000000000000000000000000
                      0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                      A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                      000000000000000000000000000000000000000000000000000005050545CBCB
                      CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                      ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                      000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                      CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                      E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                      00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                      7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                      C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                      C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                      78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                      D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                      D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                      8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                      A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                      D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                      C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                      004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                      CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                      D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                      00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                      E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                      A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                      EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                      E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                      000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                      FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                      E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                      0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                      EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                      F6FF191919C30000002B00000000000000000000000000000000000000000000
                      00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                      FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                      62ED0000005A0000000100000000000000000000000000000000000000000000
                      000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                      000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                      00000000000000000000}
                    TabOrder = 0
                    TabStop = False
                    OnClick = btnSQLAfterFixClick
                  end
                  object DBCheckBox1: TDBCheckBox
                    Left = 8
                    Top = 5
                    Width = 97
                    Height = 17
                    Caption = 'After  Active'
                    DataField = 'SQL_After_Fix_Status'
                    DataSource = DSGlobalFix
                    TabOrder = 1
                    ValueChecked = '1'
                    ValueUnchecked = '0'
                  end
                end
                object Panel4: TPanel
                  Left = 100
                  Top = 0
                  Width = 861
                  Height = 54
                  Align = alClient
                  TabOrder = 1
                  object dbsyneSQLAfterFix: TDBSynEdit
                    Left = 1
                    Top = 1
                    Width = 859
                    Height = 52
                    DataField = 'SQL_After_Fix'
                    DataSource = DSGlobalFix
                    Align = alClient
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Courier New'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                    Gutter.Font.Charset = DEFAULT_CHARSET
                    Gutter.Font.Color = clWindowText
                    Gutter.Font.Height = -11
                    Gutter.Font.Name = 'Courier New'
                    Gutter.Font.Style = []
                    Gutter.ShowLineNumbers = True
                    Highlighter = SynSQL
                  end
                end
              end
              object pnlBeforeFix: TPanel
                Left = 0
                Top = 0
                Width = 961
                Height = 57
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object Panel7: TPanel
                  Left = 0
                  Top = 0
                  Width = 100
                  Height = 57
                  Align = alLeft
                  TabOrder = 0
                  object btnSQLBeforeFix: TBitBtn
                    Left = 8
                    Top = 23
                    Width = 35
                    Height = 25
                    Glyph.Data = {
                      C6070000424DC607000000000000360000002800000016000000160000000100
                      2000000000009007000000000000000000000000000000000000000000000000
                      00000000000000000000000000000000000000000005686A69A0A8AAA9D17878
                      78A3000000160000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      000000000000000000007A7D7BC2CFD2D1FFF4F5F5FFECEDEDFF767979D30000
                      0010000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0001BFC2C0FFE6E6E6FFCDCFCEFFBDBFBEFFB5B9B7FF0D0E0E79000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000CED1CFFDFCFC
                      FDFE373837FF383939FF9A9E9CFE6B6F6EE70000001500000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000004848486EF7F8F8FF646565FF3030
                      2FFF515352FFB2B7B6FF0909096D000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      00110000000E0000000000000000999A9AE9C0C3C2FF1A1A1AFF0F0F0EFF9FA3
                      A1FF676A69D70000001D0000000E000000000000000000000000000000000000
                      000000000000000000000000000000000000000000249F9F9FEB949494E20000
                      004D4E4E4EABBFC0BFFDA2A6A5FF7D7F7EFF0D0E0CFF40403FFFCDD0CEFF6666
                      66F0999999E60000001E00000000000000000000000000000000000000000000
                      0000000000000000000C9D9D9DEBD0D0D0FED3D3D3FFE0E0E0FFD9D9D9FFD6D5
                      D5FEB9BABAFFB9BDBBFF3F3F3FFF181718FFB9BBBBFE8B8D8CFFD2D1D2FF9494
                      94E5000000070000000000000000000000000000000000000000000000000000
                      0005ABABABE1C3C3C3FEC3C3C3FFCACACAFFD2D3D2FFDAD9D9FFDBDADBFFA8AA
                      A9FFCED0CFFF282927FF6B6C6BFFDADBDBFF8B8B8BFEAAAAAADB000000010000
                      000000000000000000000000000000000000000000000000000005050545CBCB
                      CBFFC4C3C4FFC0C7C5FFACBAB3FFBEBABBFFC0BDBFFFC7C4C5FFB4B3B4FFAFAE
                      ADFF2F3031FFDFE0DFFF909190FF010101410000000000000000000000000000
                      000000000000000000000000000000000000535353ABCACACAFFCECECEFFCCCB
                      CBFFCFCBCBFF6A7B78FF247C60FF267E60FF536964FFE7E4E4FF939493FFE7E7
                      E7FFEFF0EFFF2B2B2BD100000028000000000000000000000000000000000000
                      00003838388B696969D1BDBDBDFECACACAFFD3D3D3FFCBC9CAFF8D9997FF0DAE
                      7BFF26BA72FF29B76BFF16B87DFF667773FFF5F4F4FFF0F1F2FFD7D9D7FFC5C6
                      C5FF4D4E4DF9040404B8000000220000000000000000000000007D7D7DD1C7C7
                      C7FECACACAFFCFCFCFFFD1D1D1FFDAD4D6FF459581FF1BC389FF20C27BFF22C1
                      78FF1EC384FF2D8C72FFC6C1C2FFE0E2E1FF393834FF6D6C69FF545453FFD8DA
                      D9FF696969EE0000008A0000001300000000838383D1CBCBCBFFD0D0D0FFD4D4
                      D4FFD1D1D1FFDAD5D6FF509F8BFF35D0A0FF5CDFB7FF5EE0B7FF3AD2A1FF43A4
                      8AFFB3AFB0FF787875FF5F5E5AFF939390FF26241EFFC2C3C2FFDBDDDCFEA3A5
                      A4FF000000A4000000055656567F9C9C9CC5CECECEFCD9D9D9FFD3D3D3FFD5D3
                      D3FFB2BDBAFF64C2AAFF9DEED8FFA2F2DCFF72D2B8FFADBFBBFFA0A1A0FFC0C2
                      C1FF6E6E6DFF2C2A24FFA7A7A7FFE2E5E4FFD1D2D2FF888A89FE838483FF0000
                      004A00000000000000006F6F6FB1E0E0E0FFDBDBDBFFD0D0D0FFE2E0E1FFC3CC
                      CAFF9EC4BAFF9EC6BDFFBFC9C7FFE1DEDEFF939595FF878A8AFFA7A8A9FFD7D8
                      D7FFECEDEEFFE1E2E0FF9EA09FFF929493FFE4E7E6FF070707A2000000000000
                      00000606064EE9E9E9FFE2E2E2FFDEDEDEFFD9D9D9FFE1E0E1FFE5E2E2FFE4E0
                      E1FFE0DDDDFFD4D8D6FF7B7F7DFF848584FFEDEEEEFFEDECECFFEFF0EFFFA2A5
                      A4F74546478D6E6F6FA5D9DBDAFF414141CA000000000000000CA5A5A5EAEAEA
                      EAFEEAEAEAFFEDEDEDFFE4E4E4FFD5D5D5FFD1D1D1FFCDD1D0FFCFD5D2FFE4E8
                      E6FF6E716FFFDEE0DFFFEBECECFFF2F2F2FEA6A8A7DF00000000000000000000
                      000041424263535453BC0000000000000005C5C5C5E6F3F3F3FEF0F0F0FEFAFA
                      FAFFF3F3F3FFF1F1F1FFF2F1F2FFF3F1F3FFF1F1F1FFF8F7F7FFA1A2A2FFE7E8
                      E8FEF2F2F2FFFEFFFFFF2A2B2A88000000000000000000000000000000000000
                      0000000000000000000000000014C6C6C6E6BEBEBEDE0D0D0D397D7D7DA0EEEE
                      EEFEF5F5F5FFF6F6F6FFECECECFD7878789B4244448AFCFCFCFFFBFBFBFFF5F6
                      F6FF191919C30000002B00000000000000000000000000000000000000000000
                      00000000000000000004000000020000000000000000959595CEFDFDFDFFFDFD
                      FDFF848484C30000000000000000949494BEFFFFFFFFE9EAEAFED7DAD9FF6063
                      62ED0000005A0000000100000000000000000000000000000000000000000000
                      000000000000000000000000000058585881AFAFAFCCAFAFAFCC505050760000
                      000000000000000000003434345B8B8C8CBA9EA19FDA898B8BCA323433650000
                      00000000000000000000}
                    TabOrder = 0
                    TabStop = False
                    OnClick = btnSQLBeforeFixClick
                  end
                  object DBCheckBox2: TDBCheckBox
                    Left = 8
                    Top = 5
                    Width = 97
                    Height = 17
                    Caption = 'Before  Active'
                    DataField = 'SQL_Before_Fix_Status'
                    DataSource = DSGlobalFix
                    TabOrder = 1
                    ValueChecked = '1'
                    ValueUnchecked = '0'
                  end
                end
                object Panel10: TPanel
                  Left = 100
                  Top = 0
                  Width = 861
                  Height = 57
                  Align = alClient
                  TabOrder = 1
                  object dbsyneSQLBeforeFix: TDBSynEdit
                    Left = 1
                    Top = 1
                    Width = 859
                    Height = 55
                    DataField = 'SQL_Before_Fix'
                    DataSource = DSGlobalFix
                    Align = alClient
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Courier New'
                    Font.Style = []
                    ParentColor = False
                    ParentFont = False
                    TabOrder = 0
                    Gutter.Font.Charset = DEFAULT_CHARSET
                    Gutter.Font.Color = clWindowText
                    Gutter.Font.Height = -11
                    Gutter.Font.Name = 'Courier New'
                    Gutter.Font.Style = []
                    Gutter.ShowLineNumbers = True
                    Highlighter = SynSQL
                  end
                end
              end
              object pnlBeforeAfterNavigator: TPanel
                Left = 0
                Top = 114
                Width = 961
                Height = 32
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 2
                DesignSize = (
                  961
                  32)
                object DBNavigator3: TDBNavigator
                  Left = 4
                  Top = 3
                  Width = 224
                  Height = 25
                  DataSource = DSGlobalFix
                  VisibleButtons = [nbEdit, nbPost, nbCancel, nbRefresh]
                  Anchors = [akLeft, akBottom]
                  TabOrder = 0
                end
              end
            end
          end
        end
      end
      object gbLastExecutionInformation: TGroupBox
        Left = 3
        Top = 410
        Width = 1003
        Height = 78
        Anchors = [akLeft, akRight, akBottom]
        Caption = ' Last Execution Information '
        TabOrder = 2
        object Label12: TLabel
          Left = 24
          Top = 24
          Width = 52
          Height = 13
          Caption = 'Last Check'
        end
        object Label13: TLabel
          Left = 191
          Top = 24
          Width = 71
          Height = 13
          Caption = 'Last Fail Count'
        end
        object Label14: TLabel
          Left = 318
          Top = 24
          Width = 65
          Height = 13
          Caption = 'Pass/Fail/SQL'
        end
        object Label15: TLabel
          Left = 448
          Top = 21
          Width = 37
          Height = 13
          Caption = 'Last Fix'
        end
        object dbeLastFailCount: TDBEdit
          Left = 191
          Top = 40
          Width = 121
          Height = 21
          DataField = 'Calc_Last_Fail_Count'
          DataSource = DSDiagnosticQueries
          ReadOnly = True
          TabOrder = 1
        end
        object dbePassFailState: TDBEdit
          Left = 318
          Top = 40
          Width = 121
          Height = 21
          DataField = 'Calc_Pass_Fail_State'
          DataSource = DSDiagnosticQueries
          ReadOnly = True
          TabOrder = 2
        end
        object dbedatLastCheck: TcxDBDateEdit
          Left = 24
          Top = 40
          DataBinding.DataField = 'Calc_DT_Last_Check'
          DataBinding.DataSource = DSDiagnosticQueries
          Properties.ReadOnly = True
          TabOrder = 0
          Width = 161
        end
        object dbedatLastFix: TcxDBDateEdit
          Left = 448
          Top = 40
          DataBinding.DataField = 'Calc_DT_Last_Fix'
          DataBinding.DataSource = DSDiagnosticQueries
          Properties.ReadOnly = True
          TabOrder = 3
          Width = 161
        end
      end
      object DBNavigator1: TDBNavigator
        Left = 3
        Top = 494
        Width = 240
        Height = 25
        DataSource = DSDiagnosticQueries
        Anchors = [akLeft, akBottom]
        TabOrder = 3
      end
      object btnOK: TcxButton
        Left = 911
        Top = 494
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'OK'
        OptionsImage.Glyph.SourceDPI = 96
        OptionsImage.Glyph.Data = {
          89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C
          3B0000000467414D410000B18E7CFB519300000A3D69434350696363000078DA
          9D53675453E9163DF7DEF4424B8880944B6F5215082052428B805469A2129200
          A184181240EC88A8C088A2228215191471C0D11190B1228A8541B1F701790828
          E3E0283654DE0FDE1A7DB3E6BD376FF6AFBDF639679DEF9C7D3E004660B0449A
          85AA01644A15F288001F3C362E1E2777030A542081038040982D0B89F48F0200
          E0FBF1F0EC88001FF80204E0CD6D4000006ED80486E138FC7F5017CAE40A0024
          0C00A68BC4D94200A41000327215320500320A00ECA4749902002500005B1E1B
          170F806A01003B65924F030076D224F70200B628532A0240A3004026CA148900
          D00E005897A3148B00B0600028CA91887301B09B006092A1CC940060EF00809D
          2916640310180060A2100B530108F600C0904745F000083301288C94AF78D257
          5C21CE530000F0B2648BE5929454056E21B4C41D5C5DB978A0383743AC50D884
          0984E902B908E76565CA04D2C5009333030080467644800FCEF7E3393BB83A3B
          DB38DA3A7CB5A8FF1AFC8B888D8BFF973FAFC201010084D3F545FBB3BCAC1A00
          EE1800B6F18B96B41DA0650D80D6FD2F9AC91E00D54280E6AB5FCDC3E1FBF1F0
          548542E66667979B9B6B2B110B6D85A95FF5F99F097F015FF5B3E5FBF1F0DFD7
          83FB8A9305CA0C051E11E0830BB332B294723C5B26108A719B3F1EF1DF2EFCF3
          774C8B10278BE562A9508C474BC4B912690ACECB928A240A4996149748FF9389
          7FB3EC0F98BC6B0060D57E06F6425B50BBCA06EC972E20B0E88025EC0200E477
          DF82A9D110060031068393770F0030F99BFF1D681900A0D992141C0080171185
          0B95F29CC918010080083450053668833E188305D88023B8803B78811FCC8650
          88823858004248854C90432E2C8555500425B011B64215EC865AA8874638022D
          7002CEC205B802D7E0163C805E1880E7300A6F601C411032C244588836628098
          22D68823C24566217E48301281C42189480A224594C85264355282942355C85E
          A41EF91E398E9C452E213DC83DA40F19467E433EA018CA40D9A81E6A86DAA15C
          D41B0D42A3D0F9680ABA08CD470BD10D68255A831E429BD1B3E815F416DA8B3E
          47C730C0E8180733C46C302EC6C342B1782C199363CBB162AC02ABC11AB136AC
          13BB81F56223D87B0289C022E0041B823B2190309720242C222C279412AA0807
          08CD840EC20D421F6194F099C824EA12AD896E443E31969842CC2516112B8875
          C463C4F3C45BC401E21B1289C42199935C4881A438521A6909A994B493D4443A
          43EA21F593C6C864B236D99AEC410E250BC80A7211793BF910F934F93A7980FC
          8E42A718501C29FE94788A945240A9A01CA49CA25CA70C52C6A96A5453AA1B35
          942AA22EA696516BA96DD4ABD401EA384D9D664EF3A045D1D268AB6895B446DA
          79DA43DA2B3A9D6E4477A587D325F495F44AFA61FA457A1FFD3D438361C5E031
          12184AC606C67EC619C63DC62B269369C6F462C63315CC0DCC7AE639E663E63B
          15968AAD0A5F45A4B242A55AA559E5BACA0B55AAAAA9AAB7EA02D57CD50AD5A3
          AA575547D4A86A666A3C3581DA72B56AB5E36A77D4C6D459EA0EEAA1EA99EAA5
          EA07D52FA90F699035CC34FC34441A851AFB34CE69F4B33096318BC712B256B3
          6A59E759036C12DB9CCD67A7B14BD8DFB1BBD9A39A1A9A3334A335F334AB354F
          6AF672308E1987CFC9E094718E706E733E4CD19BE23D453C65FD94C629D7A7BC
          D59AAAE5A525D62AD66AD2BAA5F5411BD7F6D34ED7DEA4DDA2FD4887A063A513
          AE93ABB34BE7BCCEC854F654F7A9C2A9C5538F4CBDAF8BEA5AE946E82ED1DDA7
          DBA53BA6A7AF17A027D3DBAE774E6F449FA3EFA59FA6BF45FF94FEB001CB6096
          81C4608BC1698367B826EE8D67E09578073E6AA86B1868A834DC6BD86D386E64
          6E34D7A8C0A8C9E89131CD986B9C6CBCC5B8DD78D4C4C024C464A94983C97D53
          AA29D734D5749B69A7E95B3373B318B3B5662D6643E65AE67CF37CF306F38716
          4C0B4F8B45163516372D49965CCB74CB9D96D7AC502B27AB54AB6AABABD6A8B5
          B3B5C47AA775CF34E234D769D26935D3EED8306CBC6D726C1A6CFA6C39B6C1B6
          05B62DB62FEC4CECE2ED36D975DA7DB677B2CFB0AFB57FE0A0E130DBA1C0A1CD
          E137472B47A163B5E3CDE9CCE9FED3574C6F9DFE7286F50CF18C5D33EE3AB19C
          429CD63AB53B7D727671963B373A0FBB98B824BAEC70B9C36573C3B8A5DC8BAE
          44571FD715AE275CDFBB39BB29DC8EB8FDEA6EE39EEE7ED07D68A6F94CF1CCDA
          99FD1E461E028FBD1EBDB3F05989B3F6CCEAF534F41478D6783EF132F61279D5
          790D7A5B7AA7791FF27EE163EF23F739E6F396E7C65BC63BE38BF906F816FB76
          FB69F8CDF5ABF27BEC6FE49FE2DFE03F1AE014B024E04C203130287053E01DBE
          1E5FC8AFE78FCE7699BD6C764710232832A82AE849B055B03CB82D040D991DB2
          39E4E11CD339D2392DA110CA0FDD1CFA28CC3C6C51D88FE1A4F0B0F0EAF0A711
          0E114B233A2359910B230F46BE89F2892A8B7A30D762AE726E7BB46A7442747D
          F4DB18DF98F298DE58BBD865B157E274E22471ADF1E4F8E8F8BAF8B1797EF3B6
          CE1B48704A284AB83DDF7C7EDEFC4B0B7416642C38B95075A160E1D14462624C
          E2C1C48F8250418D602C899FB4236954C8136E133E177989B68886C51EE272F1
          60B2477279F2508A47CAE694E154CFD48AD411094F522579991698B63BED6D7A
          68FAFEF4898C988CA64C4A6662E671A986345DDA91A59F9597D523B39615C97A
          17B92DDABA68541E24AFCB46B2E767B72AD80A99A24B69A15CA3ECCB9995539D
          F32E373AF7689E7A9E34AF6BB1D5E2F58B07F3FDF3BF5D4258225CD2BED470E9
          AAA57DCBBC97ED5D8E2C4F5ADEBEC27845E18A8195012B0FACA2AD4A5FF55381
          7D4179C1EBD531ABDB0AF50A5716F6AF0958D350A452242FBAB3D67DEDEE7584
          759275DDEBA7AFDFBEFE73B1A8F872897D4945C9C75261E9E56F1CBEA9FC6662
          43F286EE32E7B25D1B491BA51B6F6FF2DC74A05CBD3CBFBC7F73C8E6E62DF896
          E22DAFB72EDC7AA96246C5EE6DB46DCA6DBD95C195ADDB4DB66FDCFEB12AB5EA
          56B54F75D30EDD1DEB77BCDD29DA797D97D7AEC6DD7ABB4B767FD823D973776F
          C0DEE61AB39A8A7DA47D39FB9ED646D7767ECBFDB6BE4EA7AEA4EED37EE9FEDE
          0311073AEA5DEAEB0FEA1E2C6B401B940DC387120E5DFBCEF7BBD6469BC6BD4D
          9CA692C3705879F8D9F789DFDF3E1274A4FD28F768E30FA63FEC38C63A56DC8C
          342F6E1E6D496DE96D8D6BED393EFB787B9B7BDBB11F6D7FDC7FC2F044F549CD
          9365A768A70A4F4D9CCE3F3D76467666E46CCAD9FEF685ED0FCEC59EBBD911DE
          D17D3EE8FCC50BFE17CE757A779EBEE871F1C425B74BC72F732FB75C71BED2DC
          E5D475EC27A79F8E753B77375F75B9DA7ACDF55A5BCFCC9E53D73DAF9FBDE17B
          E3C24DFECD2BB7E6DCEAB93DF7F6DD3B09777AEF8AEE0EDDCBB8F7F27ECEFDF1
          072B1F121F163F527B54F158F771CDCF963F37F53AF79EECF3EDEB7A12F9E441
          BFB0FFF93FB2FFF171A0F029F369C5A0C160FD90E3D08961FFE16BCFE63D1B78
          2E7B3E3E52F48BFA2F3B5E58BCF8E157AF5FBB466347075ECA5F4EFC56FA4AFB
          D5FED7335EB78F858D3D7E93F966FC6DF13BED7707DE73DF777E88F930389EFB
          91FCB1F293E5A7B6CF419F1F4E644E4CFC130398F3FC256333A2000000206348
          524D00007A25000080830000F9FF000080E9000075300000EA6000003A980000
          176F925FC54600000006624B4744000000000000F943BB7F0000000970485973
          00000B1300000B1301009A9C180000000976704167000000160000001600DCC5
          E958000003354944415438CBD5955D6C145518869F336CBB3B6B6B170BA54D35
          0197D0E0A2450998A82415684CAC1725E1D2129AB22046138CD1C4C4352473A1
          0951C01B8DDE803696F8174C3035919A68242244A285296CCBB4A5EDB6DB9FED
          76996D87999DCEF1A274718B8A117BE177752ECE79F27E3FEF77849492C50865
          51A88B09F601941F2EC609B9049C625C53429587EBBA040245AC2A5B4DDA4D91
          B54CAE5B167616C48D97391F8AD32CBDE7DBF6C8DCAC637FF8ECD1C01D2B9E8E
          4A9C66E9ED6B6D91EB2BD753BFF629FFC6D76B47EF083CBD6FAEE12F7DF282DC
          BAB69E53463BBB8FEFA26E635DC59A9655C6BF02CFDC80B61C6D924FD66EE1F3
          EE36CE5CFB09B3DCE174EA7B8A562AF7E76B3C1FE37BAEE7CFA51F895BA0E9A6
          39E8F60F1AE4EEFA286D978FF18BFB33A9E529EE75CA0AEE2A00A9FD0EE62E8F
          881696112D2C01CC9D85F33DB2630680ADEF3F21F736ECE58BC471BADC8B98A1
          34D5FE0AEE132BC9244C529D93A2A014112D2C5BA36D74BCF803F370A97800F4
          34CCF5E4F1F736D8AF36BEC6C9E409FA7357304BA6A8F255B32C5B452661A2C7
          8C7C9A424A49440BCB779A8E802748DAC33C56BD99C6230D0517235A581E8ABE
          4BC7E437C4A7BB18A00FD5BE0BD9BD844CC2C438DB2BC841A83444F2B3F44DC5
          9F0EB512B775826A906F47DB39F1CAD779E5112D2CDF8E1EE6CCCC8F0C33483A
          38C172B5220FEDBBDA2B982DEC8798DF15112D2CCB37975257B98DD5C11AA6BC
          49EA2B9F66C7C146DE6C3E488FD2458F7D8901D94F60498064472A9FBEBA5320
          2700EFA662F1C72514D1C272D933253C5CB6899AC00338C266C3DD8FD2699DE7
          8A1B6750F6E32FF6D3FBD520998449FAC11161EA166E9C5BC00AC0A6B7D651F9
          72995F8F1962E264965E5F37BF2AE7C815E7389F3B4BD237442A30C63D4B9732
          DC3D41266152B4CD5566AD05F92F1CB7B99A083B7840A87ACC10C6B121EC128B
          3E35CEB87F84943A4E281462D01863F4D4187ACC10AEE5FEEDBE2D70DECC0169
          01E831439C3BD4891A549956B3ACF0AF60BC7F8AAB5F0E50FB5C4DF9960B8FDC
          D6A17F69693D668876ED3BD6F110D7462D7EFBF8027ACC10DEAC37795B2A0B2C
          FD67F08816968AEA478F19E20D77FF3F61168EDB7F1DFFBFAFE977DDEB79952A
          7D0F6F00000025744558746372656174652D6461746500323030392D31312D32
          385431373A31383A32382D30373A30303191B22C000000257445587464617465
          3A63726561746500323031302D30322D32305432333A32363A31352D30373A30
          30063B5C810000002574455874646174653A6D6F6469667900323031302D3031
          2D31315430383A34343A30382D30373A30306E58A8AF00000035744558744C69
          63656E736500687474703A2F2F6372656174697665636F6D6D6F6E732E6F7267
          2F6C6963656E7365732F4C47504C2F322E312F3BC1B41800000025744558746D
          6F646966792D6461746500323030392D31312D32385431343A33313A35372D30
          373A30308B1E432D0000001674455874536F75726365004372797374616C2050
          726F6A656374EBE3E48B0000002774455874536F757263655F55524C00687474
          703A2F2F65766572616C646F2E636F6D2F6372797374616C2FA591935B000000
          0049454E44AE426082}
        TabOrder = 4
        OnClick = btnOKClick
      end
      object btnCloneFix: TcxButton
        Left = 744
        Top = 494
        Width = 161
        Height = 25
        Action = actCloneDiagnosticQuery
        Anchors = [akRight, akBottom]
        TabOrder = 5
      end
    end
    object tabGridView: TTabSheet
      Caption = 'Grid View'
      ImageIndex = 1
      DesignSize = (
        1009
        537)
      object Shape1: TShape
        Left = 0
        Top = 0
        Width = 1009
        Height = 537
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Color = clBtnFace
        ExplicitHeight = 601
      end
      object Label16: TLabel
        Left = 791
        Top = 18
        Width = 78
        Height = 13
        Caption = 'Export file name'
      end
      object SearchGrid: TcxGrid
        Left = 16
        Top = 64
        Width = 977
        Height = 438
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 1
        object SearchGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          Navigator.Visible = True
          OnCellDblClick = SearchGridDBTableView1CellDblClick
          DataController.DataSource = DSDiagnosticQueries
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsSelection.CellSelect = False
          object SearchGridDBTableView1Selected: TcxGridDBColumn
            Caption = 'Select'
            DataBinding.FieldName = 'Selected'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Width = 41
          end
          object SearchGridDBTableView1Query_ID: TcxGridDBColumn
            DataBinding.FieldName = 'Query_ID'
            Width = 184
          end
          object SearchGridDBTableView1Category: TcxGridDBColumn
            DataBinding.FieldName = 'Category'
            Width = 144
          end
          object SearchGridDBTableView1Subcategory: TcxGridDBColumn
            DataBinding.FieldName = 'Subcategory'
            Width = 144
          end
          object SearchGridDBTableView1Small_Description: TcxGridDBColumn
            DataBinding.FieldName = 'Small_Description'
            Width = 300
          end
          object SearchGridDBTableView1Long_Description: TcxGridDBColumn
            DataBinding.FieldName = 'Long_Description'
            Width = 300
          end
          object SearchGridDBTableView1Pass_Fail_State: TcxGridDBColumn
            DataBinding.FieldName = 'Calc_Pass_Fail_State'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
          end
          object SearchGridDBTableView1DT_Last_Check: TcxGridDBColumn
            DataBinding.FieldName = 'Calc_DT_Last_Check'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
          end
          object SearchGridDBTableView1DT_Last_Fix: TcxGridDBColumn
            DataBinding.FieldName = 'Calc_DT_Last_Fix'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
          end
          object SearchGridDBTableView1Last_Fail_Count: TcxGridDBColumn
            DataBinding.FieldName = 'Calc_Last_Fail_Count'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.ReadOnly = True
          end
          object SearchGridDBTableView1SQL_Count: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_Count'
            Width = 300
          end
          object SearchGridDBTableView1SQL_Detail: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_Detail'
            Width = 300
          end
          object SearchGridDBTableView1Row_Version_Number: TcxGridDBColumn
            DataBinding.FieldName = 'Row_Version_Number'
          end
          object SearchGridDBTableView1Fail_Score: TcxGridDBColumn
            DataBinding.FieldName = 'Fail_Score'
          end
          object SearchGridDBTableView1Sentry_TAG: TcxGridDBColumn
            DataBinding.FieldName = 'Sentry_TAG'
            Width = 300
          end
          object SearchGridDBTableView1Log_Remotely: TcxGridDBColumn
            DataBinding.FieldName = 'Log_Remotely'
          end
          object SearchGridDBTableView1Min_DB_Ver_Required: TcxGridDBColumn
            DataBinding.FieldName = 'Min_DB_Ver_Required'
          end
          object SearchGridDBTableView1Status: TcxGridDBColumn
            DataBinding.FieldName = 'Status'
          end
          object SearchGridDBTableView1SQL_Detail_Status: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_Detail_Status'
          end
          object SearchGridDBTableView1SQL_Fix_Status: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_Fix_Status'
          end
        end
        object SearchGridLevel1: TcxGridLevel
          GridView = SearchGridDBTableView1
        end
      end
      object RGViewMode: TRadioGroup
        Left = 16
        Top = 8
        Width = 345
        Height = 49
        Caption = ' View mode  '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          '&Search'
          'Export by &row'
          'Export by &category')
        TabOrder = 0
        OnClick = RGViewModeClick
      end
      object cxButton1: TcxButton
        Left = 367
        Top = 16
        Width = 89
        Height = 42
        Action = actExpandAll
        TabOrder = 2
      end
      object cxButton2: TcxButton
        Left = 455
        Top = 16
        Width = 89
        Height = 42
        Action = actCollapseAll
        TabOrder = 3
      end
      object cxButton3: TcxButton
        Left = 544
        Top = 16
        Width = 82
        Height = 42
        Action = actSelectAll
        TabOrder = 4
      end
      object cxButton4: TcxButton
        Left = 626
        Top = 16
        Width = 88
        Height = 42
        Action = actUnselectAll
        TabOrder = 5
      end
      object cxButton5: TcxButton
        Left = 712
        Top = 16
        Width = 73
        Height = 42
        Action = actExport
        TabOrder = 6
      end
      object edExportFileName: TEdit
        Left = 791
        Top = 37
        Width = 121
        Height = 21
        TabOrder = 7
      end
      object cxButton10: TcxButton
        Left = 924
        Top = 508
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'OK'
        OptionsImage.Glyph.SourceDPI = 96
        OptionsImage.Glyph.Data = {
          89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C
          3B0000000467414D410000B18E7CFB519300000A3D69434350696363000078DA
          9D53675453E9163DF7DEF4424B8880944B6F5215082052428B805469A2129200
          A184181240EC88A8C088A2228215191471C0D11190B1228A8541B1F701790828
          E3E0283654DE0FDE1A7DB3E6BD376FF6AFBDF639679DEF9C7D3E004660B0449A
          85AA01644A15F288001F3C362E1E2777030A542081038040982D0B89F48F0200
          E0FBF1F0EC88001FF80204E0CD6D4000006ED80486E138FC7F5017CAE40A0024
          0C00A68BC4D94200A41000327215320500320A00ECA4749902002500005B1E1B
          170F806A01003B65924F030076D224F70200B628532A0240A3004026CA148900
          D00E005897A3148B00B0600028CA91887301B09B006092A1CC940060EF00809D
          2916640310180060A2100B530108F600C0904745F000083301288C94AF78D257
          5C21CE530000F0B2648BE5929454056E21B4C41D5C5DB978A0383743AC50D884
          0984E902B908E76565CA04D2C5009333030080467644800FCEF7E3393BB83A3B
          DB38DA3A7CB5A8FF1AFC8B888D8BFF973FAFC201010084D3F545FBB3BCAC1A00
          EE1800B6F18B96B41DA0650D80D6FD2F9AC91E00D54280E6AB5FCDC3E1FBF1F0
          548542E66667979B9B6B2B110B6D85A95FF5F99F097F015FF5B3E5FBF1F0DFD7
          83FB8A9305CA0C051E11E0830BB332B294723C5B26108A719B3F1EF1DF2EFCF3
          774C8B10278BE562A9508C474BC4B912690ACECB928A240A4996149748FF9389
          7FB3EC0F98BC6B0060D57E06F6425B50BBCA06EC972E20B0E88025EC0200E477
          DF82A9D110060031068393770F0030F99BFF1D681900A0D992141C0080171185
          0B95F29CC918010080083450053668833E188305D88023B8803B78811FCC8650
          88823858004248854C90432E2C8555500425B011B64215EC865AA8874638022D
          7002CEC205B802D7E0163C805E1880E7300A6F601C411032C244588836628098
          22D68823C24566217E48301281C42189480A224594C85264355282942355C85E
          A41EF91E398E9C452E213DC83DA40F19467E433EA018CA40D9A81E6A86DAA15C
          D41B0D42A3D0F9680ABA08CD470BD10D68255A831E429BD1B3E815F416DA8B3E
          47C730C0E8180733C46C302EC6C342B1782C199363CBB162AC02ABC11AB136AC
          13BB81F56223D87B0289C022E0041B823B2190309720242C222C279412AA0807
          08CD840EC20D421F6194F099C824EA12AD896E443E31969842CC2516112B8875
          C463C4F3C45BC401E21B1289C42199935C4881A438521A6909A994B493D4443A
          43EA21F593C6C864B236D99AEC410E250BC80A7211793BF910F934F93A7980FC
          8E42A718501C29FE94788A945240A9A01CA49CA25CA70C52C6A96A5453AA1B35
          942AA22EA696516BA96DD4ABD401EA384D9D664EF3A045D1D268AB6895B446DA
          79DA43DA2B3A9D6E4477A587D325F495F44AFA61FA457A1FFD3D438361C5E031
          12184AC606C67EC619C63DC62B269369C6F462C63315CC0DCC7AE639E663E63B
          15968AAD0A5F45A4B242A55AA559E5BACA0B55AAAAA9AAB7EA02D57CD50AD5A3
          AA575547D4A86A666A3C3581DA72B56AB5E36A77D4C6D459EA0EEAA1EA99EAA5
          EA07D52FA90F699035CC34FC34441A851AFB34CE69F4B33096318BC712B256B3
          6A59E759036C12DB9CCD67A7B14BD8DFB1BBD9A39A1A9A3334A335F334AB354F
          6AF672308E1987CFC9E094718E706E733E4CD19BE23D453C65FD94C629D7A7BC
          D59AAAE5A525D62AD66AD2BAA5F5411BD7F6D34ED7DEA4DDA2FD4887A063A513
          AE93ABB34BE7BCCEC854F654F7A9C2A9C5538F4CBDAF8BEA5AE946E82ED1DDA7
          DBA53BA6A7AF17A027D3DBAE774E6F449FA3EFA59FA6BF45FF94FEB001CB6096
          81C4608BC1698367B826EE8D67E09578073E6AA86B1868A834DC6BD86D386E64
          6E34D7A8C0A8C9E89131CD986B9C6CBCC5B8DD78D4C4C024C464A94983C97D53
          AA29D734D5749B69A7E95B3373B318B3B5662D6643E65AE67CF37CF306F38716
          4C0B4F8B45163516372D49965CCB74CB9D96D7AC502B27AB54AB6AABABD6A8B5
          B3B5C47AA775CF34E234D769D26935D3EED8306CBC6D726C1A6CFA6C39B6C1B6
          05B62DB62FEC4CECE2ED36D975DA7DB677B2CFB0AFB57FE0A0E130DBA1C0A1CD
          E137472B47A163B5E3CDE9CCE9FED3574C6F9DFE7286F50CF18C5D33EE3AB19C
          429CD63AB53B7D727671963B373A0FBB98B824BAEC70B9C36573C3B8A5DC8BAE
          44571FD715AE275CDFBB39BB29DC8EB8FDEA6EE39EEE7ED07D68A6F94CF1CCDA
          99FD1E461E028FBD1EBDB3F05989B3F6CCEAF534F41478D6783EF132F61279D5
          790D7A5B7AA7791FF27EE163EF23F739E6F396E7C65BC63BE38BF906F816FB76
          FB69F8CDF5ABF27BEC6FE49FE2DFE03F1AE014B024E04C203130287053E01DBE
          1E5FC8AFE78FCE7699BD6C764710232832A82AE849B055B03CB82D040D991DB2
          39E4E11CD339D2392DA110CA0FDD1CFA28CC3C6C51D88FE1A4F0B0F0EAF0A711
          0E114B233A2359910B230F46BE89F2892A8B7A30D762AE726E7BB46A7442747D
          F4DB18DF98F298DE58BBD865B157E274E22471ADF1E4F8E8F8BAF8B1797EF3B6
          CE1B48704A284AB83DDF7C7EDEFC4B0B7416642C38B95075A160E1D14462624C
          E2C1C48F8250418D602C899FB4236954C8136E133E177989B68886C51EE272F1
          60B2477279F2508A47CAE694E154CFD48AD411094F522579991698B63BED6D7A
          68FAFEF4898C988CA64C4A6662E671A986345DDA91A59F9597D523B39615C97A
          17B92DDABA68541E24AFCB46B2E767B72AD80A99A24B69A15CA3ECCB9995539D
          F32E373AF7689E7A9E34AF6BB1D5E2F58B07F3FDF3BF5D4258225CD2BED470E9
          AAA57DCBBC97ED5D8E2C4F5ADEBEC27845E18A8195012B0FACA2AD4A5FF55381
          7D4179C1EBD531ABDB0AF50A5716F6AF0958D350A452242FBAB3D67DEDEE7584
          759275DDEBA7AFDFBEFE73B1A8F872897D4945C9C75261E9E56F1CBEA9FC6662
          43F286EE32E7B25D1B491BA51B6F6FF2DC74A05CBD3CBFBC7F73C8E6E62DF896
          E22DAFB72EDC7AA96246C5EE6DB46DCA6DBD95C195ADDB4DB66FDCFEB12AB5EA
          56B54F75D30EDD1DEB77BCDD29DA797D97D7AEC6DD7ABB4B767FD823D973776F
          C0DEE61AB39A8A7DA47D39FB9ED646D7767ECBFDB6BE4EA7AEA4EED37EE9FEDE
          0311073AEA5DEAEB0FEA1E2C6B401B940DC387120E5DFBCEF7BBD6469BC6BD4D
          9CA692C3705879F8D9F789DFDF3E1274A4FD28F768E30FA63FEC38C63A56DC8C
          342F6E1E6D496DE96D8D6BED393EFB787B9B7BDBB11F6D7FDC7FC2F044F549CD
          9365A768A70A4F4D9CCE3F3D76467666E46CCAD9FEF685ED0FCEC59EBBD911DE
          D17D3EE8FCC50BFE17CE757A779EBEE871F1C425B74BC72F732FB75C71BED2DC
          E5D475EC27A79F8E753B77375F75B9DA7ACDF55A5BCFCC9E53D73DAF9FBDE17B
          E3C24DFECD2BB7E6DCEAB93DF7F6DD3B09777AEF8AEE0EDDCBB8F7F27ECEFDF1
          072B1F121F163F527B54F158F771CDCF963F37F53AF79EECF3EDEB7A12F9E441
          BFB0FFF93FB2FFF171A0F029F369C5A0C160FD90E3D08961FFE16BCFE63D1B78
          2E7B3E3E52F48BFA2F3B5E58BCF8E157AF5FBB466347075ECA5F4EFC56FA4AFB
          D5FED7335EB78F858D3D7E93F966FC6DF13BED7707DE73DF777E88F930389EFB
          91FCB1F293E5A7B6CF419F1F4E644E4CFC130398F3FC256333A2000000206348
          524D00007A25000080830000F9FF000080E9000075300000EA6000003A980000
          176F925FC54600000006624B4744000000000000F943BB7F0000000970485973
          00000B1300000B1301009A9C180000000976704167000000160000001600DCC5
          E958000003354944415438CBD5955D6C145518869F336CBB3B6B6B170BA54D35
          0197D0E0A2450998A82415684CAC1725E1D2129AB22046138CD1C4C4352473A1
          0951C01B8DDE803696F8174C3035919A68242244A285296CCBB4A5EDB6DB9FED
          76996D87999DCEF1A274718B8A117BE177752ECE79F27E3FEF77849492C50865
          51A88B09F601941F2EC609B9049C625C53429587EBBA040245AC2A5B4DDA4D91
          B54CAE5B167616C48D97391F8AD32CBDE7DBF6C8DCAC637FF8ECD1C01D2B9E8E
          4A9C66E9ED6B6D91EB2BD753BFF629FFC6D76B47EF083CBD6FAEE12F7DF282DC
          BAB69E53463BBB8FEFA26E635DC59A9655C6BF02CFDC80B61C6D924FD66EE1F3
          EE36CE5CFB09B3DCE174EA7B8A562AF7E76B3C1FE37BAEE7CFA51F895BA0E9A6
          39E8F60F1AE4EEFA286D978FF18BFB33A9E529EE75CA0AEE2A00A9FD0EE62E8F
          881696112D2C01CC9D85F33DB2630680ADEF3F21F736ECE58BC471BADC8B98A1
          34D5FE0AEE132BC9244C529D93A2A014112D2C5BA36D74BCF803F370A97800F4
          34CCF5E4F1F736D8AF36BEC6C9E409FA7357304BA6A8F255B32C5B452661A2C7
          8C7C9A424A49440BCB779A8E802748DAC33C56BD99C6230D0517235A581E8ABE
          4BC7E437C4A7BB18A00FD5BE0BD9BD844CC2C438DB2BC841A83444F2B3F44DC5
          9F0EB512B775826A906F47DB39F1CAD779E5112D2CDF8E1EE6CCCC8F0C33483A
          38C172B5220FEDBBDA2B982DEC8798DF15112D2CCB37975257B98DD5C11AA6BC
          49EA2B9F66C7C146DE6C3E488FD2458F7D8901D94F60498064472A9FBEBA5320
          2700EFA662F1C72514D1C272D933253C5CB6899AC00338C266C3DD8FD2699DE7
          8A1B6750F6E32FF6D3FBD520998449FAC11161EA166E9C5BC00AC0A6B7D651F9
          72995F8F1962E264965E5F37BF2AE7C815E7389F3B4BD237442A30C63D4B9732
          DC3D41266152B4CD5566AD05F92F1CB7B99A083B7840A87ACC10C6B121EC128B
          3E35CEB87F84943A4E281462D01863F4D4187ACC10AEE5FEEDBE2D70DECC0169
          01E831439C3BD4891A549956B3ACF0AF60BC7F8AAB5F0E50FB5C4DF9960B8FDC
          D6A17F69693D668876ED3BD6F110D7462D7EFBF8027ACC10DEAC37795B2A0B2C
          FD67F08816968AEA478F19E20D77FF3F61168EDB7F1DFFBFAFE977DDEB79952A
          7D0F6F00000025744558746372656174652D6461746500323030392D31312D32
          385431373A31383A32382D30373A30303191B22C000000257445587464617465
          3A63726561746500323031302D30322D32305432333A32363A31352D30373A30
          30063B5C810000002574455874646174653A6D6F6469667900323031302D3031
          2D31315430383A34343A30382D30373A30306E58A8AF00000035744558744C69
          63656E736500687474703A2F2F6372656174697665636F6D6D6F6E732E6F7267
          2F6C6963656E7365732F4C47504C2F322E312F3BC1B41800000025744558746D
          6F646966792D6461746500323030392D31312D32385431343A33313A35372D30
          373A30308B1E432D0000001674455874536F75726365004372797374616C2050
          726F6A656374EBE3E48B0000002774455874536F757263655F55524C00687474
          703A2F2F65766572616C646F2E636F6D2F6372797374616C2FA591935B000000
          0049454E44AE426082}
        TabOrder = 8
        OnClick = btnOKClick
      end
    end
    object tabParams: TTabSheet
      Caption = 'Parameters'
      ImageIndex = 4
      DesignSize = (
        1009
        537)
      object Shape9: TShape
        Left = 0
        Top = 0
        Width = 1009
        Height = 537
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Style = psClear
        ExplicitLeft = 104
        ExplicitTop = 64
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object cxGrid1: TcxGrid
        Left = 11
        Top = 8
        Width = 982
        Height = 488
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = DSParameters
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsData.Appending = True
          object cxGrid1DBTableView1Param: TcxGridDBColumn
            DataBinding.FieldName = 'Param'
            Width = 300
          end
          object cxGrid1DBTableView1DataType: TcxGridDBColumn
            DataBinding.FieldName = 'DataType'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.Items.Strings = (
              'String'
              'Integer'
              'Date'
              'Float')
            Width = 140
          end
          object cxGrid1DBTableView1Value: TcxGridDBColumn
            DataBinding.FieldName = 'Value'
            OnGetProperties = cxGrid1DBTableView1ValueGetProperties
            Width = 800
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object DBNavigator2: TDBNavigator
        Left = 11
        Top = 502
        Width = 240
        Height = 25
        DataSource = DSParameters
        Anchors = [akLeft, akBottom]
        TabOrder = 1
      end
    end
    object tabConfigFile: TTabSheet
      Caption = 'DatabaseFixer.cfg'
      ImageIndex = 2
      DesignSize = (
        1009
        537)
      object Shape3: TShape
        Left = 0
        Top = 0
        Width = 1009
        Height = 537
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Color = clBtnFace
        ExplicitLeft = 64
        ExplicitTop = 72
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object gbDatabase: TGroupBox
        Left = 16
        Top = 16
        Width = 977
        Height = 129
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Database configuration '
        TabOrder = 0
        DesignSize = (
          977
          129)
        object Label17: TLabel
          Left = 16
          Top = 24
          Width = 97
          Height = 13
          Caption = 'POS ADS Alias name'
        end
        object Label18: TLabel
          Left = 336
          Top = 24
          Width = 48
          Height = 13
          Caption = 'Site Name'
        end
        object Label25: TLabel
          Left = 16
          Top = 72
          Width = 103
          Height = 13
          Caption = 'DIAG ADS Alias Name'
        end
        object Label26: TLabel
          Left = 336
          Top = 72
          Width = 119
          Height = 13
          Caption = 'REMOTE ADS Alias Name'
        end
        object edPOSADSAliasName: TEdit
          Left = 16
          Top = 40
          Width = 153
          Height = 21
          TabOrder = 0
          OnChange = edPOSADSAliasNameChange
        end
        object rgPOSServerType: TRadioGroup
          Left = 175
          Top = 24
          Width = 145
          Height = 41
          Caption = 'POS Server Type'
          Columns = 2
          Items.Strings = (
            'Remote'
            'Local')
          TabOrder = 1
          OnClick = rgPOSServerTypeClick
        end
        object edSiteName: TEdit
          Left = 336
          Top = 40
          Width = 625
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
          OnChange = edSiteNameChange
        end
        object edDIAGADSAliasName: TEdit
          Left = 16
          Top = 88
          Width = 153
          Height = 21
          TabOrder = 3
          OnChange = edPOSADSAliasNameChange
        end
        object rgDIAGServerType: TRadioGroup
          Left = 175
          Top = 71
          Width = 145
          Height = 42
          Caption = 'DIAG Server Type'
          Columns = 2
          Items.Strings = (
            'Remote'
            'Local')
          TabOrder = 4
          OnClick = rgPOSServerTypeClick
        end
        object edREMOTEADSAliasName: TEdit
          Left = 336
          Top = 88
          Width = 153
          Height = 21
          TabOrder = 5
          OnChange = edPOSADSAliasNameChange
        end
        object rgREMOTEServerType: TRadioGroup
          Left = 503
          Top = 71
          Width = 145
          Height = 42
          Caption = 'REMOTE Server Type'
          Columns = 2
          Items.Strings = (
            'Remote'
            'Local')
          TabOrder = 6
          OnClick = rgPOSServerTypeClick
        end
      end
      object gbSentry: TGroupBox
        Left = 16
        Top = 160
        Width = 977
        Height = 81
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Sentry information '
        TabOrder = 1
        DesignSize = (
          977
          81)
        object Label19: TLabel
          Left = 16
          Top = 24
          Width = 18
          Height = 13
          Caption = 'Tag'
        end
        object Label20: TLabel
          Left = 184
          Top = 24
          Width = 48
          Height = 13
          Caption = 'Tag Score'
        end
        object Label21: TLabel
          Left = 336
          Top = 24
          Width = 20
          Height = 13
          Caption = 'DSN'
        end
        object edTag: TEdit
          Left = 16
          Top = 40
          Width = 153
          Height = 21
          TabOrder = 0
          OnChange = edTagChange
        end
        object edTagScore: TEdit
          Left = 184
          Top = 40
          Width = 136
          Height = 21
          TabOrder = 1
          OnChange = edTagScoreChange
        end
        object edDSN: TEdit
          Left = 336
          Top = 40
          Width = 625
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
          OnChange = edDSNChange
        end
      end
      object gbVerboseLevel: TGroupBox
        Left = 16
        Top = 256
        Width = 977
        Height = 81
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Verbose level '
        TabOrder = 2
        object Label22: TLabel
          Left = 16
          Top = 21
          Width = 38
          Height = 13
          Caption = 'Console'
        end
        object Label23: TLabel
          Left = 264
          Top = 21
          Width = 24
          Height = 13
          Caption = 'Local'
        end
        object Label24: TLabel
          Left = 520
          Top = 21
          Width = 37
          Height = 13
          Caption = 'Remote'
        end
        object chkcbConsole: TJvCheckedComboBox
          Left = 16
          Top = 40
          Width = 233
          Height = 21
          CapSelectAll = '&Select all'
          CapDeSelectAll = '&Deselect all'
          CapInvertAll = '&Invert all'
          TabOrder = 0
          OnChange = chkcbConsoleChange
        end
        object chkcbLocal: TJvCheckedComboBox
          Left = 264
          Top = 40
          Width = 233
          Height = 21
          CapSelectAll = '&Select all'
          CapDeSelectAll = '&Deselect all'
          CapInvertAll = '&Invert all'
          TabOrder = 1
          OnChange = chkcbLocalChange
        end
        object chkcbRemote: TJvCheckedComboBox
          Left = 520
          Top = 40
          Width = 233
          Height = 21
          CapSelectAll = '&Select all'
          CapDeSelectAll = '&Deselect all'
          CapInvertAll = '&Invert all'
          TabOrder = 2
          OnChange = chkcbRemoteChange
        end
      end
      object btnCfgFileApply: TcxButton
        Left = 837
        Top = 492
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Apply'
        OptionsImage.Glyph.SourceDPI = 96
        OptionsImage.Glyph.Data = {
          89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C
          3B0000000467414D410000D904DCB2DA0200000006624B4744000000000000F9
          43BB7F000000097048597300000048000000480046C96B3E0000000976704167
          000000160000001600DCC5E958000004B54944415438CBAD95DB6B1C551CC7BF
          3373CECCCEEC259BBD247BC96663939A3635F612426D1BD34A455A91824528A2
          A242151424AF82FE0105C5A78229D687528B8A2DA255445BA12D566BDB24BDE5
          9E4D93DD247B497667EF3B979D191F0295D20BD2FA7D39F083F3E13CFCBE9F03
          3C662247DDB02C0BBF5987E03A24DE99338F4CEC03A497816A1A1412BCB060A0
          841C1C30B000708F04FD1888ECE7918D19FCBA68B4679DBFF3A0CFE1DDAC1125
          5D5B5473B62E62DD0DFE0CC07300C200AE3F007A08D8B895C7D4B8266E88B4EC
          7836B273E0C58E7D07D634B6F72846D555A2C564B15CC9DE01070759443D023A
          A302844E1DB96FEE657A4F00DB9F163132A9DA37075B776F0BF50DEC6EDDF3FC
          33A11DF668439BC8B16C475ECBFAF2CA4A8EC3AB40E37B4049B188C1D4DBE4AA
          1E2A17500DBC0E3DDF06E0CF55E8DA1F818F76B5E0FBE19C7B832FFA524FD3B6
          81EDC15D7DEDEE27451326000B9AA5D8463337A20BA90425C17D402E057E8DCF
          D3137206DF2284F1AC68C96F934AF6D74D7B50BAF629B0E52C30B4DB42F769D6
          DFE56FDBBFCEB5E5E0A6C6DE4D4DF62652D26518A689743585F373BF632A31C6
          2B69DD435419E2133EF7B6F6C68EF7BBBCDD2FF09408B1F2CD30AD8C4972257F
          BAF73CE42B3B81DE33241875475EEBB077BFDDE97CAAD32138B8427D05866922
          534BE2F2C2455C1ABBA22FCD16AEAB29FC4CEC94DD6BE76DEF849CA1FE802320
          5142C052BD97153429A9C55CA5BA7CAAFF0F5EF2D0E637A3E2FA37A2E29A569E
          706CD15846DDA823534BE266FA2A6E4D4FAA2B7395BFEA291CB192384BA866BE
          A29A85BE456546B2D558786D1E0894928833DACD69DA07CBBA1515A9E80E0B6D
          7B9B85E610C3E94CC14C433354649565CC66C7313B17AFC8F3EA052D8941731E
          E7F82114899505B10485494993281792F0E97E78252FECBCC436D85C1D2CDFF4
          AE8D489C93DA25859599929946452FA1A0E5912FC84827F2C5C29C7A464DE088
          3E8F8BC630AA663BC0896BC15202BF289921465269999551B2D2283319686C91
          A10402251C6F320A53B1B2908D25E48D242AE592554828B99569F57461D63C5C
          99C0256D080AD30A587300475C48703C9628611A1D120D8B4E8EB7781D26A781
          270C1CD4012775C2CE49B0710278860753255631AEA733D3D553D9196D50BE8C
          1175161A1302ACF8EA7A727C00AA9A428213B1C053CEE5946C6187431045D106
          8FE0475088202044D02404E1E6FCB02AC44ACFCB0BF3932B5F67C6AB5F24BFC3
          AD7A1575940114FE2D13A72E00B610EA95792C12873947292B36488E16BFCB6F
          F78B01046C618485287C248C5A5137A662D3B1F1D1D8B1F8B5D2970BC7316399
          30A1DCDB520E00D445C0DE0133378C0CF5E8B7296539B7CD1509385B9C41472B
          E32321948B8A31327575ECFAD8CDA37323F913F1AF100760C1BABF52EEB84289
          030D5D301327B12CB4A9B31C5F37ED8218F189CDEE5A4DAD8FC4FE1E19BA35FC
          F9EC70EEE4EDE3580205603E588077D9AD96009CDDB0167F428E8F2A31083555
          314ADEF9F4CCC4E8C4E8E0C425F9870F8FDBB3BF7CA2C3521F6ED6FB8ADEB11E
          288F83D93880606384F4D635B3989930870E1C6B281EDE58807CE3912CBE9AE6
          1DABA76B2B78DA0162586D08F6FFF7FB0FFD9AC2FD40C35AA0AE00954560F1DC
          63BCF4FFCA3FCF540DEDF3509C9E0000002574455874646174653A6372656174
          6500323031302D30322D32335431383A31343A31372D30373A3030BED3D51E00
          00002574455874646174653A6D6F6469667900323031302D30322D3233543138
          3A31343A31372D30373A3030CF8E6DA200000035744558744C6963656E736500
          687474703A2F2F6372656174697665636F6D6D6F6E732E6F72672F6C6963656E
          7365732F4C47504C2F322E312F3BC1B4180000001474455874536F7572636500
          4372797374616C20436C656172F5E2E7A80000003A74455874536F757263655F
          55524C00687474703A2F2F636F6D6D6F6E732E77696B696D656469612E6F7267
          2F77696B692F4372797374616C5F436C656172AFBE45630000000049454E44AE
          426082}
        TabOrder = 3
        OnClick = btnCfgFileApplyClick
      end
      object btnCfgFileCancel: TcxButton
        Left = 918
        Top = 492
        Width = 75
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Cancel'
        OptionsImage.Glyph.SourceDPI = 96
        OptionsImage.Glyph.Data = {
          89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C
          3B0000000467414D410000AFC837058AE900000006624B4744000000000000F9
          43BB7F000000097048597300000048000000480046C96B3E0000000976704167
          000000160000001600DCC5E958000004F94944415438CBB5956D6855651CC07F
          CF73CEB9E7ECEEDEEB76B5BCABCDE5D0CDC2EC66A8CB8265A8481B0699910446
          84AE6F96426410D1B70AA20F86100696F40673D0C8965B4AAF30A95D4CCA6EBE
          AC6D775BBB9673CDB5ED9E7B9EF33C7DD8BCCDA28FFD3FFF9F1F3FFE2FCF5F18
          63F83FC20678B1BA9A502942291FAA4D26DF220CF5F0C4C44E57EB4ECB18A431
          585A6301426BAEA98461B87E7165E547AEEB96E77FFD75AF13866F5AC6D03A33
          8304C01866B4DEB2BCAAEABDC7AAAA2A1FADA85858178DB6FB423CF05F46BE52
          8DA978FCD39675EB6E78B0B939BAACBEFE0DDFF7773057011B4009B1FDB6BABA
          23DB93492FFEF4D310863CF8DC735EA7EFB7F705C14E4FEBB6F9D0A2314D4BA2
          D18F373634C4934F3D0555556C4C246C69DBEFF49F3EBD08382001AAABAB0FED
          48A5BCF8934FC29A35D0D848E5F3CFD3EC795EBDD6478AF3CC8BB0AEC6B68F6D
          AAA9892F6A6D85DB6F878A0ADC9616363EF28853BB7AF52BC06C2984D63A1402
          2627E1F265181840DE751789FDFBB9BFACCC5BE6FBED4ACA2D4558739352DD4D
          0B17C62A5B5B91E934E472B36F2627D1C620B4D625F0F0E0E0136D232385C977
          DF8563C7A058845C0EFBCE3B59B46F1F1B6CDB5B3A3DDD7193EF7F759FE7256E
          DCB50B67F56AC4C8084C4F43364BA1AB8BCF3FFC30C8F5F63E53023B42745CEC
          EBDBD671F97261EAE851387912B4868101EC952BA9D8BB97466322F7F8BE97DC
          BD9BC835D3420173FE3CFE975FF2755797EACB645AA594870084318697522950
          8AD098EDB72E5E7C644B34EA556EDA04E934140A108B31D5D383F67DE24D4D30
          3505B68DB974893F7B7BE93973465DF8E9A73D8E10078565B12B0866A7020021
          8818D3766E7474CA2493ED5B3A3ABCC4D5ABD8AB56C1F838E5CB97CFE68D8C80
          E3A0F279A632197AB259D5D7DFBFDB96F2F0BF1644CC870BD1D97FE54A4B573C
          FEC986E3C7DD85C522CE8A15303E3E9B635904B91C13A74EF1DDC080EACFE777
          38B67D146310C6945836406C66068CC108811002DF98DF956D1B3533C3584707
          6E2E87705D1002A314334343082094D208AD7F73E6B65202D6EC505C6F2C8042
          1836D65455756DB66D2F1C1AE2AA5204DDDD14E772229E879348509E4CD2585F
          EF48DB3E3E3C30F0B0059D621E4BFE634D9B96D4D6766F8E4613CE850B044AFD
          6D025852225C1769DBA830C44C4C70773A5D764B43437B00DBE7B34A603F08D6
          D52C5B76AC25918847B3590A4A21003367B1C0B258E03858A5760802A5106363
          DCB37EBD57BB72E591001EB80EACC2B0E9E6152BBA9A93C9989BC95028164B50
          0B8847A3FCEC79EA07CB0ACA1D075B088C31082909C210F279EEDEB0C1ABBEE3
          8EF670CE5C022CAEAB6BDB9A4A2D5870EA14C579500994555672D6F3823EAD1F
          1F0CC3E61F7CBFE07A1E969418AD1152A28280483ECFBD5BB77AA974FA5009EC
          795E345A2C12FAFEEC2F3A675A9E4AF1A3EBAA0B5353AD16BC1F11E2B3DCF4F4
          B61FFFF8A350168B615916C6181082D0F7712D0B37162B2B8147B3D9FDDD8542
          A0D6AEC516021B70962E251389A8F3E3E37B2C387CADDB8E109D8363633BBF1F
          1E2ED8F1388E652101515BCB77DF7CA3877A7A5E2D812D38702E9379FC8B3054
          329DC66968E05B29D5F9D1D1DD96310705D7872D44DBE0A54BDBBEBF78B1202B
          2A706B6A3873FAB43E77E2C4B384E10BA53946085C293FF8E5ECD9985EB2E475
          A3941ECAE5F6D8421C4608FE791505E040677E64E4D15E21DE8EC4E3DE5036FB
          B203AF9572FEAF63FA1706E92D34543849D90000002574455874637265617465
          2D6461746500323030392D31312D31355431363A30383A34342D30373A303076
          96CE470000002574455874646174653A63726561746500323031302D30322D32
          305432333A32363A31352D30373A3030063B5C81000000257445587464617465
          3A6D6F6469667900323031302D30312D31315430393A31393A32372D30373A30
          30830A214C00000035744558744C6963656E736500687474703A2F2F63726561
          74697665636F6D6D6F6E732E6F72672F6C6963656E7365732F4C47504C2F322E
          312F3BC1B41800000025744558746D6F646966792D6461746500323030392D31
          312D31355431363A30383A34342D30373A30302927B873000000197445587453
          6F6674776172650041646F626520496D616765526561647971C9653C0000000D
          74455874536F75726365004E75766F6C61AC4F35F10000003474455874536F75
          7263655F55524C00687474703A2F2F7777772E69636F6E2D6B696E672E636F6D
          2F70726F6A656374732F6E75766F6C612F763DB4520000000049454E44AE4260
          82}
        TabOrder = 4
        OnClick = btnCfgFileCancelClick
      end
    end
    object tabImportExportDiagnosticQueries: TTabSheet
      Caption = 'Import / Export Diagnostic Queries'
      ImageIndex = 3
      DesignSize = (
        1009
        537)
      object Shape4: TShape
        Left = 0
        Top = 0
        Width = 1009
        Height = 537
        Align = alClient
        Brush.Color = clBtnFace
        Pen.Color = clNone
        Pen.Style = psClear
        ExplicitLeft = 80
        ExplicitTop = 32
        ExplicitWidth = 65
        ExplicitHeight = 65
      end
      object gbImportExport: TGroupBox
        Left = 16
        Top = 16
        Width = 977
        Height = 73
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Import / Export '
        TabOrder = 0
        object cxButton6: TcxButton
          Left = 16
          Top = 32
          Width = 169
          Height = 25
          Action = actGenerateColumnsUnit
          TabOrder = 0
        end
        object cxButton7: TcxButton
          Left = 191
          Top = 32
          Width = 169
          Height = 25
          Action = FMainDoctor.actGenerateMissingColumns
          TabOrder = 1
        end
        object cxButton8: TcxButton
          Left = 366
          Top = 32
          Width = 169
          Height = 25
          Action = FMainDoctor.actImportDignosticQueries
          TabOrder = 2
        end
        object cxButton9: TcxButton
          Left = 541
          Top = 32
          Width = 169
          Height = 25
          Action = FMainDoctor.actRestoreDiagnosticQueries
          TabOrder = 3
        end
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 104
        Width = 977
        Height = 418
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'Compare Diagnostic Queries'
        TabOrder = 1
        DesignSize = (
          977
          418)
        object Button1: TButton
          Left = 16
          Top = 24
          Width = 105
          Height = 25
          Action = actConnectToRemoteAndCompare
          TabOrder = 0
        end
        object Button2: TButton
          Left = 127
          Top = 24
          Width = 74
          Height = 25
          Action = actDisconnectFromRemote
          TabOrder = 1
        end
        object GridCompare: TcxGrid
          Left = 16
          Top = 102
          Width = 945
          Height = 285
          Anchors = [akLeft, akTop, akRight, akBottom]
          TabOrder = 2
          object GridCompareDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DSCompare
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            object GridCompareDBTableView1Selected: TcxGridDBColumn
              DataBinding.FieldName = 'Selected'
            end
            object GridCompareDBTableView1Location: TcxGridDBColumn
              DataBinding.FieldName = 'Location'
              MinWidth = 10
            end
            object GridCompareDBTableView1Row_Type: TcxGridDBColumn
              DataBinding.FieldName = 'Row_Type'
            end
            object GridCompareDBTableView1Query_ID: TcxGridDBColumn
              DataBinding.FieldName = 'Query_ID'
            end
            object GridCompareDBTableView1Category: TcxGridDBColumn
              DataBinding.FieldName = 'Category'
            end
            object GridCompareDBTableView1Small_Description: TcxGridDBColumn
              DataBinding.FieldName = 'Small_Description'
            end
            object GridCompareDBTableView1Long_Description: TcxGridDBColumn
              DataBinding.FieldName = 'Long_Description'
            end
            object GridCompareDBTableView1Pass_Fail_State: TcxGridDBColumn
              DataBinding.FieldName = 'Pass_Fail_State'
            end
            object GridCompareDBTableView1DT_Last_Check: TcxGridDBColumn
              DataBinding.FieldName = 'DT_Last_Check'
            end
            object GridCompareDBTableView1DT_Last_Fix: TcxGridDBColumn
              DataBinding.FieldName = 'DT_Last_Fix'
            end
            object GridCompareDBTableView1Last_Fail_Count: TcxGridDBColumn
              DataBinding.FieldName = 'Last_Fail_Count'
            end
            object GridCompareDBTableView1SQL_Count: TcxGridDBColumn
              DataBinding.FieldName = 'SQL_Count'
            end
            object GridCompareDBTableView1SQL_Detail: TcxGridDBColumn
              DataBinding.FieldName = 'SQL_Detail'
            end
            object GridCompareDBTableView1SQL_Fix: TcxGridDBColumn
              DataBinding.FieldName = 'SQL_Fix'
            end
            object GridCompareDBTableView1Fail_Score: TcxGridDBColumn
              DataBinding.FieldName = 'Fail_Score'
            end
            object GridCompareDBTableView1Sentry_TAG: TcxGridDBColumn
              DataBinding.FieldName = 'Sentry_TAG'
            end
            object GridCompareDBTableView1Log_Remotely: TcxGridDBColumn
              DataBinding.FieldName = 'Log_Remotely'
            end
            object GridCompareDBTableView1Min_DB_Ver_Required: TcxGridDBColumn
              DataBinding.FieldName = 'Min_DB_Ver_Required'
            end
            object GridCompareDBTableView1Status: TcxGridDBColumn
              DataBinding.FieldName = 'Status'
            end
            object GridCompareDBTableView1SQL_Detail_Status: TcxGridDBColumn
              DataBinding.FieldName = 'SQL_Detail_Status'
            end
            object GridCompareDBTableView1SQL_Fix_Status: TcxGridDBColumn
              DataBinding.FieldName = 'SQL_Fix_Status'
            end
            object GridCompareDBTableView1Row_Version_Number: TcxGridDBColumn
              DataBinding.FieldName = 'Row_Version_Number'
            end
          end
          object GridCompareLevel1: TcxGridLevel
            GridView = GridCompareDBTableView1
          end
        end
        object rgCompareFilter: TRadioGroup
          Left = 16
          Top = 55
          Width = 361
          Height = 41
          Caption = 'Filtered by'
          Columns = 3
          Items.Strings = (
            'All'
            'Changes on Local'
            'Changes on Remote')
          TabOrder = 3
          OnClick = rgCompareFilterClick
        end
        object BitBtn1: TBitBtn
          Left = 295
          Top = 24
          Width = 82
          Height = 25
          Action = actCompareSelectAll
          Caption = 'Select All'
          TabOrder = 4
        end
        object BitBtn2: TBitBtn
          Left = 380
          Top = 24
          Width = 97
          Height = 25
          Action = actCompareUnselectAll
          Caption = 'Unselect All'
          TabOrder = 5
        end
        object BitBtn3: TBitBtn
          Left = 207
          Top = 24
          Width = 82
          Height = 25
          Action = actRefreshCompare
          Caption = 'Refresh'
          TabOrder = 6
        end
        object BitBtn4: TBitBtn
          Left = 483
          Top = 24
          Width = 75
          Height = 25
          Action = actPullAllFromRemote
          Caption = 'Pull All'
          TabOrder = 7
        end
        object BitBtn5: TBitBtn
          Left = 564
          Top = 24
          Width = 106
          Height = 25
          Action = actPullSelectedFromRemote
          Caption = 'Pull Selected'
          TabOrder = 8
        end
        object BitBtn6: TBitBtn
          Left = 676
          Top = 24
          Width = 75
          Height = 25
          Action = actPushAllFromLocal
          Caption = 'Push All'
          TabOrder = 9
        end
        object BitBtn7: TBitBtn
          Left = 757
          Top = 24
          Width = 100
          Height = 25
          Action = actPushSelectedFromLocal
          Caption = 'Push Selected'
          TabOrder = 10
        end
        object cxButton11: TcxButton
          Left = 902
          Top = 393
          Width = 75
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = 'OK'
          OptionsImage.Glyph.SourceDPI = 96
          OptionsImage.Glyph.Data = {
            89504E470D0A1A0A0000000D4948445200000016000000160806000000C4B46C
            3B0000000467414D410000B18E7CFB519300000A3D69434350696363000078DA
            9D53675453E9163DF7DEF4424B8880944B6F5215082052428B805469A2129200
            A184181240EC88A8C088A2228215191471C0D11190B1228A8541B1F701790828
            E3E0283654DE0FDE1A7DB3E6BD376FF6AFBDF639679DEF9C7D3E004660B0449A
            85AA01644A15F288001F3C362E1E2777030A542081038040982D0B89F48F0200
            E0FBF1F0EC88001FF80204E0CD6D4000006ED80486E138FC7F5017CAE40A0024
            0C00A68BC4D94200A41000327215320500320A00ECA4749902002500005B1E1B
            170F806A01003B65924F030076D224F70200B628532A0240A3004026CA148900
            D00E005897A3148B00B0600028CA91887301B09B006092A1CC940060EF00809D
            2916640310180060A2100B530108F600C0904745F000083301288C94AF78D257
            5C21CE530000F0B2648BE5929454056E21B4C41D5C5DB978A0383743AC50D884
            0984E902B908E76565CA04D2C5009333030080467644800FCEF7E3393BB83A3B
            DB38DA3A7CB5A8FF1AFC8B888D8BFF973FAFC201010084D3F545FBB3BCAC1A00
            EE1800B6F18B96B41DA0650D80D6FD2F9AC91E00D54280E6AB5FCDC3E1FBF1F0
            548542E66667979B9B6B2B110B6D85A95FF5F99F097F015FF5B3E5FBF1F0DFD7
            83FB8A9305CA0C051E11E0830BB332B294723C5B26108A719B3F1EF1DF2EFCF3
            774C8B10278BE562A9508C474BC4B912690ACECB928A240A4996149748FF9389
            7FB3EC0F98BC6B0060D57E06F6425B50BBCA06EC972E20B0E88025EC0200E477
            DF82A9D110060031068393770F0030F99BFF1D681900A0D992141C0080171185
            0B95F29CC918010080083450053668833E188305D88023B8803B78811FCC8650
            88823858004248854C90432E2C8555500425B011B64215EC865AA8874638022D
            7002CEC205B802D7E0163C805E1880E7300A6F601C411032C244588836628098
            22D68823C24566217E48301281C42189480A224594C85264355282942355C85E
            A41EF91E398E9C452E213DC83DA40F19467E433EA018CA40D9A81E6A86DAA15C
            D41B0D42A3D0F9680ABA08CD470BD10D68255A831E429BD1B3E815F416DA8B3E
            47C730C0E8180733C46C302EC6C342B1782C199363CBB162AC02ABC11AB136AC
            13BB81F56223D87B0289C022E0041B823B2190309720242C222C279412AA0807
            08CD840EC20D421F6194F099C824EA12AD896E443E31969842CC2516112B8875
            C463C4F3C45BC401E21B1289C42199935C4881A438521A6909A994B493D4443A
            43EA21F593C6C864B236D99AEC410E250BC80A7211793BF910F934F93A7980FC
            8E42A718501C29FE94788A945240A9A01CA49CA25CA70C52C6A96A5453AA1B35
            942AA22EA696516BA96DD4ABD401EA384D9D664EF3A045D1D268AB6895B446DA
            79DA43DA2B3A9D6E4477A587D325F495F44AFA61FA457A1FFD3D438361C5E031
            12184AC606C67EC619C63DC62B269369C6F462C63315CC0DCC7AE639E663E63B
            15968AAD0A5F45A4B242A55AA559E5BACA0B55AAAAA9AAB7EA02D57CD50AD5A3
            AA575547D4A86A666A3C3581DA72B56AB5E36A77D4C6D459EA0EEAA1EA99EAA5
            EA07D52FA90F699035CC34FC34441A851AFB34CE69F4B33096318BC712B256B3
            6A59E759036C12DB9CCD67A7B14BD8DFB1BBD9A39A1A9A3334A335F334AB354F
            6AF672308E1987CFC9E094718E706E733E4CD19BE23D453C65FD94C629D7A7BC
            D59AAAE5A525D62AD66AD2BAA5F5411BD7F6D34ED7DEA4DDA2FD4887A063A513
            AE93ABB34BE7BCCEC854F654F7A9C2A9C5538F4CBDAF8BEA5AE946E82ED1DDA7
            DBA53BA6A7AF17A027D3DBAE774E6F449FA3EFA59FA6BF45FF94FEB001CB6096
            81C4608BC1698367B826EE8D67E09578073E6AA86B1868A834DC6BD86D386E64
            6E34D7A8C0A8C9E89131CD986B9C6CBCC5B8DD78D4C4C024C464A94983C97D53
            AA29D734D5749B69A7E95B3373B318B3B5662D6643E65AE67CF37CF306F38716
            4C0B4F8B45163516372D49965CCB74CB9D96D7AC502B27AB54AB6AABABD6A8B5
            B3B5C47AA775CF34E234D769D26935D3EED8306CBC6D726C1A6CFA6C39B6C1B6
            05B62DB62FEC4CECE2ED36D975DA7DB677B2CFB0AFB57FE0A0E130DBA1C0A1CD
            E137472B47A163B5E3CDE9CCE9FED3574C6F9DFE7286F50CF18C5D33EE3AB19C
            429CD63AB53B7D727671963B373A0FBB98B824BAEC70B9C36573C3B8A5DC8BAE
            44571FD715AE275CDFBB39BB29DC8EB8FDEA6EE39EEE7ED07D68A6F94CF1CCDA
            99FD1E461E028FBD1EBDB3F05989B3F6CCEAF534F41478D6783EF132F61279D5
            790D7A5B7AA7791FF27EE163EF23F739E6F396E7C65BC63BE38BF906F816FB76
            FB69F8CDF5ABF27BEC6FE49FE2DFE03F1AE014B024E04C203130287053E01DBE
            1E5FC8AFE78FCE7699BD6C764710232832A82AE849B055B03CB82D040D991DB2
            39E4E11CD339D2392DA110CA0FDD1CFA28CC3C6C51D88FE1A4F0B0F0EAF0A711
            0E114B233A2359910B230F46BE89F2892A8B7A30D762AE726E7BB46A7442747D
            F4DB18DF98F298DE58BBD865B157E274E22471ADF1E4F8E8F8BAF8B1797EF3B6
            CE1B48704A284AB83DDF7C7EDEFC4B0B7416642C38B95075A160E1D14462624C
            E2C1C48F8250418D602C899FB4236954C8136E133E177989B68886C51EE272F1
            60B2477279F2508A47CAE694E154CFD48AD411094F522579991698B63BED6D7A
            68FAFEF4898C988CA64C4A6662E671A986345DDA91A59F9597D523B39615C97A
            17B92DDABA68541E24AFCB46B2E767B72AD80A99A24B69A15CA3ECCB9995539D
            F32E373AF7689E7A9E34AF6BB1D5E2F58B07F3FDF3BF5D4258225CD2BED470E9
            AAA57DCBBC97ED5D8E2C4F5ADEBEC27845E18A8195012B0FACA2AD4A5FF55381
            7D4179C1EBD531ABDB0AF50A5716F6AF0958D350A452242FBAB3D67DEDEE7584
            759275DDEBA7AFDFBEFE73B1A8F872897D4945C9C75261E9E56F1CBEA9FC6662
            43F286EE32E7B25D1B491BA51B6F6FF2DC74A05CBD3CBFBC7F73C8E6E62DF896
            E22DAFB72EDC7AA96246C5EE6DB46DCA6DBD95C195ADDB4DB66FDCFEB12AB5EA
            56B54F75D30EDD1DEB77BCDD29DA797D97D7AEC6DD7ABB4B767FD823D973776F
            C0DEE61AB39A8A7DA47D39FB9ED646D7767ECBFDB6BE4EA7AEA4EED37EE9FEDE
            0311073AEA5DEAEB0FEA1E2C6B401B940DC387120E5DFBCEF7BBD6469BC6BD4D
            9CA692C3705879F8D9F789DFDF3E1274A4FD28F768E30FA63FEC38C63A56DC8C
            342F6E1E6D496DE96D8D6BED393EFB787B9B7BDBB11F6D7FDC7FC2F044F549CD
            9365A768A70A4F4D9CCE3F3D76467666E46CCAD9FEF685ED0FCEC59EBBD911DE
            D17D3EE8FCC50BFE17CE757A779EBEE871F1C425B74BC72F732FB75C71BED2DC
            E5D475EC27A79F8E753B77375F75B9DA7ACDF55A5BCFCC9E53D73DAF9FBDE17B
            E3C24DFECD2BB7E6DCEAB93DF7F6DD3B09777AEF8AEE0EDDCBB8F7F27ECEFDF1
            072B1F121F163F527B54F158F771CDCF963F37F53AF79EECF3EDEB7A12F9E441
            BFB0FFF93FB2FFF171A0F029F369C5A0C160FD90E3D08961FFE16BCFE63D1B78
            2E7B3E3E52F48BFA2F3B5E58BCF8E157AF5FBB466347075ECA5F4EFC56FA4AFB
            D5FED7335EB78F858D3D7E93F966FC6DF13BED7707DE73DF777E88F930389EFB
            91FCB1F293E5A7B6CF419F1F4E644E4CFC130398F3FC256333A2000000206348
            524D00007A25000080830000F9FF000080E9000075300000EA6000003A980000
            176F925FC54600000006624B4744000000000000F943BB7F0000000970485973
            00000B1300000B1301009A9C180000000976704167000000160000001600DCC5
            E958000003354944415438CBD5955D6C145518869F336CBB3B6B6B170BA54D35
            0197D0E0A2450998A82415684CAC1725E1D2129AB22046138CD1C4C4352473A1
            0951C01B8DDE803696F8174C3035919A68242244A285296CCBB4A5EDB6DB9FED
            76996D87999DCEF1A274718B8A117BE177752ECE79F27E3FEF77849492C50865
            51A88B09F601941F2EC609B9049C625C53429587EBBA040245AC2A5B4DDA4D91
            B54CAE5B167616C48D97391F8AD32CBDE7DBF6C8DCAC637FF8ECD1C01D2B9E8E
            4A9C66E9ED6B6D91EB2BD753BFF629FFC6D76B47EF083CBD6FAEE12F7DF282DC
            BAB69E53463BBB8FEFA26E635DC59A9655C6BF02CFDC80B61C6D924FD66EE1F3
            EE36CE5CFB09B3DCE174EA7B8A562AF7E76B3C1FE37BAEE7CFA51F895BA0E9A6
            39E8F60F1AE4EEFA286D978FF18BFB33A9E529EE75CA0AEE2A00A9FD0EE62E8F
            881696112D2C01CC9D85F33DB2630680ADEF3F21F736ECE58BC471BADC8B98A1
            34D5FE0AEE132BC9244C529D93A2A014112D2C5BA36D74BCF803F370A97800F4
            34CCF5E4F1F736D8AF36BEC6C9E409FA7357304BA6A8F255B32C5B452661A2C7
            8C7C9A424A49440BCB779A8E802748DAC33C56BD99C6230D0517235A581E8ABE
            4BC7E437C4A7BB18A00FD5BE0BD9BD844CC2C438DB2BC841A83444F2B3F44DC5
            9F0EB512B775826A906F47DB39F1CAD779E5112D2CDF8E1EE6CCCC8F0C33483A
            38C172B5220FEDBBDA2B982DEC8798DF15112D2CCB37975257B98DD5C11AA6BC
            49EA2B9F66C7C146DE6C3E488FD2458F7D8901D94F60498064472A9FBEBA5320
            2700EFA662F1C72514D1C272D933253C5CB6899AC00338C266C3DD8FD2699DE7
            8A1B6750F6E32FF6D3FBD520998449FAC11161EA166E9C5BC00AC0A6B7D651F9
            72995F8F1962E264965E5F37BF2AE7C815E7389F3B4BD237442A30C63D4B9732
            DC3D41266152B4CD5566AD05F92F1CB7B99A083B7840A87ACC10C6B121EC128B
            3E35CEB87F84943A4E281462D01863F4D4187ACC10AEE5FEEDBE2D70DECC0169
            01E831439C3BD4891A549956B3ACF0AF60BC7F8AAB5F0E50FB5C4DF9960B8FDC
            D6A17F69693D668876ED3BD6F110D7462D7EFBF8027ACC10DEAC37795B2A0B2C
            FD67F08816968AEA478F19E20D77FF3F61168EDB7F1DFFBFAFE977DDEB79952A
            7D0F6F00000025744558746372656174652D6461746500323030392D31312D32
            385431373A31383A32382D30373A30303191B22C000000257445587464617465
            3A63726561746500323031302D30322D32305432333A32363A31352D30373A30
            30063B5C810000002574455874646174653A6D6F6469667900323031302D3031
            2D31315430383A34343A30382D30373A30306E58A8AF00000035744558744C69
            63656E736500687474703A2F2F6372656174697665636F6D6D6F6E732E6F7267
            2F6C6963656E7365732F4C47504C2F322E312F3BC1B41800000025744558746D
            6F646966792D6461746500323030392D31312D32385431343A33313A35372D30
            373A30308B1E432D0000001674455874536F75726365004372797374616C2050
            726F6A656374EBE3E48B0000002774455874536F757263655F55524C00687474
            703A2F2F65766572616C646F2E636F6D2F6372797374616C2FA591935B000000
            0049454E44AE426082}
          TabOrder = 11
          OnClick = btnOKClick
        end
      end
    end
  end
  object DSDiagnosticQueries: TDataSource
    DataSet = DMDatabaseFixer.tabDiagnosticQry
    OnStateChange = DSDiagnosticQueriesStateChange
    OnDataChange = DSDiagnosticQueriesDataChange
    Left = 592
    Top = 312
  end
  object SynSQL: TSynSQLSyn
    Options.AutoDetectEnabled = False
    Options.AutoDetectLineLimit = 0
    Options.Visible = False
    CommentAttri.Foreground = clOlive
    DataTypeAttri.Foreground = clRed
    DataTypeAttri.Style = []
    KeyAttri.Foreground = clBlue
    KeyAttri.Style = []
    StringAttri.Foreground = clRed
    TableNameAttri.Foreground = clOlive
    Left = 240
    Top = 224
  end
  object memDiagnostic_Queries: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 764
    Top = 376
  end
  object DSMemDiagnostic_Queries: TDataSource
    DataSet = memDiagnostic_Queries
    Left = 596
    Top = 368
  end
  object GridImageList: TImageList
    Height = 24
    Width = 24
    Left = 480
    Top = 264
    Bitmap = {
      494C01010B001800040018001800FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000060000000480000000100200000000000006C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FAF2
      EF10F4E3DC23F3E2DA25F4E2DA25F7EAE31CFDFAF90600000000000000000000
      0000000000000000000000000000FEFCFB04FAEDE51AF8E6DA25F8E6DA25F9E7
      DC23FCF3ED12FEFEFE0100000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FAF4F20DD9A3
      857FCD8357C5D18757C8D28553C8D38757B6E7BDA55AFCF8F609000000000000
      00000000000000000000FDFAF906F0C9B14EE2894FB1E37D38C8E5813BC8E685
      3FC6ECA87888FCF5F00F00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C8C776858A88FF858A
      88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A
      88FF858A88FF909492E800000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F8EFEC13CC86
      60AED69872FFF2CDA9FFF5C794FFE8A468FFD2773EE0E1AE916EFAF3EF100000
      000000000000FCF6F30CEABB9E62DD7A37D9ED6D0EFFF8750FFFF88222FFEB77
      1DFFE5894BB9FBF0E91600000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000858A88FFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFBFB04F3E7
      E11EEFDDD52AF0DED52AF1DFD629F7EDE817FEFDFD0200000000000000000000
      0000000000000000000000000000FEFEFE01FCF1EA15FBE5D629FBE5D52AFCE5
      D52AFDECDF20FEFBF90600000000000000000000000000000000FEFCFC03E9CA
      BA45CC8661C3DEAB8BFCFADBB6FFFECC93FFEFA15DFFD37334E7DFA5837DF9EF
      EA15FAF2EE11E5B19271DB7C3CE0F17C25FFFE7D17FFFD8624FFED7C23FDE382
      3ECBF1C9AE51FEFBFA0500000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000858A88FFFFFFFFFFE2E5
      E4FFE5E8E7FFE8EAE8FFEAECEBFFEDEFEEFFEAECEBFFEDEFEEFFEFF1F0FFF1F3
      F3FFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EBD7CD32B971
      4FB2B05B31D1B55E30D1B95F2FD1CB855FA0F2E0D62900000000000000000000
      0000000000000000000000000000FAEAE01FEC9F6A95EA7C30D0EE7F30D1F381
      30D1F89247B9FDDCC43B0000000000000000000000000000000000000000FDFC
      FC03EACFC23DCB825DBAD99E79FCF8CFA6FFFFBF7EFFF19548FFD56E2BECDA98
      718FDD9F7B88D97A3EE8F2822FFFFF7D18FFFB8422FFE8761EFDDE8142C3F2CF
      B946FEFBFA050000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000858A88FFFFFFFFFFDEE2
      E0FFA8ABAAFFAAACABFFABAEADFFABAEADFFABAEADFFE8EBEAFFEBEDECFFEDEF
      EFFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D3A8936CB15B
      2DF9CF8149FFD47A38FFD4702AFFBF5716F9CD8B6699F7EEE916000000000000
      00000000000000000000FCF4EF10E8A3768AE46C18F5F18026FFF5892FFFF78A
      2FFFF67A1AFAFABB8E7100000000000000000000000000000000000000000000
      0000FEFEFE01EFDAD12ECD8965A9D5926BF6F6C79BFFFFB36BFFF48932FFD965
      15F9D96211F8F4771BFFFF7D18FFF98120FFE2721EF9DC864EB5F3D9C936FEFD
      FD02000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000858A88FFFFFFFFFFDBDE
      DDFFDDE1DFFFDFE3E2FFE2E5E4FFE5E7E6FFE2E5E4FFE5E7E6FFE7E9E9FFE9EB
      EBFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DBB7A857B566
      3DECE8B283FFFFC180FFFEAC60FFE8873AFFBE591EEED7A48877FBF6F30C0000
      000000000000FDF9F708E9B69768DC7129E9F3862DFFFF993BFFFFA84FFFFA9E
      47FFF17C23F1F9C49F6000000000000000000000000000000000000000000000
      00000000000000000000F2E1D926D2937498D18960F1F3BD8EFFFFA85CFFFB7D
      1BFFFA7108FFFF780FFFF77E1DFFDD6D1EF5DC8F5EA4F4DED12EFEFEFE010000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000858A88FFFFFFFFFFD7DA
      D9FFA3A6A5FFA5A8A7FFA6A9A8FFA7ABA9FFA6A9A8FFA7ABA9FFA9ACABFFE6E8
      E7FFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F5EBE718BF80
      639FBC7149FAF2C397FFFFC07FFFFDAB5EFFDE7B32FFBB5C26DFE2BCA857FEFC
      FC03FEFEFE01ECCAB649D27338D5EA8030FFFE8724FFFF8B28FFFC9336FFEC78
      1EFDEE9253AEFCEBE01F00000000000000000000000000000000000000000000
      0000000000000000000000000000F5E8E31CD59A7C89CD8154EEEFB482FFFFA1
      51FFFF7E17FFF37A1BFFD86A1EF2DB956A96F6E5DB2400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000858A88FFFFFFFFFFD2D7
      D5FFD5D9D7FFD8DBDAFFDADEDCFFDCDFDEFFDADEDCFFDCDFDEFFDFE2E1FFE2E4
      E3FFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000ECD9
      D12EB76F4EBAC6835FFDF8CEA2FFFFC07FFFFBA85CFFD36F29FFBE6838CAEBD1
      C43BF0DACE31CE7B4BBFDE7A37FEFC8121FFFF780EFFFD7E18FFEA6F13FEE580
      3CC6F8DAC6390000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F1EE11D9A48979CC7D4FE4EAAB
      77FFEE9449FFD26A23EADC9F7C84F8EDE8170000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C8
      C776858A88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A
      88FF858A88FF858A88FF858A88FF878C8AFFA1A5A3FFA3A6A5FFA4A8A6FFDEE1
      DFFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFD
      FD02E1C4B748B1623FD3D39D7CFFFBD2A8FFFFBF7DFFF5A156FFC86524FBC173
      48B8C87C53B0D07238F9F78430FFFF7105FFFD7207FFEB6909FFDA6D23DDF1C6
      AA55FEFCFB040000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FAF3F00FDEB0976ACB7B
      4DD5CE7B48DADEA98B76F9F0EC13000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF858A88FFDADEDCFFDCDFDEFFDFE2E1FFE2E4
      E3FFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCF9F807D5AC9B64AF5F3BE9DFB498FFFED7ACFFFFBD79FFEE9950FFBE58
      1AFCBE5212FCEE7B27FFFF740CFFFF6E00FFEE6603FFD25D10EFE6AE8B74FDF7
      F40B000000000000000000000000000000000000000000000000000000000000
      0000FDFCFC03FDFCFC03FDFCFC03FEFEFE010000000000000000FBF7F50AEBCF
      C23DEACCBD42FBF5F20D0000000000000000FEFEFE01FEFCFC03FEFCFC03FEFC
      FC0300000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFE2E5E4FFE5E8E7FFE8EAE8FFEAECEBFFEDEFEEFFEAECEBFFEDEF
      EEFFEFF1F0FFF1F3F3FFFFFFFFFF858A88FFA1A5A3FFA3A6A5FFA4A8A6FFDEE1
      DFFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F9F3F10EC8927A85B46947F2E9C8AFFFFFD7AAFFFEBB77FFF09B
      51FFEF8937FFFE8626FFFF780FFFF26702FFCF5709F6DA956B94FAF1EC130000
      0000000000000000000000000000000000000000000000000000FDFBFB04E8D2
      C738D4A8936DD2A38D73D4A48D73DCB6A35CF4E9E41B00000000000000000000
      0000000000000000000000000000F8EEE916E6BEA857E0AC8D72E1AC8C73E3B0
      916EF0D2C13EFDFBFA050000000000000000000000000000000000000000858A
      88FFFFFFFFFFDEE2E0FFA8ABAAFFAAACABFFABAEADFFABAEADFFABAEADFFE8EB
      EAFFEBEDECFFEDEFEFFFFFFFFFFF858A88FFD2D6D4FFD5D9D7FFD7DAD9FFD9DD
      DBFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2E6E11EBD7B5EA5BC785AFAF3D8C3FFFFD4A5FFFFBB
      75FFFFA95AFFFF9841FFF78024FFCE5708FCCD7C4DB2F4E1D728000000000000
      0000000000000000000000000000000000000000000000000000F7EEEC13C181
      61A9BF7D52F6CD9162F9CE8957F9C47846EBCA8C6B9BEFDED728FEFEFE010000
      00000000000000000000F4E6DF20D6977290D1641CE8DB600BF9DD6A16F9D769
      1BF7D6834FB4F9EEE9160000000000000000000000000000000000000000858A
      88FFFFFFFFFFDBDEDDFFDDE1DFFFDFE3E2FFE2E5E4FFE5E7E6FFE2E5E4FFE5E7
      E6FFE7E9E9FFE9EBEBFFFFFFFFFF858A88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF858A88FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFE01E9D3CA35B46948C1C78F75FFF8E2CCFFFFD1
      9FFFFFB973FFFAA456FFD36B22FFBF6735CCEACEBF40FEFEFE01000000000000
      0000000000000000000000000000000000000000000000000000FAF4F30CCC97
      7D8AC68B64F2EED8B0FFFDE0AFFFF2BD82FFCD7A41F9C47E5AACEBD7CD320000
      000000000000F1DFD629D08D699FD36C27F5F47514FFFD7F19FFF48528FFD86E
      21F6DA956A97FBF4F00F0000000000000000000000000000000000000000858A
      88FFFFFFFFFFD7DAD9FFA3A6A5FFA5A8A7FFA6A9A8FFA7ABA9FFA6A9A8FFA7AB
      A9FFA9ACABFFE6E8E7FFFFFFFFFF858A88FF858A88FF858A88FF858A88FF858A
      88FF858A88FFA3A7A5BF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FEFDFD02DFC0B44BAF5F3CD9D3A590FFFCE8
      D2FFFECE98FFDA8A4BFFB45826E3DEB8A659FEFCFC0300000000000000000000
      000000000000000000000000000000000000000000000000000000000000F5EA
      E619CD988187C3855FECECD0A6FFFFD7A0FFF8B36FFFD27635FCC1754BBCE7CC
      C03FEAD3C738CA845FB1D57432F9F87E20FFFF811BFFF28226FFD36B23F1D895
      6D94F6E7DF20FEFEFE010000000000000000000000000000000000000000858A
      88FFFFFFFFFFD2D7D5FFD5D9D7FFD8DBDAFFDADEDCFFDCDFDEFFDADEDCFFDCDF
      DEFFDFE2E1FFE2E4E3FFFFFFFFFF858A88FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBF8F708D2A5926DB0603FE9E0BF
      AFFFE7C0A4FFB35E30EFCD9B847BFAF5F30C0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F6EDEA15D0A08B79BF7E57E6E6C296FFFECE92FFF9A456FFD77128FCBF67
      36CFC16B3CCAD87230FAF97F23FFFE7F19FFED7C22FFCE6825EBD99E7D84F7EB
      E51A00000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFCED3D1FF9EA2A0FFA0A4A1FFA1A5A3FFA3A6A5FFA1A5A3FFA3A6
      A5FFA4A8A6FFDEE1DFFFFFFFFFFF858A88FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7EFEC13C68E758BB062
      3EF6B36843FAC285699AF4EAE619000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F9F3F10ED5A9966BBE7A55DDE1B88BFFFEC888FFFB9640FFE46C
      15FFE2640BFFFA7511FFFE7F19FFE8771FFFC96728E4DAA58877F9F1ED120000
      000000000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFD2D7D5FFD5D9D7FFD8DBDAFFDADEDCFFDCDFDEFFDADEDCFFDCDF
      DEFFDFE2E1FFE2E4E3FFFFFFFFFF858A88FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F1E4DE21C184
      6798BD7D5EA1EEDDD52A00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FBF8F708DBB6A659BB7653D1DAAB7DFEFBC07EFFFF8C
      2CFFFF7409FFFD7C16FFE1701BFFC46830D9DDB29B64FAF5F30C000000000000
      000000000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFCED3D1FF9EA2A0FFA0A4A1FFA1A5A3FFA3A6A5FFA1A5A3FFA3A6
      A5FFA4A8A6FFDEE1DFFFFFFFFFFF858A88FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01F6EE
      EA15F5ECE817FEFDFD0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FCF9F807E1C2B54ABC7957C5D49F70FCF9BC
      78FFFB9035FFDB6C19FDC16B38CEE0BBA956FBF8F60900000000000000000000
      000000000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFCACFCEFFCDD1CFFFCFD4D2FFD2D6D4FFD5D9D7FFD2D6D4FFD5D9
      D7FFD7DAD9FFD9DDDBFFFFFFFFFF858A88FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFCFC03E5CBC13EBD7A59BBCD91
      61FBD38A50FDBF7044C4E3C6B847FDFBFB040000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000858A
      88FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF858A88FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE01EBD7CF30C58B
      6D99C48869A0E8D1C738FEFEFE01000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C1C3
      C281858A88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A88FF858A
      88FF858A88FF858A88FF858A88FFA3A7A5BF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01F7F0
      ED12F6EEEB14FEFDFD0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B2B6B252808B819A00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F6F6F609E9E9E916E1E1
      E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1
      E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1E11EE1E1
      E11EE1E1E11EE7E7E718F5F5F50A000000000000000000000000000000000000
      00000000000000000000B9BFB9473D7B40FF59865CB700000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFDFD02FBF0
      E916FBEFE718FEFDFC0300000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009090909B5D5D5DF35656
      56F6575757F8585858F8585858F8575857F8575757F8585858F7565656F65656
      55F6555555F6555555F6555555F6555555F6555554F6555554F6555454F65454
      54F6545454F6575757F29B9B9B88000000000000000000000000000000000000
      000000000000B9C4C047417640F6419143FF598D5DB400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE01F5D7C639E89A
      65A4EA9A63AAF6D4BD42FEFDFC03000000000000000000000000000000000000
      00000000000000000000000000000000000000000000636363FBCDCECEFF686A
      69FF969796FF9F9F9FFF919291FF858584FF7F807FFF787977FF767776FF7778
      76FF767774FF73736FFF6E6E6AFF636360FF5A5A56FF575652FF565551FF5554
      50FF55534FFF575652FF5E5E5EFB0000000000000000FEFEFE01FEFEFE010000
      0000BACDD947417845F6418B42FF3A833CFF59935EB400000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFCFB04F0CDB946E08D56C3ECA6
      66FDF19B50FEE9833DCCF4C9AD52FEFBF9060000000000000000000000000000
      000000000000000000000000000000000000000000005F6060F9C1C2C2FF5256
      54FF5D5E5DFF616261FF676767FF6A6B6AFF696A69FF696A68FF70716EFF7B7C
      79FF848482FF858683FF787874FF595955FF42413CFF3C3B36FF3B3934FF3938
      32FF3D3B35FF484640FF5D5D5DFF00000000000000000000000000000000B8C6
      CE48437741F6448B44FF3A813CFF348336FF52A056E87CBF829478C37F9778C7
      809778CA809777CB7F9777CB7F9776C97E9775C57C9773BF7A9779BB7E9079B5
      7E8F78AC7C8F80A68493BCCBBE4D000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01FDF0
      E817FDEFE619FEFDFD0200000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FCF7F50AEAC2AB54DA8954CEECAF75FDFDBB
      74FFFE8D2FFFF37819FEE97D31D7F2C09F60FDF6F20D00000000000000000000
      00000000000000000000000000000000000000000000565656FAD5D6D6FFF7F7
      F7FFF5F5F5FFF6F6F6FFF6F7F6FFF7F7F7FFF8F8F8FFF6F6F6FFF3F3F3FFEFF0
      EFFFEFEFEFFFF1F1F0FFEBEBEBFFDFDFDEFFD3D3D1FFCACAC9FFC2C1C0FFB9B8
      B7FFB2B2B0FF9C9B99FF575757FF000000000000000000000000BDCCD2483F75
      40F6478C46FF3D823FFF388B3AFF339E35FF34AF37FF34BB39FF30C737FF2CD1
      36FF28D934FF23DB30FF1EDB2DFF19D626FF13CD1FFF0DBE17FF0BAA10FF0A8F
      0DFF08780AFF1E7925FF6D9A729F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FBE7DA25F09B
      619EF19758A7FBE2D02F00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FBF6F40BE4B69D63D38451D9ECB983FFFEBE79FFFF88
      28FFFF7308FFFE7C14FFF67C1CFFE97929E1F1B78F70FDF5F00F000000000000
      00000000000000000000000000000000000000000000606261FCBDBDBDFFBCBC
      BCFFC7C7C7FFC9C9C9FFCACACAFFCBCBCBFFCBCBCBFFC8C8C8FFC2C2C2FFB4B4
      B4FFB4B4B4FFC6C6C6FFCBCBCBFFC2C2C2FFC0C0C0FFC6C6C6FFCCCCCCFFD3D3
      D3FFBDBDBDFFBCBCBCFF5D5D5CF80000000000000000A2B5B561427343F44D8F
      4BFF438444FF3E8F3FFF38A53AFF32B436FF2CBF33FF25C830FF20D02DFF1AD8
      29FF15DE26FF10DF21FF0CDD1EFF08D518FF02CA11FF00B90BFF00A505FF0083
      03FF006001FF136E1BFF6AA2709F000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FCF1EA15ECA16D92EA74
      1FF8EF7A21FBF29D60A1FCEDE31C000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F9F1EE11DBA68B77D08655E4EDC390FFFFC582FFFC943DFFEC70
      15FFEC690BFFFB7612FFFF7E17FFF7801FFFEA7825E9EFAC7E82FCF0E9160000
      000000000000000000000000000000000000000000008F9191C5DCDCDCFFDDDD
      DDFFC8C8C8FFCACACAFFCBCBCBFFD3D3D3FFDBDBDBFFC3C3C3FF979D9AFF2C7D
      56FF217B4FFF8A9791FFBDBDBDFFD6D6D6FFCACACAFFBEBEBEFFC6C6C6FFCDCD
      CDFFDEDEDEFFA7A7A7FF929292B20000000000000000E0EAF12079A29B9E327B
      2AFF218127FF219F29FF27B530FF2EC539FF38DA45FF43E951FF48F158FF4FF6
      5EFF50F760FF4CF75CFF41F452FF35EC46FF29DE39FF1ECF2CFF11B51CFF039A
      0AFF007502FF14711CFF6AA8729F000000000000000000000000000000000000
      000000000000000000000000000000000000FDF8F609ECB18C73E3762AEBF6A1
      56FFFA9843FFED6F14F1F4AE7E81FDF6F10E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F5E9E51AD39C8284CD895BEBEEC999FFFFCC8EFFFAA254FFE17627FBD679
      3EC6DA7F45C0E57A30F8FB8125FFFF7E17FFF98222FFEC7822EFEEA4728FFBE9
      DE210000000000000000000000000000000000000000CBCCCC56A9AAAAFCF3F3
      F3FFCCCCCCFFBFBFBFFFCACACAFFC0C0C0FF979797FF6D7D76FF19784AFF3996
      6EFF429C77FF127847FF5E776BFF898989FFB1B1B1FFD5D5D5FFBEBDBDFFCACA
      CAFFE7E7E7FF727272FBD2D2D2400000000000000000F3F7FB0CE4EEF71D619F
      9BA3078F09FF08AD17FF24C533FF3CE04BFF57D06AFA65E074E777EE83E785EF
      91E78BF196E788F194E77DF089E76EEF78E762E66EE650BA63F249D25EFF10B2
      1CFF008E01FF157D1CFF6AB0729F000000000000000000000000000000000000
      0000000000000000000000000000FEFDFD02EFC7AF50DC732EDEEE9A53FFFEB4
      6BFFFF8626FFF66B04FFEC7119E6F6C3A05FFEFBFA0500000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01F1E4
      DF20CC937992CC8F65F0F2D5A9FFFFD69EFFF7B16CFFD97934F9D38454B1F0D6
      C837F4DDD02FDF9568A5E37B33F6F97F20FFFF8019FFFB8625FFED7921F4EE9C
      639FFAE5D728FEFEFE01000000000000000000000000F5F5F50B818584F8FCFC
      FCFFC8C8C8FF91918EFFB1B1B1FFA0A0A0FF6F8E7FFF157A48FF48A380FF5FB5
      98FF5AB395FF52A987FF157A4AFF59836EFF909090FF9D9D9DFFA4A4A2FFB0B0
      AFFFE6E6E6FF6A6A6AF0FBFBFB040000000000000000DCE8F425C1D8EE45C4DE
      FA3B60B6A8AF25BF28FF38D947FF56F665FF73DD7FBBFCFEFC03F2FCF30FF2FC
      F30FF3FCF40FF3FCF30FF2FCF210F8FDF908FCFEFC03C1E8C54A57DD6AFF21C4
      2EFF009E03FF158F1CFF6BB8739F000000000000000000000000000000000000
      00000000000000000000FEFEFE01F2D6C639D77A40C6E58F4DFFFCBE81FFFF9E
      4AFFFF7E1AFFFE750DFFF46903FFEC7D2ED1F9D4BA45FEFDFD02000000000000
      0000000000000000000000000000000000000000000000000000F9F3F10EC890
      7493C68B64F7F0D9B0FFFADCAAFFF0B97EFFCF7A40F5D08A63A1F2DFD52A0000
      000000000000F7E6DD22E19C7294E07428F2F57512FFFD7D16FFFA8727FFED77
      1EFAEE9B639FFDF3EE11000000000000000000000000FCFCFC03A5A8A7B9EAEA
      EAFFE0E0E0FFBCBCBCFFC8C8C8FF6FA189FF1C7F4EFF59AF8FFF58B394FF32A2
      7DFF31A17CFF53B191FF5FB395FF208354FF4F8E6FFFAEAFAFFFB5B5B5FFB5B4
      B3FFC6C6C6FFA3A5A599FDFDFD020000000000000000CBDDEF379EC4E66C99C8
      F766AED6FF5F77CBA9BE4BE751FF73FF80FF89EB93B400000000000000000000
      0000000000000000000000000000E2EFFE22FAFCFD05DBF8DD2D68EB78EF40D6
      4DE916AF1BE91D9E24F677C57F92000000000000000000000000000000000000
      00000000000000000000F6E6DD22D78759AADA8042FBF9C38FFFFFB36EFFFF94
      3BFFFF8829FFFF7F1AFFFD750DFFF06703FDEE8C48B7FBE3D32C000000000000
      0000000000000000000000000000000000000000000000000000F8F0ED12C788
      65A4C07D53EDCA8C60EFCB8756EFC57947E2D1947590F4E6DF20000000000000
      00000000000000000000F9EDE619E4A47C85DE6F24DFE56B14EFE8741EEFE777
      22EDEA9355ADFCF1EA15000000000000000000000000FEFEFE01D5D7D750C1C3
      C1FBF9F9F9FFC1C0BFFF589D7AFF28885CFF67B89BFF51B191FF34A57FFF34A5
      7FFF34A57FFF34A57FFF4BAF8DFF63B89BFF2B8B61FF3A8A63FFA7A7A6FFDADA
      DAFF8B8D8CFAE1E2E130FEFEFE010000000000000000B8D1E84B7AB1DF9371B4
      F28E86C3FF8CB0D7FF8773D3A2AF91FF92FF9CF0A4B500000000000000000000
      000000000000000000000000000083B6F8A976B1FDB1E1F5EC239EFAA2A77DE9
      87A759CA5EA65DBE63B09ED9A468000000000000000000000000000000000000
      000000000000FAF3EF10D898748BD0743BF4F3C093FFFFC790FFFFA759FFF68D
      38FFF5832AFFFE8727FFFF7E1AFFFB720CFFED6808F8F09D649BFDF1E9160000
      0000000000000000000000000000000000000000000000000000FEFCFC03EFDC
      D22DDDB8A659D9B2A15EDBB4A25DE2C3B44BF7EEEA1500000000000000000000
      0000FFFEFE010000000000000000FAF2ED12EFCDB748ECBEA15EEEBFA15EF1C3
      A55AF7DDCC33FEFCFB0400000000000000000000000000000000F7F7F70A9DA3
      A1F6FAFAFAFFE1E1E1FF107845FF79C3AAFF81C8B0FF7FC8AFFF57B696FF38A9
      83FF38A983FF4EB291FF74C3A8FF74C2A7FF6FC0A4FF0F7946FFB9BDBBFFC9C9
      C9FF898D8AE4FCFCFC03000000000000000000000000A1C1DE645399D5BF469E
      EBB954AAFEB982C1FFC0D1EAF5329DE7A09187D691BA00000000000000000000
      00000000000000000000000000008DC7FFA2A1CDFFFF68B7D5BAC6FBCB64B3F5
      B86695DE996A98D79C70C2E8C642000000000000000000000000000000000000
      0000FCF8F708DDAD956AC56936ECEBB891FFFFD8ADFFFFBC7AFFF49B4FFFD765
      19FBD86212FAF38630FFFF8B2DFFFF7D19FFF8700BFFEB6A0FF1F3B28679FDF6
      F20D000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE01FEFEFE01FEFEFE01000000000000000000000000FFF7F20DFFD2
      B44BFFD0AF50FFF5EE11000000000000000000000000FEFEFE01FEFEFE01FEFE
      FE01000000000000000000000000000000000000000000000000FCFCFC03BABE
      BCB0E7E7E7FECCCCCCFF328B60FF0F7945FF0F7945FF187F4DFF6FC1A6FF3DAC
      87FF3CAC87FF63BD9FFF1F8453FF0F7945FF0E7945FF218152FFBDBEBEFFA8AA
      A9FBBEBEBE82FEFEFE0100000000000000000000000085ACD082277EC8F01883
      E0E71C8DF8E749A7FFF2BCDCFF4300000000E7F9E91F00000000000000000000
      00000000000000000000000000007CBDFCA182C1FFFF3A9BFAFF6FC4C3BFD9FA
      DC33CDF0CE32C9EACC3AE0F3E22200000000000000000000000000000000FDFC
      FC03E4C2B24DBF693CD7DFA985FFFDE1C2FFFFD19BFFF7B26FFFD8732DFBD883
      50B1DD8B57A9DD7229F8F79443FFFF892AFFFE7D19FFF56E0AFFE9731FE0F6C6
      A55AFEFBFA050000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEF3EC13FFBC8D75FF97
      49DEFF9544E3FFB78280FFF1E817000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFE01E0E2
      E04CC3C6C5F7E7E7E7FFC4C4C4FFEEEEEEFFF6F6F6FF147C49FF6ABFA2FF41B1
      8CFF40B08BFF61BE9FFF107945FFC4C8C6FFC7C7C7FFBDBDBDFFBFBFBFFF9195
      93F9EDEEEE220000000000000000000000000000000077A0C5901973C2FF0073
      D7FF0078E9FF2B97FAFFA6CAF45D00000000FAFCFE05F4F9FF0BF4F9FF0BF4F9
      FF0BF4F9FF0BF4F9FE0B0000000070ACEFA951AAFFFF0F88FDFF0A73DDFF66AC
      ABB1F5FBF50AF5FBF60AF6FCF70900000000000000000000000000000000EBD5
      CC33BD714BBFD09270FEFAE7D2FFFFE3BAFFFBCC93FFDB8544FECE743FC6F1D7
      C837F5E0D32CDA814ABAE27E38FEFC9A4AFFFF8727FFFE7C17FFF16B08FEEB80
      35CAF8D7C13EFEFEFE0100000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDF0E817FBB28183FE9A4FEAFFB7
      78FFFF9C48FFFF8120EEFFAE728EFFEDE11E0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBFB
      FB06B6BCB9F2E9E9E9FFF4F4F4FFF7F7F7FFF6F6F6FF147C49FF65BFA0FF44B4
      8EFF43B48EFF5CBE9DFF107945FFD7DBD9FFD8D8D8FFBDBDBDFFB2B3B3FFA9AD
      ADD3FEFEFE0100000000000000000000000000000000789CBF901A70C1FF006E
      D4FF006ED5FF1985E9FF3387E9F14DA4F9DF5FACFFE172B9FFE17FBFFFE181C0
      FFE17CBDFEE172B7FBE163ACF4E04C97EEF7188CFFFF0073E9FF005FBFFF0849
      9CFF60958DA8FCFDFC0300000000000000000000000000000000F4E9E51ABF7D
      5FA5C27F5FFBF4E5D9FFFFF3D8FFFDE3B5FFDF9E66FFC56730DBE8C2AE51FEFD
      FD02FEFEFE01F0CFBB44D6763AD1E88D4CFFFD9C4BFFFF8727FFFC7B17FFED69
      08FDEC8D4CB3FBE9DD2200000000000000000000000000000000000000000000
      0000000000000000000000000000FCE8DC23F6A77395F89B57F2FEBC82FFFF9B
      49FFFF7B13FFFF801BFFFF7E1BF6FFA35FA2FFE6D42B00000000000000000000
      0000000000000000000000000000000000000000000000000000FBFDFC04F6F9
      F709D8DEDB70A0A9A4FBB2BBB8FCB0BBB6FCB0BAB5FC127B47FF60C09FFF48B8
      92FF47B892FF59BF9DFF0F7845FFA0ABA7FCA2ABA7FC9FA9A4FC939C98F9DEE4
      E148F7FBF908FAFCFB050000000000000000000000007A9BB990347ECAFF207A
      D8FF1A7EDAFF1381E0FF1392FAFF2BA1FFFF4FB0FFFF67BBFFFF75C0FFFF77C1
      FFFF6FBDFFFF5AB4FFFF3BA6FFFF1292FFFF007CF0FF006CD2FF005CB3FF0048
      91FF083F8BFF629091ADD9EBDC29000000000000000000000000DAB6A659B76D
      4BEEEAD4C9FFFFFEF0FFFEF7D4FFE6BC8EFFC06733ECDBA98E71FCF7F50A0000
      000000000000FDFAF807E9BA9D62D67334E6EE914CFFFE9138FFFF8625FFF978
      16FFEA6B12F2F3C09D6200000000000000000000000000000000000000000000
      000000000000FEFEFE01F9E1D22DF09F6AA4F4A164F4FDC38DFFFFA456FFFE7E
      1AFFFF7209FFFF770EFFFF821CFFFF811DF7FF9D54AFFFDFC936FFFDFD020000
      0000000000000000000000000000000000000000000087BDA380157D49FA197D
      4CFC197D4CFC197D4CFC197D4CFC187D4CFC187D4CFC25875AF763C3A3FF51BD
      99FF51BD99FF5AC19FFF278A5BF5177C4BFC177C4BFC177C4BFC177C4BFC177C
      4BFC177C4BFC147C48FB70B0929700000000000000007D99B2905795D6FF4E93
      E6FF4898E7FF43A3F0FF3DAAF7FF39B0FEFF46B6FFFF52BAFFFF54BAFFFF4DB7
      FFFF3AAFFFFF26A4FEFF1699FAFF0F8DEFFF0E80DFFF0B71CEFF0860B8FF0553
      A8FF0453ADFF154582FE92ABA375000000000000000000000000D4A9966AB164
      3EF8CC997EFFCF9E7AFFCF986DFFBB6836F7CD8E6D93F8F0EC13000000000000
      00000000000000000000FBF5F10EE3A37C84D6671DF3E6731EFFEA7119FFEC6D
      11FFE5660BF9F1B78F7000000000000000000000000000000000000000000000
      0000FEFEFE01F6D8C837EA955EB5F1A670F9FDCA99FFFFB068FFFC8C31FFF877
      16F7FA7614F6FE7E1FFFFF7C18FFFF831EFFFF811DFBFF9546BEFFDAC03FFFFD
      FC030000000000000000000000000000000000000000177E4CF5EBF7F2FFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFEFEFEFFF8F8F8FFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFE
      FEFFFEFEFEFFEFF8F5FF147C48FB00000000000000008398AC8F83B3E4FF88C1
      FEFF80BFFAFF7CC3FCFF77C7FFFF72CAFFFF70CCFFFF72CCFFFF6FCBFFFF65C7
      FFFF5ABFFFFF53B6FDFF50ACF4FF489FEAFF3A8CDAFF357FCDFF3074C2FF3076
      C8FF285998FCAEC6B65700000000000000000000000000000000ECDAD12EBA74
      53AEAC5A33D1AD5A32D1AF5B33D0C686659AF1E2DA2500000000000000000000
      0000000000000000000000000000F8EBE31CE09B708FD67130CFD9722FD1DC74
      2ED1E4884AB5F7DBC8370000000000000000000000000000000000000000FEFC
      FB04F1CDB946E38F5CC2EFAD80FDFDD1A5FFFFBD7CFFF99746FFF2802CE8F7AE
      7986FAB5847EFA8F42E2FE882FFFFF7C18FFFF8420FFFF831FFEFF903CCBFFD0
      AE51FFFAF80700000000000000000000000000000000107A45FBFAFDFCFFB8BF
      BCFFB6BDBAFFB6BDBAFFB6BDBAFFB6BDBAFFB6BDBAFFB6BDBAFFB6BDBAFFB6BD
      BAFFFCFCFCFF3C3A34FFF8F8F7FFFEFEFEFFFEFEFEFFFEFEFEFFFEFEFEFFFEFE
      FEFFFEFEFEFFFEFEFEFF177D4BFC0000000000000000C6CCD34298A9B99489A2
      BA958AA7C0948AA9C69489ACCB9489AECF9488B0D29485AFD39782AED39981AD
      D29981ABD09980A9CD9982A7C8946E9CC7DF69A2E3FF619ADBFF649EE3FF4C76
      AEFCABC3B1560000000000000000000000000000000000000000FDFCFC03F3E8
      E31CEEDDD52AEEDDD52AEEDED629F7EEEA15FEFEFE0100000000000000000000
      000000000000000000000000000000000000FAF1EC13F6E3D728F6E2D52AF7E2
      D52AFAEAE01FFEFCFB0400000000000000000000000000000000FEFCFB04EEC8
      B34EDF8F62CBEEB891FDFDDDB7FFFECB91FFF5A25AFFEC8237E1F4B78D73FDF5
      EF10FEF7F30CFBC39C66FA8F42D9FE8225FFFF7C16FFFF8622FFFF8623FEFF8D
      37D4FFCBA55AFFFBF906000000000000000000000000107A45FBF3F6F5FFBAC1
      BEFFB8BFBCFFB8BFBCFFB8BFBCFFB8BFBCFFB8BFBCFFB8BFBCFFB8BFBCFFB8BF
      BCFFF5F5F5FF42403AFFF1F1F1FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
      F7FFF7F7F7FFF7F7F7FF177D4CFC000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007994B0A299C3F5FF97C3F8FF6F92BDFCADCF
      B55700000000FEFEFE01FEFEFE01000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F9EFEB14D98B
      5DB1E29D71FFF3C8A0FFF5C28BFFEFA462FEE68340DAF2BE9C63FDF7F30C0000
      000000000000FEFAF708FCCCAA56FB8D3BD2FD750DFEFF760DFFFF841FFFFF82
      1DFFFF9648BCFFF1E8170000000000000000000000001B804FF2D3E6DFFFE9ED
      ECFFE9EDECFFE9EDECFFE9EDECFFE9EDECFFE8ECEBFFE8ECEBFFE8ECEBFFE8EC
      EBFFE8ECEBFFE8ECEBFFE8ECEBFFE8ECEBFFE8ECEBFFE8ECEBFFE8ECEBFFE8EC
      EBFFE8ECEBFFD8E8E2FF147C48FA000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008196A9A2D4EFFFFF93ABC7FCACB3B6560000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCF6F40BE3AE
      8E75D88856BFDB8A53C3DD8A4FC4E2925DADF2CBB24DFEFAF906000000000000
      00000000000000000000FEFCFB04FDD7BC43FB9D57A8FE8B35CAFF8E39C9FF94
      43C1FFB7827EFFF7F20D0000000000000000000000009BC8B16B1B804FF2107A
      45FB107A45FB107A45FB107A45FB107A45FB107A45FB107A45FB107A45FB107A
      45FB107A45FB107A45FB107A45FB107A45FB107A45FB107A45FB107A45FB107A
      45FB107A45FB187F4CF587BDA37F000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008C97A1A4C0CBD5FFADB0B456000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBF6
      F30CF7E7DF20F7E6DD22F7E6DD22FAEEE718FEFCFC0300000000000000000000
      0000000000000000000000000000FEFDFC03FEF1E817FEE9DA25FFE9DA25FFEB
      DE21FFF6F10E0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008E91958BA9AAAB6200000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBFB
      FD045555C2AC5555C2ACFBFBFD040000000000000000FBFBFD045555C2AC5353
      C2AD5353C2AD5555C2ACFBFBFD040000000000000000FBFBFD045555C2AC5555
      C2ACFBFBFD040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009DC6A17035973BFE86B88A8A000000000000
      00009DC6A17035973BFE86B88A8A000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FBFBFD044D4D
      C0B61C1CB2F51C1CB2F54D4DC0B6FBFBFD04FBFBFD044D4DC0B61C1CB2F51414
      AEFC0C0CAAFC1B1BB2F54D4DC0B6FBFBFD04FBFBFD044D4DC0B61C1CB2F51C1C
      B2F54D4DC0B6FBFBFD0400000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A5C9A86545B850FF81FF8DFF3DAD45FF96CC9B7DA5C9
      A86545B850FF81FF8DFF3DAD45FF96CC9B7D0000000000000000000000000000
      00000000000000000000000000000000000000000000000000005B5BC5AC2121
      B5F53B3BC8FF3B3BC8FF2121B5F55050C3B65959C5AD2121B5F53B3BC8FF3B3B
      C8FF1919B3FF1818B3FF2020B5F55959C5AD5050C3B62121B5F53C3CC8FF3B3B
      C8FF2121B5F55B5BC5AC00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADCEAF5D33B03EFD62FD71FF66FC74FF5DFA6AFF2C9F35FF31AF
      3CFE62FD71FF66FC74FF5DFA6AFF35B53EFF99D19F7C00000000000000000000
      00000000000000000000000000000000000000000000000000005D5DC7AC2525
      B7F54545CDFF4141CCFF4646CDFF2525B7F52424B3E71E1EB4FC4545CDFF4141
      CCFF4646CDFF1E1EB5FF1414AEFC2424B3E72525B7F54646CDFF4242CCFF4545
      CDFF2525B7F55D5DC7AC00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009FCAA26227A733FE58EC66FF83F08CFF8BF595FF38C345FF27A733FF58EC
      66FF83F08CFF8BF595FF58F467FF4BEE59FF299331FF91D4977C000000000000
      0000000000000000000000000000000000000000000000000000FBFBFD045555
      C5B62A2ABBF55050D1FF4A4AD0FF5050D1FF2E2EBEF31919B2FC2323B9FF5050
      D1FF4A4AD0FF5050D1FF2626BAFC2F2FBEF35050D1FF4A4AD0FF5050D1FF2A2A
      BBF55555C5B6FBFBFD0400000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F5F9F60AA1C7
      A465209A2BFE5EDB6AFF8CE494FF9EE1A5FF5EAD65FF209A2BFF5EDB6AFF8CE4
      94FFA4E6ABFF90D496FF81EB8BFF49E758FF3ED44CFF298D32FF93BF967C0000
      000000000000000000000000000000000000000000000000000000000000FBFB
      FD045757C7B63131BDF55959D6FF5252D4FF5B5BD7FF5959D6FF2626BBFF2A2A
      BCFF5959D6FF5252D4FF5B5BD7FF5B5BD7FF5353D4FF5959D6FF3131BDF55757
      C7B6FBFBFD040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003A9141EA3D9F
      46FF75D47FFF9BDFA2FFB6ECBCFF2C8934FE3D9F46FF75D47FFF9BDFA2FFB6EC
      BCFF50A158FF23832BFF91D396FF75E17FFF38DA48FF33C841FF2A8F32FF97C3
      9B77000000000000000000000000000000000000000000000000000000000000
      0000FBFBFD045A5AC8B63939C2F36565DBFF6363DBFF6363DBFF6363DAFF2020
      B6FC3131BFFC6565DBFF6363DBFF6363DBFF6565DBFF3939C2F35A5AC8B6FBFB
      FD04000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003D9244EA5AA8
      61FFC8EACDFFC1E6C5FF59A360F8399041ED5AA861FFC8EACDFFC1E6C5FF55A2
      5CFF20BC2FFF27BA35FF2A8632FF91D498FF64D56FFF23C933FF27BA35FF2A8F
      33FF92C0967D0000000000000000000000000000000000000000000000000000
      0000FBFBFD045C5CCAB63F3FC6F36E6EDFFF6A6ADFFF6A6ADFFF6C6CDEFF2525
      B9FC3737C3FC6E6EDFFF6A6ADFFF6A6ADFFF6E6EDFFF4040C6F35C5CCAB6FBFB
      FD04000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F6F9F60AA3CB
      A76568AB6FFF5FA366FAC9E0CB3AF6F9F60AA3CBA76568AB6FFF5BA162FE74BD
      7AFF52C75DFF0FB71FFF1CAC2AFF379740FF90D296FF52C75DFF0FB71FFF1CAC
      2AFF2A8F33FF93C1967CEBF4EC1600000000000000000000000000000000FBFB
      FD045F5FCCB63F3FC7F57676E3FF6A6AE1FF7777E4FF7575E3FF3434C3FF3A3A
      C6FF7676E3FF6A6AE1FF7777E4FF7777E4FF6969E1FF7676E3FF3F3FC7F55F5F
      CCB6FBFBFD040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BBD8BD4ACAE1CC3A000000000000000000000000BBD8BD4ACAE1CC3A6AAD
      70D292CC98FF41B44DFF00A00CFF159E20FF44994CFF92CC98FF41B44DFF00A0
      0CFF159E20FF278E30FF328E3AEE000000000000000000000000FBFBFD046161
      CDB64242C8F67E7EE6FF7171E4FF7F7FE6FF4646CAF42D2DBEFC3E3EC8FF7E7E
      E6FF7171E4FF7F7FE6FF4141C8FC4848CBF47F7FE6FF7272E4FF7E7EE6FF4343
      CAF56161CDB6FBFBFD0400000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000081B384CF93C398FF3A9A42FF0E8511FF3B9442FF599B5EFE93C398FF3A9A
      42FF0E8511FF3B9442FF308337FA0000000000000000000000006D6DD1AC4848
      CAF68686E7FF7878E5FF8888E7FF4949CAF63B3BC2E74343C8FC8686E7FF7878
      E5FF8888E7FF4545CAFF3030BFFC3B3BC2E74949CAF68888E7FF7979E5FF8787
      E7FF4848CAF66D6DD1AC00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000088B78CD584B689FF6DAB72FF4A864EE481A6839288B78CD584B6
      89FF6DAB72FF4A864EE481A683920000000000000000000000007070D3AC4E4E
      CCF68F8FE9FF9090E9FF4F4FCCF66666D1B66F6FD2AD4E4ECCF68F8FE9FF9090
      E9FF4B4BCCFF4646CBFF4D4DCBF66F6FD2AD6666D1B64F4FCCF69090E9FF8F8F
      E9FF4E4ECCF67070D3AC00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084B488DF70AB75F2F1F5F110000000000000000084B4
      88DF70AB75F2F1F5F11000000000000000000000000000000000FCFCFD046969
      D2B65252CDF65353CDF66969D2B6FCFCFD04FCFCFD046969D2B65252CDF64E4E
      CBFC3838C3FC5353CCF66969D2B6FCFCFD04FCFCFD046969D2B65454CEF65454
      CDF66969D2B6FCFCFD0400000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FD047474D5AC7474D5ACFCFCFD040000000000000000FCFCFD047474D5AC7373
      D4AD7373D4AD7474D5ACFCFCFD040000000000000000FCFCFD047474D5AC7474
      D5ACFCFCFD040000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000060000000480000000100010000000000600300000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFF000000FFFFFFE0
      7E03FFFFFF000000FFFFFFC03C03FF8003000000FFFFFFC01803FF8003000000
      C07E03C00003FF8003000000C07E03E00007FF8003000000C03C03F0000FFF80
      03000000C01803FC001FFF8003000000C00003FE007FFF8003000000E00007FF
      00FFE00003000000E00007FF81FFE00003000000F0000FF0C30FE00003000000
      F8001FC07E03E00003000000FC003FC01C03E00003000000FC003FC01803E000
      03000000FE007FE00003E000FF000000FF00FFF0000FE000FF000000FF81FFF8
      001FE000FF000000FFC3FFFC003FE000FF000000FFC3FFFE007FE000FF000000
      FFFFFFFF00FFE000FF000000FFFFFFFF81FFE000FF000000FFFFFFFFC3FFFFFF
      FF000000FFFFFFFFFFFFFFFFFF000000FFFFFFFE7FFFFFFFFFFFFFFF800001FC
      7FFFFFFFFFFFC3FF800001F87FFFFFFFFFFF81FF800001907FFFFFFFFFFF00FF
      800001E00001FFC3FFFE007F800001C00001FFC3FFFC003F800001800001FF81
      FFF8001F800001800001FF00FFF0000F800001800001FE007FC0000380000180
      0001FC003FC01803800001807E01FC003FC03C03800001807E01F8001FC07603
      C00003807E01F0000FF1C38FC00003817E01E00007FF81FFC00007810201E000
      03FF00FFE00007800003C00003FE007FC00003800001C01803F8001F80000180
      0001C03C03F0000F800001800003C07E03E00007800001800007C07F03C00003
      800001FFFE09FFFFFFC01803800001FFFE1FFFFFFFC03C03800001FFFE3FFFFF
      FFE07E07FFFFFFFFFE7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007FFF007
      FFFFFFE18187FFF7F7FFF7F7FE31FFC00003FFF777FFF7F7FC00FFC00003FFF7
      77FFF7F7F8007FC00003FFF417FFF417F0003FC00003FFF777FFF7F7C0001FE0
      0007FFF777FFF7F7C0000FF0000FFFF7F7FFF7F7C00007F0000FFFF007FFF007
      C00001E00007FFFFFFFFFFFFF38001C00003C01FFFC01FFFFFF001C00003DFDF
      FFDFDFFFFFF801C00003DDDFFFDFDFFFFFFC63C00003DDDFFFDFDFFFFFFFFFE1
      8187D05FFFD05FFFFFFFFFFFFFFFDDDFFFDFDFFFFFFFFFFFFFFFDDDFFFDFDFFF
      FFFFFFFFFFFFDFDFFFDFDFFFFFFFFFFFFFFFC01FFFC01FFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object ActionList1: TActionList
    Images = GridImageList
    OnUpdate = ActionList1Update
    Left = 452
    Top = 312
    object actExpandAll: TAction
      Caption = 'Expand All'
      Hint = 'Expand All'
      ImageIndex = 2
      OnExecute = actExpandAllExecute
    end
    object actCollapseAll: TAction
      Caption = 'Collapse All'
      Hint = 'Collapse All'
      ImageIndex = 3
      OnExecute = actCollapseAllExecute
    end
    object actSelectAll: TAction
      Caption = 'Select All'
      Hint = 'Select All'
      ImageIndex = 0
      OnExecute = actSelectAllExecute
    end
    object actUnselectAll: TAction
      Caption = 'Unselect All'
      Hint = 'Unselect All'
      ImageIndex = 1
      OnExecute = actUnselectAllExecute
    end
    object actExport: TAction
      Caption = 'Export'
      Hint = 'Export'
      ImageIndex = 4
      OnExecute = actExportExecute
    end
    object actGenerateColumnsUnit: TAction
      Caption = 'Generate column definition unit'
      OnExecute = actGenerateColumnsUnitExecute
    end
    object actConnectToRemoteAndCompare: TAction
      Caption = 'Connect to remote'
      Hint = 'Connect to remote and compare'
      OnExecute = actConnectToRemoteAndCompareExecute
    end
    object actDisconnectFromRemote: TAction
      Caption = 'Disconnect'
      Hint = 'Disconnect from remote'
      OnExecute = actDisconnectFromRemoteExecute
    end
    object actRefreshCompare: TAction
      Caption = 'Refresh'
      Hint = 'Refresh'
      ImageIndex = 5
      OnExecute = actRefreshCompareExecute
    end
    object actPullAllFromRemote: TAction
      Caption = 'Pull All'
      Hint = 'Pull All From Remote'
      ImageIndex = 7
      OnExecute = actPullAllFromRemoteExecute
    end
    object actPullSelectedFromRemote: TAction
      Caption = 'Pull Selected'
      Hint = 'Pull Selected From Remote'
      ImageIndex = 6
      OnExecute = actPullSelectedFromRemoteExecute
    end
    object actPushAllFromLocal: TAction
      Caption = 'Push All'
      Hint = 'Push All From Local'
      ImageIndex = 9
      OnExecute = actPushAllFromLocalExecute
    end
    object actPushSelectedFromLocal: TAction
      Caption = 'Push Selected'
      Hint = 'Push Selected From Local'
      ImageIndex = 8
      OnExecute = actPushSelectedFromLocalExecute
    end
    object actCompareSelectAll: TAction
      Caption = 'Select All'
      Hint = 'Select All'
      ImageIndex = 0
      OnExecute = actCompareSelectAllExecute
    end
    object actCompareUnselectAll: TAction
      Caption = 'Unselect All'
      Hint = 'Unselect All'
      ImageIndex = 1
      OnExecute = actCompareUnselectAllExecute
    end
    object actCloneDiagnosticQuery: TAction
      Caption = 'Clone diagnostic query'
      Hint = 'Clone diagnostic query'
      ImageIndex = 10
      OnExecute = actCloneDiagnosticQueryExecute
    end
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 348
    Top = 336
  end
  object SaveUnitDlg: TSaveDialog
    DefaultExt = 'pas'
    FileName = 'uColumnDefinition.pas'
    Filter = 'Delphi unit|*.pas|All files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save column definition unit'
    Left = 358
    Top = 443
  end
  object DSCompare: TDataSource
    DataSet = FDMemCompare
    Left = 140
    Top = 448
  end
  object FDMemCompare: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 228
    Top = 384
  end
  object DSSQLFix: TDataSource
    DataSet = DMDatabaseFixer.tabDiagFix
    OnStateChange = DSSQLFixStateChange
    Left = 222
    Top = 302
  end
  object DSParameters: TDataSource
    DataSet = DMDatabaseFixer.tabDiagParams
    Left = 580
    Top = 264
  end
  object propSQLCount: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneSQLCount
    TimerInterval = 100
    Left = 766
    Top = 230
  end
  object propSQLDetail: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneSQLDetail
    TimerInterval = 100
    Left = 854
    Top = 230
  end
  object propSQLFix: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneSQLFix
    TimerInterval = 100
    Left = 934
    Top = 230
  end
  object EditRep: TcxEditRepository
    Left = 248
    Top = 48
    PixelsPerInch = 96
    object EditRepText: TcxEditRepositoryTextItem
    end
    object EditRepCalc: TcxEditRepositoryCalcItem
      Properties.Alignment.Horz = taRightJustify
    end
    object EditRepSpin: TcxEditRepositorySpinItem
      Properties.Alignment.Horz = taRightJustify
      Properties.ValueType = vtInt
    end
    object EditRepDate: TcxEditRepositoryDateItem
      Properties.DateButtons = [btnClear, btnNow, btnToday]
      Properties.ImmediatePost = True
      Properties.Kind = ckDateTime
    end
  end
  object propSQLBeforeFix: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneSQLBeforeFix
    TimerInterval = 100
    Left = 766
    Top = 278
  end
  object propSQLAfterFix: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneSQLAfterFix
    TimerInterval = 100
    Left = 854
    Top = 278
  end
  object DSGlobalFix: TDataSource
    DataSet = DMDatabaseFixer.tabGlobalFix
    Left = 647
    Top = 425
  end
  object propBeforeSQLFix: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneBeforeSQLFix
    TimerInterval = 100
    Left = 766
    Top = 326
  end
  object propAfterSQLFix: TSynCompletionProposal
    Options = [scoLimitToMatchedText, scoUseBuiltInTimer, scoEndCharCompletion, scoCompleteWithTab, scoCompleteWithEnter]
    EndOfTokenChr = '()[]. '
    TriggerChars = ':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBtnText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = [fsBold]
    Columns = <>
    ShortCut = 16416
    Editor = dbsyneAfterSQLFix
    TimerInterval = 100
    Left = 862
    Top = 326
  end
end
