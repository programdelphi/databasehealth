unit uSQLTester;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ToolWin, Vcl.ActnMan,
  Vcl.ActnCtrls, Vcl.ExtCtrls, SynEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDataControllerConditionalFormattingRulesManagerDialog,
  Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  uDataModule, Vcl.Menus, Vcl.StdCtrls, cxButtons, acQBBase, acAST,
  acQBFireDACMetaProvider, acAdvantageSynProvider, acSQLBuilderPlainText,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  SynCompletionProposal, uParameters;

type
  TFQueryBuilder = class(TForm)
    ToolBar1: TToolBar;
    cxButton1: TcxButton;
    acQueryBuilder1: TacQueryBuilder;
    Splitter1: TSplitter;
    cxButton4: TcxButton;
    acAdvantageSyntaxProvider1: TacAdvantageSyntaxProvider;
    acFireDACMetadataProvider1: TacFireDACMetadataProvider;
    acSQLBuilderPlainText1: TacSQLBuilderPlainText;
    Panel2: TPanel;
    SynEdit1: TSynEdit;
    StatusBarEdit: TStatusBar;
    pnlGrid: TPanel;
    pnlButtons: TPanel;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    pageGrid: TPageControl;
    tabData: TTabSheet;
    GridData: TcxGrid;
    GridDataDBTableView1: TcxGridDBTableView;
    GridDataLevel1: TcxGridLevel;
    tabOutput: TTabSheet;
    splitterGrid: TSplitter;
    FDQuery: TFDQuery;
    DSQuery: TDataSource;
    Output: TMemo;
    propQueryBuilder: TSynCompletionProposal;
    procedure SynEdit1Exit(Sender: TObject);
    procedure acQueryBsquilder1SQLUpdated(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure SynEdit1StatusChange(Sender: TObject; Changes: TSynStatusChanges);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetGridVisible (Value: boolean);
    function GetGridVisible: boolean;
    property GridVisible: boolean read GetGridVisible write SetGridVisible;
  public
    { Public declarations }
  end;

var
  FQueryBuilder: TFQueryBuilder;

implementation
uses uMainDoctor, uConfigForm;

{$R *.dfm}

procedure TFQueryBuilder.acQueryBsquilder1SQLUpdated(Sender: TObject);
begin
  SynEdit1.Lines.Text := acSQLBuilderPlainText1.SQL;
end;

procedure TFQueryBuilder.cxButton1Click(Sender: TObject);
begin
  try
    if FDQuery.Prepared then FDQuery.Unprepare;

    FDQuery.Close;
    FDQuery.SQL.Clear;
    FDQuery.SQL.Text := acSQLBuilderPlainText1.SQL;

    if FDQuery.ParamCount > 0 then
    begin
      FParameters.GetQueryParams(FDQuery);
      if FParameters.ShowModal <> mrOK then exit;

      FParameters.SetQueryParams(FDQuery);
    end;

    FDQuery.Open;
    GridDataDBTableView1.ClearItems;
    GridDataDBTableView1.DataController.CreateAllItems;
    GridDataDBTableView1.ApplyBestFit;
    GridVisible       := True;
    PageGrid.TabIndex := 0;
  except
    on E: EFDDBEngineException do
    begin
      Output.Lines.Clear;
      Output.Lines.Text := E.Message;
      GridVisible       := True;
      pageGrid.TabIndex := 1;
    end;
  end;
end;

procedure TFQueryBuilder.btnOKClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TFQueryBuilder.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFQueryBuilder.cxButton4Click(Sender: TObject);
begin
  GridVisible := not GridVisible;
end;

procedure TFQueryBuilder.SetGridVisible (Value: boolean);
begin
  if Value <> pageGrid.Visible then
  begin
    if pageGrid.Visible then
    begin
      pageGrid.Tag         := pageGrid.Height;
      pageGrid.Visible     := False;
      pnlGrid.ClientHeight := pnlButtons.Height;
    end
    else
    begin
      pageGrid.Visible     := True;
      pnlGrid.ClientHeight := pnlButtons.Height + pageGrid.Tag;
    end;
    splitterGrid.Enabled := pageGrid.Visible;
  end;
end;

function TFQueryBuilder.GetGridVisible: boolean;
begin
  result := pageGrid.Visible;
end;

procedure TFQueryBuilder.FormShow(Sender: TObject);
begin
  acQueryBuilder1.RefreshMetadataAsync;
end;

procedure TFQueryBuilder.SynEdit1Exit(Sender: TObject);
begin
  acSQLBuilderPlainText1.SQL := SynEdit1.Lines.Text;
end;

procedure TFQueryBuilder.SynEdit1StatusChange(Sender: TObject;
  Changes: TSynStatusChanges);
begin
  StatusBarEdit.Panels [0].Text := Format ('Lin: %d, Col: %d', [SynEdit1.CaretY, SynEdit1.CaretX]);

  if SynEdit1.SelLength = 0 then
    StatusBarEdit.Panels [1].Text := ''
  else
    StatusBarEdit.Panels [1].Text := Format ('Sel: %d', [SynEdit1.SelLength]);
end;

end.
