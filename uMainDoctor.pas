unit uMainDoctor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uConfigForm, Vcl.StdCtrls,
  System.ImageList, Vcl.ImgList, Vcl.ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters,
  cxCustomData, cxStyles, cxTL, cxTLdxBarBuiltInMenu, System.UITypes,
  cxDataControllerConditionalFormattingRulesManagerDialog, cxInplaceContainer,
  cxContainer, cxEdit, cxTreeView, Vcl.ToolWin, Vcl.Menus, cxButtons,
  cxTextEdit, cxTLData, cxFilter, cxData, cxDataStorage, System.Types,
  cxNavigator, dxDateRanges, Data.DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, dxmdaset, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, Vcl.DBCtrls, Vcl.ExtCtrls, dxBarBuiltInMenu,
  cxGridCustomPopupMenu, cxGridPopupMenu, VirtualTrees, System.Actions,
  Vcl.ActnList, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  uDataModule, FireDAC.Phys.ADSDef, FireDAC.UI.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Phys, FireDAC.Phys.ADS, FireDAC.VCLUI.Wait,
  FireDAC.Phys.ADSWrapper, Math, uLoginConfigMode, VCL.TMSLogging, cxCheckBox,
  cxMaskEdit, cxDropDownEdit, cxCheckComboBox, TypInfo, Vcl.Mask, JvExMask,
  JvToolEdit, JvCombobox, FireDAC.Stan.StorageBin, uColumnVerifProgressBar,
  cxGridExportLink, ShellAPI, IniFiles, uParameters, uUpdateChecker,
  dxStatusBar, uNewVersionDlg, System.IOUtils;

const
  ICO_Status_None    = -1;
  ICO_Status_Running =  0;
  ICO_Status_Bug     =  1;
  ICO_Status_Error   =  2;
  ICO_Status_Fixed   =  3;
  ICO_Status_OK      =  4;

const
  ICO_Update_New            = 0;
  ICO_Update_Download       = 1;
  ICO_Update_Error          = 2;
  ICO_Update_Download_Error = 3;

const
  PNL_Autoupdate           = 0;
  PNL_Cnt_Checked_Nodes    = 1;
  PNL_Cnt_Total_Passed     = 2;
  PNL_Cnt_Total_Failed     = 3;
  PNL_Cnt_Total_SQL_Failed = 4;
  PNL_Last_Execution       = 5;
  PNL_Fail_Score           = 6;
  PNL_App_Version          = 7;

const
  DmpDirectory:    WideString = 'Dmp';
  DmpExtension:    WideString = 'dmp';
  BackupDirectory: WideString = 'Backup';
  BackupExtension: WideString = 'bak';

const
  UserName = 'Dev';
  Password = 'lezaM12345$$';

type
  TTreeActionType = (tatNone, tatSelectAll, tatUnselectAll, tatInvertSelection, tatHideEmpty, tatShowAll,
                     tatExpandAll, tatCollapseAll, tatExpandAllCategoriesSelected);
  TDiagnosticQueriesStatus = (dqsNone, dqsRunning, dqsPausing, dqsPause, dqsStoping, dqsFix);
  TSelectiveFix = (sfNone, sfRunOnceGlobal, sfSelectiveOK, sfParametrizedOK);

{Tree structure}
type
  PTreeData = ^RTreeData;
  RTreeData = record
    ImageIndex:       integer;
    StateIndex:       integer;
    Category:         string;
    Subcategory:      string;
    QueryID:          string;
    SmallDescription: string;
    LongDescription:  string;
    HasErrors:        boolean;
    RemoteLogging:    boolean;
    Count:            integer;
    LastCheck:        TDateTime;
    LastFix:          TDateTime;
    QryCnt:           string;
    QryDtl:           string;
    QryFix:           string;
    MemDtl:           TdxMemData;
    SentryTag:        string;
    FailScore:        integer;
    QryDtlActive:     boolean;
    QryFixActive:     boolean;
    Fixes:            TStringList;
    IdxFix:           integer;
    SelectiveFix:     TSelectiveFix;
  end;

  TRunDiagnosticQueryThread = class (TThread)
  private
    strExceptionMsg:  string;
    strLastQuery:     string;
  protected
    procedure SendInfoLog;
    procedure SendExceptionLog;
    procedure Execute; override;
    procedure ExecuteLstSQL (LstSQL: TStringCollection; Qry: TFDQuery);
    function  FindParam (Param: string): boolean;
    procedure ReadParams (Query: TFDQuery);
  public
    Description:  string;
    LstCnt:       TStringCollection;
    LstDtl:       TStringCollection;
    Node:         PVirtualNode;
    Count:        integer;
    HasErrors:    boolean;
    QryCnt:       TFDQuery;
    QryDtl:       TFDQuery;
    QryDtlActive: boolean;
    Params:       TdxMemData;
    constructor Create (InputNode: PVirtualNode; SQLCnt, SQLDtl: TStringCollection);
    destructor Destroy; override;
  end;

  TFixQueryThread = class (TThread)
  private
    strExceptionMsg:  string;
  protected
    procedure SendInfoLog;
    procedure SendExceptionLog;
    procedure Execute; override;
    function  FindParam (Param: string): boolean;
    procedure ReadParams (Query: TFDQuery);
  public
    Description: string;
    SQL:         string;
    SQLBefore:   string;
    SQLAfter:    string;
    Conn:        TFDConnection;
    Qry:         TFDQuery;
    Node:        PVirtualNode;
    HasErrors:   boolean;
    Params:      TdxMemData;
    constructor Create (InputNode: PVirtualNode; strSQL, strSQLBefore, strSQLAfter: string);
    destructor Destroy; override;
  end;

  TObjString = class
  public
    Fix, Before, After: string;
    constructor Create;
    destructor Destroy; override;
  end;

  TFMainDoctor = class(TForm)
    ToolBar1: TToolBar;
    cxButton1: TcxButton;
    cxButton5: TcxButton;
    cxButton7: TcxButton;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    TreeDiagnosticQueries: TVirtualStringTree;
    ActionList1: TActionList;
    actTreeClearResults: TAction;
    actTreeSelectAll: TAction;
    actTreeUnselectAll: TAction;
    actTreeInvertSelection: TAction;
    cxButton10: TcxButton;
    actTreeExpandAll: TAction;
    actTreeCollapseAll: TAction;
    TreeButtonsImageList: TImageList;
    ToolBar2: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    actConfigureMode: TAction;
    actTreeRunAll: TAction;
    actTreeRunOne: TAction;
    TimerDiagnostic: TTimer;
    TreeStatusNodeImageList: TImageList;
    Panel3: TPanel;
    GridDetailsDBTableView1: TcxGridDBTableView;
    GridDetailsLevel1: TcxGridLevel;
    GridDetails: TcxGrid;
    DSDetails: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    RowEven: TcxStyle;
    RowOdd: TcxStyle;
    Header: TcxStyle;
    actTreeRunPause: TAction;
    actTreeRunResume: TAction;
    actTreeRunStop: TAction;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton6: TcxButton;
    cxButton11: TcxButton;
    actTreeHideCorrect: TAction;
    actTreeShowAll: TAction;
    actTreeFix: TAction;
    TimerFix: TTimer;
    Panel4: TPanel;
    Splitter3: TSplitter;
    Label1: TLabel;
    edLastRun: TEdit;
    Label3: TLabel;
    edLastFix: TEdit;
    Label2: TLabel;
    memLongDescription: TMemo;
    Console: TMemo;
    Splitter2: TSplitter;
    actClearLogConsole: TAction;
    cxButton9: TcxButton;
    cxButton12: TcxButton;
    Label4: TLabel;
    chkcbConsoleVerboseLevel: TJvCheckedComboBox;
    actGenerateMissingColumns: TAction;
    actImportDignosticQueries: TAction;
    OpenImportDlg: TOpenDialog;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    actRestoreDiagnosticQueries: TAction;
    MenuImageList: TImageList;
    OpenBackupDlg: TOpenDialog;
    TimerRestore: TTimer;
    pmExport: TPopupMenu;
    actExportToCSV: TAction;
    actExportToExcel: TAction;
    ExporttoExcel1: TMenuItem;
    SaveDlgExp: TSaveDialog;
    ExporttoCSV1: TMenuItem;
    actExportToExcel2: TAction;
    actExportToXML: TAction;
    ExporttoExcelXLSX1: TMenuItem;
    ExporttoXML1: TMenuItem;
    actExportToTable: TAction;
    actExportToTable1: TMenuItem;
    Panel5: TPanel;
    Label5: TLabel;
    cbFixes: TComboBox;
    btnGridSelectAll: TcxButton;
    btnInvertSelection: TcxButton;
    actGridSelectAll: TAction;
    actGridUnselectAll: TAction;
    btnUnselectAll: TcxButton;
    actGridInvertSelection: TAction;
    cbGridSelectAll: TcxCheckBox;
    Updater: TUpdateChecker;
    StatusBar: TdxStatusBar;
    StatusBarImageList: TImageList;
    procedure TreeDiagnosticQueriesGetNodeDataSize(Sender: TBaseVirtualTree;
      var NodeDataSize: Integer);
    procedure TreeDiagnosticQueriesFreeNode(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure TreeDiagnosticQueriesChange(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure TreeDiagnosticQueriesFocusChanged(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure TreeDiagnosticQueriesGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: string);
    procedure actTreeClearResultsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actTreeSelectAllExecute(Sender: TObject);
    procedure actTreeUnselectAllExecute(Sender: TObject);
    procedure actTreeInvertSelectionExecute(Sender: TObject);
    procedure actTreeCollapseAllExecute(Sender: TObject);
    procedure actTreeExpandAllExecute(Sender: TObject);
    procedure TreeDiagnosticQueriesAddToSelection(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure TreeDiagnosticQueriesRemoveFromSelection(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TreeDiagnosticQueriesChecked(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure FormDestroy(Sender: TObject);
    procedure actConfigureModeExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure actTreeRunAllExecute(Sender: TObject);
    procedure actTreeRunOneExecute(Sender: TObject);
    procedure TimerDiagnosticTimer(Sender: TObject);
    procedure TreeDiagnosticQueriesGetImageIndex(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: TImageIndex);
    procedure actTreeRunPauseExecute(Sender: TObject);
    procedure actTreeRunResumeExecute(Sender: TObject);
    procedure actTreeRunStopExecute(Sender: TObject);
    procedure actTreeHideCorrectExecute(Sender: TObject);
    procedure actTreeShowAllExecute(Sender: TObject);
    procedure TimerFixTimer(Sender: TObject);
    procedure actTreeFixExecute(Sender: TObject);
    procedure actClearLogConsoleExecute(Sender: TObject);
    procedure chkcbConsoleVerboseLevelChange(Sender: TObject);
    procedure actGenerateMissingColumnsExecute(Sender: TObject);
    procedure actImportDignosticQueriesExecute(Sender: TObject);
    procedure actRestoreDiagnosticQueriesExecute(Sender: TObject);
    procedure TimerRestoreTimer(Sender: TObject);
    procedure actExportToCSVExecute(Sender: TObject);
    procedure actExportToExcelExecute(Sender: TObject);
    procedure actExportToExcel2Execute(Sender: TObject);
    procedure actExportToXMLExecute(Sender: TObject);
    procedure actExportToTableExecute(Sender: TObject);
    procedure actGridSelectAllExecute(Sender: TObject);
    procedure actGridUnselectAllExecute(Sender: TObject);
    procedure cbFixesChange(Sender: TObject);
    procedure actGridInvertSelectionExecute(Sender: TObject);
    procedure cbGridSelectAllPropertiesChange(Sender: TObject);
    procedure UpdaterFoundNewAppVersion(Sender: TObject; ProgramName, Version,
      URLDownload, ReleaseNotes, TargetFileName, PathToInstall,
      InstallerType: string; var StartDownload: Boolean);
    procedure UpdaterStartDownload(Sender: TObject; ProgramName, Version,
      URLDownload, TargetFileName, PathToInstall: string);
    procedure UpdaterFinishDownload(Sender: TObject; ProgramName, Version,
      URLDownload, TargetFileName, PathToInstall: string);
    procedure UpdaterErrorDownload(Sender: TObject; ProgramName, Version,
      URLDownload, TargetFileName, PathToInstall, ErrMsg: string);
    procedure UpdaterFirstRunNewVersion(Sender: TObject; ReleaseNotes: string;
      var MarkAsDisplayed: Boolean);
    procedure UpdaterNotifyMustExitForUpdate(Sender: TObject);
    procedure UpdaterUpdateError(Sender: TObject; ErrorMsg: string);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Closing:             boolean;
    lstProcessingNodes:  TList;
    idxProcessingNode:   integer;
    DiagnosticStatus:    TDiagnosticQueriesStatus;
    RunThread:           TRunDiagnosticQueryThread;
    FixThread:           TFixQueryThread;
    ThreadTerminated:    boolean;
    ThreadLink:          TFDPhysADSDriverLink;
    CntChecked:          integer;
    CntPassed:           integer;
    CntFailed:           integer;
    CntSQLFailed:        integer;
    HasRun:              boolean;
    LstToFixNodes:       TList;
    SumFailScore:        integer;
    LoadingConsoleVL:    boolean;
    NodeApplyFix:        boolean;
    FCurrentDatasetNode: string;
    FFullExportDir:      string;

    procedure ApplyTreeAction (TreeAction: TTreeActionType);
    procedure UpdateCurrentNode;
    procedure StartDiagnosticQueries;
    procedure OnThreadTerminated (Sender: TObject);
    procedure UpdateLastCheckRowInformation (Node: PVirtualNode);
    procedure UpdateLastFixRowInformation (Node: PVirtualNode);
    procedure SendFailScoreToSentry;
    procedure LoadVerboseLevelConsole;
    procedure ImportDiagnosticQueries (FileName: string);
    procedure RestoreDiagnosticQueries (FileName: string);
    procedure FullRestoreDiagnosticQueries (FileName: string);
    function CountParamsFromDataSet (Query: TFDQuery; DataSet: TDataSet): integer;
    procedure ShowHideUnselectedRows (Hide: boolean);
    procedure HideUnselectedRows;
    procedure ShowUnselectedRows;
  public
    { Public declarations }
    LogPath:    string;
    ThreadConn: TFDConnection;
  end;

var
  FMainDoctor: TFMainDoctor;

implementation

{$R *.dfm}

function GetAppVersionStr: string;
type
  TBytes = array of byte;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build
end;

function SearchInPath (FileName: string): string;
var
  SPPath: array[0..255] of char;
  PathPtr: PChar;
begin
  if SearchPath(nil, PChar(FileName), nil, 255, SPPath, PathPtr) > 0 then
    Result := StrPas(SPPath)
  else
  begin
    Result := ExtractFilePath (ParamStr (0)) + FileName;

    if not FileExists (Result) then
      Result := '';
  end;
end;

function GetPOSDirectory (AliasName: string; ForceCreateExport: boolean = True): string;
var
  ADSIni, Value, Dir: string;
  Ini: TIniFile;
  p: integer;
begin
  try
    result := ExtractFilePath (ParamStr (0)) + 'Export';
    ADSIni := SearchInPath ('ADS.ini');

    if (ADSIni <> '') and FileExists (ADSIni) then
    begin
      Ini := TIniFile.Create(ADSIni);
      try
        Value := Ini.ReadString('Databases', AliasName, '');
        p     := Pos (';', Value);
        if p > 0 then
          result := ExtractFilePath (Copy (Value, 1, p - 1)) + 'Export';
      finally
        Ini.Free;
      end;
    end;
  finally
    if ForceCreateExport and not DirectoryExists (result) then
      CreateDir(result);
  end;
end;

constructor TRunDiagnosticQueryThread.Create (InputNode: PVirtualNode; SQLCnt, SQLDtl: TStringCollection {TStringList});
var
  i: integer;
begin
  inherited Create (True);

  Count     := 0;
  Node      := InputNode;
  HasErrors := False;
  LstCnt    := TStringCollection.Create;
  LstDtl    := TStringCollection.Create;

  for i := 0 to SQLCnt.Count - 1 do
    LstCnt.Add (SQLCnt.Items [i]);

  for i := 0 to SQLDtl.Count - 1 do
    LstDtl.Add (SQLDtl.Items [i]);

  QryCnt := DMDatabaseFixer.NewQuery(FMainDoctor.ThreadConn.ConnectionName);
  QryDtl := DMDatabaseFixer.NewQuery(FMainDoctor.ThreadConn.ConnectionName);
end;

destructor TRunDiagnosticQueryThread.Destroy;
begin
  QryCnt.Free;
  QryDtl.Free;
  Description := '';

  if Assigned (Params) then
    FreeAndNil (Params);

  inherited Destroy;
end;

procedure TRunDiagnosticQueryThread.SendInfoLog;
var
  Data: PTreeData;
begin
  if Assigned (Node) then
  begin
    Data                           := FMainDoctor.TreeDiagnosticQueries.GetNodeData(Node);
    DMDatabaseFixer.LogSentry.Tags := Data^.SentryTag;
  end;

  DMDatabaseFixer.Log_Info(strExceptionMsg);
end;

procedure TRunDiagnosticQueryThread.SendExceptionLog;
var
  Data: PTreeData;
begin
  if Assigned (Node) then
  begin
    Data                           := FMainDoctor.TreeDiagnosticQueries.GetNodeData(Node);
    DMDatabaseFixer.LogSentry.Tags := Data^.SentryTag;
  end;

  DMDatabaseFixer.Log_Exception(strExceptionMsg + sLineBreak + strLastQuery);
end;

procedure TRunDiagnosticQueryThread.Execute;
var
  i: integer;
begin
  i            := 0;
  HasErrors    := False;
  strLastQuery := '';

  //Executes the count queries
  try
    Screen.Cursor   := crSQLWait;
    strExceptionMsg := Format ('Execute diagnostic query [%s] to get count rows', [Description]);
    Synchronize(SendInfoLog);
    ExecuteLstSQL (LstCnt, QryCnt);
    Screen.Cursor := crDefault;

    if QryCnt.Active and not QryCnt.Eof and (i = (LstCnt.Count - 1)) then
      Count := QryCnt.Fields [0].AsInteger;
  except
    on E: Exception do
    begin
      Screen.Cursor   := crDefault;
      HasErrors       := True;
      strExceptionMsg := Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]);
      Synchronize(SendExceptionLog);
      exit;
    end;
  end;

  //Exeecute the details queries
  if QryDtlActive and (LstDtl.Count > 0) then
  begin
    try
      Screen.Cursor   := crSQLWait;
      strExceptionMsg := Format ('Execute diagnostic query [%s] to get detail rows', [Description]);
      Synchronize(SendInfoLog);
      ExecuteLstSQL (LstDtl, QryDtl);
      Screen.Cursor := crDefault;
    except
      on E: Exception do
      begin
        Screen.Cursor   := crDefault;
        HasErrors       := True;
        strExceptionMsg := Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]);
        Synchronize(SendExceptionLog);
      end;
    end;
  end;
end;

procedure TRunDiagnosticQueryThread.ExecuteLstSQL (LstSQL: TStringCollection; Qry: TFDQuery);
{
  This procedure verifies this cases:

  1. Single statement: Check the first significant token to chose Open or ExecSQL.
  2. Multiple statements in which the last instruction is a Select statement without Into clause:
       Execute in just one ExecuteSQL all the previous statements, and then execute
       the Select statement with Open.
  3. Multiple statements, in which the last instruction is not a Select statement, or is a
       Select statement, but contains a Into clause. In this case execute all the
       instructions in one single ExecuteSQL.
}
var
  FirstToken: WideString;
begin
  if LstSQL.Count = 0 then exit;

  Qry.Close;
  Qry.SQL.Clear;

  //1. Single statement: Check the first significant token to chose Open or ExecSQL.
  if LstSQL.Count = 1 then
  begin
    strLastQuery := LstSQL[0];
    FirstToken := FConfigurationMode.GetStatementType(strLastQuery);
    Qry.SQL.Add(strLastQuery);
    ReadParams (Qry);

    if FirstToken = 'SELECT' then
      Qry.Open
    else
      Qry.ExecSQL;
  end
  else
  begin
    //2. Multiple statements in which the last instruction is a Select statement without Into clause:
    //   Execute in just one ExecuteSQL all the previous statements, and then execute
    //   the Select statement with Open.
    if not FConfigurationMode.IsSelectInto (LstSQL[LstSQL.Count - 1]) then
    begin
      strLastQuery := LstSQL.Join(False);

      //Non catched execution, the caller methods must catch the exception
      //Execute all the previous statements together
      Qry.SQL.Add(strLastQuery);
      ReadParams (Qry);
      Qry.ExecSQL;

      //Non catched execution, the caller methods must catch the exception
      //Uses open for the Select statement
      strLastQuery := LstSQL[LstSQL.Count - 1];
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add(strLastQuery);
      ReadParams (Qry);
      Qry.Open;
    end
    else
    begin
      //  3. Multiple statements, in which the last instruction is not a Select statement, or is a
      //     Select statement, but contains a Into clause. In this case execute all the
      //     instructions in one single ExecuteSQL.
      strLastQuery := LstSQL.Join(True);

      //Non catched execution, the caller methods must catch the exception
      //Execute all the previous statements together
      Qry.SQL.Add(strLastQuery);
      ReadParams (Qry);
      Qry.ExecSQL;
    end;
  end;
end;

function TRunDiagnosticQueryThread.FindParam (Param: string): boolean;
begin
  result := False;
  Param  := UpperCase (Param);

  if not Assigned (Params) or (Params.RecordCount = 0) then exit;

  Params.First;
  while not Params.Eof do
  begin
    if UpperCase (Params.FieldByName ('Param').AsString) = Param then
    begin
      result := True;
      exit;
    end;

    Params.Next;
  end;
end;

procedure TRunDiagnosticQueryThread.ReadParams (Query: TFDQuery);
var
  i: integer;
begin
  if (Query.ParamCount = 0) or not Assigned (Params) or (Params.RecordCount = 0) then exit;

  for i := 0 to Query.ParamCount - 1 do
    if FindParam (Query.Params [i].Name) then
    begin
      if Params.FieldByName ('DataType').AsString = 'String' then
//      begin
//        Query.Params [i].FDDataType := dtWideString;
//        Query.Params [i].DataType   := ftString;
        Query.Params [i].AsString   := Params.FieldByName('Value').AsString
//      end
      else if Params.FieldByName ('DataType').AsString = 'Integer' then
//      begin
//        Query.Params [i].FDDataType := dtInt32;
//        Query.Params [i].DataType   := ftInteger;
        Query.Params [i].AsInteger  := Params.FieldByName('Value').AsInteger
//      end
      else if Params.FieldByName ('DataType').AsString = 'Float' then
//      begin
//        Query.Params [i].FDDataType := dtDouble;
//        Query.Params [i].DataType   := ftFloat;
        Query.Params [i].AsFloat    := Params.FieldByName('Value').AsFloat
//      end
      else if Params.FieldByName ('DataType').AsString = 'Date' then
//      begin
//        Query.Params [i].FDDataType := dtDateTime;
//        Query.Params [i].DataType   := ftDateTime;
        Query.Params [i].AsDateTime := Params.FieldByName('Value').AsDateTime
//      end;
    end;

  if Query.Prepared then
    Query.Unprepare;
  Query.Prepare;
end;

constructor TFixQueryThread.Create (InputNode: PVirtualNode; strSQL, strSQLBefore, strSQLAfter: string);
begin
  inherited Create (True);

  Node      := InputNode;
  HasErrors := False;
  SQL       := strSQL;
  SQLBefore := strSQLBefore;
  SQLAfter  := strSQLAfter;
  Qry       := DMDatabaseFixer.NewQuery(FMainDoctor.ThreadConn.ConnectionName);
  Params    := nil;
end;

destructor TFixQueryThread.Destroy;
begin
  Qry.Free;
  Description := '';

  if Assigned (Params) then
    FreeAndNil (Params);

  inherited Destroy;
end;

procedure TFixQueryThread.SendInfoLog;
var
  Data: PTreeData;
begin
  Data                           := FMainDoctor.TreeDiagnosticQueries.GetNodeData(Node);
  DMDatabaseFixer.LogSentry.Tags := Data^.SentryTag;

  DMDatabaseFixer.Log_Info(strExceptionMsg);
end;

procedure TFixQueryThread.SendExceptionLog;
var
  Data: PTreeData;
begin
  Data                           := FMainDoctor.TreeDiagnosticQueries.GetNodeData(Node);
  DMDatabaseFixer.LogSentry.Tags := Data^.SentryTag;

  DMDatabaseFixer.Log_Exception(strExceptionMsg + sLineBreak + SQL);
end;

function TFixQueryThread.FindParam (Param: string): boolean;
begin
  result := False;
  Param  := UpperCase (Param);

  if not Assigned (Params) or (Params.RecordCount = 0) then exit;

  Params.First;
  while not Params.Eof do
  begin
    if UpperCase (Params.FieldByName ('Param').AsString) = Param then
    begin
      result := True;
      exit;
    end;

    Params.Next;
  end;
end;

procedure TFixQueryThread.ReadParams (Query: TFDQuery);
var
  i: integer;
begin
  if (Query.ParamCount = 0) or not Assigned (Params) or (Params.RecordCount = 0) then exit;

  for i := 0 to Query.ParamCount - 1 do
    if FindParam (Query.Params [i].Name) then
    begin
      if Params.FieldByName ('DataType').AsString = 'String' then
        Query.Params [i].AsString   := Params.FieldByName('Value').AsString
      else if Params.FieldByName ('DataType').AsString = 'Integer' then
        Query.Params [i].AsInteger  := Params.FieldByName('Value').AsInteger
      else if Params.FieldByName ('DataType').AsString = 'Float' then
        Query.Params [i].AsFloat    := Params.FieldByName('Value').AsFloat
      else if Params.FieldByName ('DataType').AsString = 'Date' then
        Query.Params [i].AsDateTime := Params.FieldByName('Value').AsDateTime;
    end;
end;

procedure TFixQueryThread.Execute;
begin
  HasErrors := False;

  //Executes the script before fix queries
  if not SQLBefore.IsEmpty then
    try
      Screen.Cursor   := crSQLWait;
      strExceptionMsg := Format ('Execute before fix query [%s]', [Description]);
      Synchronize(SendInfoLog);
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add(SQLBefore);
      ReadParams(Qry);
      Conn.StartTransaction;
      Qry.ExecSQL;
      Conn.Commit;
      Screen.Cursor := crDefault;
    except
      on E: Exception do
      begin
        Screen.Cursor   := crDefault;
        HasErrors       := True;
        strExceptionMsg := Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]);
        Synchronize(SendExceptionLog);
      end;
    end;

  //Executes the fix queries
  try
    Screen.Cursor   := crSQLWait;
    strExceptionMsg := Format ('Execute fix query [%s]', [Description]);
    Synchronize(SendInfoLog);
    Qry.Close;
    Qry.SQL.Clear;
    Qry.SQL.Add(SQL);
    ReadParams(Qry);
    Conn.StartTransaction;
    Qry.ExecSQL;
    Conn.Commit;
    Screen.Cursor := crDefault;
  except
    on E: Exception do
    begin
      Screen.Cursor   := crDefault;
      HasErrors       := True;
      strExceptionMsg := Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]);
      Synchronize(SendExceptionLog);
    end;
  end;

  //Executes the script after fix queries
  if not SQLAfter.IsEmpty then
    try
      Screen.Cursor   := crSQLWait;
      strExceptionMsg := Format ('Execute after fix query [%s]', [Description]);
      Synchronize(SendInfoLog);
      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add(SQLAfter);
      ReadParams(Qry);
      Conn.StartTransaction;
      Qry.ExecSQL;
      Conn.Commit;
      Screen.Cursor := crDefault;
    except
      on E: Exception do
      begin
        Screen.Cursor   := crDefault;
        HasErrors       := True;
        strExceptionMsg := Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]);
        Synchronize(SendExceptionLog);
      end;
    end;
end;

constructor TObjString.Create;
begin
  inherited Create;

  Fix    := '';
  Before := '';
  After  := '';
end;

destructor TObjString.Destroy;
begin
  Fix    := '';
  Before := '';
  After  := '';

  inherited Destroy;
end;

procedure TFMainDoctor.actTreeInvertSelectionExecute(Sender: TObject);
begin
  ApplyTreeAction(tatInvertSelection);
  UpdateCurrentNode;
end;

procedure TFMainDoctor.actTreeRunAllExecute(Sender: TObject);
var
  CategoryNode, SubcategoryNode, QueryNode: PVirtualNode;
  Data: PTreeData;
  Query: TFDQuery;
  i: integer;
begin
  Query := DMDatabaseFixer.NewQuery(DMDatabaseFixer.FDConn.ConnectionName);
  try
    LstToFixNodes.Clear;
    lstProcessingNodes.Clear;
    CategoryNode := TreeDiagnosticQueries.GetFirst;
    FParameters.ClearParameters;
    while Assigned (CategoryNode) do
    begin
      if CategoryNode^.CheckState = csCheckedNormal then
      begin
        SubcategoryNode := CategoryNode^.FirstChild;
        while Assigned (SubcategoryNode) do
        begin
          if SubcategoryNode^.CheckState = csCheckedNormal then
          begin
            QueryNode := SubcategoryNode^.FirstChild;
            while Assigned (QueryNode) do
            begin
              if QueryNode^.CheckState = csCheckedNormal then
              begin
                lstProcessingNodes.Add(QueryNode);

                if Assigned (QueryNode) then
                begin
                  Data := TreeDiagnosticQueries.GetNodeData(QueryNode);
                  if Assigned (Data) then
                  begin
                    if Data^.QryCnt <> '' then
                    begin
                      Query.Close;
                      Query.SQL.Clear;
                      Query.SQL.Text := Data^.QryCnt;

                      for i := 0 to Query.ParamCount - 1 do
                        FParameters.AddParameter(Query.Params [i].Name);
                    end;

                    if Data^.QryDtl <> '' then
                    begin
                      Query.Close;
                      Query.SQL.Clear;
                      Query.SQL.Text := Data^.QryDtl;

                      for i := 0 to Query.ParamCount - 1 do
                        FParameters.AddParameter(Query.Params [i].Name);
                    end;
                  end;
                end;
              end;

              QueryNode := QueryNode^.NextSibling;
            end;
          end;

          SubcategoryNode := SubcategoryNode^.NextSibling;
        end;

      end;

      CategoryNode := CategoryNode^.NextSibling;
    end;

    if (FParameters.memParams.RecordCount > 0) and (FParameters.ShowModal <> mrOK) then
    begin
      LstToFixNodes.Clear;
      lstProcessingNodes.Clear;
      exit;
    end;

    ApplyTreeAction (tatExpandAllCategoriesSelected);
    StatusBar.Panels [PNL_Last_Execution].Text := 'Run all selected: ' + FormatDateTime ('mm/dd/yyyy hh:nn:ss', Now);
    StartDiagnosticQueries;
  finally
    Query.Free;
  end;
end;

procedure TFMainDoctor.actTreeRunOneExecute(Sender: TObject);
var
  Node:  PVirtualNode;
  Data:  PTreeData;
  s:     string;
  Query: TFDQuery;
  i:     integer;
begin
  lstProcessingNodes.Clear;
  FParameters.ClearParameters;
  Node := TreeDiagnosticQueries.GetFirstSelected;
  if Assigned (Node) {and (Node^.CheckState = csCheckedNormal)} then
  begin
    lstProcessingNodes.Add(Node);
    Data  := TreeDiagnosticQueries.GetNodeData(Node);
    s     := Data^.SmallDescription;
    Query := DMDatabaseFixer.NewQuery(DMDatabaseFixer.FDConn.ConnectionName);
    try
      if Assigned (Data) then
      begin
        if Data^.QryCnt <> '' then
        begin
          Query.Close;
          Query.SQL.Clear;
          Query.SQL.Text := Data^.QryCnt;

          for i := 0 to Query.ParamCount - 1 do
            FParameters.AddParameter(Query.Params [i].Name);
        end;

        if Data^.QryDtl <> '' then
        begin
          Query.Close;
          Query.SQL.Clear;
          Query.SQL.Text := Data^.QryDtl;

          for i := 0 to Query.ParamCount - 1 do
            FParameters.AddParameter(Query.Params [i].Name);
        end;
      end;
    finally
      Query.Free;
    end;
  end;

  if (FParameters.memParams.RecordCount > 0) and (FParameters.ShowModal <> mrOK) then
  begin
    LstToFixNodes.Clear;
    lstProcessingNodes.Clear;
    exit;
  end;

  StatusBar.Panels [PNL_Last_Execution].Text := Format ('Run [%s]: ' + FormatDateTime ('mm/dd/yyyy hh:nn:ss', Now), [s]);
  StartDiagnosticQueries;
end;

procedure TFMainDoctor.actTreeRunPauseExecute(Sender: TObject);
begin
  DiagnosticStatus := dqsPausing;
end;

procedure TFMainDoctor.actTreeRunResumeExecute(Sender: TObject);
begin
  DiagnosticStatus        := dqsRunning;
  TimerDiagnostic.Enabled := True;
end;

procedure TFMainDoctor.actTreeRunStopExecute(Sender: TObject);
begin
  DiagnosticStatus := dqsStoping;
end;

procedure TFMainDoctor.StartDiagnosticQueries;
begin
  HasRun                  := True;
  RunThread               := nil;
  idxProcessingNode       := 0;
  DiagnosticStatus        := dqsRunning;
  TimerDiagnostic.Enabled := True;
end;

procedure TFMainDoctor.actTreeSelectAllExecute(Sender: TObject);
begin
  ApplyTreeAction(tatSelectAll);
  UpdateCurrentNode;
end;

procedure TFMainDoctor.actTreeShowAllExecute(Sender: TObject);
begin
  ApplyTreeAction(tatShowAll);
end;

procedure TFMainDoctor.actTreeUnselectAllExecute(Sender: TObject);
begin
  ApplyTreeAction(tatUnselectAll);
  UpdateCurrentNode;
end;

procedure TFMainDoctor.ApplyTreeAction (TreeAction: TTreeActionType);
var
  root: PVirtualNode;

  procedure Preorder (Node: PVirtualNode);
  var
    Data:             PTreeData;
    Child:            PVirtualNode;
    CntVisibleChilds: integer;
  begin
    if not Assigned (Node) then exit;

    if Node^.CheckType = ctCheckBox then
      case TreeAction of
        tatSelectAll:       Node^.CheckState := csCheckedNormal;
        tatUnselectAll:     Node^.CheckState := csUncheckedNormal;
        tatInvertSelection: if Node^.CheckState = csCheckedNormal then
                              Node^.CheckState := csUncheckedNormal
                            else
                              Node^.CheckState := csCheckedNormal;
        tatExpandAllCategoriesSelected:
          if (TreeDiagnosticQueries.GetNodeLevel (Node) = 0) and
             (Node^.CheckState = csCheckedNormal) then
             TreeDiagnosticQueries.Expanded [Node] := True;
       end;

    case TreeAction of
      tatExpandAll:       TreeDiagnosticQueries.Expanded [Node] := True;
      tatCollapseAll:     TreeDiagnosticQueries.Expanded [Node] := False;
      tatShowAll:         Node^.States := Node^.States + [vsVisible];
      tatHideEmpty: begin
        Data := TreeDiagnosticQueries.GetNodeData(Node);
        if (TreeDiagnosticQueries.GetNodeLevel (Node) =  1) and
           (Data^.ImageIndex = ICO_Status_Bug)               and
           (Data^.Count = 0) then
          Node^.States := Node^.States - [vsVisible];
      end;
    end;

    CntVisibleChilds := 0;
    child            := Node^.FirstChild;
    while Assigned (child) do
    begin
      Preorder (child);
      if vsVisible in child.States then
        Inc (CntVisibleChilds);

      child := child^.NextSibling;
    end;

    if (TreeAction = tatHideEmpty) and (TreeDiagnosticQueries.GetNodeLevel (Node) = 0) and (CntVisibleChilds = 0) then
      Node^.States := Node^.States - [vsVisible];
  end;
begin
  root := TreeDiagnosticQueries.GetFirst;
  while Assigned (root) do
  begin
    Preorder (root);

    root := root^.NextSibling;
  end;
  TreeDiagnosticQueries.Refresh;
end;

procedure TFMainDoctor.actClearLogConsoleExecute(Sender: TObject);
begin
  Console.Lines.Clear;
end;

procedure TFMainDoctor.actConfigureModeExecute(Sender: TObject);
const
  Password = '9999';
begin
  FConfigModeLogin.Password := Password;
  if FConfigModeLogin.ShowModal <> mrOk then exit;

  FConfigurationMode.ShowModal;
  if FConfigurationMode.HasChanged then
  begin
    actTreeClearResults.Execute;
    FParameters.CopyAllParams;
  end;
end;

procedure TFMainDoctor.actExportToCSVExecute(Sender: TObject);
var
  SaveAll: boolean;
begin
  if GridDetailsDBTableView1.DataController.DataSetRecordCount = 0 then exit;

  SaveDlgExp.DefaultExt := '.csv';
  SaveDlgExp.Filter     := 'CSV File|*.csv|All Files|*.*';
  SaveDlgExp.FileName   := FCurrentDatasetNode + SaveDlgExp.DefaultExt;
  SaveDlgExp.InitialDir := FFullExportDir;
  SaveAll               := GridDetailsDBTableView1.Controller.SelectedRecordCount = 0;
  if SaveDlgExp.Execute (Self.WindowHandle) then
  begin
    try
      Cursor := crHourGlass;
      ExportGridToCSV(SaveDlgExp.FileName, GridDetails, True, SaveAll);
      case MessageDlg ('Export successful. Press OK to open file, and press YES to show in folder', mtInformation, [mbOK, mbYes, mbCancel], 0) of
        mrOK:  ShellExecute(Self.WindowHandle, 'open', PChar(SaveDlgExp.FileName), nil, nil, SW_SHOWNORMAL);
        mrYes: ShellExecute(Self.WindowHandle,
                   'open',
                   'explorer.exe',
                   PChar('/select, "' + SaveDlgExp.FileName + '"'),
                   nil,
                   SW_SHOWNORMAL);
      end;
    finally
      Cursor := crDefault;
    end;
  end;
end;

procedure TFMainDoctor.actExportToExcel2Execute(Sender: TObject);
var
  SaveAll: boolean;
begin
  if GridDetailsDBTableView1.DataController.DataSetRecordCount = 0 then exit;

  SaveDlgExp.DefaultExt := '.xlsx';
  SaveDlgExp.Filter     := 'Excel File|*.xlsx|All Files|*.*';
  SaveDlgExp.FileName   := FCurrentDatasetNode + SaveDlgExp.DefaultExt;
  SaveDlgExp.InitialDir := FFullExportDir;
  SaveAll               := GridDetailsDBTableView1.Controller.SelectedRecordCount = 0;
  if SaveDlgExp.Execute (Self.WindowHandle) then
  begin
    try
      Cursor := crHourGlass;
      ExportGridToXLSX(SaveDlgExp.FileName, GridDetails, True, SaveAll);
      case MessageDlg ('Export successful. Press OK to open file, and press YES to show in folder', mtInformation, [mbOK, mbYes, mbCancel], 0) of
        mrOK:  ShellExecute(Self.WindowHandle, 'open', PChar(SaveDlgExp.FileName), nil, nil, SW_SHOWNORMAL);
        mrYes: ShellExecute(Self.WindowHandle,
                   'open',
                   'explorer.exe',
                   PChar('/select, "' + SaveDlgExp.FileName + '"'),
                   nil,
                   SW_SHOWNORMAL);
      end;
    finally
      Cursor := crDefault;
    end;
  end;
end;

procedure TFMainDoctor.ShowHideUnselectedRows (Hide: boolean);
var
  i: integer;
begin
//  for i := 0 to GridDetailsDBTableView1.ViewData.RowCount - 1 do
//    GridDetailsDBTableView1.ViewData.Rows [i].Visible := not Hide or GridDetailsDBTableView1.ViewData.Rows [i].Selected;
end;

procedure TFMainDoctor.HideUnselectedRows;
begin
  GridDetailsDBTableView1.ViewData.BeginUpdate;
  ShowHideUnselectedRows(True);
end;

procedure TFMainDoctor.ShowUnselectedRows;
begin
  ShowHideUnselectedRows(False);
  GridDetailsDBTableView1.ViewData.EndUpdate;
end;

procedure TFMainDoctor.actExportToExcelExecute(Sender: TObject);
var
  SaveAll: boolean;
begin
  if GridDetailsDBTableView1.DataController.DataSetRecordCount = 0 then exit;

  SaveDlgExp.DefaultExt := '.xls';
  SaveDlgExp.Filter     := 'Excel File|*.xls|All Files|*.*';
  SaveDlgExp.FileName   := FCurrentDatasetNode + SaveDlgExp.DefaultExt;
  SaveDlgExp.InitialDir := FFullExportDir;
  SaveAll               := GridDetailsDBTableView1.Controller.SelectedRecordCount = 0;
  if SaveDlgExp.Execute (Self.WindowHandle) then
  begin
    try
      Cursor := crHourGlass;
      HideUnselectedRows;
      ExportGridToExcel(SaveDlgExp.FileName, GridDetails, True, SaveAll);
      ShowUnselectedRows;
      case MessageDlg ('Export successful. Press OK to open file, and press YES to show in folder', mtInformation, [mbOK, mbYes, mbCancel], 0) of
        mrOK:  ShellExecute(Self.WindowHandle, 'open', PChar(SaveDlgExp.FileName), nil, nil, SW_SHOWNORMAL);
        mrYes: ShellExecute(Self.WindowHandle,
                   'open',
                   'explorer.exe',
                   PChar('/select, "' + SaveDlgExp.FileName + '"'),
                   nil,
                   SW_SHOWNORMAL);
      end;
    finally
      Cursor := crDefault;
    end;
  end;
end;

procedure TFMainDoctor.actExportToTableExecute(Sender: TObject);
const
  InvalidChars = [';', '=', '+', '<', '>', '|', '"', '[', ']', '\', '/', '''', ':', '*', '?', ' '];
var
  NewTable: TFDTable;
  TableName, TableExt: string;
  FlagCreate: boolean;
  Conn: TFDConnection;

  function RemoveInvalidChars (s: string): string;
  var
    i: integer;
  begin
    result := '';
    for i := 1 to Length (s) do
      if not (s [i] in InvalidChars) then
        result := result + s [i];
  end;

  function IsValidFilename (s: string): boolean;
  var
    i: integer;
  begin
    result := True;
    for i := 1 to Length (s) do
      result := result and not (s [i] in InvalidChars)
  end;

var
  s, tmp: string;
  L: TStringList;
  i, c: integer;
  A: TArray <string>;
begin
  if GridDetailsDBTableView1.DataController.DataSetRecordCount = 0 then exit;

  SaveDlgExp.DefaultExt := '.adt';
  SaveDlgExp.Filter     := 'Advantage Database Server Table|*.adt|All Files|*.*';
  SaveDlgExp.FileName   := FCurrentDatasetNode + SaveDlgExp.DefaultExt;
  SaveDlgExp.InitialDir := FFullExportDir;
  if SaveDlgExp.Execute (Self.WindowHandle) then
  begin
    if FileExists (SaveDlgExp.FileName) then
    try
      DeleteFile(SaveDlgExp.FileName);
    except
      MessageDlg (Format ('The %s file cannot be overwritten because it is currently in use', [SaveDlgExp.FileName]), mtError, [mbOK], 0);
      exit;
    end;

    try
      Cursor              := crHourGlass;
      TableExt            := ExtractFileExt  (SaveDlgExp.FileName);
      TableName           := ExtractFileName (SaveDlgExp.FileName);
      TableName           := Copy (TableName, 1, Length (TableName) - Length (TableExt));
      Conn                := TFDConnection.Create(Self);
      Conn.ConnectionName := 'Temp';
      Conn.DriverName     := 'ADS';
      Conn.LoginPrompt    := False;
      Conn.Params.Clear;
      Conn.Params.Add('DriverID=ADS');
      Conn.Params.Add(Format ('Database=%s', [ExtractFileDir (SaveDlgExp.FileName)]));
      Conn.Params.Add('ServerTypes=Local');
      Conn.Params.Add('TableType=ADT');
      try
        Conn.Connected := True;
      except
      end;

      NewTable            := TFDTable.Create(Self);
      NewTable.TableName  := TableName;
      NewTable.Connection := Conn;
      NewTable.FieldDefs.Assign(GridDetailsDBTableView1.DataController.DataSource.DataSet.FieldDefs);
      NewTable.CreateDataSet;
      NewTable.Close;

      if GridDetailsDBTableView1.Controller.SelectedRecordCount > 0 then
      begin
        L := TStringList.Create;
        try
          tmp := TPath.GetTempFileName;
          ExportGridToCSV(tmp, GridDetails, True, False, #9, 'tmp');
          L.LoadFromFile(tmp);
          NewTable.Active := True;

          for i := 1 to L.Count - 1 do
            if L [i] <> '' then
            begin
              A := L [i].Split([#9]);

              if Length (A) = NewTable.FieldDefs.Count then
              begin
                NewTable.Append;
                for c := 0 to NewTable.FieldDefs.Count - 1 do
                begin
                  s := Copy (A [c], 2, A [c].Length - 2);
                  NewTable.Fields [c].AsString := s;
                end;
                NewTable.Post;
              end;
            end;
          DeleteFile (tmp);
        finally
          L.Free;
        end;
      end
      else
        NewTable.CopyDataSet(GridDetailsDBTableView1.DataController.DataSource.DataSet);

      NewTable.Close;
      if MessageDlg ('Export succesfull, do you show in folder?', mtConfirmation, mbYesNo, 0) = mrYes then
        ShellExecute(Self.WindowHandle,
                     'open',
                     'explorer.exe',
                     PChar('/select, "' + SaveDlgExp.FileName + '"'),
                     nil,
                     SW_SHOWNORMAL);
    finally
      Cursor := crDefault;
      NewTable.Free;
      Conn.Free;
    end;
  end;
end;

procedure TFMainDoctor.actExportToXMLExecute(Sender: TObject);
var
  SaveAll: boolean;
begin
  if GridDetailsDBTableView1.DataController.DataSetRecordCount = 0 then exit;

  SaveDlgExp.DefaultExt := '.xml';
  SaveDlgExp.Filter     := 'XML File|*.xml|All Files|*.*';
  SaveDlgExp.FileName   := FCurrentDatasetNode + SaveDlgExp.DefaultExt;
  SaveDlgExp.InitialDir := FFullExportDir;
  if SaveDlgExp.Execute (Self.WindowHandle) then
  begin
    try
      Cursor := crHourGlass;
      ExportGridToXML(SaveDlgExp.FileName, GridDetails, True, SaveAll);
      case MessageDlg ('Export successful. Press OK to open file, and press YES to show in folder', mtInformation, [mbOK, mbYes, mbCancel], 0) of
        mrOK:  ShellExecute(Self.WindowHandle, 'open', PChar(SaveDlgExp.FileName), nil, nil, SW_SHOWNORMAL);
        mrYes: ShellExecute(Self.WindowHandle,
                   'open',
                   'explorer.exe',
                   PChar('/select, "' + SaveDlgExp.FileName + '"'),
                   nil,
                   SW_SHOWNORMAL);
      end;
    finally
      Cursor := crDefault;
    end;
  end;
end;

procedure TFMainDoctor.actGenerateMissingColumnsExecute(Sender: TObject);
begin
  if MessageDlg ('This process may take several minutes, are you sure to proceed?', mtConfirmation, mbYesNoCancel, 0) = mrYes then
    DMDatabaseFixer.CumulativeColumnVerification (True);
end;

procedure TFMainDoctor.actGridInvertSelectionExecute(Sender: TObject);
var
  i: integer;
begin
  GridDetailsDBTableView1.ViewData.BeginUpdate;
  for i := 0 to GridDetailsDBTableView1.ViewData.RowCount - 1 do
    GridDetailsDBTableView1.ViewData.Rows [i].Selected := not GridDetailsDBTableView1.ViewData.Rows [i].Selected;
  GridDetailsDBTableView1.ViewData.EndUpdate;
end;

procedure TFMainDoctor.actGridSelectAllExecute(Sender: TObject);
begin
  GridDetailsDBTableView1.Controller.SelectAll;
end;

procedure TFMainDoctor.actGridUnselectAllExecute(Sender: TObject);
begin
  GridDetailsDBTableView1.Controller.ClearSelection;
end;

procedure TFMainDoctor.actImportDignosticQueriesExecute(Sender: TObject);
var
  memTmp:   TdxMemData;
  stream:   TFileStream;
  FileName: WideString;
begin
  OpenImportDlg.InitialDir := ExtractFilePath (ParamStr (0)) +  DmpDirectory;
  OpenImportDlg.DefaultExt := DmpExtension;
  if not OpenImportDlg.Execute (Self.WindowHandle) then exit;

  ImportDiagnosticQueries(OpenImportDlg.FileName);
end;

procedure TFMainDoctor.ImportDiagnosticQueries (FileName: string);
var
  memQry:     TFDMemTable;
  Total, i:   integer;
  bm:         TBookmark;
  TestThread: TRunDiagnosticQueryThread;
  RowUpdated: integer;
  RowFailed:  integer;

  function SuccessfulQryCnt (Description, QryCnt: Widestring): boolean;
  var
    LstCnt, LstDtl: TStringCollection; //TStringList;
  begin
    result := False;

    ThreadTerminated           := False;
    LstCnt                     := FConfigurationMode.SplitInstructionsSemicolon (QryCnt, False);
    LstDtl                     := FConfigurationMode.SplitInstructionsSemicolon ('',     False);
    TestThread                 := TRunDiagnosticQueryThread.Create(nil, LstCnt, LstDtl);
    TestThread.Description     := Description;
    TestThread.QryDtlActive    := False;
    TestThread.OnTerminate     := OnThreadTerminated;
    TestThread.FreeOnTerminate := False;
    TestThread.Resume;

    try
      while not ThreadTerminated do
      begin
        Application.ProcessMessages;
        Sleep (100);
        Application.ProcessMessages;
      end;

      result := not TestThread.HasErrors;
    finally
      ThreadTerminated := False;
      FreeAndNil (RunThread);
    end;
  end;

  procedure AssignData;
  begin
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Query_ID').AsString            := memQry.FieldByName('Query_ID').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Category').AsString            := memQry.FieldByName('Category').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Small_Description').AsString   := memQry.FieldByName('Small_Description').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Long_Description').AsString    := memQry.FieldByName('Long_Description').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Pass_Fail_State').AsInteger    := memQry.FieldByName('Pass_Fail_State').AsInteger;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Check').AsDateTime     := memQry.FieldByName('DT_Last_Check').AsDateTime;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Fix').AsDateTime       := memQry.FieldByName('DT_Last_Fix').AsDateTime;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Last_Fail_Count').AsInteger    := memQry.FieldByName('Last_Fail_Count').AsInteger;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Count').AsString           := memQry.FieldByName('SQL_Count').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail').AsString          := memQry.FieldByName('SQL_Detail').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix').AsString             := memQry.FieldByName('SQL_Fix').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Row_Version_Number').AsInteger := memQry.FieldByName('Row_Version_Number').AsInteger;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Fail_Score').AsInteger         := memQry.FieldByName('Fail_Score').AsInteger;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Sentry_TAG').AsString          := memQry.FieldByName('Sentry_TAG').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Log_Remotely').AsBoolean       := memQry.FieldByName('Log_Remotely').AsBoolean;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Min_DB_Ver_Required').AsString := memQry.FieldByName('Min_DB_Ver_Required').AsString;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('Status').AsBoolean             := memQry.FieldByName('Status').AsBoolean;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail_Status').AsBoolean  := memQry.FieldByName('SQL_Detail_Status').AsBoolean;
    DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix_Status').AsBoolean     := memQry.FieldByName('SQL_Fix_Status').AsBoolean;
  end;

  procedure UpdateRecord;
  begin
    DMDatabaseFixer.tabDiagnosticQry.Edit;
    AssignData;
    DMDatabaseFixer.tabDiagnosticQry.Post;
  end;

  procedure AppendRecord;
  begin
    DMDatabaseFixer.tabDiagnosticQry.Append;
    AssignData;
    DMDatabaseFixer.tabDiagnosticQry.Post;
  end;

  function CreateBackup: boolean;
  var
    BackupFile: WideString;
    memTmp:   TFDMemTable;
  begin
    result     := True;
    BackupFile := Format ('%s\Backup_%s (%s).%s', [ExtractFilePath (ParamStr (0)) + BackupDirectory,
                          FormatDateTime ('yyyy-mm-dd_hh_nn', Now), ExtractFileName (FileName), BackupExtension]);
    memTmp     := TFDMemTable.Create(Self);

    try
      //Copy all rows from Diagnotic_Queries
      memTmp.CopyDataSet(DMDatabaseFixer.tabDiagnosticQry, [coStructure, coRestart, coAppend]);

      try
        memTmp.SaveToFile(BackupFile, sfBinary);
      except
        on E: Exception do
        begin
          DMDatabaseFixer.Log_Exception(Format ('Export error. Class [%s] Message [%s]', [E.ClassName, E.Message]));
          result := False;
        end;
      end;
    finally
      memTmp.Free;
    end;
  end;

begin
  if not CreateBackup then
  begin
    MessageDlg ('An error has ocurred during backup process', mtError, [mbOK], 0);
    exit;
  end;

  DMDatabaseFixer.Log_Trace('procedure TFMainDoctor.ImportDiagnosticQueries (FileName: string);');
  TestThread                := nil;
  bm                        := DMDatabaseFixer.tabDiagnosticQry.GetBookmark;
  DMDatabaseFixer.Importing := True;
  ActionList1.UpdateAction(actTreeClearResults);
  Application.ProcessMessages;
  DMDatabaseFixer.tabDiagnosticQry.DisableControls;

  try
    memQry := TFDMemTable.Create(Self);

    try
      memQry.LoadFromFile(FileName);
      memQry.Open;

      memQry.Last;
      Total      := memQry.RecordCount;
      RowUpdated := 0;
      RowFailed  := 0;

      memQry.First;
      StatusBar.Panels [PNL_Last_Execution].Text    := '';
      StatusBar.Panels [PNL_Cnt_Total_Passed].Text  := '';
      StatusBar.Panels [PNL_Cnt_Total_Failed].Text  := '';
      FColumnVerifProgressBar.btnCancel.Enabled     := False;
      FColumnVerifProgressBar.ProgressBar1.Position := 0;
      FColumnVerifProgressBar.Caption := 'Import diagnostic queries';
      FColumnVerifProgressBar.Show;

      i := 0;
      while not memQry.Eof do
      begin
        Application.ProcessMessages;
        if DMDatabaseFixer.tabDiagnosticQry.FindKey ([memQry.FieldByName('Query_ID').AsString]) then
        begin
          if DMDatabaseFixer.tabDiagnosticQry.FieldByName ('Row_Version_Number').AsInteger < memQry.FieldByName ('Row_Version_Number').AsInteger then
          begin
            if SuccessfulQryCnt (memQry.FieldByName('Small_Description').AsString, memQry.FieldByName('SQL_Count').AsString) then
            begin
              UpdateRecord;

              Inc (RowUpdated);
            end
            else
            begin
              Inc (RowFailed);
            end;
          end;
        end
        else
        begin
          if SuccessfulQryCnt (memQry.FieldByName('Small_Description').AsString, memQry.FieldByName('SQL_Count').AsString) then
          begin
            AppendRecord;

            Inc (RowUpdated);
          end
          else
          begin
            Inc (RowFailed);
          end;
        end;

        memQry.Next;
        Inc (i);

        StatusBar.Panels [PNL_Cnt_Total_Passed].Text := Format ('Rows Updated: %d', [RowUpdated]);
        StatusBar.Panels [PNL_Cnt_Total_Failed].Text := Format ('Rows Failed: %d',  [RowFailed]);

        FColumnVerifProgressBar.ProgressBar1.Position := Round ((i / Total)* 100);
        Application.ProcessMessages;
      end;
    finally
      memQry.Free;
      FColumnVerifProgressBar.Hide;
      MessageDlg ('Import process successful', mtInformation, [mbOk], 0);
    end;

  finally
    DMDatabaseFixer.tabDiagnosticQry.GotoBookmark(bm);
    DMDatabaseFixer.tabDiagnosticQry.FreeBookmark(bm);
    DMDatabaseFixer.tabDiagnosticQry.EnableControls;
    DMDatabaseFixer.Importing := False;
    StatusBar.Panels [PNL_Last_Execution].Text := 'Import: ' + FormatDateTime ('mm/dd/yyyy hh:nn:ss', Now);
    actTreeClearResults.Execute;
  end;
end;


procedure TFMainDoctor.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
var
  Connected, Importing, HasDetails: boolean;
  Node: PVirtualNode;
begin
  Node                              := TreeDiagnosticQueries.GetFirstSelected;
  Connected                         := DMDatabaseFixer.FDConn.Connected;
  Importing                         := DMDatabaseFixer.Importing;
  HasDetails                        := GridDetailsDBTableView1.DataController.DataSetRecordCount > 0;
  actGenerateMissingColumns.Enabled := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actImportDignosticQueries.Enabled := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actTreeSelectAll.Enabled          := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actTreeUnselectAll.Enabled        := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actTreeInvertSelection.Enabled    := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actTreeClearResults.Enabled       := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actConfigureMode.Enabled          := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actTreeRunAll.Enabled             := not Importing and Connected and (DiagnosticStatus = dqsNone);
  actTreeHideCorrect.Enabled        := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasRun;
  actTreeShowAll.Enabled            := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasRun;
  actTreeRunOne.Enabled             := not Importing and Connected and (DiagnosticStatus = dqsNone)    and Assigned (Node) and (TreeDiagnosticQueries.SelectedCount = 1) and  Assigned (Node.Parent) and not Assigned (Node.FirstChild);
  actTreeRunPause.Enabled           := not Importing and Connected and (DiagnosticStatus = dqsRunning) and ((lstProcessingNodes.Count - 1) > idxProcessingNode);
  actTreeRunStop.Enabled            := not Importing and Connected and (DiagnosticStatus = dqsRunning) and ((lstProcessingNodes.Count - 1) > idxProcessingNode);
  actTreeRunResume.Enabled          := not Importing and Connected and (DiagnosticStatus = dqsPause);
  actTreeFix.Enabled                := not Importing and Connected and (DiagnosticStatus = dqsNone)    and NodeApplyFix; // (LstToFixNodes.Count > 0) and (LstToFixNodes.IndexOf(Node) >= 0);
  actExportToCSV.Enabled            := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasDetails;
  actExportToExcel.Enabled          := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasDetails;
  actExportToExcel2.Enabled         := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasDetails;
  actExportToXML.Enabled            := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasDetails;
  actExportToTable.Enabled          := not Importing and Connected and (DiagnosticStatus = dqsNone)    and HasDetails;

  if not Importing and Connected and (DiagnosticStatus = dqsNone) then
    TreeDiagnosticQueries.TreeOptions.MiscOptions := TreeDiagnosticQueries.TreeOptions.MiscOptions - [toReadOnly]
  else
    TreeDiagnosticQueries.TreeOptions.MiscOptions := TreeDiagnosticQueries.TreeOptions.MiscOptions + [toReadOnly];
end;

procedure TFMainDoctor.actTreeCollapseAllExecute(Sender: TObject);
begin
  ApplyTreeAction(tatCollapseAll);
end;

procedure TFMainDoctor.actTreeExpandAllExecute(Sender: TObject);
begin
  ApplyTreeAction(tatExpandAll);
end;

function TFMainDoctor.CountParamsFromDataSet (Query: TFDQuery; DataSet: TDataSet): integer;
var
  i: integer;
begin
  result := 0;

  for i := 0 to Query.ParamCount - 1 do
    if DataSet.FindField (Query.Params [i].Name) <> nil then
      Inc (result);
end;

procedure TFMainDoctor.actTreeFixExecute(Sender: TObject);
var
  Node:       PVirtualNode;
  Data:       PTreeData;
  Query:      TFDQuery;
  Obj:        TObjString;
  i, p:       integer;
  pt:         TPoint;
  SQLBefore,
  SQLAfter:   string;
  Required:   TStringList;
  Column:     TcxGridDBColumn;

  procedure ExtractParamsFromSQL (SQL: string; Query: TFDQuery);
  var
    i: integer;
  begin
    if SQL.IsEmpty then exit;

    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SQL);

    for i := 0 to Query.ParamCount - 1 do
      if Data^.MemDtl.FindField (Query.Params [i].Name) = nil then
      begin
        if Required.IndexOf (Query.Params [i].Name) = -1 then
          Required.Add(Query.Params [i].Name)
      end
      else
        FParameters.ExcludedParams.Add(UpperCase(Query.Params [i].Name));

    FParameters.GetQueryParams(Query);
  end;

begin
  if TreeDiagnosticQueries.SelectedCount <> 1 then exit;

  Node := TreeDiagnosticQueries.GetFirstSelected();
  Data := TreeDiagnosticQueries.GetNodeData(Node);
  if (Data^.Fixes.Count = 0) or
     ((Data^.Fixes.Count > 1) and (not Assigned (Data^.MemDtl) or (Data^.MemDtl.RecordCount = 0))) then exit;

  if (Data^.Fixes.Count > 1) and (Data^.IdxFix = -1) then
  begin
    MessageDlg ('You need to select a fix', mtInformation, [mbOK], 0);
    exit;
  end;

  if {(Data^.Fixes.Count > 1) and} (Data^.SelectiveFix = sfSelectiveOK) and (GridDetailsDBTableView1.Controller.SelectedRecordCount = 0) then
  begin
    MessageDlg ('No rows selected', mtInformation, [mbOK], 0);
    exit;
  end;

  Obj := nil;
  if Data^.Fixes.Count = 1 then
    Obj := TObjString (Data^.Fixes.Objects [0])
  else if (Data^.Fixes.Count > 1) and (Data^.IdxFix > -1) then
    Obj := TObjString (Data^.Fixes.Objects [Data^.IdxFix]);

  if not Assigned (Obj) or (Obj.Fix = '') then exit;

  Query := DMDatabaseFixer.NewQuery(ThreadConn.ConnectionName);
  try
    FParameters.ExcludedParams.Clear;
    Required  := TStringList.Create;
    SQLBefore := '';
    SQLAfter  := '';

    if not Obj.Before.IsEmpty then
      SQLBefore := Obj.Before
    else if DMDatabaseFixer.tabGlobalFix.FieldByName('SQL_Before_Fix_Status').AsBoolean then
      SQLBefore := DMDatabaseFixer.tabGlobalFix.FieldByName('SQL_Before_Fix').AsString;

    ExtractParamsFromSQL (SQLBefore, Query);

    if not Obj.After.IsEmpty then
      SQLAfter := Obj.After
    else if DMDatabaseFixer.tabGlobalFix.FieldByName('SQL_After_Fix_Status').AsBoolean then
      SQLAfter := DMDatabaseFixer.tabGlobalFix.FieldByName('SQL_After_Fix').AsString;

    ExtractParamsFromSQL (SQLAfter, Query);

    DiagnosticStatus := dqsFix;
    if {(Data^.Fixes.Count > 1) and} (Data^.SelectiveFix = sfSelectiveOK) and Assigned (Data^.MemDtl) and (Data^.MemDtl.RecordCount > 0) then
    begin
      Data^.StateIndex := ICO_Status_Running;
      StatusBar.Panels [PNL_Last_Execution].Text := Format ('Fix [%s]: ' + FormatDateTime ('mm/dd/yyyy hh:nn:ss', Now), [Data^.SmallDescription]);
      TreeDiagnosticQueries.Refresh;

      try
        ExtractParamsFromSQL (Obj.Fix, Query);

        if Required.Count > 0 then
        begin
//          FParameters.GetQueryParams(Query);
          if FParameters.ShowModal <> mrOK then exit;
        end;

        if not SQLBefore.IsEmpty then
        begin
          Query.Close;
          Query.SQL.Clear;
          Query.SQL.Add(SQLBefore);
          Query.ExecSQL;
          Application.ProcessMessages;
        end;

        Query.Close;
        Query.SQL.Clear;
        Query.SQL.Add(Obj.Fix);
        for i := 0 to GridDetailsDBTableView1.Controller.SelectedRecordCount - 1 do
        begin
          for p := 0 to Query.ParamCount - 1 do
          begin
            Column := GridDetailsDBTableView1.GetColumnByFieldName(Query.Params [p].Name);
            if Assigned (Column) then
              Query.Params [p].AsString := GridDetailsDBTableView1.Controller.SelectedRecords [i].Values [Column.Index];
          end;

          if Query.ParamCount > FParameters.ExcludedParams.Count then
            FParameters.SetQueryParams(Query);

          Query.ExecSQL;
          Application.ProcessMessages;
        end;

        if not SQLAfter.IsEmpty then
        begin
          Query.Close;
          Query.SQL.Clear;
          Query.SQL.Add(SQLAfter);
          Query.ExecSQL;
          Application.ProcessMessages;
        end;
     finally
        DiagnosticStatus := dqsNone;
        UpdateCurrentNode;
        DiagnosticStatus := dqsNone;
        Screen.Cursor    := crDefault;
        TreeDiagnosticQueries.Refresh;
        GetCursorPos(pt);
        Inc (pt.X);
        Inc (pt.Y);
        SetCursorPos(pt.X, pt.Y);
        MessageDlg ('Process finished', mtInformation, [mbOK], 0);
      end;
    end
    else
    begin
      FixThread                 := TFixQueryThread.Create (Node, Obj.Fix, SQLBefore, SQLAfter);
      FixThread.Conn            := ThreadConn;
      FixThread.Description     := Data^.SmallDescription;
      FixThread.FreeOnTerminate := False;
      FixThread.OnTerminate     := OnThreadTerminated;

      if FParameters.memParams.RecordCount > 0 then
      begin
        FixThread.Params := TdxMemData.Create(Self);
        FixThread.Params.CopyFromDataSet(FParameters.memParams);
      end;

      FixThread.Resume;
      TimerFix.Enabled := True;
      UpdateCurrentNode;
      StatusBar.Panels [PNL_Last_Execution].Text := Format ('Fix [%s]: ' + FormatDateTime ('mm/dd/yyyy hh:nn:ss', Now), [Data^.SmallDescription]);
    end;
  finally
    Query.Free;
    Required.Free;
  end;
end;

procedure TFMainDoctor.actTreeHideCorrectExecute(Sender: TObject);
begin
  ApplyTreeAction(tatHideEmpty);
end;

procedure TFMainDoctor.FullRestoreDiagnosticQueries (FileName: string);
var
  memTmp: TFDMemTable;
  Query: TFDQuery;
begin
  DMDatabaseFixer.Log_Trace('procedure TFMainDoctor.FullRestoreDiagnosticQueries (FileName: string);');
  Query                     := DMDatabaseFixer.NewQuery;
  memTmp                    := TFDMemTable.Create(Self);
  DMDatabaseFixer.Importing := True;
  DMDatabaseFixer.tabDiagnosticQry.DisableControls;

  try
    try
      memTmp.LoadFromFile(FileName);
    except
      on E: Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Export error. Class [%s] Message [%s]', [E.ClassName, E.Message]));
        MessageDlg ('An error has ocurred during the restore process', mtError, [mbOk], 0);
        exit;
      end;
    end;

    DMDatabaseFixer.FDConn.StartTransaction;
    DMDatabaseFixer.tabDiagnosticQry.Close;
    Query.SQL.Add('Delete From Diagnostic_Queries');
    Query.ExecSQL;
    DMDatabaseFixer.FDConn.Commit;
    DMDatabaseFixer.tabDiagnosticQry.Open;

    memTmp.Open;
    while not memTmp.Eof do
    begin
      DMDatabaseFixer.tabDiagnosticQry.Append;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Query_ID').AsString            := memTmp.FieldByName ('Query_ID').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Category').AsString            := memTmp.FieldByName ('Category').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Small_Description').AsString   := memTmp.FieldByName ('Small_Description').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Long_Description').AsString    := memTmp.FieldByName ('Long_Description').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Pass_Fail_State').AsInteger    := memTmp.FieldByName ('Pass_Fail_State').AsInteger;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Check').AsDateTime     := memTmp.FieldByName ('DT_Last_Check').AsDateTime;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Fix').AsDateTime       := memTmp.FieldByName ('DT_Last_Fix').AsDateTime;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Last_Fail_Count').AsInteger    := memTmp.FieldByName ('Last_Fail_Count').AsInteger;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Count').AsString           := memTmp.FieldByName ('SQL_Count').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail').AsString          := memTmp.FieldByName ('SQL_Detail').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix').AsString             := memTmp.FieldByName ('SQL_Fix').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Row_Version_Number').AsInteger := memTmp.FieldByName ('Row_Version_Number').AsInteger;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Fail_Score').AsInteger         := memTmp.FieldByName ('Fail_Score').AsInteger;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Sentry_TAG').AsString          := memTmp.FieldByName ('Sentry_TAG').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Log_Remotely').AsBoolean       := memTmp.FieldByName ('Log_Remotely').AsBoolean;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Min_DB_Ver_Required').AsString := memTmp.FieldByName ('Min_DB_Ver_Required').AsString;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('Status').AsBoolean             := memTmp.FieldByName ('Status').AsBoolean;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail_Status').AsBoolean  := memTmp.FieldByName ('SQL_Detail_Status').AsBoolean;
      DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix_Status').AsBoolean     := memTmp.FieldByName ('SQL_Fix_Status').AsBoolean;
      DMDatabaseFixer.tabDiagnosticQry.Post;

      memTmp.Next;
    end;
    MessageDlg ('Restore process successful', mtInformation, [mbOk], 0);
  finally
    memTmp.Free;
    DMDatabaseFixer.Importing := False;
    DMDatabaseFixer.tabDiagnosticQry.EnableControls;
    TimerRestore.Enabled := True;
  end;
end;


procedure TFMainDoctor.RestoreDiagnosticQueries (FileName: string);
var
  memTmp: TFDMemTable;
  bm: TBookmark;
begin
  memTmp := TFDMemTable.Create(Self);

  try
    try
      memTmp.LoadFromFile(FileName);
    except
      on E: Exception do
      begin
        DMDatabaseFixer.Log_Exception(Format ('Export error. Class [%s] Message [%s]', [E.ClassName, E.Message]));
        MessageDlg ('An error has ocurred during the restore process', mtError, [mbOk], 0);
        exit;
      end;
    end;

    bm := DMDatabaseFixer.tabDiagnosticQry.GetBookmark;
    DMDatabaseFixer.tabDiagnosticQry.DisableControls;
    try
      memTmp.Open;
      while not memTmp.Eof do
      begin
        if DMDatabaseFixer.tabDiagnosticQry.FindKey ([memTmp.FieldByName('Query_ID').AsString]) and
           (DMDatabaseFixer.tabDiagnosticQry.FieldByName ('Row_Version_Number').AsInteger > memTmp.FieldByName ('Row_Version_Number').AsInteger) then
        begin
          DMDatabaseFixer.tabDiagnosticQry.Edit;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Category').AsString            := memTmp.FieldByName ('Category').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Small_Description').AsString   := memTmp.FieldByName ('Small_Description').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Long_Description').AsString    := memTmp.FieldByName ('Long_Description').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Pass_Fail_State').AsInteger    := memTmp.FieldByName ('Pass_Fail_State').AsInteger;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Check').AsDateTime     := memTmp.FieldByName ('DT_Last_Check').AsDateTime;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('DT_Last_Fix').AsDateTime       := memTmp.FieldByName ('DT_Last_Fix').AsDateTime;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Last_Fail_Count').AsInteger    := memTmp.FieldByName ('Last_Fail_Count').AsInteger;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Count').AsString           := memTmp.FieldByName ('SQL_Count').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail').AsString          := memTmp.FieldByName ('SQL_Detail').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix').AsString             := memTmp.FieldByName ('SQL_Fix').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Row_Version_Number').AsInteger := memTmp.FieldByName ('Row_Version_Number').AsInteger;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Fail_Score').AsInteger         := memTmp.FieldByName ('Fail_Score').AsInteger;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Sentry_TAG').AsString          := memTmp.FieldByName ('Sentry_TAG').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Log_Remotely').AsBoolean       := memTmp.FieldByName ('Log_Remotely').AsBoolean;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Min_DB_Ver_Required').AsString := memTmp.FieldByName ('Min_DB_Ver_Required').AsString;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('Status').AsBoolean             := memTmp.FieldByName ('Status').AsBoolean;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Detail_Status').AsBoolean  := memTmp.FieldByName ('SQL_Detail_Status').AsBoolean;
          DMDatabaseFixer.tabDiagnosticQry.FieldByName('SQL_Fix_Status').AsBoolean     := memTmp.FieldByName ('SQL_Fix_Status').AsBoolean;
          DMDatabaseFixer.tabDiagnosticQry.Post;
        end;

        memTmp.Next;
      end;
    finally
      DMDatabaseFixer.tabDiagnosticQry.GotoBookmark(bm);
      DMDatabaseFixer.tabDiagnosticQry.FreeBookmark(bm);
      DMDatabaseFixer.tabDiagnosticQry.EnableControls;
    end;
  finally
    memTmp.Free;
    actTreeClearResults.Execute;
    MessageDlg ('Restore process successful', mtInformation, [mbOk], 0);
  end;
end;

procedure TFMainDoctor.actRestoreDiagnosticQueriesExecute(Sender: TObject);
begin
  OpenBackupDlg.InitialDir := ExtractFilePath (ParamStr (0)) + BackupDirectory;
  OpenBackupDlg.DefaultExt := BackupExtension;

  if not OpenBackupDlg.Execute (Self.WindowHandle) then exit;
  FullRestoreDiagnosticQueries (OpenBackupDlg.FileName);
end;

procedure TFMainDoctor.actTreeClearResultsExecute(Sender: TObject);
const
  NewLine: string = #13#10;
var
  Query, QueryFix:       TFDQuery;
  CategoryNode,
  SubcategoryNode,
  ChildNode:             PVirtualNode;
  CategoryData,
  SubcategoryData,
  ChildData:             PTreeData;
  PrevCategory:          string;
  PrevSubcategory:       string;
  ServerVersion:         string;
  MinVersion:            string;
  LstServerVersion:      TStringList;
  bm:                    TBookmark;
  Obj:                   TObjString;

  function IsValidVersion (Version: string): boolean;
  var
    LstVersion: TStringList;
    i, VerServer, VerQry: integer;
  begin
    if Version = '' then
    begin
      result := True;
      exit;
    end;

    LstVersion := TStringList.Create;
    try
      LstVersion.Text := StringReplace(Version, '.', NewLine, [rfReplaceAll]);
      i               := 0;
      result          := True;
      while result and (i < (LstVersion.Count - 1)) and (i < (LstServerVersion.Count - 1)) do
      begin
        try
          VerServer := StrToInt (LstServerVersion [i]);
          VerQry    := StrToInt (LstVersion       [i]);

          if VerServer > VerQry then
            Exit
          else if VerServer < VerQry then
            result := False;
        except
          on E: EConvertError do
            result := False;
        end;

        Inc (i);
      end;
    finally
      LstVersion.Free;
    end;
  end;

begin
  DMDatabaseFixer.tabDiagnosticQry.Refresh;
  LstToFixNodes.Clear;
  TreeDiagnosticQueries.Clear;
  Query              := DMDatabaseFixer.NewQuery ('DIAG');
  QueryFix           := DMDatabaseFixer.NewQuery ('DIAG');
  CntChecked         := 0;
  CntPassed          := 0;
  CntFailed          := 0;
  CntSQLFailed       := 0;
  SumFailScore       := 0;
  HasRun             := False;
  ServerVersion      := DMDatabaseFixer.GetServerVersion;
  LstServerVersion   := TStringList.Create;
  QueryFix.SQL.Add('Select * From Diagnostic_Fix_Queries Where Query_ID = :Query_ID');
  try
    LstServerVersion.Text := StringReplace(ServerVersion, '.', NewLine, [rfReplaceAll]);

    try
      DMDatabaseFixer.LogSentry.Tags := DMDatabaseFixer.gSentryTags;
      DMDatabaseFixer.Log_Trace('Clear results, generate tree information');

      Query.SQL.Add('Select DQ.*');
      Query.SQL.Add('  From Diagnostic_Queries DQ');
      Query.SQL.Add(' Where DQ.Status     = 1');
      Query.SQL.Add(' Order By DQ.Category, DQ.Subcategory, DQ.Small_Description');
      Query.Open;
    except
      on E: Exception do
        DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                      sLineBreak + Query.SQL.Text);
    end;

    PrevCategory    := '';
    PrevSubcategory := '';
    CategoryNode    := nil;
    SubcategoryNode := nil;
    while not Query.Eof do
    begin
      MinVersion := Query.FieldByName ('Min_DB_Ver_Required').AsString;

      if IsValidVersion (MinVersion) then
      begin
        if PrevCategory <> Query.FieldByName ('Category').AsString then
        begin
          PrevSubcategory            := '';
          PrevCategory               := Query.FieldByName ('Category').AsString;
          CategoryNode               := TreeDiagnosticQueries.AddChild(nil);
          CategoryData               := TreeDiagnosticQueries.GetNodeData(CategoryNode);
          CategoryData^.Category     := PrevCategory;
          CategoryData^.Subcategory  := '';
          CategoryData^.QueryID      := '';
          CategoryData^.ImageIndex   := ICO_Status_None;
          CategoryData^.StateIndex   := ICO_Status_None;
          CategoryData^.MemDtl       := nil;
          CategoryData^.HasErrors    := False;
          CategoryData^.LastCheck    := 0;
          CategoryData^.LastFix      := 0;
          CategoryData^.SentryTag    := '';
          CategoryData^.Fixes        := nil;
          CategoryData^.IdxFix       := -1;
          CategoryData^.SelectiveFix := sfNone;
          CategoryNode^.CheckType    := ctCheckBox;
          CategoryNode^.CheckState   := csUncheckedNormal;
        end;

        if (CategoryNode.ChildCount = 0) or (PrevSubcategory <> Query.FieldByName ('Subcategory').AsString) then
        begin
          PrevSubcategory               := Query.FieldByName ('Subcategory').AsString;
          SubcategoryNode               := TreeDiagnosticQueries.AddChild(CategoryNode);
          SubcategoryData               := TreeDiagnosticQueries.GetNodeData(SubcategoryNode);
          SubcategoryData^.Category     := PrevCategory;
          SubcategoryData^.Subcategory  := PrevSubcategory;
          SubcategoryData^.QueryID      := '';
          SubcategoryData^.ImageIndex   := ICO_Status_None;
          SubcategoryData^.StateIndex   := ICO_Status_None;
          SubcategoryData^.MemDtl       := nil;
          SubcategoryData^.HasErrors    := False;
          SubcategoryData^.LastCheck    := 0;
          SubcategoryData^.LastFix      := 0;
          SubcategoryData^.SentryTag    := '';
          SubcategoryData^.Fixes        := nil;
          SubcategoryData^.IdxFix       := -1;
          SubcategoryData^.SelectiveFix := sfNone;
          SubcategoryNode^.CheckType    := ctCheckBox;
          SubcategoryNode^.CheckState   := csUncheckedNormal;
        end;

        ChildNode                   := TreeDiagnosticQueries.AddChild(SubcategoryNode);
        ChildData                   := TreeDiagnosticQueries.GetNodeData(ChildNode);
        ChildData^.Category         := Query.FieldByName ('Category').AsString;
        ChildData^.Subcategory      := Query.FieldByName ('Subcategory').AsString;
        ChildData^.QueryID          := Query.FieldByName ('Query_ID').AsString;
        ChildData^.SmallDescription := Query.FieldByName ('Small_Description').AsString;
        ChildData^.LongDescription  := Query.FieldByName ('Long_Description').AsString;
        ChildData^.Count            := -1;
        ChildData^.QryCnt           := Query.FieldByName ('SQL_Count').AsString;
        ChildData^.QryDtl           := Query.FieldByName ('SQL_Detail').AsString;
        ChildData^.QryFix           := Query.FieldByName ('SQL_Fix').AsString;
        ChildData^.SentryTag        := Query.FieldByName ('Sentry_TAG').AsString;
        ChildData^.MemDtl           := TdxMemData.Create(Self);
        ChildData^.HasErrors        := False;
        ChildData^.ImageIndex       := ICO_Status_None;
        ChildData^.StateIndex       := ICO_Status_None;
        ChildData^.LastCheck        := 0;
        ChildData^.LastFix          := 0;
        ChildData^.RemoteLogging    := UpperCase (Query.FieldByName('Log_Remotely').AsString) = 'TRUE';
        ChildData^.FailScore        := Query.FieldByName ('Fail_Score').AsInteger;
        ChildData^.QryDtlActive     := Query.FieldByName ('SQL_Detail_Status').AsBoolean;
        ChildData^.QryFixActive     := Query.FieldByName ('SQL_Fix_Status').AsBoolean;
        ChildData^.Fixes            := TStringList.Create;
        ChildData^.IdxFix           := -1;
        ChildData^.SelectiveFix     := sfNone;
        ChildNode^.CheckType        := ctCheckBox;
        ChildNode^.CheckState       := csUncheckedNormal;

        bm := DMDatabaseFixer.tabStatistics.GetBookmark;
        DMDatabaseFixer.tabStatistics.DisableControls;
        try
          if DMDatabaseFixer.FindStatisticsQueryID (Query.FieldByName ('Query_ID').AsString) then
          begin
            if not DMDatabaseFixer.tabStatistics.FieldByName ('DT_Last_Check').IsNull then
              ChildData^.LastCheck := DMDatabaseFixer.tabStatistics.FieldByName ('DT_Last_Check').AsDateTime;

            if not DMDatabaseFixer.tabStatistics.FieldByName ('DT_Last_Fix').IsNull then
              ChildData^.LastFix := DMDatabaseFixer.tabStatistics.FieldByName ('DT_Last_Fix').AsDateTime;
          end;
        finally
          DMDatabaseFixer.tabStatistics.GotoBookmark(bm);
          DMDatabaseFixer.tabStatistics.FreeBookmark(bm);
          DMDatabaseFixer.tabStatistics.EnableControls;
        end;

        QueryFix.Close;
        QueryFix.ParamByName('Query_ID').AsString := ChildData^.QueryID;
        QueryFix.Open;
        while not QueryFix.Eof do
        begin
          if QueryFix.FieldByName ('SQL_Fix_Status').AsBoolean then
          begin
            Obj     := TObjString.Create;
            Obj.Fix := QueryFix.FieldByName('SQL_Fix').AsString;

            if QueryFix.FieldByName('SQL_Before_Fix_Status').AsBoolean then
              Obj.Before := QueryFix.FieldByName('SQL_Single_Before_Fix').AsString;

            if QueryFix.FieldByName('SQL_After_Fix_Status').AsBoolean then
              Obj.After  := QueryFix.FieldByName('SQL_Single_After_Fix').AsString;

            ChildData^.Fixes.AddObject(QueryFix.FieldByName ('Fix_Description').AsString, Obj);
          end;

          QueryFix.Next;
        end;
      end;

      Query.Next;
    end;
  finally
    LstServerVersion.Free;
    Query.Free;
    QueryFix.Free;
    UpdateCurrentNode;
  end;
end;

procedure TFMainDoctor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Updater.UpdateForInstall then
    Updater.Update;
end;

procedure TFMainDoctor.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Closing := True;
end;

procedure TFMainDoctor.FormCreate(Sender: TObject);
begin
  LogPath                := ExtractFilePath (ParamStr (0)) + 'Logs';
  Closing                := False;
  lstProcessingNodes     := TList.Create;
  DiagnosticStatus       := dqsNone;
  idxProcessingNode      := -1;
  CntChecked             := 0;
  CntPassed              := 0;
  CntFailed              := 0;
  CntSQLFailed           := 0;
  HasRun                 := False;
  RunThread              := nil;
  FixThread              := nil;
  ThreadTerminated       := False;
  ThreadLink             := TFDPhysADSDriverLink.Create (Self);
  ThreadConn             := TFDConnection.Create (Self);
  ThreadConn.LoginPrompt := False;
  LstToFixNodes          := TList.Create;
  LoadingConsoleVL       := False;
  NodeApplyFix           := False;

  if not DirectoryExists (LogPath) then
    CreateDir(LogPath);
end;

procedure TFMainDoctor.FormDestroy(Sender: TObject);
begin
  ThreadConn.Connected := False;
  lstProcessingNodes.Clear;
  lstProcessingNodes.Free;
  ThreadConn.Free;
  ThreadLink.Free;
  LstToFixNodes.Free;
end;

procedure TFMainDoctor.FormShow(Sender: TObject);
var
  i: integer;
  PullAll: boolean;
begin
  if not DirectoryExists ('.\Logs') then
    CreateDir('.\Logs');

  if not DirectoryExists ('.\' + DmpDirectory) then
    CreateDir ('.\' + DmpDirectory);

  if not DirectoryExists ('.\' + BackupDirectory) then
    CreateDir ('.\' + BackupDirectory);

  FFullExportDir := GetPOSDirectory (DMDatabaseFixer.gPOSADSAliasName);
  if not DirectoryExists (FFullExportDir) then
    CreateDir (FFullExportDir);

  if DMDatabaseFixer.gPOSADSAliasName = '' then
  begin
    MessageDlg ('POS_ADSAliasName is not setted in cfg file. It is necessary to set a valid value in the cfg file before running the application', mtWarning, [mbOK], 0);
    Close;
  end;

  if DMDatabaseFixer.gDIAGADSAliasName = '' then
  begin
    MessageDlg ('DIAG_ADSAliasName is not setted in cfg file. It is necessary to set a valid value in the cfg file before running the application', mtWarning, [mbOK], 0);
    Close;
  end;

  StatusBar.Panels [PNL_App_Version].Text := 'Version: ' + GetAppVersionStr;
  try
    FConfigurationMode.ReadDIAGConnection(DMDatabaseFixer.FDConnDiag, 'DIAG');

    FConfigurationMode.ReadPOSConnection(DMDatabaseFixer.FDConn, 'DiagnosticADS');

    if not DMDatabaseFixer.FDConn.Connected then
    begin
      MessageDlg ('POS_ADSAliasName have an invalid connection from the ads.ini. It is necessary to set a valid value in the cfg file before running the application', mtWarning, [mbOK], 0);
      exit;
    end;

    DMDatabaseFixer.ParametrizeDriverLink(ThreadLink);
    FConfigurationMode.ReadPOSConnection (ThreadConn, 'ThreadConn');

    if not ThreadConn.Connected then
    begin
      MessageDlg ('DIAG_ADSAliasName have an invalid connection from the ads.ini. It is necessary to set a valid value in the cfg file before running the application', mtWarning, [mbOK], 0);
      exit;
    end;

    if not DMDatabaseFixer.DiagnosticTableExists then
    begin
      DMDatabaseFixer.CreateDiagnosticTable;
      if not DMDatabaseFixer.DiagnosticTableExists then
      begin
        DMDatabaseFixer.FDConnDiag.Close;
        DMDatabaseFixer.FDConn.Close;
        ThreadConn.Close;

        MessageDlg ('An error has ocurred during the Diagnostic Table creation. Please see details in the log', mtError, [mbOk], 0);
        Halt;
      end;
    end;

    if not DMDatabaseFixer.FixTableExists then
    begin
      DMDatabaseFixer.CreateFixTable;
      if not DMDatabaseFixer.FixTableExists then
      begin
        DMDatabaseFixer.FDConnDiag.Close;
        DMDatabaseFixer.FDConn.Close;
        ThreadConn.Close;

        MessageDlg ('An error has ocurred during the Fix Diagnostic Table creation. Please see details in the log', mtError, [mbOk], 0);
        Halt;
      end;
    end;

    if not DMDatabaseFixer.ParamsTableExists then
    begin
      DMDatabaseFixer.CreateParamsTable;
      if not DMDatabaseFixer.ParamsTableExists then
      begin
        DMDatabaseFixer.FDConnDiag.Close;
        DMDatabaseFixer.FDConn.Close;
        ThreadConn.Close;

        MessageDlg ('An error has ocurred during the Fix Diagnostic Table creation. Please see details in the log', mtError, [mbOk], 0);
        Halt;
      end;
    end;

    if not DMDatabaseFixer.GlobalFixTableExists then
    begin
      DMDatabaseFixer.CreateGlobalFixTable;
      if not DMDatabaseFixer.GlobalFixTableExists then
      begin
        DMDatabaseFixer.FDConnDiag.Close;
        DMDatabaseFixer.FDConn.Close;
        ThreadConn.Close;

        MessageDlg ('An error has ocurred during the Global Fix Diagnostic Table creation. Please see details in the log', mtError, [mbOk], 0);
        Halt;
      end;
    end;

    LoadVerboseLevelConsole;
    DMDatabaseFixer.CumulativeColumnVerification (False);
    DMDatabaseFixer.tabDiagnosticQry.Open;
    DMDatabaseFixer.tabDiagFix.Open;
    DMDatabaseFixer.tabDiagParams.Open;
    DMDatabaseFixer.tabGlobalFix.Open;
    FParameters.CopyAllParams;

    PullAll := False;
    for i := 1 to ParamCount do
    begin
      PullAll := PullAll or (UpperCase (ParamStr (i)) = 'PULLALL');
    end;

    if PullAll then
      FConfigurationMode.PullAllCommandLine;

    actTreeClearResults.Execute;

    Updater.DBVersion := DMDatabaseFixer.GetServerVersion;
    Updater.BeginCheckUpdate;
  except
    on E: EADSNativeException do
    begin
      MessageDlg (E.Message, mtError, [mbOk], 0);
      Halt;
    end;
  end;
end;

procedure TFMainDoctor.cbFixesChange(Sender: TObject);
var
  Data:      PTreeData;
  Node:      PVirtualNode;
  NodeLevel: integer;
  Query:     TFDQuery;
  S:         TObjString;
begin
  Node := TreeDiagnosticQueries.GetFirstSelected;
  if (TreeDiagnosticQueries.SelectedCount = 1) and Assigned (Node) then
  begin
    Data      := TreeDiagnosticQueries.GetNodeData(Node);
    NodeLevel := TreeDiagnosticQueries.GetNodeLevel(Node);
    if Assigned (Data) and (NodeLevel = 2) and Assigned (Data^.MemDtl) and (Data^.Fixes.Count > 1) and (Data^.IdxFix <> cbFixes.ItemIndex) then
    begin
      Data^.IdxFix := cbFixes.ItemIndex;

      if cbFixes.ItemIndex >= 0 then
      begin
        S := TObjString (cbFixes.Items.Objects [cbFixes.ItemIndex]);
        if Assigned (S) and (S.Fix <> '') then
        begin
          Query := DMDatabaseFixer.NewQuery(DMDatabaseFixer.FDConnDiag.ConnectionName);
          try
            Query.SQL.Add(S.Fix);
            FParameters.ExcludedParams.Clear;

            //Validate detail parameters
            if Query.ParamCount = 0 then
            begin
              Data^.SelectiveFix := sfRunOnceGlobal;
              MessageDlg ('The selected fix has no parameters so it will be executed only once', mtInformation, [mbOK], 0);
            end
            else
            begin
              if CountParamsFromDataSet (Query, Data^.MemDtl) = 0 then
                Data^.SelectiveFix := sfParametrizedOK
              else
                Data^.SelectiveFix := sfSelectiveOK
{              n := 0;

              for i := 0 to Query.ParamCount - 1 do
                if Data^.MemDtl.FieldDefs.IndexOf (Query.Params.Items [i].Name) > -1 then
                  FParameters.ExcludedParams.Add(UpperCase (Query.Params.Items [i].Name))
                else
                  Inc (n);

              if n = 0 then
                 Data^.SelectiveFix := sfSelectiveOK
              else if n > 0 then
              begin
                Data^.SelectiveFix := sfSelectiveMissingParams;
                MessageDlg (Format ('The fix script contains %d parameter(s) that do not appear in the details', [n]), mtInformation, [mbOK], 0);
              end}
            end;
          finally
            Query.Free;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFMainDoctor.cbGridSelectAllPropertiesChange(Sender: TObject);
begin
  if cbGridSelectAll.Checked then
    actGridSelectAll.Execute
  else
    actGridUnselectAll.Execute;
end;

procedure TFMainDoctor.chkcbConsoleVerboseLevelChange(Sender: TObject);
var
  vl:   TVerboseLevel;
begin
  if LoadingConsoleVL then exit;

  DMDatabaseFixer.vlConsole := [];
  for vl := Low (TVerboseLevel) to High (TVerboseLevel) do
    if chkcbConsoleVerboseLevel.Checked [Ord (vl)] then
      DMDatabaseFixer.vlConsole := DMDatabaseFixer.vlConsole + [vl];
end;

procedure TFMainDoctor.LoadVerboseLevelConsole;
var
  vl: TVerboseLevel;
  s:  string;
begin
  LoadingConsoleVL := True;
  try
    chkcbConsoleVerboseLevel.Items.Clear;
    for vl := Low (TVerboseLevel) to High (TVerboseLevel) do
    begin
      s := GetEnumName (TypeInfo (TVerboseLevel), Integer (vl));
      s := Copy (s, 3, Length (s));

      chkcbConsoleVerboseLevel.Items.Add(s);
    end;

    for vl := Low (TVerboseLevel) to High (TVerboseLevel) do
      chkcbConsoleVerboseLevel.Checked [Ord (vl)] := vl in DMDatabaseFixer.vlConsole;
  finally
    LoadingConsoleVL := False;
  end;
end;


procedure TFMainDoctor.OnThreadTerminated (Sender: TObject);
begin
  ThreadTerminated := True;
end;

procedure TFMainDoctor.UpdateLastCheckRowInformation (Node: PVirtualNode);
var
  Qry: TFDQuery;
  Data: PTreeData;
begin
  if not Assigned (Node) or (TreeDiagnosticQueries.GetNodeLevel(Node) <> 1) then exit;
  DMDatabaseFixer.gSentryTags := '';
  DMDatabaseFixer.Log_Trace('procedure TFMainDoctor.UpdateLastCheckRowInformation (Node: PVirtualNode); [Start]');

  Qry := DMDatabaseFixer.NewQuery('LOCAL');
  try
    Data := TreeDiagnosticQueries.GetNodeData(Node);
    if Data^.QueryID <> '' then
    begin
      DMDatabaseFixer.gSentryTags := Data^.SentryTag;
      DMDatabaseFixer.Log_Debug (Format ('Query_ID: %s',        [Data^.QueryID]));
      DMDatabaseFixer.Log_Debug (Format ('Last_Fail_Count: %d', [Data^.Count]));
      DMDatabaseFixer.Log_Debug (Format ('DT_Last_Check: %s',   [FormatDateTime('mm/dd/yyyy hh:nn:ss', Data^.LastCheck)]));
      DMDatabaseFixer.Log_Debug (Format ('Pass_Fail_State: %d', [Ord (Data^.HasErrors or (Data^.Count > 0))]));

      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add('Merge Diag_Statistics On (Query_ID = :Query_ID)');
      Qry.SQL.Add('When Matched Then');
      Qry.SQL.Add('  Update Set DT_Last_Check   = :DT_Last_Check,');
      Qry.SQL.Add('             Last_Fail_Count = :Last_Fail_Count,');
      Qry.SQL.Add('             Pass_Fail_State = :Pass_Fail_State');
      Qry.SQL.Add('When Not Matched Then');
      Qry.SQL.Add('  Insert (Query_ID, DT_Last_Check, Last_Fail_Count, Pass_Fail_State) Values (:Query_ID, :DT_Last_Check, :Last_Fail_Count, :Pass_Fail_State)');
      Qry.ParamByName('DT_Last_Check').AsDateTime  := Data^.LastCheck;
      Qry.ParamByName('Last_Fail_Count').AsInteger := Data^.Count;
      Qry.ParamByName('Pass_Fail_State').AsInteger := Ord (Data^.HasErrors or (Data^.Count > 0));
      Qry.ParamByName('Query_ID').AsString         := Data^.QueryID;

      try
        Qry.ExecSQL;

        DMDatabaseFixer.tabStatistics.Refresh;
      except
        on E: Exception do
          DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                        sLineBreak + Qry.SQL.Text);
      end;
    end;
  finally
    Qry.Free;
    DMDatabaseFixer.Log_Trace('procedure TFMainDoctor.UpdateLastCheckRowInformation (Node: PVirtualNode);  [Finish]');
  end;
end;

procedure TFMainDoctor.UpdateLastFixRowInformation (Node: PVirtualNode);
var
  Qry: TFDQuery;
  Data: PTreeData;
begin
  if not Assigned (Node) or (TreeDiagnosticQueries.GetNodeLevel(Node) <> 1) then exit;
  DMDatabaseFixer.gSentryTags := '';
  DMDatabaseFixer.Log_Trace('procedure TFMainDoctor.UpdateLastFixRowInformation (Node: PVirtualNode); [Start]');

  Qry := DMDatabaseFixer.NewQuery('LOCAL');
  try
    Data := TreeDiagnosticQueries.GetNodeData(Node);
    if Data^.QueryID <> '' then
    begin
      DMDatabaseFixer.gSentryTags := Data^.SentryTag;
      DMDatabaseFixer.Log_Debug(Format ('Query_ID: %s',    [Data^.QueryID]));
      DMDatabaseFixer.Log_Debug(Format ('DT_Last_Fix: %s', [FormatDateTime('mm/dd/yyyy hh:nn:ss', Data^.LastFix)]));

      Qry.Close;
      Qry.SQL.Clear;
      Qry.SQL.Add('Merge Diag_Statistics On (Query_ID = :Query_ID)');
      Qry.SQL.Add('When Matched Then');
      Qry.SQL.Add('  Update Set DT_Last_Fix = :DT_Last_Fix');
      Qry.SQL.Add('When Not Matched Then');
      Qry.SQL.Add('  Insert (Query_ID, DT_Last_Fix) Values (:Query_ID, :DT_Last_Fix)');
      Qry.ParamByName('DT_Last_Fix').AsDateTime := Data^.LastFix;
      Qry.ParamByName('Query_ID').AsString      := Data^.QueryID;

      try
        Qry.ExecSQL;

        DMDatabaseFixer.tabStatistics.Refresh;
      except
        on E: Exception do
          DMDatabaseFixer.Log_Exception(Format ('Error: [%s] Message: [%s]', [E.ClassName, E.Message]) +
                                        sLineBreak + Qry.SQL.Text);
      end;
    end;
  finally
    Qry.Free;
    DMDatabaseFixer.Log_Trace('procedure TFMainDoctor.UpdateLastFixRowInformation (Node: PVirtualNode); [Finish]');
  end;
end;

procedure TFMainDoctor.SendFailScoreToSentry;
var
  tmpSentryVerboseLevel: TSetVerboseLevel;
begin
  if DMDatabaseFixer.gSentryTagScore = '' then exit;

  //Enforces to send info to Sentry
  try
    //Saves the original setting
    tmpSentryVerboseLevel    := DMDatabaseFixer.vlSentry;

    //Include Info
    DMDatabaseFixer.vlSentry := DMDatabaseFixer.vlSentry + [vlInfo];

    //Send Info
    DMDatabaseFixer.LogSentry.Tags := Format ('%s,%d', [DMDatabaseFixer.gSentryTagScore, SumFailScore]);
    DMDatabaseFixer.Log_Info(Format ('Fail Score: %d', [SumFailScore]));
  finally
    //Restore original settings
    DMDatabaseFixer.vlSentry := tmpSentryVerboseLevel;
  end;
end;

procedure TFMainDoctor.TimerDiagnosticTimer(Sender: TObject);
var
  Node: PVirtualNode;
  Data: PTreeData;
  LstCnt, LstDtl: TStringCollection; //TStringList;
begin
  try
    TimerDiagnostic.Enabled := False;

    if not Assigned (RunThread) then
    begin
      if not Math.InRange (idxProcessingNode, 0, lstProcessingNodes.Count - 1) then
      begin
        DiagnosticStatus := dqsNone;

        exit;
      end;

      if not (DiagnosticStatus in [dqsPausing, dqsPause, dqsStoping]) then
      begin
        DiagnosticStatus          := dqsRunning;
        Node                      := lstProcessingNodes [idxProcessingNode];
        Data                      := TreeDiagnosticQueries.GetNodeData(Node);
        Data^.ImageIndex          := ICO_Status_Running;
        Data^.StateIndex          := ICO_Status_None;
        LstCnt                    := FConfigurationMode.SplitInstructionsSemicolon (Data^.QryCnt, False);
        LstDtl                    := FConfigurationMode.SplitInstructionsSemicolon (Data^.QryDtl, False);
        ThreadTerminated          := False;
        RunThread                 := TRunDiagnosticQueryThread.Create(Node, LstCnt, LstDtl);
        RunThread.Description     := Data^.SmallDescription;
        RunThread.QryDtlActive    := Data^.QryDtlActive;
        RunThread.OnTerminate     := OnThreadTerminated;
        RunThread.FreeOnTerminate := False;

        if FParameters.memParams.RecordCount > 0 then
        begin
          RunThread.Params := TdxMemData.Create(Self);
          RunThread.Params.CopyFromDataSet(FParameters.memParams);
        end;

        RunThread.Resume;
        TreeDiagnosticQueries.ScrollIntoView(Node, False);
        TreeDiagnosticQueries.Refresh;
      end
      else
      begin
        case DiagnosticStatus of
          dqsPausing: DiagnosticStatus := dqsPause;
          dqsStoping: begin
            DiagnosticStatus := dqsNone;
            MessageDlg ('Process stopped', mtInformation, [mbOK], 0);
          end;
        end;
      end;
    end
    else
    begin
      if ThreadTerminated then
      begin
        Node             := RunThread.Node;
        Data             := TreeDiagnosticQueries.GetNodeData(Node);
        Data^.Count      := RunThread.Count;
        Data^.HasErrors  := RunThread.HasErrors;
        Data^.LastCheck  := Now;

        if RunThread.HasErrors then
        begin
          Data^.ImageIndex := ICO_Status_Error;
          Inc (CntSQLFailed);
        end
        else
        begin
          if RunThread.Count > 0 then
          begin
            Data^.ImageIndex := ICO_Status_Bug;
            Inc (CntFailed);
            Inc (SumFailScore, Data^.FailScore);
          end
          else
          begin
            Data^.ImageIndex := ICO_Status_OK;
            Inc (CntPassed);
          end;

          if (RunThread.Count > 0) and Data^.QryFixActive and (Data^.QryFix <> '') then
            LstToFixNodes.Add(Node);

          if Assigned (RunThread.QryDtl) and (RunThread.QryDtl.Active) then
          begin
            RunThread.QryDtl.Last;
            Data^.MemDtl.ReadOnly := False;
            Data^.MemDtl.CopyFromDataSet(RunThread.QryDtl);
            Data^.MemDtl.Last;
            Data^.MemDtl.First;
            Data^.MemDtl.ReadOnly := True;
            if (Data^.Count = 0) and (Data^.MemDtl.RecordCount > 0) then
              Data^.Count := Data^.MemDtl.RecordCount;
          end;
        end;
        UpdateLastCheckRowInformation (Node);
        ThreadTerminated := False;
        FreeAndNil (RunThread);
        Inc (idxProcessingNode);
        UpdateCurrentNode;
        TreeDiagnosticQueries.Refresh;

        case DiagnosticStatus of
          dqsPausing: DiagnosticStatus := dqsPause;
          dqsStoping: begin
            DiagnosticStatus := dqsNone;
            MessageDlg ('Process stopped', mtInformation, [mbOK], 0);
          end;
          else begin
            if idxProcessingNode > (lstProcessingNodes.Count - 1) then
            begin
              SendFailScoreToSentry;
              MessageDlg ('Process finished', mtInformation, [mbOK], 0);
            end;
          end;
        end;
      end;
    end;
  finally
    TimerDiagnostic.Enabled := DiagnosticStatus in [dqsRunning, dqsPausing, dqsStoping];
  end;
end;

procedure TFMainDoctor.TimerFixTimer(Sender: TObject);
var
  Data: PTreeData;
  pt: TPoint;
begin
  TimerFix.Enabled := False;

  try
    if ThreadTerminated then
    begin
      Data := TreeDiagnosticQueries.GetNodeData(FixThread.Node);
      if FixThread.HasErrors then
        Data^.StateIndex := ICO_Status_Error
      else
      begin
        Data^.StateIndex := ICO_Status_Fixed;
        Data^.LastFix    := Now;
        LstToFixNodes.Remove(FixThread.Node);
      end;

      UpdateLastFixRowInformation(FixThread.Node);
      FreeAndNil (FixThread);
      UpdateCurrentNode;
      DiagnosticStatus := dqsNone;
      Screen.Cursor    := crDefault;
      TreeDiagnosticQueries.Refresh;
      GetCursorPos(pt);
      Inc (pt.X);
      Inc (pt.Y);
      SetCursorPos(pt.X, pt.Y);
      MessageDlg ('Process finished', mtInformation, [mbOK], 0);
    end;
  finally
    TimerFix.Enabled := not ThreadTerminated;
  end;
end;

procedure TFMainDoctor.TimerRestoreTimer(Sender: TObject);
begin
  TimerRestore.Enabled := False;
  actTreeClearResults.Execute;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesAddToSelection(
  Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  UpdateCurrentNode;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesChange(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
  Sender.Refresh;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesChecked(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
var
  Child, Subchild: PVirtualNode;
begin
  if Assigned (Node) then
    case TreeDiagnosticQueries.GetNodeLevel(Node) of
      0: begin
           Child := Node.FirstChild;
           while Assigned (Child) do
           begin
             Child^.CheckState := Node^.CheckState;
             Subchild := Child^.FirstChild;
             while Assigned (Subchild) do
             begin
               Subchild^.CheckState := Node^.CheckState;

               Subchild := Subchild^.NextSibling;
             end;

             Child := Child^.NextSibling;
           end;
         end;
      1: begin
           if Node.CheckState = csCheckedNormal then
             Node.Parent.CheckState := csCheckedNormal;

           Child := Node.FirstChild;
           while Assigned (Child) do
           begin
             Child^.CheckState := Node^.CheckState;
             Child             := Child^.NextSibling;
           end;
         end;
      2: begin
           if Node.CheckState = csCheckedNormal then
           begin
             Node.Parent.Parent.CheckState := csCheckedNormal;
             Node.Parent.CheckState        := csCheckedNormal;
           end;
         end;
    end;

  UpdateCurrentNode;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesFocusChanged(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex);
begin
  Sender.Refresh;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesFreeNode(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
var
  Data: PTreeData;
  s: TObjString;
  i: integer;
begin
  if not Assigned (Node) then exit;

  {Get the pointer data}
  Data                   := Sender.GetNodeData(Node);

  {Release the strings assigned memory}
  Data^.Category         := '';
  Data^.Subcategory      := '';
  Data^.QueryID          := '';
  Data^.SmallDescription := '';
  Data^.LongDescription  := '';
  Data^.QryCnt           := '';
  Data^.QryDtl           := '';
  Data^.QryFix           := '';

  if Assigned (Data^.MemDtl) then
    FreeAndNil (Data^.MemDtl);

  if Assigned (Data^.Fixes) then
  begin
    for i := 0 to Data^.Fixes.Count - 1 do
    begin
      Data^.Fixes [i]         := '';
      s                       := TObjString (Data^.Fixes.Objects [i]);
      Data^.Fixes.Objects [i] := nil;
      S.Free;
    end;

    FreeAndNil (Data^.Fixes);
  end;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesGetImageIndex(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
  Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: TImageIndex);
var
  Data: PTreeData;
begin
  if not Assigned (Node) or not (Kind in [ikNormal, ikSelected, ikState]) then exit;

  Data := Sender.GetNodeData(Node);

  case Kind of
    ikNormal,
    ikSelected: ImageIndex := Data^.ImageIndex;
    ikState:    ImageIndex := Data^.StateIndex;
  end;
end;

procedure TFMainDoctor.TreeDiagnosticQueriesGetNodeDataSize(Sender: TBaseVirtualTree;
  var NodeDataSize: Integer);
begin
  NodeDataSize := SizeOf (RTreeData);
end;

procedure TFMainDoctor.TreeDiagnosticQueriesGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: string);
var
  Data: PTreeData;
begin
  CellText := '';
  if not Assigned (Node) then exit;

  Data := Sender.GetNodeData(Node);
  case Sender.GetNodeLevel (Node) of
    0: CellText := Data^.Category;
    1: if Data^.Subcategory.IsEmpty then
         CellText := '<No subcategory>'
       else
         CellText := Data^.Subcategory;
    2: begin
         CellText := Data^.SmallDescription;

         if Data^.StateIndex = ICO_Status_Fixed then
           CellText := Format ('%s (Fixed)', [CellText])
         else if Data^.Count > -1 then
           CellText := Format ('%s (%d)', [CellText, Data^.Count]);
       end;
  end;
end;

procedure TFMainDoctor.UpdateCurrentNode;
  procedure UpdateChecked;
  var
    Node, Child, Subchild: PVirtualNode;
  begin
    CntChecked := 0;

    Node := TreeDiagnosticQueries.GetFirst;
    while Assigned (Node) do
    begin
      if Node^.CheckState = csCheckedNormal then
      begin
        Child := Node^.FirstChild;

        while Assigned (Child) do
        begin
          if Child^.CheckState = csCheckedNormal then
          begin
            Subchild := Child^.FirstChild;
            while Assigned (Subchild) do
            begin
              if Subchild^.CheckState = csCheckedNormal then
                Inc (CntChecked);

              Subchild := Subchild^.NextSibling;
            end;
          end;

          Child := Child^.NextSibling;
        end;
      end;

      Node := Node^.NextSibling;
    end;
  end;

  function IntToStrCommaSeparated (n: integer): string;
  var
    R: Real;
  begin
    R      := n;
    result := Trim (Format ('%8.0n', [R]));
  end;

var
  Data:      PTreeData;
  Node:      PVirtualNode;
  NodeLevel: integer;
  ShowCheck: boolean;
  Query:     TFDQuery;
  Obj:       TObjString;
begin
  FCurrentDatasetNode := '';
  if Closing then Exit;

  UpdateChecked;
  StatusBar.Panels [PNL_Cnt_Checked_Nodes].Text    := Format ('Selected Items: %s',   [IntToStrCommaSeparated (CntChecked)]);
  StatusBar.Panels [PNL_Cnt_Total_Passed].Text     := Format ('Total Passed: %s',     [IntToStrCommaSeparated (CntPassed)]);
  StatusBar.Panels [PNL_Cnt_Total_Failed].Text     := Format ('Total Failed: %s',     [IntToStrCommaSeparated (CntFailed)]);
  StatusBar.Panels [PNL_Cnt_Total_SQL_Failed].Text := Format ('Total SQL Failed: %s', [IntToStrCommaSeparated (CntSQLFailed)]);
  StatusBar.Panels [PNL_Fail_Score].Text           := Format ('Fail Score: %s',       [IntToStrCommaSeparated (SumFailScore)]);

  memLongDescription.Lines.Text  := '';
  edLastRun.Text                 := '';
  edLastFix.Text                 := '';
  Node                           := TreeDiagnosticQueries.GetFirstSelected;
  actGridSelectAll.Enabled       := False;
  actGridUnselectAll.Enabled     := False;
  actGridInvertSelection.Enabled := False;
  cbGridSelectAll.Enabled        := False;
  ShowCheck                      := False;
  cbFixes.ItemIndex              := -1;
  cbFixes.Text                   := '';
  cbFixes.Enabled                := False;
  cbFixes.Items.Clear;
  if (TreeDiagnosticQueries.SelectedCount = 1) and Assigned (Node) then
  begin
    Data                          := TreeDiagnosticQueries.GetNodeData(Node);
    NodeLevel                     := TreeDiagnosticQueries.GetNodeLevel(Node);
    NodeApplyFix                  := (NodeLevel = 2) and (Data^.Fixes.Count > 0);
    memLongDescription.Lines.Text := Data^.LongDescription;

    if Data^.LastCheck <> 0 then
      edLastRun.Text := FormatDateTime('mm/dd/yyyy hh:nn:ss', Data^.LastCheck);

    if Data^.LastFix <> 0 then
      edLastFix.Text := FormatDateTime('mm/dd/yyyy hh:nn:ss', Data^.LastFix);

    GridDetailsDBTableView1.ClearItems;
    if NodeLevel = 2 then
    begin
      if Assigned (Data^.MemDtl) and Data^.MemDtl.Active and (DSDetails.DataSet <> Data^.MemDtl) then
      begin
        Data^.MemDtl.First;
        DSDetails.DataSet   := Data^.MemDtl;
        FCurrentDatasetNode := Data^.SmallDescription;

        GridDetailsDBTableView1.DataController.CreateAllItems;
        GridDetailsDBTableView1.ApplyBestFit;

        if Data^.Fixes.Count > 1 then
        begin
          cbFixes.Items.Assign(Data^.Fixes);
          cbFixes.Enabled                := True;
          cbFixes.ItemIndex              := Data^.IdxFix;
          actGridSelectAll.Enabled       := True;
          actGridUnselectAll.Enabled     := True;
          actGridInvertSelection.Enabled := True;
          cbGridSelectAll.Enabled        := True;
          ShowCheck                      := True;
        end
        else if Data^.Fixes.Count = 1 then
        begin
          Data^.SelectiveFix := sfRunOnceGlobal;
          Query := DMDatabaseFixer.NewQuery(DMDatabaseFixer.FDConn.ConnectionName);
          try
            Obj            := TObjString (Data^.Fixes.Objects [0]);
            Query.SQL.Text := Obj.Fix;
            if CountParamsFromDataSet (Query, Data^.MemDtl) > 0 then
            begin
              actGridSelectAll.Enabled       := True;
              actGridUnselectAll.Enabled     := True;
              actGridInvertSelection.Enabled := True;
              cbGridSelectAll.Enabled        := True;
              ShowCheck                      := True;
              Data^.SelectiveFix             := sfSelectiveOK;
            end;
          finally
            Query.Free;
          end;
        end;
      end;
    end;
  end
  else
  begin
    DSDetails.DataSet := nil;
    NodeApplyFix      := False;
  end;
  //if ShowCheck then
    GridDetailsDBTableView1.OptionsSelection.CheckBoxVisibility := [cbvDataRow]
//  else
//    GridDetailsDBTableView1.OptionsSelection.CheckBoxVisibility := []
end;

procedure TFMainDoctor.TreeDiagnosticQueriesRemoveFromSelection(
  Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  UpdateCurrentNode;
end;

{$region 'Autoupdate actions'}
procedure TFMainDoctor.UpdaterErrorDownload(Sender: TObject; ProgramName,
  Version, URLDownload, TargetFileName, PathToInstall, ErrMsg: string);
begin
  StatusBar.Panels [PNL_Autoupdate].Text := 'Download error';
  (StatusBar.Panels [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_Download_Error;
end;

procedure TFMainDoctor.UpdaterFinishDownload(Sender: TObject; ProgramName,
  Version, URLDownload, TargetFileName, PathToInstall: string);
begin
  StatusBar.Panels [PNL_Autoupdate].Text := 'Ready for update';
  (StatusBar.Panels [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_New;
end;

procedure TFMainDoctor.UpdaterFirstRunNewVersion(Sender: TObject;
  ReleaseNotes: string; var MarkAsDisplayed: Boolean);
begin
  FNewVersionDlg.Installed        := True;
  FNewVersionDlg.Version          := Updater.Version;
  FNewVersionDlg.Notes.Lines.Text := ReleaseNotes;
  FNewVersionDlg.ShowModal;
end;

procedure TFMainDoctor.UpdaterFoundNewAppVersion(Sender: TObject; ProgramName,
  Version, URLDownload, ReleaseNotes, TargetFileName, PathToInstall,
  InstallerType: string; var StartDownload: Boolean);
begin
  StatusBar.Panels [PNL_Autoupdate].Text := 'New version found';
  (StatusBar.Panels [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_New;
end;

procedure TFMainDoctor.UpdaterNotifyMustExitForUpdate(Sender: TObject);
begin
  if Updater.UpdatePriority = upNow then
    MessageDlg ('It is necessary to close the application to install the new update', mtInformation, [mbOK], 0);

  Close;
end;

procedure TFMainDoctor.UpdaterStartDownload(Sender: TObject; ProgramName,
  Version, URLDownload, TargetFileName, PathToInstall: string);
begin
  StatusBar.Panels [PNL_Autoupdate].Text := 'Downloading update';
  (StatusBar.Panels [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_Download;
end;

procedure TFMainDoctor.UpdaterUpdateError(Sender: TObject; ErrorMsg: string);
begin
  ErrorMsg := Format ('An error occurred in the update process with the message: [%s]', [ErrorMsg]);

  StatusBar.Panels [PNL_Autoupdate].Text := 'Download error';
  (StatusBar.Panels [PNL_Autoupdate].PanelStyle as TdxStatusBarTextPanelStyle).ImageIndex := ICO_Update_Error;
  DMDatabaseFixer.Log_Exception(ErrorMsg);

  MessageDlg (ErrorMsg, mtError, [mbOK], 0);
end;

{$endregion}

end.
