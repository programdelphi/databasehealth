unit uColumnVerifProgressBar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  dxSkinsDefaultPainters, Vcl.StdCtrls, cxButtons;

type
  TFColumnVerifProgressBar = class(TForm)
    ProgressBar1: TProgressBar;
    btnCancel: TcxButton;
    procedure btnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FColumnVerifProgressBar: TFColumnVerifProgressBar;

implementation

{$R *.dfm}

procedure TFColumnVerifProgressBar.btnCancelClick(Sender: TObject);
begin
  Close;
end;

end.
