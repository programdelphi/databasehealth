object DMDatabaseFixer: TDMDatabaseFixer
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 320
  Width = 543
  object FDPhysADSDriverLink1: TFDPhysADSDriverLink
    VendorLib = 'ace32.dll'
    Left = 56
    Top = 32
  end
  object FDConn: TFDConnection
    ConnectionName = 'DiagnosticADS'
    Params.Strings = (
      'DriverID=ADS'
      'ServerTypes=Local'
      'TableType=ADT'
      'Port=6262'
      'TablePassword=lezaM123$51'
      'User_Name=Dev'
      'Password=lezaM12345$$')
    LoginPrompt = False
    Left = 160
    Top = 32
  end
  object tabDiagnosticQry: TFDTable
    BeforePost = tabDiagnosticQryBeforePost
    AfterPost = tabDiagnosticQryAfterPost
    AfterDelete = tabDiagnosticQryAfterDelete
    OnCalcFields = tabDiagnosticQryCalcFields
    OnNewRecord = tabDiagnosticQryNewRecord
    IndexFieldNames = 'Query_ID'
    Connection = FDConnDiag
    UpdateOptions.UpdateTableName = 'Diagnostic_Queries'
    UpdateOptions.KeyFields = 'Query_ID'
    TableName = 'Diagnostic_Queries'
    Left = 232
    Top = 32
    object tabDiagnosticQryQuery_ID: TGuidField
      FieldName = 'Query_ID'
      Origin = 'Query_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Size = 38
    end
    object tabDiagnosticQryCategory: TStringField
      FieldName = 'Category'
      Origin = 'Category'
      Size = 100
    end
    object tabDiagnosticQrySmall_Description: TStringField
      FieldName = 'Small_Description'
      Origin = 'Small_Description'
      Size = 100
    end
    object tabDiagnosticQryLong_Description: TStringField
      FieldName = 'Long_Description'
      Origin = 'Long_Description'
      Size = 1024
    end
    object tabDiagnosticQryPass_Fail_State: TIntegerField
      FieldName = 'Pass_Fail_State'
      Origin = 'Pass_Fail_State'
    end
    object tabDiagnosticQryDT_Last_Check: TSQLTimeStampField
      FieldName = 'DT_Last_Check'
      Origin = 'DT_Last_Check'
    end
    object tabDiagnosticQryDT_Last_Fix: TSQLTimeStampField
      FieldName = 'DT_Last_Fix'
      Origin = 'DT_Last_Fix'
    end
    object tabDiagnosticQryLast_Fail_Count: TIntegerField
      FieldName = 'Last_Fail_Count'
      Origin = 'Last_Fail_Count'
    end
    object tabDiagnosticQrySQL_Count: TStringField
      FieldName = 'SQL_Count'
      Origin = 'SQL_Count'
      Size = 8192
    end
    object tabDiagnosticQrySQL_Detail: TStringField
      FieldName = 'SQL_Detail'
      Origin = 'SQL_Detail'
      Size = 8192
    end
    object tabDiagnosticQrySQL_Fix: TStringField
      FieldName = 'SQL_Fix'
      Origin = 'SQL_Fix'
      Size = 8192
    end
    object tabDiagnosticQryFail_Score: TIntegerField
      FieldName = 'Fail_Score'
      Origin = 'Fail_Score'
    end
    object tabDiagnosticQrySentry_TAG: TStringField
      FieldName = 'Sentry_TAG'
      Origin = 'Sentry_TAG'
      Size = 512
    end
    object tabDiagnosticQryLog_Remotely: TBooleanField
      FieldName = 'Log_Remotely'
      Origin = 'Log_Remotely'
    end
    object tabDiagnosticQryMin_DB_Ver_Required: TStringField
      FieldName = 'Min_DB_Ver_Required'
      Origin = 'Min_DB_Ver_Required'
      Size = 25
    end
    object tabDiagnosticQryStatus: TBooleanField
      FieldName = 'Status'
      Origin = 'Status'
    end
    object tabDiagnosticQrySQL_Detail_Status: TBooleanField
      FieldName = 'SQL_Detail_Status'
      Origin = 'SQL_Detail_Status'
    end
    object tabDiagnosticQrySQL_Fix_Status: TBooleanField
      FieldName = 'SQL_Fix_Status'
      Origin = 'SQL_Fix_Status'
    end
    object tabDiagnosticQrySelected: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'Selected'
      Calculated = True
    end
    object tabDiagnosticQryRow_Version_Number: TIntegerField
      FieldName = 'Row_Version_Number'
      Origin = 'Row_Version_Number'
    end
    object tabDiagnosticQryCalc_Pass_Fail_State: TIntegerField
      DisplayLabel = 'Pass_Fail_State'
      FieldKind = fkCalculated
      FieldName = 'Calc_Pass_Fail_State'
      Calculated = True
    end
    object tabDiagnosticQryCalc_DT_Last_Check: TSQLTimeStampField
      DisplayLabel = 'DT_Last_Check'
      FieldKind = fkCalculated
      FieldName = 'Calc_DT_Last_Check'
      Calculated = True
    end
    object tabDiagnosticQryCalc_DT_Last_Fix: TSQLTimeStampField
      DisplayLabel = 'DT_Last_Fix'
      FieldKind = fkCalculated
      FieldName = 'Calc_DT_Last_Fix'
      Calculated = True
    end
    object tabDiagnosticQryCalc_Last_Fail_Count: TIntegerField
      DisplayLabel = 'Last_Fail_Count'
      FieldKind = fkCalculated
      FieldName = 'Calc_Last_Fail_Count'
      Calculated = True
    end
    object tabDiagnosticQrySubcategory: TStringField
      FieldName = 'Subcategory'
      Origin = 'Subcategory'
      Size = 100
    end
  end
  object FDRemoteConn: TFDConnection
    ConnectionName = 'ADSRemoteConn'
    Params.Strings = (
      'DriverID=ADS'
      'Password=lezaM12345$$'
      'Port=6262'
      'ServerTypes=Remote'
      'TablePassword=lezaM123$51'
      'TableType=ADT'
      'User_Name=Dev')
    LoginPrompt = False
    Left = 432
    Top = 32
  end
  object tabRemoteDiagnosticQry: TFDTable
    IndexFieldNames = 'Query_ID'
    Connection = FDRemoteConn
    UpdateOptions.UpdateTableName = 'Diagnostic_Queries'
    TableName = 'Diagnostic_Queries'
    Left = 432
    Top = 80
  end
  object FDConnDiag: TFDConnection
    Params.Strings = (
      'DriverID=ADS'
      'TableType=ADT'
      'Password=lezaM12345$$'
      'User_Name=dev'
      'ServerTypes=Local'
      'Alias=TisWin3dd')
    LoginPrompt = False
    Left = 160
    Top = 88
  end
  object FDConnLocal: TFDConnection
    Params.Strings = (
      'DriverID=ADS')
    LoginPrompt = False
    Left = 160
    Top = 160
  end
  object tabStatistics: TFDTable
    Connection = FDConnLocal
    UpdateOptions.UpdateTableName = 'Diag_Statistics'
    TableName = 'Diag_Statistics'
    Left = 224
    Top = 160
  end
  object tabDiagFix: TFDTable
    BeforePost = tabDiagFixBeforePost
    AfterPost = tabDiagFixAfterPost
    AfterDelete = tabDiagFixAfterDelete
    IndexFieldNames = 'Query_ID;Fix_ID'
    MasterSource = FConfigurationMode.DSDiagnosticQueries
    MasterFields = 'Query_ID'
    Connection = FDConnDiag
    UpdateOptions.UpdateTableName = 'Diagnostic_Fix_Queries'
    TableName = 'Diagnostic_Fix_Queries'
    Left = 312
    Top = 32
  end
  object tabDiagParams: TFDTable
    AfterInsert = tabDiagParamsAfterInsert
    AfterPost = tabDiagParamsAfterPost
    AfterDelete = tabDiagParamsAfterDelete
    IndexFieldNames = 'Param'
    Connection = FDConnDiag
    UpdateOptions.UpdateTableName = 'Diagnostic_Params'
    TableName = 'Diagnostic_Params'
    Left = 312
    Top = 88
  end
  object tabGlobalFix: TFDTable
    Connection = FDConnDiag
    UpdateOptions.UpdateTableName = 'Diagnostic_Global_Queries'
    TableName = 'Diagnostic_Global_Queries'
    Left = 312
    Top = 152
  end
end
