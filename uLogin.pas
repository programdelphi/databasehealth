unit uLogin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinsDefaultPainters,
  Vcl.StdCtrls, cxButtons, uDataModule, FireDAC.Phys.ADSWrapper;

type
  TFLogin = class(TForm)
    Label1: TLabel;
    edUserName: TEdit;
    Label2: TLabel;
    edPassword: TEdit;
    btnOK: TcxButton;
    btnCancel: TcxButton;
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edUserNameChange(Sender: TObject);
  private
    { Private declarations }
    LoginRetries: integer;
    LoginCount:   integer;
  public
    { Public declarations }
  end;

var
  FLogin: TFLogin;

implementation

{$R *.dfm}

procedure TFLogin.btnOKClick(Sender: TObject);
begin
  Inc (LoginCount);

  DMDatabaseFixer.FDConn.Params.UserName := edUserName.Text;
  DMDatabaseFixer.FDConn.Params.Password := edPassword.Text;

  try
    DMDatabaseFixer.FDConn.Connected := True;
  except
    on E: EADSNativeException do
      MessageDlg (E.Message, mtError, [mbOk], 0);
  end;

  if not DMDatabaseFixer.FDConn.Connected and (LoginCount >= LoginRetries) then
  begin
    MessageDlg ('Number of retries exceded!', mtInformation, [mbOK], 0);
    Halt;
  end;

  if DMDatabaseFixer.FDConn.Connected then
    ModalResult := mrOk;
end;

procedure TFLogin.edUserNameChange(Sender: TObject);
begin
  btnOK.Enabled := edUserName.Text <> '';
end;

procedure TFLogin.FormCreate(Sender: TObject);
begin
  LoginRetries := 3;
end;

procedure TFLogin.FormShow(Sender: TObject);
begin
  LoginCount      := 0;
  edUserName.Text := '';
  edPassword.Text := '';
end;

end.
